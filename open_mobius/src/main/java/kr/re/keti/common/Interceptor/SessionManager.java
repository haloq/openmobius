package kr.re.keti.common.Interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * session Manager.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
public class SessionManager extends HandlerInterceptorAdapter {

	@Autowired
	private MessageSource messageSource;
	/** The logger. */
	private Logger logger = Logger.getLogger(this.getClass());
		
	private String sApiID = "";
	private String sUserID = "";

	private String sErrorCode = "";
	private String sErrorMsg = "";
	
	private String redirectPage;
	
	private List<String>  listNoSession;
	
	
	/**
	 * getRedirectPage
	 * @return
	 */
	public String getRedirectPage() {
		return redirectPage;
	}
	
	/**
	 * setRedirectPage
	 * @param redirectPage
	 */
	public void setRedirectPage(String redirectPage) {
		this.redirectPage = redirectPage;
	}
	
	/**
	 * getListNoSession
	 * @return
	 */
	public List<String> getListNoSession() {
		return listNoSession;
	}
	
	/**
	 * setListNoSession
	 * @param listNoSession
	 */
	public void setListNoSession(List<String> listNoSession) {
		this.listNoSession = listNoSession;
	}

	/**
	 * preHandle
	 */
	@Override
	public boolean preHandle(
			HttpServletRequest request,
			HttpServletResponse response, 
			Object handler) throws Exception {

		CommonUtil.setMessageSource(messageSource);
		
		String sRequestPath = request.getPathInfo();
		if (logger.isDebugEnabled()) {
		}
		
		return true;
	}
	
	/**
	 * postHandle
	 */
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		
		String locale = request.getHeader("locale") != null ? request.getHeader("locale") : "ko";		// locale
//		logger.debug("#######################################");
//		logger.debug("postHandle - sErrorCode:" + sErrorCode);
//		logger.debug("postHandle - sErrorMsg:" + sErrorMsg);
//		logger.debug("#######################################");

		sErrorCode = "";
		sErrorMsg = "";
	}
}