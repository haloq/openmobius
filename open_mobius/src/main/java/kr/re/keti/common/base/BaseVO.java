package kr.re.keti.common.base;

import org.apache.commons.lang.builder.*;
import java.io.Serializable;

/**
 * base domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
public class BaseVO implements Serializable {

	private static final long serialVersionUID = -2258414165344881650L;

	public BaseVO() {
	}

	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
	}

	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}
}
