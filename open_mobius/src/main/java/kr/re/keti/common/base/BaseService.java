package kr.re.keti.common.base;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * base Service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Service
@Transactional(rollbackFor=java.lang.Exception.class)
public class BaseService {

}
