package kr.re.keti.common.exception;

/**
 * mobius Exception.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
public class MobiusException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8865812297971621821L;
	
	private String code = "200";
	private String message = "";
	
	public MobiusException(String code, String msg){
		this.code = code;
		this.message = msg;
	}
	public MobiusException(String msg){
		this.code = "600";
		this.message = msg;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}	
}
