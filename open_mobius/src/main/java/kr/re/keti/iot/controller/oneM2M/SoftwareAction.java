package kr.re.keti.iot.controller.oneM2M;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.NodeVO;
import kr.re.keti.iot.domain.oneM2M.RemoteCSEVO;
import kr.re.keti.iot.domain.oneM2M.SoftwareVO;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.AEService;
import kr.re.keti.iot.service.oneM2M.NodeService;
import kr.re.keti.iot.service.oneM2M.RemoteCSEService;
import kr.re.keti.iot.service.oneM2M.SoftwareService;
import kr.re.keti.iot.service.oneM2M.common.MCommonService;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * software management Action.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Controller
public class SoftwareAction {

	private static final Log logger = LogFactory.getLog(SoftwareAction.class);

	@Autowired
	private MongoLogService mongoLogService;

	@Autowired
	private MCommonService mCommonService;

	@Autowired
	private RemoteCSEService remoteCSEService;

	@Autowired
	private NodeService nodeService;

	@Autowired
	private SoftwareService softwareService;
	
	@Autowired
	private AEService aEService;

	/**
	 * create software
	 * @param nodeID
	 * @param labels
	 * @param softwareProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/node-{nodeID:.*}", method = RequestMethod.POST, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" }, params = "ty=software")
	@ResponseBody
	public ResponseEntity<Object> softwareCreate(@PathVariable String nodeID
											   , @RequestParam(value = "nm", required = false) String labels
											   , @RequestBody SoftwareVO softwareProfile
											   , HttpServletRequest request
											   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		SoftwareVO responseVo = new SoftwareVO();
		HttpStatus httpStatus = HttpStatus.CREATED;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey 	= CommonUtil.nvl(request.getHeader("dKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(nodeID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "nodeID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(softwareProfile)) {
			isError = true;
			responseCode = "600";
			responseMsg = "<software> " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(softwareProfile.getVersion())) {
			isError = true;
			responseCode = "600";
			responseMsg = "<software> version " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus.OK);
		}

		try {
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSEByNodeLink(nodeID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			NodeVO nodeItem = null;
			if (CommonUtil.isEmpty((nodeItem = nodeService.findOneNode(nodeID)))) {
				throw new IotException("625", CommonUtil.getMessage("msg.device.node.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, null, dKey, nodeID);
			
			SoftwareVO softwareItem = null;
			if (!CommonUtil.isEmpty(softwareItem = softwareService.findOneSoftwareByName(nodeItem.getResourceID(), labels))) {
				//responseVo.setSoftware(softwareItem);
				BeanUtils.copyProperties(softwareItem, responseVo);
				throw new IotException("609", CommonUtil.getMessage("msg.device.software.duplicate.text", locale));
			}
			
			softwareProfile.setLabels(labels);
			softwareProfile.setName(labels);
			softwareItem = softwareService.createSoftware(nodeItem, softwareProfile);
			//responseVo.setSoftware(softwareItem);
			BeanUtils.copyProperties(softwareItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);

		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * update software
	 * @param nodeID
	 * @param softwareID
	 * @param softwareProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/node-{nodeID}/software-{softwareID}", method = RequestMethod.PUT, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> softwareUpdate(@PathVariable String nodeID
											   , @PathVariable String softwareID
											   , @RequestBody SoftwareVO softwareProfile
											   , HttpServletRequest request
											   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		SoftwareVO responseVo = new SoftwareVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey 	= CommonUtil.nvl(request.getHeader("dKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(nodeID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "nodeID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(softwareID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "softwareID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(softwareProfile)) {
			isError = true;
			responseCode = "600";
			responseMsg = "<software> " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}

		try {
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSEByNodeLink(nodeID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			NodeVO nodeItem = null;
			if (CommonUtil.isEmpty((nodeItem = nodeService.findOneNode(nodeID)))) {
				throw new IotException("625", CommonUtil.getMessage("msg.device.node.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, null, dKey, nodeID);
			
			SoftwareVO softwareItem = null;
			if (CommonUtil.isEmpty((softwareItem = softwareService.findOneSoftware("resourceID", softwareID)))) {
				throw new IotException("632", CommonUtil.getMessage("msg.device.software.noRegi.text", locale));
			}
			
			softwareProfile.setResourceID(softwareID);
			softwareItem = softwareService.updateSoftware(softwareProfile, false);
			//responseVo.setSoftware(softwareItem);
			BeanUtils.copyProperties(softwareItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * delete software
	 * @param nodeID
	 * @param softwareID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/node-{nodeID}/software-{softwareID}", method = RequestMethod.DELETE, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Void> softwareDelete(@PathVariable String nodeID
											 , @PathVariable String softwareID
											 , HttpServletRequest request
											 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		Object responseVo = new Object();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey 	= CommonUtil.nvl(request.getHeader("dKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(nodeID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "nodeID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(softwareID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "softwareID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Void>(httpStatus);
		}

		try {
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSEByNodeLink(nodeID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			NodeVO nodeItem = null;
			if (CommonUtil.isEmpty((nodeItem = nodeService.findOneNode(nodeID)))) {
				throw new IotException("625", CommonUtil.getMessage("msg.device.node.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, null, dKey, nodeID);
			
			SoftwareVO softwareItem = null;
			if (CommonUtil.isEmpty((softwareItem = softwareService.findOneSoftware("resourceID", softwareID)))) {
				throw new IotException("632", CommonUtil.getMessage("msg.device.software.noRegi.text", locale));
			}
			
			softwareService.deleteSoftware("resourceID", softwareItem.getResourceID());

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Void>(httpStatus);
	}
	
	/**
	 * retrieve software
	 * @param nodeID
	 * @param softwareID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/node-{nodeID}/software-{softwareID}", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> softwareRetrieve(@PathVariable String nodeID
												 , @PathVariable String softwareID
												 , HttpServletRequest request
												 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();
		
		SoftwareVO responseVo = new SoftwareVO();
		HttpStatus httpStatus = HttpStatus.OK;
		
		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey		= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic 	= CommonUtil.nvl(request.getHeader("topic"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(nodeID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "nodeID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(softwareID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "softwareID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);
			
			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			
			RemoteCSEVO findRemoteCse = null;
			if (CommonUtil.isEmpty(findRemoteCse = remoteCSEService.findOneRemoteCSEByNodeLink(nodeID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			NodeVO nodeItem = null;
			if (CommonUtil.isEmpty((nodeItem = nodeService.findOneNode(nodeID)))) {
				throw new IotException("625", CommonUtil.getMessage("msg.device.node.noRegi.text", locale));
			}
			
			SoftwareVO softwareItem = null;
			if (CommonUtil.isEmpty((softwareItem = softwareService.findOneSoftware("resourceID", softwareID)))) {
				throw new IotException("632", CommonUtil.getMessage("msg.device.software.noRegi.text", locale));
			}
			
			//responseVo.setSoftware(softwareItem);
			BeanUtils.copyProperties(softwareItem, responseVo);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();
			
			httpStatus = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
}