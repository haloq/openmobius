package kr.re.keti.iot.domain.oneM2M;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import kr.re.keti.common.base.BaseVO;

/**
 * locationPolicy domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@XmlRootElement(name="locationPolicy", namespace="http://www.onem2m.org/xml/protocols")
@XmlAccessorType(value=XmlAccessType.FIELD)
public class LocationPolicyVO extends BaseVO {
	private static final long serialVersionUID = 1L;

	private String resourceType;
	private String resourceID;
	private String parentID;
	private String creationTime;
	private String lastModifiedTime;
	private String expirationTime;
	private String accessControlPolicyIDs;
	private String labels;
	private String locationSource;
	private String locationUpdatePeriod;
	private String locationTargetId;
	private String locationServer;
	private String locationContainerID;
	private String locationContainerName;
	private String locationStatus;

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
	
	public String getResourceID() {
		return resourceID;
	}

	public void setResourceID(String resourceID) {
		this.resourceID = resourceID;
	}

	public String getParentID() {
		return parentID;
	}

	public void setParentID(String parentID) {
		this.parentID = parentID;
	}

	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	public String getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(String lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public String getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getAccessControlPolicyIDs() {
		return accessControlPolicyIDs;
	}

	public void setAccessControlPolicyIDs(String accessControlPolicyIDs) {
		this.accessControlPolicyIDs = accessControlPolicyIDs;
	}

	public String getLabels() {
		return labels;
	}

	public void setLabels(String labels) {
		this.labels = labels;
	}

	public String getLocationSource() {
		return locationSource;
	}

	public void setLocationSource(String locationSource) {
		this.locationSource = locationSource;
	}

	public String getLocationUpdatePeriod() {
		return locationUpdatePeriod;
	}

	public void setLocationUpdatePeriod(String locationUpdatePeriod) {
		this.locationUpdatePeriod = locationUpdatePeriod;
	}

	public String getLocationTargetId() {
		return locationTargetId;
	}

	public void setLocationTargetId(String locationTargetId) {
		this.locationTargetId = locationTargetId;
	}

	public String getLocationServer() {
		return locationServer;
	}

	public void setLocationServer(String locationServer) {
		this.locationServer = locationServer;
	}

	public String getLocationContainerID() {
		return locationContainerID;
	}

	public void setLocationContainerID(String locationContainerID) {
		this.locationContainerID = locationContainerID;
	}

	public String getLocationContainerName() {
		return locationContainerName;
	}

	public void setLocationContainerName(String locationContainerName) {
		this.locationContainerName = locationContainerName;
	}

	public String getLocationStatus() {
		return locationStatus;
	}

	public void setLocationStatus(String locationStatus) {
		this.locationStatus = locationStatus;
	}

	@Override
	public String toString() {
		return "LocationPolicyVO [resourceType=" + resourceType
				+ ", resourceID=" + resourceID + ", parentID=" + parentID
				+ ", creationTime=" + creationTime + ", lastModifiedTime="
				+ lastModifiedTime + ", expirationTime=" + expirationTime
				+ ", accessControlPolicyIDs=" + accessControlPolicyIDs
				+ ", labels=" + labels + ", locationSource=" + locationSource
				+ ", locationUpdatePeriod=" + locationUpdatePeriod
				+ ", locationTargetId=" + locationTargetId
				+ ", locationServer=" + locationServer
				+ ", locationContainerID=" + locationContainerID
				+ ", locationContainerName=" + locationContainerName
				+ ", locationStatus=" + locationStatus + "]";
	};

}
