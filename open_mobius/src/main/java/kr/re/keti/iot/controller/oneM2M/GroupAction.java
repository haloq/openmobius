package kr.re.keti.iot.controller.oneM2M;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.re.keti.iot.domain.apilog.CallerVO;
import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.ExecInstanceVO;
import kr.re.keti.iot.domain.oneM2M.GroupVO;
import kr.re.keti.iot.domain.oneM2M.ListOfResourceVO;
import kr.re.keti.iot.domain.oneM2M.MgmtCmdVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao.ONEM2M;
import kr.re.keti.iot.service.apilog.ApiLogService;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.ExecInstanceService;
import kr.re.keti.iot.service.oneM2M.GroupService;
import kr.re.keti.iot.service.oneM2M.MgmtCmdService;
import kr.re.keti.iot.service.oneM2M.RemoteCSEService;
import kr.re.keti.iot.service.oneM2M.common.MCommonService;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * group management Action.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Controller
public class GroupAction {

	private static final Log logger = LogFactory.getLog(GroupAction.class);
	
	@Autowired
	private MongoLogService mongoLogService;
	
	@Autowired
	private MCommonService mCommonService;

	@Autowired
	private RemoteCSEService remoteCSEService;

	@Autowired
	private MgmtCmdService mgmtCmdService;
	
	@Autowired
	private GroupService groupService;
	
	@Autowired
	private ExecInstanceService execInstanceService;
	
	@Autowired
	private ApiLogService apiLogService;
	
	/**
	 * request group control
	 * @param groupName
	 * @param mgmtCmdName
	 * @param mgmtCmdProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/group-{groupName}/fanOutPoint/mgmtCmd-{mgmtCmdName}", method = RequestMethod.POST, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" }, params = "ty=execInstance")
	@ResponseBody
	public ResponseEntity<Object> groupControl(@PathVariable String groupName
											 , @PathVariable String mgmtCmdName
											 , @RequestBody MgmtCmdVO mgmtCmdProfile
											 , HttpServletRequest request
											 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();
		
		ListOfResourceVO responseVo = new ListOfResourceVO();
		responseVo.setName(ONEM2M.EXEC_INSTANCE.getName());
		
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey		= CommonUtil.nvl(request.getHeader("uKey"), "");
		CallerVO callerVO = null;
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(groupName)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "groupName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(mgmtCmdName)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "mgmtCmdName " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		else if(CommonUtil.isEmpty(mgmtCmdProfile)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<mgmtCmd> " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		
		if(isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			
			GroupVO findGroupItem = null;
			if(CommonUtil.isEmpty(findGroupItem = groupService.findOneGroupByName(groupName))) {
				throw new IotException("633", CommonUtil.getMessage("msg.group.noRegi.text", locale));
			}
			
			String remoteCSEIds = findGroupItem.getMembersList();
			if(CommonUtil.isEmpty(remoteCSEIds)) {
				throw new IotException("600", "membersList " + CommonUtil.getMessage("msg.input.empty.text"));
			}
			
			String[] arrRemoteCSEId = remoteCSEIds.split(",");
			List<ExecInstanceVO> findListExecInstance = null;
			findListExecInstance = groupService.groupControl(arrRemoteCSEId, mgmtCmdName, mgmtCmdProfile);
			
			responseVo.setExecInstanceList(findListExecInstance);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	
	/**
	 * retrieve group control result
	 * @param groupName
	 * @param mgmtCmdName
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/group-{groupName}/fanOutPoint/mgmtCmd-{mgmtCmdName}", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> groupControlRetrieve(@PathVariable String groupName
													 , @PathVariable String mgmtCmdName
													 , HttpServletRequest request
													 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ListOfResourceVO responseVo = new ListOfResourceVO();
		responseVo.setName(ONEM2M.EXEC_INSTANCE.getName());
		
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg  = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey		= CommonUtil.nvl(request.getHeader("uKey"), "");
		String listExecInstance = request.getHeader("execInstanceList");
		CallerVO callerVO = null;
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(groupName)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "groupName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(mgmtCmdName)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "mgmtCmdName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(listExecInstance)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "execInstance List " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if(isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
				
		try {
			List<String> listExecInstanceIds = Arrays.asList(listExecInstance.split(","));
			List<String> listMgmtCmdIds = new ArrayList<String>();
			
			GroupVO findGroupItem = null;
			if(CommonUtil.isEmpty(findGroupItem = groupService.findOneGroupByName(groupName))) {
				throw new IotException("633", CommonUtil.getMessage("msg.group.noRegi.text", locale));
			}
			
			String membersList = findGroupItem.getMembersList();
			if(!CommonUtil.isEmpty(membersList)) {
				List<String> listRemoteCSEIds = Arrays.asList(membersList.split(","));
				
				if(remoteCSEService.getCount(listRemoteCSEIds) < 1) {
					throw new IotException("621", CommonUtil.getMessage("msg.group.device.empty.text", locale));
				}
				
				MgmtCmdVO findMgmtCmdItem = null;
				for (String remoteCSEID : listRemoteCSEIds) {
					findMgmtCmdItem = mgmtCmdService.findOneMgmtCmdByLabels(remoteCSEID, mgmtCmdName);
					if(!CommonUtil.isEmpty(findMgmtCmdItem)) {
						listMgmtCmdIds.add(findMgmtCmdItem.getResourceID());
					}
				}
				
				if(CommonUtil.isEmpty(listMgmtCmdIds) || listMgmtCmdIds.size() < 1) {
					throw new IotException("627", CommonUtil.getMessage("msg.group.mgmtCmdsList.empty.text", locale));
				}
				
			} else {
				throw new IotException("600", CommonUtil.getMessage("msg.group.membersList.empty.text", locale));
			}
			
			for (String execInstanceID : listExecInstanceIds) {
				if(execInstanceService.getCount(listMgmtCmdIds, execInstanceID) < 1) {
					throw new IotException("691", CommonUtil.getMessage("msg.group.execInstancesList.noauth", new String[]{execInstanceID}));
				}
			}
			
			List<ExecInstanceVO> findListExecInstance = null;
			if(CommonUtil.isEmpty(findListExecInstance = execInstanceService.findListExecInstanceIds(listExecInstanceIds))) {
				throw new IotException("628", CommonUtil.getMessage("msg.device.execInstance.noRegi.text", locale));
			}
			
			responseVo.setExecInstanceList(findListExecInstance);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}

}