package kr.re.keti.iot.service.oneM2M.common;


import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.re.keti.iot.domain.common.CertKeyVO;
import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.mdao.oneM2M.AEDao;
import kr.re.keti.iot.mdao.oneM2M.AccessControlPolicyDao;
import kr.re.keti.iot.mdao.oneM2M.RemoteCSEDao;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.util.CertUtil;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.ntels.nisf.util.PropertiesUtil;

import biz.source_code.base64Coder.Base64Coder;

/**
 * MCommon management Service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Service
public class MCommonService {

	private static final Log logger = LogFactory.getLog(MCommonService.class);
	
	@Autowired
	private MongoLogService mongoLogService;
	
	@Autowired
	private RemoteCSEDao remoteCSEDao;
	
	@Autowired
	private AEDao aEDao;
	
	@Autowired
	private AccessControlPolicyDao accessControlPolicyDao;
	
	@Autowired
	private CertUtil certUtil;
	
	
	/**
	 * verify dKey by device
	 * @param checkMccP
	 * @param remoteCSEId
	 * @param dKey
	 * @throws IotException
	 */
	public void verifyDKey(boolean checkMccP, String remoteCSEId, String dKey) throws IotException {
		this.verifyDKey(checkMccP, remoteCSEId, dKey, null);
	}
	
	/**
	 * verify aKey by app
	 * @param checkMccP
	 * @param appID
	 * @param aKey
	 * @throws IotException
	 */
	public void verifyAKey(boolean checkMccP, String appID, String aKey) throws IotException {
		this.verifyAKey(checkMccP, appID, aKey, null);
	}
	
	
	
	/**
	 * verify dKey by device
	 * @param checkMccP
	 * @param remoteCSEId
	 * @param dKey
	 * @param nodeId
	 * @throws IotException
	 */
	public void verifyDKey(boolean checkMccP, String remoteCSEId, String dKey, String nodeId) throws IotException {
		if (checkMccP) return;
		
		if(CommonUtil.isEmpty(dKey)) {
			throw new IotException("600", "dKey " + CommonUtil.getMessage("msg.input.empty.text"));
			
		} else if(CommonUtil.isEmpty(remoteCSEId) && CommonUtil.isEmpty(nodeId)) {
			throw new IotException("600", "remoteCSEID or nodeID " + CommonUtil.getMessage("msg.input.empty.text"));
			
		}
		
		try{
			mongoLogService.log(logger, LEVEL.DEBUG, "dKey decode before	: " + dKey);
			dKey = Base64Coder.decodeString(dKey);
			mongoLogService.log(logger, LEVEL.DEBUG, "dKey decode after		: " + dKey);
		}catch(Exception e){
			e.printStackTrace();
			throw new IotException("600", "dKey " + CommonUtil.getMessage("msg.base64.decodeFail.text"));
		}		
		
		Query query = new Query();
		query.addCriteria(Criteria.where("dKey").is(dKey));
		
		if(!CommonUtil.isEmpty(remoteCSEId))	query.addCriteria(Criteria.where("resourceID").is(remoteCSEId));
		if(!CommonUtil.isEmpty(nodeId))			query.addCriteria(Criteria.where("nodeLink").is(nodeId));
		
		if (remoteCSEDao.findOne(query) == null) {
			throw new IotException("602",CommonUtil.getMessage("msg.device.dKey.verifyFail.text"));
		}
	}
	
	/**
	 * verify aKey by app
	 * @param checkMccP
	 * @param appId
	 * @param aKey
	 * @param nodeId
	 * @throws IotException
	 */
	public void verifyAKey(boolean checkMccP, String appId, String aKey, String nodeId) throws IotException {
		if (checkMccP) return;
		
		if(CommonUtil.isEmpty(aKey)) {
			throw new IotException("600", "aKey " + CommonUtil.getMessage("msg.input.empty.text"));
			
		} else if(CommonUtil.isEmpty(appId) && CommonUtil.isEmpty(nodeId)) {
			throw new IotException("600", "appId " + CommonUtil.getMessage("msg.input.empty.text"));
			
		}
		
		try{
			mongoLogService.log(logger, LEVEL.DEBUG, "aKey decode before	: " + aKey);
			aKey = Base64Coder.decodeString(aKey);
			mongoLogService.log(logger, LEVEL.DEBUG, "aKey decode after		: " + aKey);
			
		}catch(Exception e){
			e.printStackTrace();
			throw new IotException("600", "aKey " + CommonUtil.getMessage("msg.base64.decodeFail.text"));
		}		
		
		Query query = new Query();
		query.addCriteria(Criteria.where("aKey").is(aKey));
		
		if(!CommonUtil.isEmpty(appId))	query.addCriteria(Criteria.where("appId").is(appId));
		
		if (aEDao.findOne(query) == null) {
			throw new IotException("605",CommonUtil.getMessage("msg.device.aKey.verifyFail.text"));
		}
	}
	
	/**
	 * patten check device ID
	 * @param checkMccP
	 * @param deviceID
	 * @throws IotException
	 */
	public void checkPatternDeviceId(boolean checkMccP, String deviceID) throws IotException {
		if (checkMccP) return;
		
		boolean isError = false;
		String responseCode	= "200";
		String responseMsg	= "";
		
		String[] arrDeviceId = deviceID.split("\\.");
		int deviceIdLen = arrDeviceId.length;
		if(deviceIdLen < 7 || deviceIdLen > 11){
			isError			= true;
			responseCode	= "600";
			responseMsg		= CommonUtil.getMessage("device.error.notformat.deviceId");
		}
		
		else if(arrDeviceId[0].length() !=1 || arrDeviceId[1].length() !=1 || arrDeviceId[2].length() !=3 || arrDeviceId[3].length() !=1 ){
			isError			= true;
			responseCode	= "600";
			responseMsg		= CommonUtil.getMessage("device.error.notformat.deviceId");
		}
		
		if(isError){
			throw new IotException(responseCode, responseMsg);
		}
	}
	
	/**
	 * patten check app ID
	 * @param checkMccP
	 * @param appID
	 * @throws IotException
	 */
	public void checkPatternAppId(boolean checkMccP, String appID) throws IotException {
		if (checkMccP) return;
		
		boolean isError = false;
		String responseCode	= "200";
		String responseMsg	= "";
		
		String[] arrAppId = appID.split("\\.");
		int appIdLen = arrAppId.length;
		if(appIdLen < 7 || appIdLen > 11){
			isError			= true;
			responseCode	= "600";
			responseMsg		= CommonUtil.getMessage("device.error.notformat.appId");
		}
		
		else if(arrAppId[0].length() !=1 || arrAppId[1].length() !=1 || arrAppId[2].length() !=3 || arrAppId[3].length() !=1 ){
			isError			= true;
			responseCode	= "600";
			responseMsg		= CommonUtil.getMessage("device.error.notformat.appId");
		}
		
		if(isError){
			throw new IotException(responseCode, responseMsg);
		}

	}
	
	/**
	 * create dKey
	 * @param checkMccP
	 * @param deviceID
	 * @return
	 * @throws IotException
	 */
	public String createDeviceKey(boolean checkMccP, String deviceID) throws IotException {
		if (checkMccP) return null;
		
		CertKeyVO certKeyVO = null;
		String dKey = "";
		
		try {
			certKeyVO	= certUtil.createDeviceKey(deviceID);
			dKey		= certKeyVO.getCert_client_id();
			
			if(!"200".equals(certKeyVO.getResult_code())) {
				throw new IotException(certKeyVO.getResult_code(), certKeyVO.getResult_msg());
			}
			else if(CommonUtil.isEmpty(dKey)) {
				
				throw new IotException("600", "[dKey create error] " + certKeyVO.getResult_msg());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			throw new IotException("600", "[dKey createDeviceKey Exception] " + e.getMessage());
		}
		
		return dKey;
	}
	
	/**
	 * create aKey
	 * @param checkMccP
	 * @param appID
	 * @return
	 * @throws IotException
	 */
	public String createAKey(boolean checkMccP, String appID) throws IotException {
		if (checkMccP) return null;
		
		CertKeyVO certKeyVO = null;
		String dKey = "";
		
		try {
			certKeyVO	= certUtil.createDeviceKey(appID);
			dKey		= certKeyVO.getCert_client_id();
			
			if(!"200".equals(certKeyVO.getResult_code())) {
				throw new IotException(certKeyVO.getResult_code(), certKeyVO.getResult_msg());
			}
			else if(CommonUtil.isEmpty(dKey)) {
				
				throw new IotException("600", "[aKey create error] " + certKeyVO.getResult_msg());
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			
			throw new IotException("600", "[aKey createAKey Exception] " + e.getMessage());
		}
		
		return dKey;
	}
	
	/**
	 * set response header
	 * @param request
	 * @param response
	 * @param obj
	 */
	public void setResponseHeader(HttpServletRequest request, HttpServletResponse response, Object obj) {
		try {
			
			String location = null;
			int iContentLength = 0;
			String[] nm = request.getParameterValues("nm");
			String[] resourceID = request.getParameterValues("resourceID");
			String contentType = CommonUtil.nvl(request.getHeader("Content-Type"), "");
			String requestId = CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
			String xml = CommonUtil.marshalToXmlString(obj);
			if (xml != null) {
				iContentLength = xml.getBytes("UTF-8").length;
			}
			String contentLength = Integer.toString(iContentLength > 0 ? iContentLength : 0);
			String uri = CommonUtil.getURL(request);
			if (nm != null && nm.length > 0 && nm[0] != null && !"".equals(nm[0])){
				location = uri + "/" + nm[0];
			} else if (resourceID != null && resourceID.length > 0 && resourceID[0] != null && !"".equals(resourceID)){
				location = uri + "-" + resourceID[0];
			} else {
				location = uri;
			}
			
			if (location != null && !"".equals(location)) {
				response.setHeader("Location", location);
			}
			if (contentType != null && !"".equals(contentType)) {
				response.setHeader("Content-Type", contentType);
			}
			if (contentLength != null && !"".equals(contentLength)) {
				response.setHeader("Content-Length", contentLength);
			}
			if (requestId != null && !"".equals(requestId)) {
				response.setHeader("X-M2M-RI", requestId);
			}
			
			logger.debug("Location      : " + location);
			logger.debug("Content-Type  : " + contentType);
			logger.debug("Content-Length: " + contentLength);
			logger.debug("X-M2M-RI      : " + requestId);
		} catch (UnsupportedEncodingException e) {
			logger.error(e.getMessage());
		}
	}
	
	/**
	 * check mcc'
	 * @param from
	 * @return
	 */
	public boolean getCheckMccP(String from) {
		if (from == null || "".equals(from)) return false;
		
		boolean checkMccP = false;
		String[] mccPList = PropertiesUtil.get("config", "mp.oneM2M.mccPList").split("\\|");
		
		for (int i=0; i<mccPList.length; i++) {
			String mccP = mccPList[i];
			if (from.equals(mccP)) {
				checkMccP = true;
				return checkMccP;
			}
		}
		
		return checkMccP;
	}
}