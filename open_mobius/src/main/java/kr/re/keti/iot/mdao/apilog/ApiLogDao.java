package kr.re.keti.iot.mdao.apilog;

import java.util.List;

import kr.re.keti.iot.domain.apilog.ApiLogVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao;

import org.springframework.stereotype.Repository;

/**
 * contentInstance management Dao.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Repository
public class ApiLogDao extends GenericMongoDao {
	
	public ApiLogDao() {
		collectionName = ONEM2M.API_LOG.getName();
		cls = ApiLogVO.class;
	}
	
	/**
	 * multi insert
	 * @param apiLogList
	 * @throws Exception
	 */
	public void multi_insert(List<ApiLogVO> apiLogList) throws Exception {
		mongoTemplate.insert(apiLogList, collectionName);
	}
}
