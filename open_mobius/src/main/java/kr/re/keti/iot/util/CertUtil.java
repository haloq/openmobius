package kr.re.keti.iot.util;

import kr.re.keti.iot.domain.common.CertKeyVO;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ntels.nisf.util.PropertiesUtil;

/**
 * certUtil.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Component
public class CertUtil {
	
	private static final Log logger = LogFactory.getLog(CertUtil.class);
	
	@Autowired
	private RestTemplate restTemplate;
	
	
	/**
	 * create dKey
	 * @param sDeviceID
	 * @return
	 */
	public CertKeyVO createDeviceKey(String sDeviceID) {

		CertKeyVO certKeyVO = new CertKeyVO();
		certKeyVO.setCert_id(sDeviceID);
		certKeyVO.setOU(PropertiesUtil.get("openapi", "iot.certEngine.OU.device"));
		
		certKeyVO = createKeyByCallAuthServer(certKeyVO);
		certKeyVO.setCert_type(CommonUtil.cert_types.get("device"));
		
		return certKeyVO; 
	}
	
	/**
	 * create key by call authServer
	 * @param certKeyVO
	 * @return
	 */
	private CertKeyVO createKeyByCallAuthServer(CertKeyVO certKeyVO){
		
		String param = "";
		String url = PropertiesUtil.get("openapi", "iot.certEngine.createKey");
		
		if(certKeyVO.getOU().equals(PropertiesUtil.get("openapi", "iot.certEngine.OU.device"))){
			param = "OU=" + certKeyVO.getOU() + "&CN=" + certKeyVO.getCert_id();
		}else if(certKeyVO.getOU().equals(PropertiesUtil.get("openapi", "iot.certEngine.OU.user"))){
			param = "OU=" + certKeyVO.getOU() + "&CN=" + certKeyVO.getCert_id() + "&email=" + certKeyVO.getEmail();
		}else if(certKeyVO.getOU().equals(PropertiesUtil.get("openapi", "iot.certEngine.OU.platform"))){
			param = "OU=" + certKeyVO.getOU() + "&CN=" + certKeyVO.getCert_id();
		}
		
		String callResult = connectAuthServer(param, url);
		
		Gson gson = new GsonBuilder().serializeNulls().create();
		CertKeyVO resultkeyVO = gson.fromJson(callResult, CertKeyVO.class); 
		
		if(resultkeyVO == null || resultkeyVO.getErrno() == null){
			
			resultkeyVO = new CertKeyVO();
			resultkeyVO.setResult_code("600");
			resultkeyVO.setResult_msg(CommonUtil.getMessage("msg.apicall.error.text"));
			
		}else if(resultkeyVO.getErrno() == 0){
			
			resultkeyVO.setResult_code("200");
			resultkeyVO.setCert_id(certKeyVO.getCert_id());
			resultkeyVO.setCert_client_id(resultkeyVO.getCid());
			resultkeyVO.setCert_public_key(resultkeyVO.getPub());
			resultkeyVO.setCert_private_key(resultkeyVO.getPri());
			
		}else{
			resultkeyVO.setResult_code("600");
			resultkeyVO.setResult_msg(resultkeyVO.getErrmsg());
		}
		
		return resultkeyVO;
	}
	
	/**
	 * connect authServer
	 * @param param
	 * @param url
	 * @return
	 */
	public String connectAuthServer(String param, String url){
		
		url = PropertiesUtil.get("openapi", "CertEngine.url") + url;
		
		HttpHeaders headers = new HttpHeaders();
		
		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
		headers.add("Accept", "*/*");
		
		logger.info("### param : " + param);
		logger.info("### url : " + url);
		HttpEntity<String> entity = new HttpEntity<String>(param, headers);
		
		
		ResponseEntity<String> response = restTemplate.exchange(
				 url
				,HttpMethod.POST
				,entity
				,String.class);
		logger.info("### body : " + response.getBody());

		//Gson gson = new GsonBuilder().serializeNulls().create();
		//CertKeyVO certKeyVO = gson.fromJson(response.getBody(), CertKeyVO.class);

		return response.getBody();
	}
}
