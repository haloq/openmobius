package kr.re.keti.iot.controller.oneM2M;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.ContainerVO;
import kr.re.keti.iot.domain.oneM2M.MgmtCmdVO;
import kr.re.keti.iot.domain.oneM2M.SubscriptionVO;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.ContainerService;
import kr.re.keti.iot.service.oneM2M.MgmtCmdService;
import kr.re.keti.iot.service.oneM2M.RemoteCSEService;
import kr.re.keti.iot.service.oneM2M.SubscriptionService;
import kr.re.keti.iot.service.oneM2M.common.MCommonService;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * subscription management Action.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>khlee</li>
 *         </ul>
 */
@Controller
public class SubscriptionAction {

	private static final Log logger = LogFactory.getLog(SubscriptionAction.class);

	@Autowired
	private MongoLogService mongoLogService;

	@Autowired
	private MCommonService mCommonService;

	@Autowired
	private RemoteCSEService remoteCSEService;
	
	@Autowired
	private ContainerService containerService;
	
	@Autowired
	private MgmtCmdService mgmtCmdService;
	
	@Autowired
	private SubscriptionService subscriptionService;
	
	/**
	 * create container subscription
	 * @param remoteCSEID
	 * @param containerName
	 * @param labels
	 * @param subscriptionProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/container-{containerName}", method = RequestMethod.POST, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" }, params = {"ty=subscription"})
	@ResponseBody
	public ResponseEntity<Object> remoteCSEContainerSubscriptionCreate(@PathVariable String remoteCSEID
																	 , @PathVariable String containerName
																	 , @RequestParam(value = "nm", required = false) String labels
																	 , @RequestBody SubscriptionVO subscriptionProfile
																	 , HttpServletRequest request
																	 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		SubscriptionVO responseVo = new SubscriptionVO();
		HttpStatus httpStatus = HttpStatus.CREATED;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey   	= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic  	= CommonUtil.nvl(request.getHeader("topic"), "");;

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(subscriptionProfile)) {
			isError = true;
			responseCode = "600";
			responseMsg = "<subscription> " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}

		try {
			
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			ContainerVO containerItem = null;
			if (CommonUtil.isEmpty(containerItem = containerService.findOneContainerByLabels(remoteCSEID, containerName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			SubscriptionVO findSubscriptionItem = null;
			if (!CommonUtil.isEmpty((findSubscriptionItem = subscriptionService.findOneSubscriptionByLabels(containerItem.getResourceID(), labels)))) {
				//responseVo.setSubscription(findSubscriptionItem);
				BeanUtils.copyProperties(findSubscriptionItem, responseVo);
				throw new IotException("609", CommonUtil.getMessage("msg.device.subscription.duplicate.text", locale));
			}
			
			subscriptionService.checkSubscriptionValidation(subscriptionProfile);
			
			subscriptionProfile.setParentID(containerItem.getResourceID());
			subscriptionProfile.setLabels(labels);
			subscriptionProfile.setCreator(remoteCSEID);
			SubscriptionVO subscriptionItem = subscriptionService.createSubscription(subscriptionProfile);
			//responseVo.setSubscription(subscriptionItem);
			BeanUtils.copyProperties(subscriptionItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * delete container subscription
	 * @param remoteCSEID
	 * @param containerName
	 * @param subscriptionID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/container-{containerName}/subscription-{subscriptionID}", method = RequestMethod.DELETE, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Void> remoteCSEContainerSubscriptionDelete(@PathVariable String remoteCSEID
																   , @PathVariable String containerName
																   , @PathVariable String subscriptionID
																   , HttpServletRequest request
																   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		Object responseVo = new Object();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey   	= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic  	= CommonUtil.nvl(request.getHeader("topic"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(subscriptionID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "subscriptionID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Void>(httpStatus);
		}

		try {
			
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			ContainerVO containerItem = null;
			if (CommonUtil.isEmpty(containerItem = containerService.findOneContainerByLabels(remoteCSEID, containerName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			SubscriptionVO subscriptionItem = null;
			if (CommonUtil.isEmpty(subscriptionItem = subscriptionService.findOneSubscription(containerItem.getResourceID(), subscriptionID))) {
				throw new IotException("638", CommonUtil.getMessage("msg.device.subscription.noRegi.text", locale));
			}
			
			subscriptionService.deleteSubscription(containerItem.getResourceID(), subscriptionItem.getResourceID());

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Void>(httpStatus);
	}
	
	/**
	 * update container subscription
	 * @param remoteCSEID
	 * @param containerName
	 * @param subscriptionID
	 * @param subscriptionProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/container-{containerName}/subscription-{subscriptionID}", method = RequestMethod.PUT, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> remoteCSEContainerSubscriptionUpdate(@PathVariable String remoteCSEID
																	 , @PathVariable String containerName
																	 , @PathVariable String subscriptionID
																	 , @RequestBody SubscriptionVO subscriptionProfile
																	 , HttpServletRequest request
																	 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		SubscriptionVO responseVo = new SubscriptionVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey   	= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic  	= CommonUtil.nvl(request.getHeader("topic"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(subscriptionID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "subscriptionID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(subscriptionProfile)) {
			isError = true;
			responseCode = "600";
			responseMsg = "<subscription> " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}

		try {
			
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			ContainerVO containerItem = null;
			if (CommonUtil.isEmpty(containerItem = containerService.findOneContainerByLabels(remoteCSEID, containerName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			SubscriptionVO subscriptionItem = null;
			if (CommonUtil.isEmpty(subscriptionItem = subscriptionService.findOneSubscription(containerItem.getResourceID(), subscriptionID))) {
				throw new IotException("638", CommonUtil.getMessage("msg.device.subscription.noRegi.text", locale));
			}
			
			subscriptionService.checkSubscriptionValidation(subscriptionProfile);
			
			subscriptionProfile.setParentID(containerItem.getResourceID());
			subscriptionProfile.setResourceID(subscriptionItem.getResourceID());
			subscriptionItem = subscriptionService.updateSubscription(subscriptionProfile);
			//responseVo.setSubscription(subscriptionItem);
			BeanUtils.copyProperties(subscriptionItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}

	/**
	 * retrieve container subscription
	 * @param remoteCSEID
	 * @param containerName
	 * @param subscriptionID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/container-{containerName}/subscription-{subscriptionID}", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> remoteCSEContainerSubscriptionRetrieve(@PathVariable String remoteCSEID
																	   , @PathVariable String containerName
																	   , @PathVariable String subscriptionID
																	   , HttpServletRequest request
																	   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		SubscriptionVO responseVo = new SubscriptionVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg  = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey   	= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic  	= CommonUtil.nvl(request.getHeader("topic"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(subscriptionID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "subscriptionID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if(isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			 
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			ContainerVO containerItem = null;
			if (CommonUtil.isEmpty(containerItem = containerService.findOneContainerByLabels(remoteCSEID, containerName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			SubscriptionVO findSubscriptionItem = null;
			if (CommonUtil.isEmpty(findSubscriptionItem = subscriptionService.findOneSubscription(containerItem.getResourceID(), subscriptionID))) {
				throw new IotException("638", CommonUtil.getMessage("msg.device.subscription.noRegi.text", locale));
			}
			
			//responseVo.setSubscription(findSubscriptionItem);
			BeanUtils.copyProperties(findSubscriptionItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * create mgmtCmd subscription
	 * @param remoteCSEID
	 * @param mgmtCmdName
	 * @param labels
	 * @param subscriptionProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/mgmtCmd-{mgmtCmdName}", method = RequestMethod.POST, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" }, params = {"ty=subscription"})
	@ResponseBody
	public ResponseEntity<Object> remoteCSEMgmtCmdSubscriptionCreate(@PathVariable String remoteCSEID
																   , @PathVariable String mgmtCmdName
																   , @RequestParam(value = "nm", required = false) String labels
																   , @RequestBody SubscriptionVO subscriptionProfile
																   , HttpServletRequest request
																   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		SubscriptionVO responseVo = new SubscriptionVO();
		HttpStatus httpStatus = HttpStatus.CREATED;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey   	= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic  	= CommonUtil.nvl(request.getHeader("topic"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(mgmtCmdName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "mgmtCmdName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(subscriptionProfile)) {
			isError = true;
			responseCode = "600";
			responseMsg = "<subscription> " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}

		try {
			
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			MgmtCmdVO mgmtCmdItem = null;
			if (CommonUtil.isEmpty(mgmtCmdItem = mgmtCmdService.findOneMgmtCmdByLabels(remoteCSEID, mgmtCmdName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.mgmtCmd.noRegi.text", locale));
			}
			
			SubscriptionVO findSubscriptionItem = null;
			if (!CommonUtil.isEmpty((findSubscriptionItem = subscriptionService.findOneSubscriptionByLabels(mgmtCmdItem.getResourceID(), labels)))) {
				//responseVo.setSubscription(findSubscriptionItem);
				BeanUtils.copyProperties(findSubscriptionItem, responseVo);
				throw new IotException("609", CommonUtil.getMessage("msg.device.subscription.duplicate.text", locale));
			}
			
			subscriptionService.checkSubscriptionValidation(subscriptionProfile);
			
			subscriptionProfile.setParentID(mgmtCmdItem.getResourceID());
			subscriptionProfile.setLabels(labels);
			subscriptionProfile.setCreator(remoteCSEID);
			SubscriptionVO subscriptionItem = subscriptionService.createSubscription(subscriptionProfile);
			//responseVo.setSubscription(subscriptionItem);
			BeanUtils.copyProperties(subscriptionItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}

	/**
	 * delete mgmtCmd subscription
	 * @param remoteCSEID
	 * @param mgmtCmdName
	 * @param subscriptionID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/mgmtCmd-{mgmtCmdName}/subscription-{subscriptionID}", method = RequestMethod.DELETE, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Void> remoteCSEMgmtCmdSubscriptionDelete(@PathVariable String remoteCSEID
																 , @PathVariable String mgmtCmdName
																 , @PathVariable String subscriptionID
																 , HttpServletRequest request
																 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		Object responseVo = new Object();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey   	= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic  	= CommonUtil.nvl(request.getHeader("topic"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(mgmtCmdName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "mgmtCmdName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(subscriptionID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "subscriptionID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Void>(httpStatus);
		}

		try {
			
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			MgmtCmdVO mgmtCmdItem = null;
			if (CommonUtil.isEmpty(mgmtCmdItem = mgmtCmdService.findOneMgmtCmdByLabels(remoteCSEID, mgmtCmdName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.mgmtCmd.noRegi.text", locale));
			}
			
			SubscriptionVO subscriptionItem = null;
			if (CommonUtil.isEmpty(subscriptionItem = subscriptionService.findOneSubscription(mgmtCmdItem.getResourceID(), subscriptionID))) {
				throw new IotException("638", CommonUtil.getMessage("msg.device.subscription.noRegi.text", locale));
			}
			
			subscriptionService.deleteSubscription(mgmtCmdItem.getResourceID(), subscriptionItem.getResourceID());

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Void>(httpStatus);
	}
	
	/**
	 * update mgmtCmd subscription
	 * @param remoteCSEID
	 * @param mgmtCmdName
	 * @param subscriptionID
	 * @param subscriptionProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/mgmtCmd-{mgmtCmdName}/subscription-{subscriptionID}", method = RequestMethod.PUT, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> remoteCSEMgmtCmdSubscriptionUpdate(@PathVariable String remoteCSEID
																   , @PathVariable String mgmtCmdName
																   , @PathVariable String subscriptionID
																   , @RequestBody SubscriptionVO subscriptionProfile
																   , HttpServletRequest request
																   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		SubscriptionVO responseVo = new SubscriptionVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey   	= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic  	= CommonUtil.nvl(request.getHeader("topic"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(mgmtCmdName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "mgmtCmdName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(subscriptionID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "subscriptionID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(subscriptionProfile)) {
			isError = true;
			responseCode = "600";
			responseMsg = "<subscription> " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}

		try {
			
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			MgmtCmdVO mgmtCmdItem = null;
			if (CommonUtil.isEmpty(mgmtCmdItem = mgmtCmdService.findOneMgmtCmdByLabels(remoteCSEID, mgmtCmdName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.mgmtCmd.noRegi.text", locale));
			}
			
			SubscriptionVO subscriptionItem = null;
			if (CommonUtil.isEmpty(subscriptionItem = subscriptionService.findOneSubscription(mgmtCmdItem.getResourceID(), subscriptionID))) {
				throw new IotException("638", CommonUtil.getMessage("msg.device.subscription.noRegi.text", locale));
			}
			
			subscriptionService.checkSubscriptionValidation(subscriptionProfile);
			
			subscriptionProfile.setParentID(mgmtCmdItem.getResourceID());
			subscriptionProfile.setResourceID(subscriptionItem.getResourceID());
			subscriptionItem = subscriptionService.updateSubscription(subscriptionProfile);
			//responseVo.setSubscription(subscriptionItem);
			BeanUtils.copyProperties(subscriptionItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}

	/**
	 * retrieve mgmtCmd subscription
	 * @param remoteCSEID
	 * @param mgmtCmdName
	 * @param subscriptionID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/mgmtCmd-{mgmtCmdName}/subscription-{subscriptionID}", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> remoteCSEMgmtCmdSubscriptionRetrieve(@PathVariable String remoteCSEID
																	 , @PathVariable String mgmtCmdName
																	 , @PathVariable String subscriptionID
																	 , HttpServletRequest request
																	 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		SubscriptionVO responseVo = new SubscriptionVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg  = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey   	= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic  	= CommonUtil.nvl(request.getHeader("topic"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(mgmtCmdName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "mgmtCmdName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(subscriptionID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "subscriptionID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if(isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			 
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			MgmtCmdVO mgmtCmdItem = null;
			if (CommonUtil.isEmpty(mgmtCmdItem = mgmtCmdService.findOneMgmtCmdByLabels(remoteCSEID, mgmtCmdName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.mgmtCmd.noRegi.text", locale));
			}
			
			SubscriptionVO findSubscriptionItem = null;
			if (CommonUtil.isEmpty(findSubscriptionItem = subscriptionService.findOneSubscription(mgmtCmdItem.getResourceID(), subscriptionID))) {
				throw new IotException("638", CommonUtil.getMessage("msg.device.subscription.noRegi.text", locale));
			}
			
			//responseVo.setSubscription(findSubscriptionItem);
			BeanUtils.copyProperties(findSubscriptionItem, responseVo);
			

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
}