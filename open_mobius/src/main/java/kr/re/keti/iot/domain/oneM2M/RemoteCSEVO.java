package kr.re.keti.iot.domain.oneM2M;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import kr.re.keti.common.base.BaseVO;

/**
 * remoteCSE domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@XmlRootElement(name="remoteCSE", namespace="http://www.onem2m.org/xml/protocols")
@XmlAccessorType(value=XmlAccessType.FIELD)
public class RemoteCSEVO extends BaseVO {
	private static final long serialVersionUID = 1L;

	private String resourceType;
	private String resourceID;
	private String parentID;
	private String creationTime;
	private String lastModifiedTime;
	private String expirationTime;
	private String accessControlPolicyIDs;
	private String labels;
	private String cseType;
	private String pointOfAccess;
	private String CSEBase;
	@XmlElement(name = "CSE-ID", required=true)
	private String cseId;
	private String requestReachability;
	private String nodeLink;
	@XmlTransient
	private String passCode;
	private String dKey;
	@XmlTransient
	private String updateStatus;
	@XmlTransient
	private String mappingYn;
	
	public String getResourceType() {
		return resourceType;
	}
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
	public String getResourceID() {
		return resourceID;
	}
	public void setResourceID(String resourceID) {
		this.resourceID = resourceID;
	}
	public String getParentID() {
		return parentID;
	}
	public void setParentID(String parentID) {
		this.parentID = parentID;
	}
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}
	public String getLastModifiedTime() {
		return lastModifiedTime;
	}
	public void setLastModifiedTime(String lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}
	public String getExpirationTime() {
		return expirationTime;
	}
	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}
	public String getAccessControlPolicyIDs() {
		return accessControlPolicyIDs;
	}
	public void setAccessControlPolicyIDs(String accessControlPolicyIDs) {
		this.accessControlPolicyIDs = accessControlPolicyIDs;
	}
	public String getLabels() {
		return labels;
	}
	public void setLabels(String labels) {
		this.labels = labels;
	}
	public String getCseType() {
		return cseType;
	}
	public void setCseType(String cseType) {
		this.cseType = cseType;
	}
	public String getPointOfAccess() {
		return pointOfAccess;
	}
	public void setPointOfAccess(String pointOfAccess) {
		this.pointOfAccess = pointOfAccess;
	}
	public String getCSEBase() {
		return CSEBase;
	}
	public void setCSEBase(String cSEBase) {
		CSEBase = cSEBase;
	}
	public String getCseId() {
		return cseId;
	}
	public void setCseId(String cseId) {
		this.cseId = cseId;
	}
	public String getRequestReachability() {
		return requestReachability;
	}
	public void setRequestReachability(String requestReachability) {
		this.requestReachability = requestReachability;
	}
	public String getNodeLink() {
		return nodeLink;
	}
	public void setNodeLink(String nodeLink) {
		this.nodeLink = nodeLink;
	}
	public String getPassCode() {
		return passCode;
	}
	public void setPassCode(String passCode) {
		this.passCode = passCode;
	}
	public String getdKey() {
		return dKey;
	}
	public void setdKey(String dKey) {
		this.dKey = dKey;
	}
	public String getUpdateStatus() {
		return updateStatus;
	}
	public void setUpdateStatus(String updateStatus) {
		this.updateStatus = updateStatus;
	}
	public String getMappingYn() {
		return mappingYn;
	}
	public void setMappingYn(String mappingYn) {
		this.mappingYn = mappingYn;
	}
	@Override
	public String toString() {
		return "RemoteCSEVO [resourceType=" + resourceType + ", resourceID="
				+ resourceID + ", parentID=" + parentID + ", creationTime="
				+ creationTime + ", lastModifiedTime=" + lastModifiedTime
				+ ", expirationTime=" + expirationTime
				+ ", accessControlPolicyIDs=" + accessControlPolicyIDs
				+ ", labels=" + labels + ", cseType=" + cseType
				+ ", pointOfAccess=" + pointOfAccess + ", CSEBase=" + CSEBase
				+ ", cseId=" + cseId + ", requestReachability="
				+ requestReachability + ", nodeLink=" + nodeLink
				+ ", passCode=" + passCode + ", dKey=" + dKey
				+ ", updateStatus=" + updateStatus + ", mappingYn=" + mappingYn
				+ "]";
	}
}