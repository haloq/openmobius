package kr.re.keti.iot.service.oneM2M;

import java.util.List;
import java.util.regex.Pattern;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.NodeVO;
import kr.re.keti.iot.domain.oneM2M.RemoteCSEVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao.MOBIUS;
import kr.re.keti.iot.mdao.oneM2M.RemoteCSEDao;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.util.CommonUtil;
import kr.re.keti.iot.util.oneM2M.CommonCode;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

/**
 * remoteCSE management Service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Service
public class RemoteCSEService {

	private static final Log logger = LogFactory.getLog(RemoteCSEService.class);

	@Autowired
	private MongoLogService mongoLogService;

	@Autowired
	private RemoteCSEDao remoteCSEDao;
	
	@Autowired
	private NodeService nodeService;	
	
	@Autowired
	private ContainerService containerService;	
	
	@Autowired
	private MgmtCmdService mgmtCmdService;	
	
	@Autowired
	private AccessControlPolicyService accessControlPolicyService;	

	/**
	 * find remoteCSE by remoteCSEID
	 * @param remoteCSEID
	 * @return
	 * @throws IotException
	 */
	public RemoteCSEVO findOneRemoteCSE(String remoteCSEID) throws IotException {

		RemoteCSEVO findRemoteCSEItem = null;

		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(remoteCSEID));

		try {

			findRemoteCSEItem = (RemoteCSEVO) remoteCSEDao.findOne(query);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.findFail.text"));
		}

		return findRemoteCSEItem;
	}
	
	/**
	 * find remoteCSE by nodeId
	 * @param nodeId
	 * @return
	 * @throws IotException
	 */
	public RemoteCSEVO findOneRemoteCSEByNodeLink(String nodeId) throws IotException {
		
		RemoteCSEVO findRemoteCSEItem = null;
		
		Query query = new Query();
		query.addCriteria(Criteria.where("nodeLink").is(nodeId));
		
		try {
			
			findRemoteCSEItem = (RemoteCSEVO) remoteCSEDao.findOne(query);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.findFail.text"));
		}
		
		return findRemoteCSEItem;
	}
	
	/**
	 * find remoteCSE list by remoteCSEID
	 * @param remoteCSEID
	 * @return
	 * @throws IotException
	 */
	public List<RemoteCSEVO> findLikeListRemoteCSEById(String remoteCSEID) throws IotException {
		
		List<RemoteCSEVO> findListRemoteCSEItem = null;
		String reegexResourceId =  "^" + remoteCSEID;
		
		Query query = new Query();
		query.addCriteria(Criteria.where("mappingYn").is("N"));
		query.addCriteria(Criteria.where("resourceID").regex(Pattern.compile(reegexResourceId)));
		
		try {
			findListRemoteCSEItem = (List<RemoteCSEVO>) remoteCSEDao.find(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.findFail.text"));
		}
		
		return findListRemoteCSEItem;
	}	
	
	/**
	 * find remoteCSE count by remoteCSEID
	 * @param remoteCSEID
	 * @return
	 */
	public long getCount(String remoteCSEID){
		
		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(remoteCSEID));
		
		long cnt = 0;
		
		try {
			cnt = remoteCSEDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "remoteCSE get count");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		return cnt;
	}
	
	/**
	 * find remoteCSE count by listRemoteCSEIds
	 * @param listRemoteCSEIds
	 * @return
	 */
	public long getCount(List<String> listRemoteCSEIds){
		
		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").in(listRemoteCSEIds));
		
		long cnt = 0;
		
		try {
			cnt = remoteCSEDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "remoteCSE get count");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		return cnt;
	}
	
	/**
	 * find remoteCSE count by remoteCSEID-passCode
	 * @param remoteCSEID
	 * @param passCode
	 * @return
	 */
	public long getCount(String remoteCSEID, String passCode){
		
		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(remoteCSEID));
		query.addCriteria(Criteria.where("passCode").is(passCode));
		
		long cnt = 0;
		
		try {
			cnt = remoteCSEDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "remoteCSE get count");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		return cnt;
	}
	
	/**
	 * create remoteCSE
	 * @param remoteCSEItem
	 * @param remoteCSEVOProfile
	 * @return
	 * @throws IotException
	 */
	public RemoteCSEVO createRemoteCSE(RemoteCSEVO remoteCSEItem, RemoteCSEVO remoteCSEVOProfile) throws IotException {
		
		boolean isError = false;
		String errMsg = "";
		
		if(CommonUtil.isEmpty(remoteCSEVOProfile)) {
			isError = true;
			errMsg = "<CSEBase> " + CommonUtil.getMessage("msg.input.empty.text");
		}
		else if(CommonUtil.isEmpty(remoteCSEItem.getResourceID())){
			isError = true;
			errMsg = "resource_id " + CommonUtil.getMessage("msg.input.empty.text");
		}
		
		
		if(isError){
			mongoLogService.log(logger, LEVEL.ERROR, errMsg);
			throw new IotException("600", errMsg);
		}
		
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		remoteCSEItem.setResourceType(CommonCode.RESOURCE_TYPE.REMOTE_CSE.getName());
		remoteCSEItem.setParentID(MOBIUS.NAME.getName());
		remoteCSEItem.setCSEBase(remoteCSEVOProfile.getCSEBase());
		remoteCSEItem.setRequestReachability(remoteCSEVOProfile.getRequestReachability());
		remoteCSEItem.setAccessControlPolicyIDs(remoteCSEVOProfile.getAccessControlPolicyIDs());
		remoteCSEItem.setCseType(remoteCSEVOProfile.getCseType());
		remoteCSEItem.setPointOfAccess(remoteCSEVOProfile.getPointOfAccess());
		remoteCSEItem.setMappingYn("N");
		
		remoteCSEItem.setCreationTime(currentTime);
		remoteCSEItem.setLastModifiedTime(currentTime);
		
		NodeVO nodeItem = null;
		try {
			//add Node
			nodeItem = nodeService.createNode(remoteCSEItem, currentTime);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.node.createFail.text"));
		}
		
		try {
			//add RemoteCSE
			if(!CommonUtil.isEmpty(nodeItem) && !CommonUtil.isEmpty(nodeItem.getNodeID())) {
				remoteCSEItem.setNodeLink(nodeItem.getNodeID());
			}
			remoteCSEDao.insert(remoteCSEItem);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.regFail.text"));
		}
		
		return findOneRemoteCSE(remoteCSEItem.getResourceID());
	}
	
	/**
	 * delete remoteCSE by remoteCSEID
	 * @param remoteCSEID
	 * @throws IotException
	 */
	public void deleteRemoteCSE(String remoteCSEID) throws IotException {
		
		Query query = new Query(Criteria.where("resourceID").is(remoteCSEID));
		
		if(this.getCount(remoteCSEID) < 1){
			throw new IotException("604", CommonUtil.getMessage("msg.device.empty.text") + " : remoteCSEID ["+remoteCSEID+"]");
		}
		
		try {
			remoteCSEDao.remove(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "remoteCSE remove");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.delFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "remoteCSE delete success");
	}
	
	/**
	 * delete remoteCSE with child by deviceId
	 * @param deviceId
	 * @throws IotException
	 */
	public void deleteRemoteCSEChild(String deviceId) throws IotException {
		
		try {
			nodeService.deleteNodeChild(deviceId);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "node remove");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			
			throw new IotException("600", CommonUtil.getMessage("msg.device.node.delFail.text"));
		}
		
		try {
			containerService.deleteContainerByRemoteCSEID(deviceId);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "remoteCSE inner container remove");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			
			throw new IotException("600", CommonUtil.getMessage("msg.device.container.delFail.text"));
		}
		
		try {
			mgmtCmdService.deleteMgmtCmdByRemoteCSEID(deviceId);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "remoteCSE inner mgmtCmd remove");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			
			throw new IotException("600", CommonUtil.getMessage("msg.mgmtcmd.delFail.text"));
		}
		
		this.deleteRemoteCSE(deviceId);
	}
	
	/**
	 * update remoteCSE dKey
	 * @param remoteCSEItem
	 * @return
	 * @throws IotException
	 */
	public RemoteCSEVO updateRemoteCSEDkey(RemoteCSEVO remoteCSEItem) throws IotException {
		mongoLogService.log(logger, LEVEL.DEBUG, "updateRemoteCSEDkey start");
		
		String resourceID = remoteCSEItem.getResourceID();
		NodeVO nodeItem = new NodeVO();
		
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		if(nodeService.getCount(resourceID) < 1){
			nodeItem = nodeService.createNode(remoteCSEItem, currentTime);
		}
		
		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(resourceID));
		
		Update update = new Update();
		update.set("lastModifiedTime", currentTime);
		update.set("dKey", remoteCSEItem.getdKey());
		
		try {
			
			if(!CommonUtil.isEmpty(nodeItem) && !CommonUtil.isEmpty(nodeItem.getNodeID())) {
				update.set("nodeLink", nodeItem.getNodeID());
			}
			
			remoteCSEDao.update(query, update);
	
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.upFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "updateRemoteCSEDkey update end");
		
		return this.findOneRemoteCSE(resourceID);
	}
	
	/**
	 * update remoteCSE
	 * @param remoteCSEID
	 * @param remoteCSEVOProfile
	 * @return
	 * @throws IotException
	 */
	public RemoteCSEVO updateRemoteCSE(String remoteCSEID, RemoteCSEVO remoteCSEVOProfile) throws IotException {
		mongoLogService.log(logger, LEVEL.DEBUG, "remoteCSE update start");
		
		RemoteCSEVO findRemoteCSEItem = new RemoteCSEVO();
		
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		String accessControlPolicyIDs = remoteCSEVOProfile.getAccessControlPolicyIDs();
		String labels = remoteCSEVOProfile.getLabels();
		String cseType = remoteCSEVOProfile.getCseType();
		String pointOfAccess = remoteCSEVOProfile.getPointOfAccess();
		String requestReachability = remoteCSEVOProfile.getRequestReachability();
		String nodeLink = remoteCSEVOProfile.getNodeLink();
		
		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(remoteCSEID));
		
		Update update = new Update();
		if(!CommonUtil.isNull(currentTime))				update.set("lastModifiedTime", currentTime);
		if(!CommonUtil.isNull(accessControlPolicyIDs))	update.set("accessControlPolicyIDs", accessControlPolicyIDs);
		if(!CommonUtil.isNull(labels))					update.set("labels", labels);
		if(!CommonUtil.isNull(cseType))					update.set("cseType", cseType);
		if(!CommonUtil.isNull(pointOfAccess))			update.set("pointOfAccess", pointOfAccess);
		if(!CommonUtil.isNull(requestReachability))		update.set("requestReachability", requestReachability);
		if(!CommonUtil.isNull(nodeLink))				update.set("nodeLink", nodeLink);
		
		try {
			
			remoteCSEDao.update(query, update);
			
			mongoLogService.log(logger, LEVEL.DEBUG, "remoteCSE update success");
			
			findRemoteCSEItem = this.findOneRemoteCSE(remoteCSEID);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "remoteCSE update Exception");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.upFail.text"));
		}
		
		try { 
			nodeService.updateNode(findRemoteCSEItem, currentTime);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "RemoteCSEService.updateRemoteCSE Exception");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.node.upFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "remoteCSE update end");
		
		return findRemoteCSEItem;
	}
}