package kr.re.keti.iot.mdao.common;

import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.ReturnType;
import org.springframework.data.redis.core.RedisCallback;

/**
 * redis Sequence script
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
public class RedisSequenceScript implements RedisCallback<String>{
	
	private final String KEY_NAME = "key_sequence";
	
	private final String INIT_TIME_SEC = "10";
	
	private final String script = 	"local sequence = redis.call('INCR','"+KEY_NAME+"') " +
										"if sequence == 1 then " +
										"redis.call('EXPIRE','"+KEY_NAME+"',"+INIT_TIME_SEC+") " +
										"end " +
										"return sequence";
	
	/**
	 * getArgvs
	 * @return
	 */
	public byte[] getArgvs(){
		byte[] argvs = {0};
		return argvs;
	}

	/**
	 * doInRedis
	 */
	@Override
	public String doInRedis(RedisConnection connection) throws DataAccessException {
		
		long seq = connection.eval(script.getBytes(), ReturnType.VALUE, 0, getArgvs());
		return String.valueOf(seq);
	}

}
