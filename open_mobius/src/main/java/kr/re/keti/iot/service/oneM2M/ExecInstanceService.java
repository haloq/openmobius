package kr.re.keti.iot.service.oneM2M;

import java.util.ArrayList;
import java.util.List;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.ExecInstanceVO;
import kr.re.keti.iot.domain.oneM2M.MgmtCmdVO;
import kr.re.keti.iot.mdao.common.SequenceDao;
import kr.re.keti.iot.mdao.common.SequenceDao.MovType;
import kr.re.keti.iot.mdao.common.SequenceDao.SEQ_PREFIX;
import kr.re.keti.iot.mdao.common.SequenceDao.SeqType;
import kr.re.keti.iot.mdao.oneM2M.ExecInstanceDao;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.SubscriptionService.SUBSCRIPTION_RESOURCE_STATUS_TYPE;
import kr.re.keti.iot.util.CommonUtil;
import kr.re.keti.iot.util.oneM2M.CommonCode.RESOURCE_TYPE;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

/**
 * execInstance management Service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Service
public class ExecInstanceService {

private static final Log logger = LogFactory.getLog(ExecInstanceService.class);
	
	@Autowired
	private MongoLogService mongoLogService;
	
	@Autowired
	private SubscriptionService subscriptionService;
	
	@Autowired
	private SequenceDao seqDao;
	
	@Autowired
	private ExecInstanceDao execInstanceDao;
	
	public enum EXEC_STATUS{
	
		INITIATED("1"), 
		STARTED("2"), 
		FINISHED("3"),
		CANCELLING("4"),
		CANCELLED("5"),
		STATUS_NON_CANCELLABLE("6");
		
		private final String id;
		EXEC_STATUS(String id){
			this.id = id;
		}
		public String getValue() {return id;}
			
	}
	
	public enum EXEC_RESULT{
		STATUS_REQUEST_UNSUPPORTED("1"),
		STATUS_REQUEST_DENIED("2"),
		STATUS_CANCELLATION_DENIED("3"),
		STATUS_INTERNAL_ERROR("4"),
		STATUS_INVALID_ARGUMENTS("5"),
		STATUS_RESOURCES_EXCEEDED("6"),
		STATUS_FILE_TRANSFER_FAILED("7"),
		STATUS_FILE_TRANSFER_SERVER_AUTHENTICATION_FAILURE("8"),
		STATUS_UNSUPPORTED_PROTOCOL("9"),
		STATUS_UPLOAD_FAILED("10"),
		STATUS_FILE_TRANSFER_FAILED_MULTICAST_GROUP_UNABLE_JOIN("11"),
		STATUS_FILE_TRANSFER_FAILED_SERVER_CONTACT_FAILED("12"),
		STATUS_FILE_TRANSFER_FAILED_FILE_ACCESS_FAILED("13"),
		STATUS_FILE_TRANSFER_FAILED_DOWNLOAD_INCOMPLETE("14"),
		STATUS_FILE_TRANSFER_FAILED_FILE_CORRUPTED("15"),
		STATUS_FILE_TRANSFER_FILE_AUTHENTICATION_FAILURE("16"),
		//STATUS_FILE_TRANSFER_FAILED("17"),
		//STATUS_FILE_TRANSFER_SERVER_AUTHENTICATION_FAILURE("18"),
		STATUS_FILE_TRANSFER_WINDOW_EXCEEDED("19"),
		STATUS_INVALID_UUID_FORMAT("20"),
		STATUS_UNKNOWN_EXECUTION_ENVIRONMENT("21"),
		STATUS_DISABLED_EXECUTION_ENVIRONMENT("22"),
		STATUS_EXECUTION_ENVIRONMENT_MISMATCH("23"),
		STATUS_DUPLICATE_DEPLOYMENT_UNIT("24"),
		STATUS_SYSTEM_RESOURCES_EXCEEDED("25"),
		STATUS_UNKNOWN_DEPLOYMENT_UNIT("26"),
		STATUS_INVALID_DEPLOYMENT_UNIT_STATE("27"),
		STATUS_INVALID_DEPLOYMENT_UNIT_UPDATE_DOWNGRADE_DISALLOWED("28"),
		STATUS_INVALID_DEPLOYMENT_UNIT_UPDATE_UPGRADE_DISALLOWED("29"),
		STATUS_INVALID_DEPLOYMENT_UNIT_UPDATE_VERSION_EXISTS("30");
		
		private final String id;
		EXEC_RESULT(String id){
			this.id = id;
		}
		public String getValue() {return id;}
	}
	
	/**
	 * generate resourceID
	 * @return
	 * @throws IotException
	 */
	private String generateResourceId() throws IotException {
		StringBuffer bufSeq = new StringBuffer(SEQ_PREFIX.EXEC_INSTANCE.getValue());
		long longSeq = 0;
		
		try {
			
			longSeq = seqDao.move(MovType.UP, SeqType.EXEC_INSTANCE);
			
			bufSeq.append(String.format("%010d", longSeq));
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.execInstance.id.createFail.text"));
		}
		
		return bufSeq.toString();
	}

	/**
	 * retrieve execInstance count by resourceID
	 * @param resourceID
	 * @return
	 */
	public long getCount(String resourceID){
		
		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(resourceID));
		
		long cnt = 0;
		
		try {
			cnt = execInstanceDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "execInstance get count");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		return cnt;
	}	
	
	/**
	 * retrieve execInstance count by mgmtCmdId-resourceID
	 * @param mgmtCmdId
	 * @param resourceID
	 * @return
	 */
	public long getCountByMgmtCmdId(String mgmtCmdId, String resourceID){
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(mgmtCmdId));
		query.addCriteria(Criteria.where("resourceID").is(resourceID));
		
		long cnt = 0;
		
		try {
			cnt = execInstanceDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "execInstance get count");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		return cnt;
	}	
	
	/**
	 * retrieve execInstance count by mgmtCmdIds-resourceID
	 * @param mgmtCmdIds
	 * @param resourceID
	 * @return
	 */
	public long getCount(List<String> mgmtCmdIds, String resourceID){
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").in(mgmtCmdIds));
		query.addCriteria(Criteria.where("resourceID").is(resourceID));
		
		long cnt = 0;
		
		try {
			cnt = execInstanceDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "execInstance get count");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		return cnt;
	}	
	
	/**
	 * create execInstance
	 * @param mgmtCmdProfile
	 * @return
	 * @throws IotException
	 */
	public ExecInstanceVO createExecInstance(MgmtCmdVO mgmtCmdProfile) throws IotException {
		mongoLogService.log(logger, LEVEL.DEBUG, "execInstance create start");
		
		String resourceId = this.generateResourceId();
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		ExecInstanceVO execInstanceItem = new ExecInstanceVO();
		ExecInstanceVO findExecInstanceItem = new ExecInstanceVO();
		
		execInstanceItem.setResourceType(RESOURCE_TYPE.EXEC_INSTANCE.getName());
		execInstanceItem.setResourceID(resourceId);
		execInstanceItem.setParentID(mgmtCmdProfile.getResourceID());
		
		execInstanceItem.setCreationTime(currentTime);
		execInstanceItem.setLastModifiedTime(currentTime);
		
		execInstanceItem.setAccessControlPolicyIDs(mgmtCmdProfile.getAccessControlPolicyIDs());
		execInstanceItem.setExecStatus(EXEC_STATUS.INITIATED.getValue());
		execInstanceItem.setExecTarget(mgmtCmdProfile.getExecTarget());
		execInstanceItem.setExecMode(mgmtCmdProfile.getExecMode());
		execInstanceItem.setExecFrequency(mgmtCmdProfile.getExecFrequency());
		execInstanceItem.setExecDelay(mgmtCmdProfile.getExecDelay());
		execInstanceItem.setExecNumber(mgmtCmdProfile.getExecNumber());
		execInstanceItem.setExecReqArgs(mgmtCmdProfile.getExecReqArgs());

		try { 
			execInstanceDao.insert(execInstanceItem);
			mongoLogService.log(logger, LEVEL.DEBUG, "execInstance create success");
			
			findExecInstanceItem = this.findOneExecInstance(resourceId);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.execInstance.createFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "execInstance create end");
		
		subscriptionService.sendSubscription(execInstanceItem.getParentID(), execInstanceItem.getResourceID(), SUBSCRIPTION_RESOURCE_STATUS_TYPE.CHILD_CREATED, ExecInstanceVO.class, findExecInstanceItem);
		
		return findExecInstanceItem;
	}
	
	/**
	 * find execInstance by resourceId
	 * @param resourceId
	 * @return
	 * @throws IotException
	 */
	public ExecInstanceVO findOneExecInstance(String resourceId) throws IotException {
		ExecInstanceVO findExecInstanceItem = null;

		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(resourceId));

		try {
			findExecInstanceItem = (ExecInstanceVO) execInstanceDao.findOne(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.execInstance.findFail.text"));
		}

		return findExecInstanceItem;
	}
	
	/**
	 * find execInstance by mgmtCmdId-resourceId
	 * @param mgmtCmdId
	 * @param resourceId
	 * @return
	 * @throws IotException
	 */
	public ExecInstanceVO findOneExecInstanceByMgmtCmdId(String mgmtCmdId, String resourceId) throws IotException {
		ExecInstanceVO findExecInstanceItem = null;
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(mgmtCmdId));
		query.addCriteria(Criteria.where("resourceID").is(resourceId));
		
		try {
			findExecInstanceItem = (ExecInstanceVO) execInstanceDao.findOne(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.execInstance.findFail.text"));
		}
		
		return findExecInstanceItem;
	}
	
	/**
	 * find execInstanceList by listExecInstanceId
	 * @param listExecInstanceId
	 * @return
	 * @throws IotException
	 */
	public List<ExecInstanceVO> findListExecInstanceIds(List<String> listExecInstanceId) throws IotException {
		List<ExecInstanceVO> findExecInstanceList = null;
		
		if(CommonUtil.isEmpty(listExecInstanceId) || listExecInstanceId.size() == 0) {
			throw new IotException("600", "execInstance List " + CommonUtil.getMessage("msg.input.empty.text"));
		}
		
		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").in(listExecInstanceId));
		
		try {
			findExecInstanceList = (List<ExecInstanceVO>) execInstanceDao.find(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.execInstance.findFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "execInstance list search success");
		
		return findExecInstanceList;
		
	}	
	
	/**
	 * find execInstanceList by mgmtCmdId
	 * @param mgmtCmdId
	 * @return
	 * @throws IotException
	 */
	public List<ExecInstanceVO> findListInitedExecInstanceByParentID(String mgmtCmdId) throws IotException {
		List<ExecInstanceVO> findExecInstanceList = null;
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(mgmtCmdId));
		query.addCriteria(Criteria.where("execStatus").is(EXEC_STATUS.INITIATED.getValue()));
		
		try {
			findExecInstanceList = (List<ExecInstanceVO>) execInstanceDao.find(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.execInstance.findFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "mgmtCmd inner execInstance list search success");
		
		return findExecInstanceList;
		
	}
	
	/**
	 * retrieve polling execInstanceList
	 * @param mgmtCmdId
	 * @return
	 * @throws IotException
	 */
	public List<ExecInstanceVO> pollingExecInstanceRetrieve(String mgmtCmdId) throws IotException {
		
		List<ExecInstanceVO> findListExecInstance = this.findListInitedExecInstanceByParentID(mgmtCmdId);
		
		for (ExecInstanceVO execInstanceVO : findListExecInstance) {
			execInstanceVO.setExecStatus(EXEC_STATUS.STARTED.getValue());
			this.updateExecInstanceResult(execInstanceVO);
			
		}
		
		return findListExecInstance;
		
	}

	/**
	 * delete  execInstance by mgmtCmdId
	 * @param mgmtCmdId
	 * @throws IotException
	 */
	public void deleteExecInstance(String mgmtCmdId) throws IotException {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(mgmtCmdId));
		
		try {
			execInstanceDao.remove(query);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.execinstance.delFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "execInstance delete success");
		
	}
	
	/**
	 * delete  execInstance by listMgmtCmdId
	 * @param listMgmtCmdId
	 * @throws IotException
	 */
	public void deleteExecInstanceByMgmtCmdIds(ArrayList<String> listMgmtCmdId) throws IotException {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").in(listMgmtCmdId));
		
		try {
			execInstanceDao.remove(query);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.contentinstance.delFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "mgmtCmd inner execInstance delete success");
		
	}

	/**
	 * update execInstanceResult
	 * @param execInstanceProfile
	 * @return
	 * @throws IotException
	 */
	public ExecInstanceVO updateExecInstanceResult(ExecInstanceVO execInstanceProfile) throws IotException {
		mongoLogService.log(logger, LEVEL.DEBUG, "execInstance updateExecInstanceResult start");
		
		if(this.getCount(execInstanceProfile.getResourceID()) < 1) {
			throw new IotException("628", CommonUtil.getMessage("msg.device.execInstance.noRegi.text"));
		}
		
		
		ExecInstanceVO findExecInstanceItem = new ExecInstanceVO();
		
		String resourceId = execInstanceProfile.getResourceID();
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(resourceId));
		
		Update update = new Update();
		update.set("lastModifiedTime", currentTime);
		update.set("execStatus", execInstanceProfile.getExecStatus());
		update.set("execResult", execInstanceProfile.getExecResult());
		
		try { 
			
			execInstanceDao.update(query, update);
			
			mongoLogService.log(logger, LEVEL.DEBUG, "execInstance update success");
			
			findExecInstanceItem = this.findOneExecInstance(resourceId);
	
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.execInstance.upFail.text"));
		}

		mongoLogService.log(logger, LEVEL.DEBUG, "execInstance updateExecInstanceResult end");
		
		return findExecInstanceItem;
	}
	
}