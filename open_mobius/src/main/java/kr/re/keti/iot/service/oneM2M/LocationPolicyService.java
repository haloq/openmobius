package kr.re.keti.iot.service.oneM2M;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.ContainerVO;
import kr.re.keti.iot.domain.oneM2M.LocationPolicyVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao.MOBIUS;
import kr.re.keti.iot.mdao.common.SequenceDao;
import kr.re.keti.iot.mdao.common.SequenceDao.MovType;
import kr.re.keti.iot.mdao.common.SequenceDao.SEQ_PREFIX;
import kr.re.keti.iot.mdao.common.SequenceDao.SeqType;
import kr.re.keti.iot.mdao.oneM2M.LocationPolicyDao;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.util.CommonUtil;
import kr.re.keti.iot.util.oneM2M.CommonCode.RESOURCE_TYPE;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

/**
 * locationPolicy management Service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>khlee</li>
 *         </ul>
 */
@Service
public class LocationPolicyService {

	private static final Log logger = LogFactory.getLog(LocationPolicyService.class);

	@Autowired
	private MongoLogService mongoLogService;
	
	@Autowired
	private ContainerService containerService;

	@Autowired
	private LocationPolicyDao locationPolicyDao;

	@Autowired
	private SequenceDao seqDao;

	/**
	 * find locationPolicy by key-value
	 * @param key
	 * @param value
	 * @return
	 * @throws IotException
	 */
	public LocationPolicyVO findOneLocationPolicy(String key, String value) throws IotException {
		LocationPolicyVO findLocationPolicyItem = null;

		Query query = new Query();
		query.addCriteria(Criteria.where(key).is(value));

		try {
			findLocationPolicyItem = (LocationPolicyVO) locationPolicyDao.findOne(query);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.locationPolicy.findFail.text"));
		}

		return findLocationPolicyItem;
	}
	
	/**
	 * find locationPolicy by locationTargetId-labels
	 * @param locationTargetId
	 * @param labels
	 * @return
	 * @throws IotException
	 */
	public LocationPolicyVO findOneLocationPolicyByLabels(String locationTargetId , String labels) throws IotException {
		LocationPolicyVO findLocationPolicyItem = null;

		Query query = new Query();
		query.addCriteria(Criteria.where("locationTargetId").is(locationTargetId));
		query.addCriteria(Criteria.where("labels").is(labels));

		try {
			findLocationPolicyItem = (LocationPolicyVO) locationPolicyDao.findOne(query);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.locationPolicy.findFail.text"));
		}

		return findLocationPolicyItem;
	}

	/**
	 * create locationPolicy
	 * @param locationPolicyProfile
	 * @return
	 * @throws IotException
	 */
	public LocationPolicyVO createLocationPolicy(LocationPolicyVO locationPolicyProfile) throws IotException {
		
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		LocationPolicyVO locationPolicyItem = new LocationPolicyVO();
		locationPolicyItem.setParentID(MOBIUS.NAME.getName());
		locationPolicyItem.setResourceType(RESOURCE_TYPE.LOCATION_POLICY.getName());
		locationPolicyItem.setLabels(locationPolicyProfile.getLabels());
		locationPolicyItem.setAccessControlPolicyIDs(locationPolicyProfile.getAccessControlPolicyIDs());
		locationPolicyItem.setLocationSource(locationPolicyProfile.getLocationSource());
		locationPolicyItem.setLocationUpdatePeriod(locationPolicyProfile.getLocationUpdatePeriod());
		locationPolicyItem.setLocationTargetId(locationPolicyProfile.getLocationTargetId());
		locationPolicyItem.setLocationServer(locationPolicyProfile.getLocationServer());
		locationPolicyItem.setLocationContainerID(locationPolicyProfile.getLocationContainerID());
		locationPolicyItem.setLocationContainerName(locationPolicyProfile.getLocationContainerName());
		locationPolicyItem.setLocationStatus(locationPolicyProfile.getLocationStatus());
		locationPolicyItem.setCreationTime(currentTime);
		locationPolicyItem.setLastModifiedTime(currentTime);
		
		try {
			Long locationPolicyId = seqDao.move(MovType.UP, SeqType.LOCATION_POLICY);
			locationPolicyItem.setResourceID(SEQ_PREFIX.LOCATION_POLICY.getValue() + String.format("%010d", locationPolicyId));

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.locationPolicy.createFail.text"));
		}
		
		try {
			locationPolicyDao.insert(locationPolicyItem);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.locationPolicy.createFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "locationPolicy create success");
		
		ContainerVO containerItem = new ContainerVO();
		containerItem.setResourceID(locationPolicyItem.getLocationContainerID());
		containerItem.setLocationID(locationPolicyItem.getResourceID());
		try {
			containerService.updateContainerLocationPolicy(containerItem);
			
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.locationPolicy.createFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "container locationPolicy update success");
		
		return this.findOneLocationPolicy("resourceID", locationPolicyItem.getResourceID());
		
	}

	/**
	 * delete locationPolicy by key-value
	 * @param key
	 * @param value
	 * @throws IotException
	 */
	public void deleteLocationPolicy(String key, String value) throws IotException {
		
		LocationPolicyVO locationPolicyItem = this.findOneLocationPolicy(key, value);
		if(CommonUtil.isEmpty(locationPolicyItem)) {
			throw new IotException("604", CommonUtil.getMessage("msg.device.locationPolicy.noRegi.text"));
		}
		
		ContainerVO containerItem = new ContainerVO();
		containerItem.setResourceID(locationPolicyItem.getLocationContainerID());
		containerItem.setLocationID("");
		try {
			containerService.updateContainerLocationPolicy(containerItem);
			
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.container.upFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "container locationPolicy update success");
		
		Query query = new Query();
		query.addCriteria(Criteria.where(key).is(value));
		try {

			locationPolicyDao.remove(query);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.locationPolicy.delFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "locationPolicy delete success");

	}
	
	/**
	 * update locationPolicy
	 * @param locationPolicyProfile
	 * @return
	 * @throws IotException
	 */
	public LocationPolicyVO updateLocationPolicy(LocationPolicyVO locationPolicyProfile) throws IotException {
		
		LocationPolicyVO findLocationPolicyItem = this.findOneLocationPolicy("resourceID", locationPolicyProfile.getResourceID());
		if(CommonUtil.isEmpty(findLocationPolicyItem)) {
			throw new IotException("604", CommonUtil.getMessage("msg.device.locationPolicy.noRegi.text"));
		}
		
		String locationSource 			= locationPolicyProfile.getLocationSource();
		String locationUpdatePeriod 	= locationPolicyProfile.getLocationUpdatePeriod();
		String locationTargetId 		= locationPolicyProfile.getLocationTargetId();
		String locationServer 			= locationPolicyProfile.getLocationServer();
		String locationContainerID 		= locationPolicyProfile.getLocationContainerID();
		String locationContainerName 	= locationPolicyProfile.getLocationContainerName();
		String locationStatus 			= locationPolicyProfile.getLocationStatus();
		String currentTime 				= CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		Update update = new Update();
		if(!CommonUtil.isNull(locationSource))			update.set("locationSource", locationSource);
		if(!CommonUtil.isNull(locationUpdatePeriod))	update.set("locationUpdatePeriod", locationUpdatePeriod);
		if(!CommonUtil.isNull(locationTargetId))		update.set("locationTargetId", locationTargetId);
		if(!CommonUtil.isNull(locationServer))			update.set("locationServer", locationServer);
		if(!CommonUtil.isNull(locationContainerID))		update.set("locationContainerID", locationContainerID);
		if(!CommonUtil.isNull(locationContainerName))	update.set("locationContainerName", locationContainerName);
		if(!CommonUtil.isNull(locationStatus))			update.set("locationStatus", locationStatus);
														update.set("lastModifiedTime", currentTime);
		
		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(locationPolicyProfile.getResourceID()));
		try { 
			
			locationPolicyDao.update(query, update);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.locationPolicy.upFail.text"));
		}
		
		if(!locationContainerID.equals(findLocationPolicyItem.getLocationContainerID())) {
			try {
					ContainerVO containerItem = new ContainerVO();
					containerItem.setResourceID(findLocationPolicyItem.getLocationContainerID());
					containerItem.setLocationID("");
					containerService.updateContainerLocationPolicy(containerItem);
					
					containerItem = new ContainerVO();
					containerItem.setResourceID(locationPolicyProfile.getLocationContainerID());
					containerItem.setLocationID(locationPolicyProfile.getResourceID());
					containerService.updateContainerLocationPolicy(containerItem);
						
			} catch (Exception e) {
				e.printStackTrace();
				mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
				throw new IotException("600",CommonUtil.getMessage("msg.device.container.upFail.text"));
			}
			mongoLogService.log(logger, LEVEL.INFO, "container locationPolicy update success");
		}
		
		mongoLogService.log(logger, LEVEL.INFO, "locationPolicy update success");
		
		return this.findOneLocationPolicy("resourceID", locationPolicyProfile.getResourceID());

	}

}