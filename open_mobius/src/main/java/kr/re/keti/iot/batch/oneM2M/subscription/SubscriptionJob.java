package kr.re.keti.iot.batch.oneM2M.subscription;

import java.util.List;

import kr.re.keti.iot.domain.oneM2M.SubscriptionPendingVO;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.SubscriptionService;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

/**
 * subscription panding mgmt batch
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>khlee</li>
 *         </ul>
 */
@Controller
public class SubscriptionJob {
	
	private static final Log logger = LogFactory.getLog(SubscriptionJob.class);

	@Autowired
	private MongoLogService mongoLogService;

	@Autowired
	private SubscriptionService subscriptionService;
	
	/**
	 * subscription panding retransmission
	 */
	@Scheduled(fixedDelay = 50000)
	private void subscriptionPending() {
		
		Long runTime = System.currentTimeMillis();
		try {
			mongoLogService.log(logger, LEVEL.DEBUG, "subscriptionPending scheduler start!!!");
			
			List<SubscriptionPendingVO> pendingList = subscriptionService.findSubscriptionPending();
			if(!CommonUtil.isEmpty(pendingList) && pendingList.size() > 0) {
				this.sendScriptionPending(pendingList);
			}
			

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			e.printStackTrace();
		} finally {
			mongoLogService.log(logger, LEVEL.DEBUG, "subscriptionPending scheduler end!!! (runTime : " + (System.currentTimeMillis() - runTime) + "ms)");
		}

	}
	
	/**
	 * retransmission success data delete
	 * @param pendingList
	 */
	private void sendScriptionPending(List<SubscriptionPendingVO> pendingList) {
		for(SubscriptionPendingVO subscriptionPendingItem : pendingList) {
			try {
				subscriptionService.sendSubscription(subscriptionPendingItem.getNotificationUri(), subscriptionPendingItem.getContent());
				subscriptionService.deleteSubscriptionPending(subscriptionPendingItem.getPadingKey());
				
			} catch(Exception e) {
				e.printStackTrace();
			}
			
		}
	}
}
