package kr.re.keti.iot.mdao.oneM2M;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import kr.re.keti.iot.domain.oneM2M.ContentInstanceVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao.ONEM2M;
import kr.re.keti.iot.mdao.common.GenericRedisDaoImpl;
import kr.re.keti.iot.util.CommonUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.redis.connection.SortParameters.Order;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.SessionCallback;
import org.springframework.data.redis.core.query.SortQueryBuilder;
import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * contentInstance management Dao Implement.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Repository
public class ContentInstanceDaoImpl extends GenericRedisDaoImpl<ContentInstanceVO> implements ContentInstanceDao {

	@Autowired
	protected MongoTemplate mongoTemplate;

	private String collectionName = ONEM2M.CONTENT_INSTANCE.getName();
	private Class cls = ContentInstanceVO.class;
	
	/**
	 * retrieve objectsKeys
	 * @param offset
	 * @param count
	 * @return
	 * @throws Exception
	 */
	public List<String> getObjectsKeys(long offset, long count) throws Exception {

		SortQueryBuilder<String> builder = SortQueryBuilder.sort("timestamp");
		builder.alphabetical(true);
		builder.order(Order.ASC);
		builder.limit(offset, count);
		return template.sort(builder.build());
	}
	
	/**
	 * retrieve objects by keys
	 * @param keys
	 * @return
	 * @throws Exception
	 */
	public List<ContentInstanceVO> getObjectsByKeys(List<String> keys) throws Exception {
		Gson gson = new Gson();
		List<ContentInstanceVO> objs = new ArrayList<ContentInstanceVO>();

		List<String> results = template.opsForValue().multiGet(keys);

		for (String item : results) {
			if(!CommonUtil.isEmpty(item)) {
				objs.add(gson.fromJson(item, ContentInstanceVO.class));
			}
		}
		return objs;
	}
	
	/**
	 * redis delete objects by keys
	 * @param keys
	 * @throws Exception
	 */
	public void redisDeleteObjectsByKeys(List<String> keys) throws Exception {
		template.delete(keys);
		for (String key : keys) {
			template.opsForList().remove("timestamp", 1, key);
			if (!CommonUtil.isEmpty(key)) {
				template.opsForList().remove("timestamp", 1, key);
			}
		}
	}
	
	/**
	 * upsert
	 * @param obj
	 * @throws Exception
	 */
	public void upsert(Object obj) throws Exception {
		mongoTemplate.save(obj, this.collectionName);
	}

	/**
	 * insert
	 * @param obj
	 * @throws Exception
	 */
	public void insert(Object obj) throws Exception {
		mongoTemplate.insert(obj, this.collectionName);
	}

	/**
	 * find
	 * @param query
	 * @return
	 * @throws Exception
	 */
	public List<?> find(Query query) throws Exception {
		return mongoTemplate.find(query, this.cls, this.collectionName);
	}

	/**
	 * findOne
	 * @param query
	 * @return
	 * @throws Exception
	 */
	public Object findOne(Query query) throws Exception {
		return mongoTemplate.findOne(query, this.cls, this.collectionName);
	}

	/**
	 * findAll
	 * @return
	 * @throws Exception
	 */
	public List<?> findAll() throws Exception {
		return mongoTemplate.findAll(this.cls, this.collectionName);
	}

	/**
	 * count
	 * @param query
	 * @return
	 * @throws Exception
	 */
	public long count(Query query) throws Exception {
		return mongoTemplate.count(query, this.collectionName);
	}

	/**
	 * update
	 * @param query
	 * @param update
	 * @throws Exception
	 */
	public void update(Query query, Update update) throws Exception {
		mongoTemplate.updateMulti(query, update, this.collectionName);
	}

	/**
	 * remove
	 * @param query
	 * @throws Exception
	 */
	public void remove(Query query) throws Exception {
		mongoTemplate.remove(query, this.collectionName);
	}

	/**
	 * remove
	 * @param obj
	 * @throws Exception
	 */
	public void remove(Object obj) throws Exception {
		mongoTemplate.remove(obj, this.collectionName);
	}
	
	/**
	 * multi insert
	 * @param contentInstanceList
	 * @throws Exception
	 */
	public void multi_insert(List<ContentInstanceVO> contentInstanceList) throws Exception {
		mongoTemplate.insert(contentInstanceList, collectionName);
	}

	/**
	 * retrieve redis objects by containerId
	 * @param containerId
	 * @return
	 * @throws Exception
	 */
	public List<ContentInstanceVO> getRedisObjectsByContainerId(String containerId) throws Exception {
		List<ContentInstanceVO> contentInstanceList = null;
		
		Set<String> keys = template.keys("*_" + containerId + "_*");
		if (!CommonUtil.isEmpty(keys) && keys.size() > 0) {
			List<String> keyList = new ArrayList<String>();
			Iterator<String> it = keys.iterator();
			while (it.hasNext()) {
				String key = (String) it.next();
				keyList.add(key);
			}
			contentInstanceList = this.getObjectsByKeys(keyList);
		}

		return contentInstanceList;
	}
	
	/**
	 * delete redis objects by containerId
	 * @param containerId
	 * @throws Exception
	 */
	public void deleteRedisObjectsByContainerId(String containerId) throws Exception {
		Set<String> keys = template.keys("*_" + containerId + "_*");
		if (!CommonUtil.isEmpty(keys) && keys.size() > 0) {
			List<String> keyList = new ArrayList<String>();
			Iterator<String> it = keys.iterator();
			while (it.hasNext()) {
				String key = (String) it.next();
				keyList.add(key);
			}
			this.redisDeleteObjectsByKeys(keyList);
		}
	}
	
	/**
	 * put
	 * @param contentInstance
	 * @return
	 */
	public String put(ContentInstanceVO contentInstance) {
		return this.put(contentInstance, this.collectionName);
	}

	/**
	 * put
	 * @param contentInstance
	 * @param collectionName
	 * @return
	 */
	public String put(ContentInstanceVO contentInstance, String collectionName) {
		final String time = getTime();
		final String containerId = contentInstance.getParentID();
		final String collection = collectionName;
		
		Gson gson = new Gson();
		Type ObjectType = new TypeToken<ContentInstanceVO>() {
		}.getType();

		final String json = gson.toJson(contentInstance);
		SessionCallback<String> sessionCallback = new SessionCallback<String>() {
			public String execute(RedisOperations operations) throws DataAccessException {
				
				String size = getSequence();
				
				operations.multi();

				try {
					String key = time + "." + size + "_" + containerId + "_" + collection;
					
					ListOperations<String, String> listOper = operations.opsForList();
					listOper.rightPush("timestamp", key);
					
					BoundValueOperations<String, String> valueOper = operations.boundValueOps(key);
					valueOper.set(json);
					
					operations.exec();

					return key;

				} catch (Exception e) {
					operations.discard();
					e.printStackTrace();
					return "fail";
				}
			}
		};
		return template.execute(sessionCallback);
	}
}