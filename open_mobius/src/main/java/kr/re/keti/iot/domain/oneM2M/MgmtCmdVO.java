package kr.re.keti.iot.domain.oneM2M;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import kr.re.keti.common.base.BaseVO;

/**
 * mgmtCmd domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@XmlRootElement(name="mgmtCmd", namespace="http://www.onem2m.org/xml/protocols")
@XmlAccessorType(value=XmlAccessType.FIELD)
public class MgmtCmdVO extends BaseVO {
	private static final long serialVersionUID = 1L;

	private String resourceType;
	private String resourceID;
	private String parentID;
	private String creationTime;
	private String lastModifiedTime;
	private String expirationTime;
	private String accessControlPolicyIDs;
	private String labels;
	private String description;
	private String cmdType;
	private String execReqArgs;
	private String execTarget;
	private String execMode;
	private String execFrequency;
	private String execDelay;
	private String execNumber;

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceID() {
		return resourceID;
	}

	public void setResourceID(String resourceID) {
		this.resourceID = resourceID;
	}

	public String getParentID() {
		return parentID;
	}

	public void setParentID(String parentID) {
		this.parentID = parentID;
	}

	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	public String getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(String lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public String getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getAccessControlPolicyIDs() {
		return accessControlPolicyIDs;
	}

	public void setAccessControlPolicyIDs(String accessControlPolicyIDs) {
		this.accessControlPolicyIDs = accessControlPolicyIDs;
	}

	public String getLabels() {
		return labels;
	}

	public void setLabels(String labels) {
		this.labels = labels;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCmdType() {
		return cmdType;
	}

	public void setCmdType(String cmdType) {
		this.cmdType = cmdType;
	}

	public String getExecReqArgs() {
		return execReqArgs;
	}

	public void setExecReqArgs(String execReqArgs) {
		this.execReqArgs = execReqArgs;
	}

	public String getExecTarget() {
		return execTarget;
	}

	public void setExecTarget(String execTarget) {
		this.execTarget = execTarget;
	}

	public String getExecMode() {
		return execMode;
	}

	public void setExecMode(String execMode) {
		this.execMode = execMode;
	}

	public String getExecFrequency() {
		return execFrequency;
	}

	public void setExecFrequency(String execFrequency) {
		this.execFrequency = execFrequency;
	}

	public String getExecDelay() {
		return execDelay;
	}

	public void setExecDelay(String execDelay) {
		this.execDelay = execDelay;
	}

	public String getExecNumber() {
		return execNumber;
	}

	public void setExecNumber(String execNumber) {
		this.execNumber = execNumber;
	}

	@Override
	public String toString() {
		return "MgmtCmdVO [resourceType=" + resourceType + ", resourceID="
				+ resourceID + ", parentID=" + parentID + ", creationTime="
				+ creationTime + ", lastModifiedTime=" + lastModifiedTime
				+ ", expirationTime=" + expirationTime
				+ ", accessControlPolicyIDs=" + accessControlPolicyIDs
				+ ", labels=" + labels + ", description=" + description
				+ ", cmdType=" + cmdType + ", execReqArgs=" + execReqArgs
				+ ", execTarget=" + execTarget + ", execMode=" + execMode
				+ ", execFrequency=" + execFrequency + ", execDelay="
				+ execDelay + ", execNumber=" + execNumber + "]";
	}
}
