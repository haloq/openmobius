package kr.re.keti.iot.service.oneM2M;

import java.util.ArrayList;
import java.util.List;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.ExecInstanceVO;
import kr.re.keti.iot.domain.oneM2M.GroupVO;
import kr.re.keti.iot.domain.oneM2M.MgmtCmdVO;
import kr.re.keti.iot.mdao.oneM2M.GroupDao;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * group management Service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Service
public class GroupService {

	private static final Log logger = LogFactory.getLog(GroupService.class);

	@Autowired
	private MongoLogService mongoLogService;

	@Autowired
	private GroupDao groupDao;
	
	@Autowired
	private MgmtCmdService mgmtCmdService;
	
	/**
	 * find group by resourceId
	 * @param resourceId
	 * @return
	 * @throws IotException
	 */
	public GroupVO findOneGroup(String resourceId) throws IotException {

		GroupVO findGroupItem = null;

		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(resourceId));

		try {

			findGroupItem = (GroupVO) groupDao.findOne(query);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.group.findFail.text"));
		}

		return findGroupItem;
	}
	
	/**
	 * find group by groupName
	 * @param groupName
	 * @return
	 * @throws IotException
	 */
	public GroupVO findOneGroupByName(String groupName) throws IotException {
		
		GroupVO findGroupItem = null;
		
		Query query = new Query();
		query.addCriteria(Criteria.where("groupName").is(groupName));
		
		try {
			
			findGroupItem = (GroupVO) groupDao.findOne(query);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.group.findFail.text"));
		}
		
		return findGroupItem;
	}
	
	/**
	 * find group count by resourceId
	 * @param resourceId
	 * @return
	 */
	public long getCount(String resourceId){
		
		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(resourceId));
		
		long cnt = 0;
		
		try {
			cnt = groupDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "group get count");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		return cnt;
	}
	
	/**
	 * request group control
	 * @param arrRemoteCSEId
	 * @param mgmtCmdName
	 * @param mgmtCmdProfile
	 * @return
	 * @throws IotException
	 */
	public List<ExecInstanceVO> groupControl(String[] arrRemoteCSEId, String mgmtCmdName, MgmtCmdVO mgmtCmdProfile) throws IotException {
		List<ArrayList<String[]>> findListAceess = new ArrayList<ArrayList<String[]>>();
		List<ExecInstanceVO> findListExecInstance = new ArrayList<ExecInstanceVO>();
		List<MgmtCmdVO> findListMgmtCmd = new ArrayList<MgmtCmdVO>();
		ArrayList<String[]> listPointOfAceess = null;
		ExecInstanceVO findExecInstance = null;
		MgmtCmdVO findMgmtCmdItem = null;
		
		for (String remoteCSEID : arrRemoteCSEId) {
			findMgmtCmdItem = mgmtCmdService.findOneMgmtCmdByLabels(remoteCSEID, mgmtCmdName);
			findMgmtCmdItem.setExecReqArgs(mgmtCmdProfile.getExecReqArgs());
			
			if(!CommonUtil.isEmpty(findMgmtCmdItem)) {
				listPointOfAceess = mgmtCmdService.getPointOfAccessListByRemoteCSEID(remoteCSEID);
				
				findListMgmtCmd.add(findMgmtCmdItem);
				findListAceess.add(listPointOfAceess);
			}
		}
		
		for (MgmtCmdVO mgmtCmdVO : findListMgmtCmd) {
			
			findExecInstance = mgmtCmdService.createExecInstance(mgmtCmdVO);
			
			if(!CommonUtil.isEmpty(findExecInstance)) {
				findListExecInstance.add(findExecInstance);
			}
			
		}
		
		for (int i = 0; i < findListMgmtCmd.size(); i++) {
			MgmtCmdVO mgmtCmd = findListMgmtCmd.get(i);
			
			boolean isRequestSuccess = mgmtCmdService.callDevice(mgmtCmd, findListExecInstance.get(i), findListAceess.get(i));
			
			if (isRequestSuccess) {
				
				mongoLogService.log(logger, LEVEL.DEBUG, "[groupControl] success");
			} else {
				
				mongoLogService.log(logger, LEVEL.ERROR, "[groupControl] fail");
			}
			
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "[groupControl] success");
		
		return findListExecInstance;
	}
}