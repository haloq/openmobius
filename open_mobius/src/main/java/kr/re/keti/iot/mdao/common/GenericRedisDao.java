package kr.re.keti.iot.mdao.common;

import java.util.List;

import kr.re.keti.iot.domain.common.GenericModel;

/**
 * generic redis management Dao.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
public interface GenericRedisDao<E extends GenericModel> {
	
	/**
	 * put
	 * @param obj
	 * @return
	 */
	public String put(E obj);
	
	/**
	 * put
	 * @param obj
	 * @param collectionName
	 * @return
	 */
	public String put(E obj, String collectionName);

	/**
	 * delete
	 * @param id
	 */
	public void delete(String id);

	/**
	 * getObjects
	 * @return
	 */
	public List<E> getObjects();

	/**
	 * getObjects
	 * @param offset
	 * @param count
	 * @return
	 */
	public List<E> getObjects(long offset, long count);

	/**
	 * get
	 * @param id
	 * @return
	 */
	public E get(String id);
}