package kr.re.keti.iot.service.oneM2M;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.AEVO;
import kr.re.keti.iot.domain.oneM2M.SoftwareVO;
import kr.re.keti.iot.mdao.common.SequenceDao;
import kr.re.keti.iot.mdao.common.GenericMongoDao.MOBIUS;
import kr.re.keti.iot.mdao.common.GenericMongoDao.ONEM2M;
import kr.re.keti.iot.mdao.common.SequenceDao.MovType;
import kr.re.keti.iot.mdao.common.SequenceDao.SEQ_PREFIX;
import kr.re.keti.iot.mdao.common.SequenceDao.SeqType;
import kr.re.keti.iot.mdao.oneM2M.AEDao;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.util.CommonUtil;
import kr.re.keti.iot.util.oneM2M.CommonCode.RESOURCE_TYPE;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

/**
 * AE management Service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Service
public class AEService {

	private static final Log logger = LogFactory.getLog(AEService.class);

	@Autowired
	private MongoLogService mongoLogService;

	@Autowired
	private AEDao aEDao;
	
	@Autowired
	private SequenceDao seqDao;
	
	/**
	 * generate resourceID
	 * @return
	 * @throws IotException
	 */
	private String generateResourceId() throws IotException {
		long longSeq = 0;
		StringBuffer bufSeq = new StringBuffer(SEQ_PREFIX.AE.getValue());
		
		try {
			longSeq = seqDao.move(MovType.UP, SeqType.AE);
			
			bufSeq.append(String.format("%010d", longSeq));

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.AE.id.createFail.text"));
		}
		
		return bufSeq.toString();
	}
	
	/**
	 * retrieve AE count by resourceId
	 * @param resourceId
	 * @return
	 */
	public long getCount(String resourceId){
		
		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(resourceId));
		
		long cnt = 0;
		
		try {
			cnt = aEDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "AE get count");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		return cnt;
	}

	/**
	 * find AE by key-value
	 * @param key
	 * @param value
	 * @return
	 * @throws IotException
	 */
	public AEVO findOneAE(String key, String value) throws IotException {
		AEVO findAEItem = null;

		Query query = new Query();
		query.addCriteria(Criteria.where(key).is(value));

		try {
			findAEItem = (AEVO) aEDao.findOne(query);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.AE.findFail.text"));
		}

		return findAEItem;
	}
	
	/**
	 * find AE by appId
	 * @param appId
	 * @return
	 * @throws IotException
	 */
	public AEVO findOneAE(String appId) throws IotException {
		AEVO findAEItem = null;
		
		Query query = new Query();
		query.addCriteria(Criteria.where("appId").is(appId));
		
		try {
			findAEItem = (AEVO) aEDao.findOne(query);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.AE.findFail.text"));
		}
		
		return findAEItem;
	}
	
	/**
	 * find AE by remoteCSEID-appId
	 * @param remoteCSEID
	 * @param appId
	 * @return
	 * @throws IotException
	 */
	public AEVO findOneAEByRemotCSEID(String remoteCSEID, String appId) throws IotException {
		AEVO findAEItem = null;
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(remoteCSEID));
		query.addCriteria(Criteria.where("appId").is(appId));
		
		try {
			findAEItem = (AEVO) aEDao.findOne(query);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.AE.findFail.text"));
		}
		
		return findAEItem;
	}
	
	/**
	 * create AE
	 * @param aeProfile
	 * @return
	 * @throws IotException
	 */
	public AEVO createAE(AEVO aeProfile) throws IotException {
		
		String appID	= aeProfile.getAppId();
		String parentID	= aeProfile.getParentID();
		
		boolean isError = false;
		String errMsg = "";
		
		if(CommonUtil.isEmpty(appID)) {
			isError = true;
			errMsg = "appID " + CommonUtil.getMessage("msg.input.empty.text");
		}
		else if(CommonUtil.isEmpty(parentID)) {
			isError = true;
			errMsg = "parentID " + CommonUtil.getMessage("msg.input.empty.text");
		}
		
		if(isError){
			mongoLogService.log(logger, LEVEL.ERROR, errMsg);
			throw new IotException("600", errMsg);
		}
		
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		if(parentID.equals(MOBIUS.NAME.getName())) {
			aeProfile.setAeId(this.generateResourceId());
		}
		
		aeProfile.setResourceType(RESOURCE_TYPE.AE.getName());
		aeProfile.setCreationTime(currentTime);
		aeProfile.setLastModifiedTime(currentTime);
		
		try {
			aEDao.insert(aeProfile);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.AE.createFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.INFO, "AE create success");

		return this.findOneAE("appId", aeProfile.getAppId());
	}
	
	/**
	 * create AE
	 * @param softwareInfo
	 * @return
	 * @throws IotException
	 */
	public AEVO createAE(SoftwareVO softwareInfo) throws IotException {
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		AEVO aEItem = new AEVO();
		
		aEItem.setResourceType(ONEM2M.AE.getName());
		aEItem.setParentID(softwareInfo.getResourceID());
		aEItem.setAeId(softwareInfo.getResourceID());
		aEItem.setAppId(softwareInfo.getResourceID());
		aEItem.setAccessControlPolicyIDs(softwareInfo.getAccessControlPolicyIDs());
		aEItem.setLabels(softwareInfo.getLabels());
		aEItem.setName(softwareInfo.getName());		
		aEItem.setCreationTime(currentTime);
		aEItem.setLastModifiedTime(currentTime);
		try {
			aEDao.insert(aEItem);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.AE.createFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "AE create success");
		
		return this.findOneAE("parentID", aEItem.getParentID());
	}

	/**
	 * delete AE by key-value
	 * @param key
	 * @param value
	 * @throws IotException
	 */
	public void deleteAE(String key, String value) throws IotException {

		Query query = new Query();
		query.addCriteria(Criteria.where(key).is(value));
		try {

			aEDao.remove(query);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.AE.delFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "AE delete success");

	}
	
	/**
	 * delete AE by remoteCSEID-appId
	 * @param remoteCSEID
	 * @param appId
	 * @throws IotException
	 */
	public void deleteAEByRemoteCSEID(String remoteCSEID, String appId) throws IotException {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(remoteCSEID));
		query.addCriteria(Criteria.where("appId").is(appId));
		
		try {
			
			aEDao.remove(query);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.AE.delFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "AE delete success");
		
	}
	
	/**
	 * update aKey
	 * @param aeItem
	 * @return
	 * @throws IotException
	 */
	public AEVO updateAEAkey(AEVO aeItem) throws IotException {
		
		String appId = aeItem.getAppId();
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");

		Query query = new Query();
		query.addCriteria(Criteria.where("appId").is(appId));
		
		Update update = new Update();
		update.set("lastModifiedTime", currentTime);
		update.set("aKey", aeItem.getaKey());
		
		try { 
			aEDao.update(query, update);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.AE.upFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "AE update success");
		
		return this.findOneAE("appId", appId);

	}
	
	/**
	 * update AE
	 * @param aeProfile
	 * @param orgAEItem
	 * @return
	 * @throws IotException
	 */
	public AEVO updateAE(AEVO aeProfile, AEVO orgAEItem) throws IotException {
		
		String appId		= orgAEItem.getAppId();
		String parentID		= orgAEItem.getParentID();
		String currentTime	= CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		Query query = new Query();
		query.addCriteria(Criteria.where("appId").is(appId));
		query.addCriteria(Criteria.where("parentID").is(parentID));
		
		String name				= aeProfile.getName();
		String pointOfAccess	= aeProfile.getPointOfAccess();
		String nodeLink			= aeProfile.getNodeLink();
		String aeId				= aeProfile.getAeId();
		
		Update update = new Update();
		if(!CommonUtil.isNull(name))			update.set("labels", name);
		if(!CommonUtil.isNull(name))			update.set("name", name);
		if(!CommonUtil.isNull(pointOfAccess))	update.set("pointOfAccess", pointOfAccess);
		if(!CommonUtil.isNull(nodeLink))		update.set("nodeLink", nodeLink);
												update.set("lastModifiedTime", currentTime);
		
		if(!CommonUtil.isEmpty(parentID) && !parentID.equals(MOBIUS.NAME.getName())) {
			if(!CommonUtil.isNull(aeId)) update.set("aeId", aeId);
		}
		
		try { 
			aEDao.update(query, update);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.AE.upFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "AE update success");
		
		return this.findOneAE("appId", appId);
		
	}
	
	/**
	 * update AE
	 * @param softwareItem
	 * @return
	 * @throws IotException
	 */
	public AEVO updateAE(SoftwareVO softwareItem) throws IotException {
		String currentTime	= CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		AEVO aEItem = this.findOneAE("parentID", softwareItem.getResourceID());
		if(CommonUtil.isEmpty(aEItem)) {
			throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text"));	
		}
		
		String labels					= softwareItem.getLabels();
		String name						= softwareItem.getName();
		
		Update update = new Update();
		if(!CommonUtil.isNull(labels))						update.set("labels", labels);
		if(!CommonUtil.isNull(name))						update.set("name", name);
															update.set("lastModifiedTime", currentTime);
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(softwareItem.getResourceID()));
		try { 
			aEDao.update(query, update);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.AE.upFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "AE update success");
		
		return this.findOneAE("parentID", aEItem.getParentID());
		
	}

}