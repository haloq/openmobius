package kr.re.keti.iot.controller.oneM2M;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.AEVO;
import kr.re.keti.iot.domain.oneM2M.LocationPolicyVO;
import kr.re.keti.iot.domain.oneM2M.RemoteCSEVO;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.AEService;
import kr.re.keti.iot.service.oneM2M.ContainerService;
import kr.re.keti.iot.service.oneM2M.LocationPolicyService;
import kr.re.keti.iot.service.oneM2M.RemoteCSEService;
import kr.re.keti.iot.service.oneM2M.common.MCommonService;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * locationPolicy management Action.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>khlee</li>
 *         </ul>
 */
@Controller
public class LocationPolicyAction {

	private static final Log logger = LogFactory.getLog(LocationPolicyAction.class);

	@Autowired
	private MongoLogService mongoLogService;

	@Autowired
	private MCommonService mCommonService;
	
	@Autowired
	private RemoteCSEService remoteCSEService;
	
	@Autowired
	private AEService aEService;
	
	@Autowired
	private ContainerService containerService;

	@Autowired
	private LocationPolicyService locationPolicyService;
	
	/**
	 * create locationPolicy
	 * @param labels
	 * @param locationPolicyProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius", method = RequestMethod.POST, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" }, params = "ty=locationPolicy")
	@ResponseBody
	public ResponseEntity<Object> locationPolicyCreate(@RequestParam(value = "nm", required = false) String labels
													 , @RequestBody LocationPolicyVO locationPolicyProfile
													 , HttpServletRequest request
													 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		LocationPolicyVO responseVo = new LocationPolicyVO();
		HttpStatus httpStatus = HttpStatus.CREATED;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dkey 	= CommonUtil.nvl(request.getHeader("dKey"), "");
		String akey 	= CommonUtil.nvl(request.getHeader("aKey"), "");
		String locationSource     	= locationPolicyProfile.getLocationSource();
		String locationTargetId     = locationPolicyProfile.getLocationTargetId();
		String locationContainerId  = locationPolicyProfile.getLocationContainerID();
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(locationPolicyProfile)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<locationPolicy> " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(locationSource)) {
			isError = true;
			responseCode = "600";
			responseMsg = "<locationPolicy> locationSource " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(locationTargetId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "<locationPolicy> locationTargetId " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(locationContainerId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "<locationPolicy> locationContainerId " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus.OK);
		}
		
		try {
			 
			mCommonService.checkPatternDeviceId(checkMccP, locationTargetId);
			
			RemoteCSEVO findRemoteCSEItem = remoteCSEService.findOneRemoteCSE(locationTargetId);
			AEVO 		findAEItem 		  = aEService.findOneAE("appId", locationTargetId);
			if (!CommonUtil.isEmpty(findRemoteCSEItem)) {
				mCommonService.verifyDKey(checkMccP, locationTargetId, dkey);
				
			} else if (!CommonUtil.isEmpty(findAEItem)) {
				findRemoteCSEItem = remoteCSEService.findOneRemoteCSE(findAEItem.getParentID());
				if (!CommonUtil.isEmpty(findRemoteCSEItem)) {
					mCommonService.verifyDKey(checkMccP, findRemoteCSEItem.getResourceID(), dkey);
					
				} else {
					mCommonService.verifyAKey(checkMccP, findAEItem.getAppId(), akey);	
				}
				
			} else {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			if (CommonUtil.isEmpty(containerService.findOneContainer(locationTargetId, locationPolicyProfile.getLocationContainerID()))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			LocationPolicyVO findLocationPolicyItem = null;
			if (!CommonUtil.isEmpty((findLocationPolicyItem = locationPolicyService.findOneLocationPolicyByLabels(locationTargetId, labels)))) {
				//responseVo.setLocationPolicy(findLocationPolicyItem);
				BeanUtils.copyProperties(findLocationPolicyItem, responseVo);
				throw new IotException("609", CommonUtil.getMessage("msg.device.locationPolicy.duplicate.text", locale));
			}
			
			locationPolicyProfile.setLabels(labels);
			findLocationPolicyItem = locationPolicyService.createLocationPolicy(locationPolicyProfile);
			//responseVo.setLocationPolicy(findLocationPolicyItem);
			BeanUtils.copyProperties(findLocationPolicyItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);

		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * delete locationPolicy
	 * @param locationPolicyID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/locationPolicy-{locationPolicyID}", method = RequestMethod.DELETE, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Void> locationPolicyDelete(@PathVariable String locationPolicyID
													 , HttpServletRequest request
													 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		Object responseVo = new Object();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dkey 	= CommonUtil.nvl(request.getHeader("dKey"), "");
		String akey 	= CommonUtil.nvl(request.getHeader("aKey"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(locationPolicyID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "locationPolicyID" + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Void>(httpStatus);
		}

		try {
			
			LocationPolicyVO findLocationPolicyItem = null;
			if (CommonUtil.isEmpty((findLocationPolicyItem = locationPolicyService.findOneLocationPolicy("resourceID", locationPolicyID)))) {
				throw new IotException("634", CommonUtil.getMessage("msg.device.locationPolicy.noRegi.text", locale));
			}
			
			String locationTargetId       = findLocationPolicyItem.getLocationTargetId();
			RemoteCSEVO findRemoteCSEItem = remoteCSEService.findOneRemoteCSE(locationTargetId);
			AEVO 		findAEItem 		  = aEService.findOneAE("appId", locationTargetId);
			if (!CommonUtil.isEmpty(findRemoteCSEItem)) {
				mCommonService.verifyDKey(checkMccP, locationTargetId, dkey);
				
			} else if (!CommonUtil.isEmpty(findAEItem)) {
				findRemoteCSEItem = remoteCSEService.findOneRemoteCSE(findAEItem.getParentID());
				if (!CommonUtil.isEmpty(findRemoteCSEItem)) {
					mCommonService.verifyDKey(checkMccP, findRemoteCSEItem.getResourceID(), dkey);
					
				} else {
					mCommonService.verifyAKey(checkMccP, findAEItem.getAppId(), akey);
				}
				
			} else {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			locationPolicyService.deleteLocationPolicy("resourceID", findLocationPolicyItem.getResourceID());

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Void>(httpStatus);
	}
	
	/**
	 * update locationPolicy
	 * @param locationPolicyID
	 * @param locationPolicyProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/locationPolicy-{locationPolicyID}", method = RequestMethod.PUT, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> locationPolicyUpdate(@PathVariable String locationPolicyID
													 , @RequestBody LocationPolicyVO locationPolicyProfile
													 , HttpServletRequest request
													 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		LocationPolicyVO responseVo = new LocationPolicyVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dkey 	= CommonUtil.nvl(request.getHeader("dKey"), "");
		String akey 	= CommonUtil.nvl(request.getHeader("aKey"), "");
		String locationTargetId     = locationPolicyProfile.getLocationTargetId();
		String locationContainerId  = locationPolicyProfile.getLocationContainerID();
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(locationPolicyID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "locationPolicyID" + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(locationPolicyProfile)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<locationPolicy> " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(locationTargetId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "<locationPolicy> locationTargetId " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(locationContainerId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "<locationPolicy> locationContainerId " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}

		try {
			 
			mCommonService.checkPatternDeviceId(checkMccP, locationTargetId);
			
			LocationPolicyVO findLocationPolicyItem = null;
			if (CommonUtil.isEmpty((findLocationPolicyItem = locationPolicyService.findOneLocationPolicy("resourceID", locationPolicyID)))) {
				throw new IotException("634", CommonUtil.getMessage("msg.device.locationPolicy.noRegi.text", locale));
			}
			
			RemoteCSEVO findRemoteCSEItem = remoteCSEService.findOneRemoteCSE(locationTargetId);
			AEVO 		findAEItem 		  = aEService.findOneAE("appId", locationTargetId);
			if (!CommonUtil.isEmpty(findRemoteCSEItem)) {
				mCommonService.verifyDKey(checkMccP, locationTargetId, dkey);
				
			} else if (!CommonUtil.isEmpty(findAEItem)) {
				findRemoteCSEItem = remoteCSEService.findOneRemoteCSE(findAEItem.getParentID());
				if (!CommonUtil.isEmpty(findRemoteCSEItem)) {
					mCommonService.verifyDKey(checkMccP, findRemoteCSEItem.getResourceID(), dkey);
					
				} else {
					mCommonService.verifyAKey(checkMccP, findAEItem.getAppId(), akey);	
				}
				
			} else {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			if (CommonUtil.isEmpty(containerService.findOneContainer(locationPolicyProfile.getLocationTargetId(), locationPolicyProfile.getLocationContainerID()))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			locationPolicyProfile.setResourceID(locationPolicyID);
			findLocationPolicyItem = locationPolicyService.updateLocationPolicy(locationPolicyProfile);
			//responseVo.setLocationPolicy(findLocationPolicyItem);
			BeanUtils.copyProperties(findLocationPolicyItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * retrieve locationPolicy
	 * @param locationPolicyID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/locationPolicy-{locationPolicyID}", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> locationPolicyRetrieve(@PathVariable String locationPolicyID
													   , HttpServletRequest request
													   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		LocationPolicyVO responseVo = new LocationPolicyVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey		= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic 	= CommonUtil.nvl(request.getHeader("topic"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(locationPolicyID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "locationPolicyID" + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}

		try {
			
			LocationPolicyVO findLocationPolicyItem = null;
			if (CommonUtil.isEmpty((findLocationPolicyItem = locationPolicyService.findOneLocationPolicy("resourceID", locationPolicyID)))) {
				throw new IotException("634", CommonUtil.getMessage("msg.device.locationPolicy.noRegi.text", locale));
			}
			
			String locationTargetId			= findLocationPolicyItem.getLocationTargetId();
			RemoteCSEVO findRemoteCSEItem	= remoteCSEService.findOneRemoteCSE(locationTargetId);
			AEVO findAEItem					= aEService.findOneAE("appId", locationTargetId);
			
			String remoteCSEID = "";
			if (!CommonUtil.isEmpty(findRemoteCSEItem)) {
				
				remoteCSEID = findRemoteCSEItem.getResourceID();
				
			} else if (!CommonUtil.isEmpty(findAEItem)) {
				
				findRemoteCSEItem = remoteCSEService.findOneRemoteCSE(findAEItem.getParentID());
				if (!CommonUtil.isEmpty(findRemoteCSEItem)) {
					remoteCSEID = findRemoteCSEItem.getResourceID();
				}
				
			} else {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			//responseVo.setLocationPolicy(findLocationPolicyItem);
			BeanUtils.copyProperties(findLocationPolicyItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}

}