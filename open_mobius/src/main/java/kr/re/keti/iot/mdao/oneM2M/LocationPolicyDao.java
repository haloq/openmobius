package kr.re.keti.iot.mdao.oneM2M;

import kr.re.keti.iot.domain.oneM2M.LocationPolicyVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao;

import org.springframework.stereotype.Repository;

/**
 * locationPolicy management Dao.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Repository
public class LocationPolicyDao extends GenericMongoDao {
	
	public LocationPolicyDao() {
		collectionName = ONEM2M.LOCATION_POLICY.getName();
		cls = LocationPolicyVO.class;
	}
}
