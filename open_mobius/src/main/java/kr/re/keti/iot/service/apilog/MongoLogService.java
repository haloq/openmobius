package kr.re.keti.iot.service.apilog;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ntels.nisf.util.PropertiesUtil;

import kr.re.keti.iot.domain.mongo.LogVO;
import kr.re.keti.iot.mdao.common.SequenceDao;
import kr.re.keti.iot.mdao.common.SequenceDao.MovType;
import kr.re.keti.iot.mdao.common.SequenceDao.SeqType;
import kr.re.keti.iot.mdao.log.LogDao;
import kr.re.keti.iot.util.CommonUtil;


/**
 * log management Service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Service
public class MongoLogService {
	private static final Log logger = LogFactory.getLog(MongoLogService.class);
	private String targetServer = PropertiesUtil.get("config","mlog.target_server.platform");
	
	public enum LEVEL{INFO,	DEBUG, WARN, ERROR}
	
	@Autowired
	private LogDao logDao;
	
	@Autowired
	private SequenceDao seqDao;
	
	/**
	 * call log
	 * @param logger
	 * @param level
	 * @param message
	 */
	public void log(Log logger, LEVEL level, String message) {
		
		if(level.equals(LEVEL.ERROR)) {
			long logKeySeq = seqDao.move(MovType.UP,SeqType.MMP_LOG_MSG_TBL);
			String logKey = Long.toString(logKeySeq);
			
			LogVO log = new LogVO();
			log.setLog_key(logKey);
			log.setTarget_server(targetServer);
			log.setLevel(level.name());
			log.setMessage(message);
			log.setReg_dt(CommonUtil.getNow("yyyyMMddHHmmssSSS"));
			
			try {
				logDao.insert(log);
			} catch (Exception e) {
				MongoLogService.logger.debug(e.getMessage());
			}
			logger.error(message);
		} else if (level.equals(LEVEL.WARN)) {
			logger.warn(message);
		} else if (level.equals(LEVEL.DEBUG)) {
			logger.debug(message);
		} else if (level.equals(LEVEL.INFO)) {
			logger.info(message);
		} 
	}
}