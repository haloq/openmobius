package kr.re.keti.iot.domain.oneM2M;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * commonMApi domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@XmlRootElement(name="m2m:mobius")
@XmlAccessorType(value = XmlAccessType.FIELD)
public class CommonMApiVO implements Serializable {

	private static final long serialVersionUID = 1l;

	private String resultCode;
	private String resultMsg;
	private String accessToken;
	private String totalListCount;
	private String countPerPage;
	private String startIndex;
	private List<String> deviceIdList;
	
	@javax.xml.bind.annotation.XmlElement(name = "CSEBase")
	private CSEBaseVO cseBase;
	
	@javax.xml.bind.annotation.XmlElement(name = "AE")
	private AEVO ae;
	
	@javax.xml.bind.annotation.XmlElement(name = "remoteCSE")
	private RemoteCSEVO remoteCSE;

	@javax.xml.bind.annotation.XmlElement(name = "container")
	private ContainerVO container;

	@javax.xml.bind.annotation.XmlElement(name = "contentInstance")
	private ContentInstanceVO contentInstance;
	
	@XmlElementWrapper(name="contentInstanceList")
	@javax.xml.bind.annotation.XmlElement(name = "contentInstance")
	private List<ContentInstanceVO> contentInstanceList;

	@javax.xml.bind.annotation.XmlElement(name = "mgmtCmd")
	private MgmtCmdVO mgmtCmd;

	@javax.xml.bind.annotation.XmlElement(name = "execInstance")
	private ExecInstanceVO execInstance;
	
	@javax.xml.bind.annotation.XmlElement(name = "mgmtObj")
	private MgmtObjVO mgmtObj;
	
	@javax.xml.bind.annotation.XmlElement(name = "deviceInfo")
	private DeviceInfoVO deviceInfo;
	
	@javax.xml.bind.annotation.XmlElement(name = "firmware")
	private FirmwareVO firmware;
	
	@javax.xml.bind.annotation.XmlElement(name = "group")
	private GroupVO group;
	
	@javax.xml.bind.annotation.XmlElement(name = "locationPolicy")
	private LocationPolicyVO locationPolicy;
	
	@javax.xml.bind.annotation.XmlElement(name = "node")
	private NodeVO node;
	
	@javax.xml.bind.annotation.XmlElement(name = "software")
	private SoftwareVO software;
	
	@javax.xml.bind.annotation.XmlElement(name = "subscription")
	private SubscriptionVO subscription;
	
	@javax.xml.bind.annotation.XmlElementWrapper(name = "mashupList")
	@javax.xml.bind.annotation.XmlElement(name = "mashup")
	private List<MashupVO> mashupList; 
	
	@XmlElementWrapper(name="containerList")
	@javax.xml.bind.annotation.XmlElement(name = "container")
	private List<ContainerOnlyIdVO> containerList;

	@XmlElementWrapper(name="mgmtCmdList")
	@javax.xml.bind.annotation.XmlElement(name = "mgmtCmd")
	private List<MgmtCmdOnlyIdVO> mgmtCmdList;
	
	@XmlElementWrapper(name="execInstanceList")
	@javax.xml.bind.annotation.XmlElement(name = "execInstance")
	private List<ExecInstanceVO> execInstanceList;

	public String getResultCode() {
		return resultCode;
	}

	public void setResultCode(String resultCode) {
		this.resultCode = resultCode;
	}

	public String getResultMsg() {
		return resultMsg;
	}

	public void setResultMsg(String resultMsg) {
		this.resultMsg = resultMsg;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getTotalListCount() {
		return totalListCount;
	}

	public void setTotalListCount(String totalListCount) {
		this.totalListCount = totalListCount;
	}

	public String getCountPerPage() {
		return countPerPage;
	}

	public void setCountPerPage(String countPerPage) {
		this.countPerPage = countPerPage;
	}

	public String getStartIndex() {
		return startIndex;
	}

	public void setStartIndex(String startIndex) {
		this.startIndex = startIndex;
	}

	public List<String> getDeviceIdList() {
		return deviceIdList;
	}

	public void setDeviceIdList(List<String> deviceIdList) {
		this.deviceIdList = deviceIdList;
	}

	public CSEBaseVO getCseBase() {
		return cseBase;
	}

	public void setCseBase(CSEBaseVO cseBase) {
		this.cseBase = cseBase;
	}

	public AEVO getAe() {
		return ae;
	}

	public void setAe(AEVO ae) {
		this.ae = ae;
	}

	public RemoteCSEVO getRemoteCSE() {
		return remoteCSE;
	}

	public void setRemoteCSE(RemoteCSEVO remoteCSE) {
		this.remoteCSE = remoteCSE;
	}

	public ContainerVO getContainer() {
		return container;
	}

	public void setContainer(ContainerVO container) {
		this.container = container;
	}

	public ContentInstanceVO getContentInstance() {
		return contentInstance;
	}

	public void setContentInstance(ContentInstanceVO contentInstance) {
		this.contentInstance = contentInstance;
	}

	public List<ContentInstanceVO> getContentInstanceList() {
		return contentInstanceList;
	}

	public void setContentInstanceList(List<ContentInstanceVO> contentInstanceList) {
		this.contentInstanceList = contentInstanceList;
	}

	public MgmtCmdVO getMgmtCmd() {
		return mgmtCmd;
	}

	public void setMgmtCmd(MgmtCmdVO mgmtCmd) {
		this.mgmtCmd = mgmtCmd;
	}

	public ExecInstanceVO getExecInstance() {
		return execInstance;
	}

	public void setExecInstance(ExecInstanceVO execInstance) {
		this.execInstance = execInstance;
	}

	public MgmtObjVO getMgmtObj() {
		return mgmtObj;
	}

	public void setMgmtObj(MgmtObjVO mgmtObj) {
		this.mgmtObj = mgmtObj;
	}

	public DeviceInfoVO getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(DeviceInfoVO deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public FirmwareVO getFirmware() {
		return firmware;
	}

	public void setFirmware(FirmwareVO firmware) {
		this.firmware = firmware;
	}

	public GroupVO getGroup() {
		return group;
	}

	public void setGroup(GroupVO group) {
		this.group = group;
	}

	public LocationPolicyVO getLocationPolicy() {
		return locationPolicy;
	}

	public void setLocationPolicy(LocationPolicyVO locationPolicy) {
		this.locationPolicy = locationPolicy;
	}

	public NodeVO getNode() {
		return node;
	}

	public void setNode(NodeVO node) {
		this.node = node;
	}

	public SoftwareVO getSoftware() {
		return software;
	}

	public void setSoftware(SoftwareVO software) {
		this.software = software;
	}

	public SubscriptionVO getSubscription() {
		return subscription;
	}

	public void setSubscription(SubscriptionVO subscription) {
		this.subscription = subscription;
	}

	public List<MashupVO> getMashupList() {
		return mashupList;
	}

	public void setMashupList(List<MashupVO> mashupList) {
		this.mashupList = mashupList;
	}

	public List<ContainerOnlyIdVO> getContainerList() {
		return containerList;
	}

	public void setContainerList(List<ContainerOnlyIdVO> containerList) {
		this.containerList = containerList;
	}

	public List<MgmtCmdOnlyIdVO> getMgmtCmdList() {
		return mgmtCmdList;
	}

	public void setMgmtCmdList(List<MgmtCmdOnlyIdVO> mgmtCmdList) {
		this.mgmtCmdList = mgmtCmdList;
	}

	public List<ExecInstanceVO> getExecInstanceList() {
		return execInstanceList;
	}

	public void setExecInstanceList(List<ExecInstanceVO> execInstanceList) {
		this.execInstanceList = execInstanceList;
	}

	@Override
	public String toString() {
		return "CommonMApiVO [resultCode=" + resultCode + ", resultMsg=" + resultMsg + ", accessToken=" + accessToken + ", totalListCount=" + totalListCount + ", countPerPage=" + countPerPage + ", startIndex=" + startIndex + ", deviceIdList=" + deviceIdList + ", cseBase=" + cseBase + ", ae=" + ae + ", remoteCSE=" + remoteCSE + ", container=" + container
				+ ", contentInstance=" + contentInstance + ", contentInstanceList=" + contentInstanceList + ", mgmtCmd=" + mgmtCmd + ", execInstance=" + execInstance + ", mgmtObj=" + mgmtObj + ", deviceInfo=" + deviceInfo + ", firmware=" + firmware + ", group=" + group + ", locationPolicy=" + locationPolicy + ", node=" + node + ", software=" + software
				+ ", subscription=" + subscription + ", mashupList=" + mashupList + ", containerList=" + containerList + ", mgmtCmdList=" + mgmtCmdList + ", execInstanceList=" + execInstanceList + "]";
	}
	
}