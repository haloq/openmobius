package kr.re.keti.iot.service.oneM2M;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.ContentInstanceVO;
import kr.re.keti.iot.mdao.common.SequenceDao;
import kr.re.keti.iot.mdao.common.SequenceDao.MovType;
import kr.re.keti.iot.mdao.common.SequenceDao.SEQ_PREFIX;
import kr.re.keti.iot.mdao.common.SequenceDao.SeqType;
import kr.re.keti.iot.mdao.oneM2M.ContentInstanceDao;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.SubscriptionService.SUBSCRIPTION_RESOURCE_STATUS_TYPE;
import kr.re.keti.iot.util.CommonUtil;
import kr.re.keti.iot.util.oneM2M.CommonCode.RESOURCE_TYPE;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * contentInstance management Service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>khlee</li>
 *         </ul>
 */
@Service
public class ContentInstanceService {

	private static final Log logger = LogFactory.getLog(ContentInstanceService.class);
	
	@Autowired
	private MongoLogService mongoLogService;

	@Autowired
	private ContainerService containerService;
	
	@Autowired
	private SubscriptionService subscriptionService;

	@Autowired
	private SequenceDao seqDao;

	@Autowired
	private ContentInstanceDao contentInstanceDao;
	
	/**
	 * find contentInstance by containerId-contentInstanceId
	 * @param containerId
	 * @param contentInstanceId
	 * @return
	 * @throws IotException
	 */
	public ContentInstanceVO findOneContentInstance(String containerId, String contentInstanceId) throws IotException {
		ContentInstanceVO findContentInstanceItem = null;

		try {
/*			
			List<ContentInstanceVO> contentInstanceList = this.getRedisObjectsByContainerId(containerId);
			if(!CommonUtil.isEmpty(contentInstanceList) && contentInstanceList.size() > 0) {
				for(ContentInstanceVO contentInstanceItem : contentInstanceList) {
					if(contentInstanceItem.getResourceID().equals(contentInstanceId)) {
						findContentInstanceItem = contentInstanceItem;
						break;
					}
				}
			}
*/			
//			if(CommonUtil.isEmpty(findContentInstanceItem)) {
				Query query = new Query();
				query.addCriteria(Criteria.where("parentID").is(containerId));
				query.addCriteria(Criteria.where("resourceID").is(contentInstanceId));
				findContentInstanceItem = (ContentInstanceVO) contentInstanceDao.findOne(query);
//			}
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.contentInstance.findFail.text"));
		}

		return findContentInstanceItem;
	}
	
	/**
	 * retrieve contentInstance count by key-value
	 * @param key
	 * @param value
	 * @return
	 * @throws IotException
	 */
	public Long findCountContentInstance(String key, String value) throws IotException {
		Long totalCount = 0l;

		Query query = new Query();
		query.addCriteria(Criteria.where(key).is(value));

		try {
			totalCount = contentInstanceDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.contentInstance.findFail.text"));
		}

		return totalCount;
	}
	
	/**
	 * find contentInstance by query
	 * @param query
	 * @return
	 * @throws IotException
	 */
	public List<ContentInstanceVO> findContentInstance(Query query) throws IotException {
		List<ContentInstanceVO> findContentInstanceList = null;
		try {
			findContentInstanceList = (List<ContentInstanceVO>) contentInstanceDao.find(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.contentInstance.findFail.text"));
		}

		return findContentInstanceList;
	}
	
	/**
	 * create contentInstance
	 * @param contentInstanceProfile
	 * @return
	 * @throws IotException
	 */
	public ContentInstanceVO createContentInstance(ContentInstanceVO contentInstanceProfile) throws IotException {
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
/*
		ContainerVO findContainerVO = containerService.findOneContainer(contentInstanceProfile.getParentID());
		if (CommonUtil.isEmpty(findContainerVO)) {
			throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text"));
		}
*/
		
		ContentInstanceVO contentInstanceItem = new ContentInstanceVO();
		try {
			Long contentInstanceId = seqDao.move(MovType.UP, SeqType.CONTENT_INSTANCE);
			contentInstanceItem.setResourceID(SEQ_PREFIX.CONTENT_INSTANCE.getValue() + String.format("%017d", contentInstanceId));

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.contentinstance.createFail.text"));
		}

		try {
			contentInstanceItem.setParentID(contentInstanceProfile.getParentID());
			contentInstanceItem.setResourceType(RESOURCE_TYPE.CONTENT_INSTANCE.getName());
			contentInstanceItem.setLabels(contentInstanceProfile.getLabels());
			contentInstanceItem.setTypeOfContent(contentInstanceProfile.getTypeOfContent());
			contentInstanceItem.setContent(contentInstanceProfile.getContent());
			contentInstanceItem.setContentSize(String.valueOf(contentInstanceProfile.getContent().getBytes("UTF-8").length));
			contentInstanceItem.setLinkType(contentInstanceProfile.getLinkType());
			contentInstanceItem.setStateTag("0");
			contentInstanceItem.setCreationTime(currentTime);
			contentInstanceItem.setLastModifiedTime(currentTime);
		} catch (UnsupportedEncodingException e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.contentinstance.createFail.text"));
		}
		try {
			contentInstanceDao.put(contentInstanceItem);
			//contentInstanceDao.insert(contentInstanceItem);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.contentinstance.createFail.text"));
		}
		
		subscriptionService.sendSubscription(contentInstanceItem.getParentID(), contentInstanceItem.getResourceID(), SUBSCRIPTION_RESOURCE_STATUS_TYPE.CHILD_CREATED, ContentInstanceVO.class, contentInstanceItem);
/*
		try {
			containerService.updateContainerCurrentInstanceValue(findContainerVO);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.container.upFail.text"));
		}

*/
		mongoLogService.log(logger, LEVEL.INFO, "contentInstance create success");

		return contentInstanceItem;
	}

	/**
	 * delete contentInstance by containerId
	 * @param containerId
	 * @throws IotException
	 */
	public void deleteContentInstanceByContainerId(String containerId) throws IotException {

		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(containerId));
		
		try {
			contentInstanceDao.remove(query);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.contentinstance.delFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.INFO, "contentInstance delete success");

	}

	/**
	 * delete contentInstance by listContainerId
	 * @param listContainerId
	 * @throws IotException
	 */
	public void deleteContentInstanceByContainerIds(ArrayList<String> listContainerId) throws IotException {

		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").in(listContainerId));

		try {
			contentInstanceDao.remove(query);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.contentinstance.delFail.text"));
		}

		mongoLogService.log(logger, LEVEL.INFO, "container inner contentInstance delete success");

	}

	/**
	 * delete contentInstance by containerId-resourceID
	 * @param containerId
	 * @param contentInstanceInfo
	 * @throws IotException
	 */
	public void deleteContentInstance(String containerId, ContentInstanceVO contentInstanceInfo) throws IotException {
/*
		ContainerVO findContainerVO = containerService.findOneContainer(containerId);
		if (CommonUtil.isEmpty(findContainerVO)) {
			throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text"));
		}
		
		ContentInstanceVO findContentInstanceItem = null;
		if (CommonUtil.isEmpty((findContentInstanceItem = this.findOneContentInstance(containerId, contentInstanceId)))) {
			throw new IotException("629", CommonUtil.getMessage("msg.contentinstance.noRegi.text"));
		}
*/
		
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where("parentID").is(containerId));
			query.addCriteria(Criteria.where("resourceID").is(contentInstanceInfo.getResourceID()));
			contentInstanceDao.remove(query);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.contentinstance.delFail.text"));
		}
		
		subscriptionService.sendSubscription(containerId, contentInstanceInfo.getResourceID(), SUBSCRIPTION_RESOURCE_STATUS_TYPE.CHILD_DELETED, ContentInstanceVO.class, contentInstanceInfo);
		
		try {
			containerService.updateContainerCurrentInstanceValue(containerId);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.container.upFail.text"));
		}

		mongoLogService.log(logger, LEVEL.INFO, "contentInstance delete success");

	}

	/**
	 * find contentInstance by containerId
	 * @param containerId
	 * @return
	 * @throws Exception
	 */
	public List<ContentInstanceVO> findContentInstance(String containerId) throws Exception {
		List<ContentInstanceVO> contentInstanceLIst = null;

		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(containerId));
		//query.with(new Sort(Sort.Direction.ASC, "creationTime"));
		query.with(new Sort(Sort.Direction.ASC, "resourceID"));
		try {
			contentInstanceLIst = (List<ContentInstanceVO>) contentInstanceDao.find(query);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.contentInstance.findFail.text"));
		}

		return contentInstanceLIst;
	}

	/**
	 * find redisObjects by containerId
	 * @param containerId
	 * @return
	 * @throws Exception
	 */
	public List<ContentInstanceVO> getRedisObjectsByContainerId(String containerId) throws Exception {
		List<ContentInstanceVO> findContentInstanceList = null;
		try {

			findContentInstanceList = contentInstanceDao.getRedisObjectsByContainerId(containerId);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.container.upFail.text"));
		}
		return findContentInstanceList;
	}

	/**
	 * delete redisObjects by containerId
	 * @param containerId
	 * @throws Exception
	 */
	public void deleteRedisObjectsByContainerId(String containerId) throws Exception {
		try {

			contentInstanceDao.deleteRedisObjectsByContainerId(containerId);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.contentinstance.delFail.text"));
		}
	}

}