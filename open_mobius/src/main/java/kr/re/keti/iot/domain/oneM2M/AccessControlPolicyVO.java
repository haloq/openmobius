package kr.re.keti.iot.domain.oneM2M;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import kr.re.keti.common.base.BaseVO;

/**
 * accessControlPolicy domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@XmlRootElement(name="accessControlPolicy", namespace="http://www.onem2m.org/xml/protocols")
@XmlAccessorType(value=XmlAccessType.FIELD)
public class AccessControlPolicyVO extends BaseVO {
	private static final long serialVersionUID = 1L;

	private String resourceType;
	private String resourceID;
	private String parentID;
	private String creationTime;
	private String lastModifiedTime;
	private String expirationTime;
	private String labels;
	private PrivilegesVO privileges;
	private PrivilegesVO selfPrivileges;
	
	public AccessControlPolicyVO() {
		privileges = new PrivilegesVO();
		selfPrivileges = new PrivilegesVO();
	}
	
	public static class PrivilegesVO {
		private String accessControlOriginators;
		private String accessControlContexts;
		private String accessControlOperations;
		
		public String getAccessControlOriginators() {
			return accessControlOriginators;
		}
		public void setAccessControlOriginators(String accessControlOriginators) {
			this.accessControlOriginators = accessControlOriginators;
		}
		public String getAccessControlContexts() {
			return accessControlContexts;
		}
		public void setAccessControlContexts(String accessControlContexts) {
			this.accessControlContexts = accessControlContexts;
		}
		public String getAccessControlOperations() {
			return accessControlOperations;
		}
		public void setAccessControlOperations(String accessControlOperations) {
			this.accessControlOperations = accessControlOperations;
		}
		@Override
		public String toString() {
			return "privileges [accessControlOriginators="
					+ accessControlOriginators + ", accessControlContexts="
					+ accessControlContexts + ", accessControlOperations="
					+ accessControlOperations + "]";
		}
	}
	
	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceID() {
		return resourceID;
	}

	public void setResourceID(String resourceID) {
		this.resourceID = resourceID;
	}

	public String getParentID() {
		return parentID;
	}

	public void setParentID(String parentID) {
		this.parentID = parentID;
	}

	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	public String getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(String lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public String getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getLabels() {
		return labels;
	}

	public void setLabels(String labels) {
		this.labels = labels;
	}

	public PrivilegesVO getPrivileges() {
		return privileges;
	}

	public void setPrivileges(PrivilegesVO privileges) {
		this.privileges = privileges;
	}

	public PrivilegesVO getSelfPrivileges() {
		return selfPrivileges;
	}

	public void setSelfPrivileges(PrivilegesVO selfPrivileges) {
		this.selfPrivileges = selfPrivileges;
	}

	@Override
	public String toString() {
		return "AccessControlPolicyVO [resourceType=" + resourceType
				+ ", resourceID=" + resourceID + ", parentID=" + parentID
				+ ", creationTime=" + creationTime + ", lastModifiedTime="
				+ lastModifiedTime + ", expirationTime=" + expirationTime
				+ ", labels=" + labels + ", privileges=" + privileges
				+ ", selfPrivileges=" + selfPrivileges + "]";
	}
}
