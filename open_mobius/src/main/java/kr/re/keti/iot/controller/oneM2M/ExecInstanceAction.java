package kr.re.keti.iot.controller.oneM2M;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.re.keti.iot.domain.apilog.CallerVO;
import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.ExecInstanceVO;
import kr.re.keti.iot.domain.oneM2M.ListOfResourceVO;
import kr.re.keti.iot.domain.oneM2M.MgmtCmdVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao.ONEM2M;
import kr.re.keti.iot.service.apilog.ApiLogService;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.ExecInstanceService;
import kr.re.keti.iot.service.oneM2M.MgmtCmdService;
import kr.re.keti.iot.service.oneM2M.RemoteCSEService;
import kr.re.keti.iot.service.oneM2M.common.MCommonService;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * execInstance management Action.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Controller
public class ExecInstanceAction {

	private static final Log logger = LogFactory.getLog(ExecInstanceAction.class);
	
	@Autowired
	private MongoLogService mongoLogService;
	
	@Autowired
	private MCommonService mCommonService;

	@Autowired
	private RemoteCSEService remoteCSEService;

	@Autowired
	private MgmtCmdService mgmtCmdService;
	
	@Autowired
	private ExecInstanceService execInstanceService;
	
	@Autowired
	private ApiLogService apiLogService;

	
	/**
	 * execInstance retrieve
	 * @param remoteCSEID
	 * @param mgmtCmdName
	 * @param execInstanceID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:[^9.9.999.9].*}/mgmtCmd-{mgmtCmdName}/execInstance-{execInstanceID}", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> execInstanceRetrieve(@PathVariable String remoteCSEID
													 , @PathVariable String mgmtCmdName
													 , @PathVariable String execInstanceID
													 , HttpServletRequest request
													 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ExecInstanceVO responseVo = new ExecInstanceVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg  = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey		= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic 	= CommonUtil.nvl(request.getHeader("topic"), "");
		CallerVO callerVO = null;
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(remoteCSEID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(mgmtCmdName)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "mgmtCmdName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(execInstanceID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "execInstanceID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if(isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			
			if(remoteCSEService.getCount(remoteCSEID) < 1) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			if(mgmtCmdService.getCountByLabels(remoteCSEID, mgmtCmdName) < 1) {
				throw new IotException("627", CommonUtil.getMessage("msg.mgmtCmd.noRegi.text", locale));
			}
			
			ExecInstanceVO findExecInstanceItem = null;
			if (CommonUtil.isEmpty(findExecInstanceItem = execInstanceService.findOneExecInstance(execInstanceID))) {
				throw new IotException("628", CommonUtil.getMessage("msg.device.execInstance.noRegi.text", locale));
			}
			
			//responseVo.setExecInstance(findExecInstanceItem);
			BeanUtils.copyProperties(findExecInstanceItem, responseVo);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}

	
	/**
	 * polling execInstance retrieve
	 * @param remoteCSEID
	 * @param mgmtCmdName
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:[^9.9.999.9].*}/mgmtCmd-{mgmtCmdName}/execInstance", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> pollingExecInstanceRetrieve(@PathVariable String remoteCSEID
															, @PathVariable String mgmtCmdName
															, HttpServletRequest request
															, HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();
		
		ListOfResourceVO responseVo = new ListOfResourceVO();
		responseVo.setName(ONEM2M.EXEC_INSTANCE.getName());
		
		HttpStatus httpStatus = HttpStatus.OK;
	
		String responseCode = "200";
		String responseMsg  = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dkey		= CommonUtil.nvl(request.getHeader("dKey"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(remoteCSEID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(mgmtCmdName)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "mgmtCmdName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if(isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);
			
			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			
			if(remoteCSEService.getCount(remoteCSEID) < 1) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			MgmtCmdVO findMgmtCmdItem = null;
			if(CommonUtil.isEmpty(findMgmtCmdItem = mgmtCmdService.findOneMgmtCmdByLabels(remoteCSEID, mgmtCmdName))) {
				throw new IotException("627", CommonUtil.getMessage("msg.mgmtCmd.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dkey);			
			
			List<ExecInstanceVO> findListExecInstance = null;
			if (CommonUtil.isEmpty(findListExecInstance = execInstanceService.pollingExecInstanceRetrieve(findMgmtCmdItem.getResourceID()))) {
				throw new IotException("628", CommonUtil.getMessage("msg.device.execInstance.noRegi.text", locale));
			}
			
			responseVo.setExecInstanceList(findListExecInstance);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();
	
			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
	
		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}

}