package kr.re.keti.iot.domain.mongo;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Log domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Document
public class LogVO {
	private String log_key = "";
	private String target_server = "";
	private String level = "";
	private String message = "";
	private String reg_dt = "";
	
	public String getLog_key() {
		return log_key;
	}
	public void setLog_key(String log_key) {
		this.log_key = log_key;
	}
	public String getTarget_server() {
		return target_server;
	}
	public void setTarget_server(String target_server) {
		this.target_server = target_server;
	}
	public String getLevel() {
		return level;
	}
	public void setLevel(String level) {
		this.level = level;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getReg_dt() {
		return reg_dt;
	}
	public void setReg_dt(String reg_dt) {
		this.reg_dt = reg_dt;
	}
	
	@Override
	public String toString() {
		return "LogVO [log_key=" + log_key + ", target_server=" + target_server
				+ ", level=" + level + ", message=" + message + ", reg_dt="
				+ reg_dt + "]";
	}
}