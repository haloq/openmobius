package kr.re.keti.iot.controller.oneM2M;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.AEVO;
import kr.re.keti.iot.domain.oneM2M.ContainerOnlyIdVO;
import kr.re.keti.iot.domain.oneM2M.ContainerVO;
import kr.re.keti.iot.domain.oneM2M.ListOfResourceVO;
import kr.re.keti.iot.domain.oneM2M.RemoteCSEVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao.ONEM2M;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.AEService;
import kr.re.keti.iot.service.oneM2M.ContainerService;
import kr.re.keti.iot.service.oneM2M.RemoteCSEService;
import kr.re.keti.iot.service.oneM2M.common.MCommonService;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * container management Action.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>khlee</li>
 *         </ul>
 */
@Controller
public class ContainerAction {

	private static final Log logger = LogFactory.getLog(ContainerAction.class);

	@Autowired
	private MongoLogService mongoLogService;

	@Autowired
	private MCommonService mCommonService;

	@Autowired
	private RemoteCSEService remoteCSEService;
	
	@Autowired
	private AEService aEService;

	@Autowired
	private ContainerService containerService;

	
	/**
	 * remoteCSE container create
	 * @param remoteCSEID
	 * @param labels
	 * @param containerProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}", method = RequestMethod.POST, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" }, params = {"ty=container"})
	@ResponseBody
	public ResponseEntity<Object> remoteCSEContainerCreate(@PathVariable String remoteCSEID
														 , @RequestParam(value = "nm", required = false) String labels
														 , @RequestBody ContainerVO containerProfile
														 , HttpServletRequest request
														 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ContainerVO responseVo = new ContainerVO();
		HttpStatus httpStatus = HttpStatus.CREATED;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey 	= CommonUtil.nvl(request.getHeader("dKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(labels)) {
			isError = true;
			responseCode = "600";
			responseMsg = "nm " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(containerProfile)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<container> " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, HttpStatus.OK);
		}

		try {
			
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			RemoteCSEVO findRemoteCSEItem = null;
			if (CommonUtil.isEmpty((findRemoteCSEItem = remoteCSEService.findOneRemoteCSE(remoteCSEID)))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dKey);
			
			ContainerVO findContainerItem = null;
			if (!CommonUtil.isEmpty((findContainerItem = containerService.findOneContainerByLabels(remoteCSEID, labels)))) {
				//responseVo.setContainer(findContainerItem);
				BeanUtils.copyProperties(findContainerItem, responseVo);
				throw new IotException("609", CommonUtil.getMessage("msg.container.duplicate.text", locale));
			}
			
			containerProfile.setParentID(findRemoteCSEItem.getResourceID());
			containerProfile.setAccessControlPolicyIDs(findRemoteCSEItem.getAccessControlPolicyIDs());
			containerProfile.setLabels(labels);
			
			ContainerVO containerItem = containerService.createContainer(containerProfile);
			//responseVo.setContainer(containerItem);
			BeanUtils.copyProperties(containerItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);

		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * remoteCSE container delete
	 * @param remoteCSEID
	 * @param containerName
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID}/container-{containerName}", method = RequestMethod.DELETE, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Void> remoteCSEContainerDelete(@PathVariable String remoteCSEID
													   , @PathVariable String containerName
													   , HttpServletRequest request
													   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		Object responseVo = new Object();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey 	= CommonUtil.nvl(request.getHeader("dKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Void>(httpStatus);
		}

		try {
			
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dKey);
			
			ContainerVO containerItem = null;
			if (CommonUtil.isEmpty(containerItem = containerService.findOneContainerByLabels(remoteCSEID, containerName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			containerService.deleteContainer(remoteCSEID, containerItem.getResourceID());

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Void>(httpStatus);
	}
	
	/**
	 * remoteCSE container update
	 * @param remoteCSEID
	 * @param containerName
	 * @param containerProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID}/container-{containerName}", method = RequestMethod.PUT, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> remoteCSEContainerUpdate(@PathVariable String remoteCSEID
														 , @PathVariable String containerName
														 , @RequestBody ContainerVO containerProfile
														 , HttpServletRequest request
														 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ContainerVO responseVo = new ContainerVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey 	= CommonUtil.nvl(request.getHeader("dKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(containerProfile)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<container> " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}

		try {
			
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			RemoteCSEVO findRemoteCSEItem = null;
			if (CommonUtil.isEmpty((findRemoteCSEItem = remoteCSEService.findOneRemoteCSE(remoteCSEID)))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dKey);
			
			ContainerVO containerItem = null;
			if (CommonUtil.isEmpty(containerItem = containerService.findOneContainerByLabels(remoteCSEID, containerName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			containerProfile.setParentID(findRemoteCSEItem.getResourceID());
			containerProfile.setAccessControlPolicyIDs(findRemoteCSEItem.getAccessControlPolicyIDs());
			containerProfile.setResourceID(containerItem.getResourceID());
			containerItem = containerService.updateContainer(containerProfile);
			//responseVo.setContainer(containerItem);
			BeanUtils.copyProperties(containerItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * remoteCSE container List retrieve
	 * @param remoteCSEID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/container", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> remoteCSEContainerListRetrieve(@PathVariable String remoteCSEID
															   , HttpServletRequest request
															   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ListOfResourceVO responseVo = new ListOfResourceVO();
		responseVo.setName(ONEM2M.CONTAINER.getName());
		
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey 	= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic 	= CommonUtil.nvl(request.getHeader("topic"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}

		try {
			
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			List<ContainerOnlyIdVO> findContainerList = null;
			if (CommonUtil.isEmpty(findContainerList = containerService.findContainerOnlyId("parentID", remoteCSEID))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			responseVo.setContainerList(findContainerList);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * remoteCSE container retrieve
	 * @param remoteCSEID
	 * @param containerName
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID}/container-{containerName}", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> remoteCSEContainerRetrieve(@PathVariable String remoteCSEID
														   , @PathVariable String containerName
														   , HttpServletRequest request
														   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ContainerVO responseVo = new ContainerVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg  = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey		= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic 	= CommonUtil.nvl(request.getHeader("topic"), "");	
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else
		
		if(isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			 
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			ContainerVO findContainerItem = null;
			if (CommonUtil.isEmpty(findContainerItem = containerService.findOneContainerByLabels(remoteCSEID, containerName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			//responseVo.setContainer(findContainerItem);
			BeanUtils.copyProperties(findContainerItem, responseVo);
			

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * AE container create
	 * @param appId
	 * @param labels
	 * @param containerProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/AE-{appId:.*}", method = RequestMethod.POST, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" }, params = {"ty=container"})
	@ResponseBody
	public ResponseEntity<Object> AEContainerCreate(@PathVariable String appId
												  , @RequestParam(value = "nm", required = false) String labels
												  , @RequestBody ContainerVO containerProfile
												  , HttpServletRequest request
												  , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ContainerVO responseVo = new ContainerVO();
		HttpStatus httpStatus = HttpStatus.CREATED;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String aKey 	= CommonUtil.nvl(request.getHeader("aKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(appId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "appId " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(labels)) {
			isError = true;
			responseCode = "600";
			responseMsg = "nm " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(containerProfile)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<container> " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}

		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, HttpStatus.OK);
		}

		try {
			
			mCommonService.checkPatternAppId(checkMccP, appId);
			
			AEVO findAEItem = null;
			if (CommonUtil.isEmpty((findAEItem = aEService.findOneAE("appId", appId)))) {
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			mCommonService.verifyAKey(checkMccP, appId, aKey);
			
			ContainerVO findContainerItem = null;
			if (!CommonUtil.isEmpty((findContainerItem = containerService.findOneContainerByLabels(appId, labels)))) {
				//responseVo.setContainer(findContainerItem);
				BeanUtils.copyProperties(findContainerItem, responseVo);
				throw new IotException("609", CommonUtil.getMessage("msg.container.duplicate.text", locale));
			}
			
			containerProfile.setParentID(findAEItem.getAppId());
			containerProfile.setAccessControlPolicyIDs(findAEItem.getAccessControlPolicyIDs());
			containerProfile.setLabels(labels);
			
			ContainerVO containerItem = containerService.createContainer(containerProfile);
			//responseVo.setContainer(containerItem);
			BeanUtils.copyProperties(containerItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);

		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * AE container delete
	 * @param appId
	 * @param containerName
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/AE-{appId:.*}/container-{containerName}", method = RequestMethod.DELETE, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Void> AEContainerDelete(@PathVariable String appId
												, @PathVariable String containerName
												, HttpServletRequest request
												, HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		Object responseVo = new Object();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String aKey 	= CommonUtil.nvl(request.getHeader("aKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(appId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "appId " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Void>(httpStatus);
		}

		try {
			
			mCommonService.checkPatternAppId(checkMccP, appId);
			
			if (CommonUtil.isEmpty(aEService.findOneAE("appId", appId))) {
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			mCommonService.verifyAKey(checkMccP, appId, aKey);
			
			ContainerVO containerItem = null;
			if (CommonUtil.isEmpty(containerItem = containerService.findOneContainerByLabels(appId, containerName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			containerService.deleteContainer(appId, containerItem.getResourceID());

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Void>(httpStatus);
	}
	
	/**
	 * AE container update
	 * @param appId
	 * @param containerName
	 * @param containerProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/AE-{appId:.*}/container-{containerName}", method = RequestMethod.PUT, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> AEContainerUpdate(@PathVariable String appId
												  , @PathVariable String containerName
												  , @RequestBody ContainerVO containerProfile
												  , HttpServletRequest request
												  , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ContainerVO responseVo = new ContainerVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String aKey 	= CommonUtil.nvl(request.getHeader("aKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(appId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "appId " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(containerProfile)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<container> " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}

		try {
			
			mCommonService.checkPatternAppId(checkMccP, appId);
			
			AEVO findAEItem = null;
			if (CommonUtil.isEmpty((findAEItem = aEService.findOneAE("appId", appId)))) {
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			mCommonService.verifyAKey(checkMccP, appId, aKey);
			
			ContainerVO containerItem = null;
			if (CommonUtil.isEmpty(containerItem = containerService.findOneContainerByLabels(appId, containerName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			containerProfile.setParentID(findAEItem.getAppId());
			containerProfile.setAccessControlPolicyIDs(findAEItem.getAccessControlPolicyIDs());
			containerProfile.setResourceID(containerItem.getResourceID());
			containerItem = containerService.updateContainer(containerProfile);
			//responseVo.setContainer(containerItem);
			BeanUtils.copyProperties(containerItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}

	
	
	/**
	 * AE container List retrieve
	 * @param appId
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/AE-{appId:.*}/container", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> AEContainerListRetrieve(@PathVariable String appId
														, HttpServletRequest request
														, HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ListOfResourceVO responseVo = new ListOfResourceVO();
		responseVo.setName(ONEM2M.CONTAINER.getName());
		
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey 	= CommonUtil.nvl(request.getHeader("uKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(appId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "appId " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}

		try {
			
			mCommonService.checkPatternAppId(checkMccP, appId);
			
			if (CommonUtil.isEmpty(aEService.findOneAE("appId", appId))) {
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			List<ContainerOnlyIdVO> findContainerList = null;
			if (CommonUtil.isEmpty(findContainerList = containerService.findContainerOnlyId("parentID", appId))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			responseVo.setContainerList(findContainerList);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * AE container retrieve
	 * @param appId
	 * @param containerName
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/AE-{appId:.*}/container-{containerName}", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> AEContainerRetrieve(@PathVariable String appId
													, @PathVariable String containerName
													, HttpServletRequest request
													, HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ContainerVO responseVo = new ContainerVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg  = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey		= CommonUtil.nvl(request.getHeader("uKey"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(appId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "appId " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if(isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			
			mCommonService.checkPatternAppId(checkMccP, appId);
			
			if (CommonUtil.isEmpty(aEService.findOneAE("appId", appId))) {
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			ContainerVO findContainerItem = null;
			if (CommonUtil.isEmpty(findContainerItem = containerService.findOneContainerByLabels(appId, containerName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			//responseVo.setContainer(findContainerItem);
			BeanUtils.copyProperties(findContainerItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * RemoteCSE AE container create
	 * @param remoteCSEID
	 * @param appId
	 * @param labels
	 * @param containerProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/AE-{appId:.*}", method = RequestMethod.POST, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" }, params = {"ty=container"})
	@ResponseBody
	public ResponseEntity<Object> remoteCSEAEContainerCreate(@PathVariable String remoteCSEID
														   , @PathVariable String appId
														   , @RequestParam(value = "nm", required = false) String labels
														   , @RequestBody ContainerVO containerProfile
														   , HttpServletRequest request
														   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ContainerVO responseVo = new ContainerVO();
		HttpStatus httpStatus = HttpStatus.CREATED;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey 	= CommonUtil.nvl(request.getHeader("dKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(appId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "appId " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(labels)) {
			isError = true;
			responseCode = "600";
			responseMsg = "nm " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(containerProfile)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<container> " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}

		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus.OK);
		}

		try {
			
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			mCommonService.checkPatternAppId(checkMccP, appId);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dKey);
			
			AEVO findAEItem = null;
			if (CommonUtil.isEmpty((findAEItem = aEService.findOneAE("appId", appId)))) {
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			String aeParentID = findAEItem.getParentID();
			if(CommonUtil.isEmpty(aeParentID) || !aeParentID.equals(remoteCSEID)) {
				throw new IotException("600", CommonUtil.getMessage("msg.device.AE.noauth.text", locale));
			}
			
			ContainerVO findContainerItem = null;
			if (!CommonUtil.isEmpty((findContainerItem = containerService.findOneContainerByLabels(appId, labels)))) {
				//responseVo.setContainer(findContainerItem);
				BeanUtils.copyProperties(findContainerItem, responseVo);
				throw new IotException("609", CommonUtil.getMessage("msg.container.duplicate.text", locale));
			}
			
			containerProfile.setParentID(findAEItem.getAppId());
			containerProfile.setAccessControlPolicyIDs(findAEItem.getAccessControlPolicyIDs());
			containerProfile.setLabels(labels);
			ContainerVO containerItem = containerService.createContainer(containerProfile);
			//responseVo.setContainer(containerItem);
			BeanUtils.copyProperties(containerItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * RemoteCSE AE container delete
	 * @param remoteCSEID
	 * @param appId
	 * @param containerName
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/AE-{appId:.*}/container-{containerName}", method = RequestMethod.DELETE, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Void> remoteCSEAEContainerDelete(@PathVariable String remoteCSEID
														 , @PathVariable String appId
														 , @PathVariable String containerName
														 , HttpServletRequest request
														 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		Object responseVo = new Object();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey 	= CommonUtil.nvl(request.getHeader("dKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(appId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "appId " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Void>(httpStatus);
		}

		try {
			
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			mCommonService.checkPatternAppId(checkMccP, appId);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dKey);
			
			AEVO findAEItem = null;
			if (CommonUtil.isEmpty(findAEItem = aEService.findOneAE("appId", appId))) {
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			String aeParentID = findAEItem.getParentID();
			if(CommonUtil.isEmpty(aeParentID) || !aeParentID.equals(remoteCSEID)) {
				throw new IotException("600", CommonUtil.getMessage("msg.device.AE.noauth.text", locale));
			}
			
			ContainerVO containerItem = null;
			if (CommonUtil.isEmpty(containerItem = containerService.findOneContainerByLabels(appId, containerName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			containerService.deleteContainer(appId, containerItem.getResourceID());

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Void>(httpStatus);
	}
	
	/**
	 * RemoteCSE AE container update
	 * @param remoteCSEID
	 * @param appId
	 * @param containerName
	 * @param containerProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/AE-{appId:.*}/container-{containerName}", method = RequestMethod.PUT, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> remoteCSEAEContainerUpdate(@PathVariable String remoteCSEID
														   , @PathVariable String appId
														   , @PathVariable String containerName
														   , @RequestBody ContainerVO containerProfile
														   , HttpServletRequest request
														   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ContainerVO responseVo = new ContainerVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey 	= CommonUtil.nvl(request.getHeader("dKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(appId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "appId " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(containerProfile)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<container> " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}

		try {
			
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			mCommonService.checkPatternAppId(checkMccP, appId);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dKey);
			
			AEVO findAEItem = null;
			if (CommonUtil.isEmpty(findAEItem = aEService.findOneAE("appId", appId))) {
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			String aeParentID = findAEItem.getParentID();
			if(CommonUtil.isEmpty(aeParentID) || !aeParentID.equals(remoteCSEID)) {
				throw new IotException("600", CommonUtil.getMessage("msg.device.AE.noauth.text", locale));
			}
			
			ContainerVO containerItem = null;
			if (CommonUtil.isEmpty(containerItem = containerService.findOneContainerByLabels(appId, containerName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			containerProfile.setParentID(findAEItem.getAppId());
			containerProfile.setAccessControlPolicyIDs(findAEItem.getAccessControlPolicyIDs());
			containerProfile.setResourceID(containerItem.getResourceID());
			containerItem = containerService.updateContainer(containerProfile);
			//responseVo.setContainer(containerItem);
			BeanUtils.copyProperties(containerItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * RemoteCSE AE container List retrieve
	 * @param remoteCSEID
	 * @param appId
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/AE-{appId:.*}/container", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> remoteCSEAEContainerListRetrieve(@PathVariable String remoteCSEID
																 , @PathVariable String appId
																 , HttpServletRequest request
																 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ListOfResourceVO responseVo = new ListOfResourceVO();
		responseVo.setName(ONEM2M.CONTAINER.getName());
		
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey 	= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic 	= CommonUtil.nvl(request.getHeader("topic"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(appId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "appId " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}

		try {
			
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			mCommonService.checkPatternAppId(checkMccP, appId);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			AEVO findAEItem = null;
			if (CommonUtil.isEmpty(findAEItem = aEService.findOneAE("appId", appId))) {
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			String aeParentID = findAEItem.getParentID();
			if(CommonUtil.isEmpty(aeParentID) || !aeParentID.equals(remoteCSEID)) {
				throw new IotException("600", CommonUtil.getMessage("msg.device.AE.noauth.text", locale));
			}
			
			List<ContainerOnlyIdVO> findContainerList = null;
			if (CommonUtil.isEmpty(findContainerList = containerService.findContainerOnlyId("parentID", appId))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			responseVo.setContainerList(findContainerList);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * RemoteCSE AE container retrieve
	 * @param remoteCSEID
	 * @param appId
	 * @param containerName
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/AE-{appId:.*}/container-{containerName}", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> remoteCSEAEContainerRetrieve(@PathVariable String remoteCSEID
															 , @PathVariable String appId
															 , @PathVariable String containerName
															 , HttpServletRequest request
															 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ContainerVO responseVo = new ContainerVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg  = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey		= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic 	= CommonUtil.nvl(request.getHeader("topic"), "");	
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(appId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "appId " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if(isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			mCommonService.checkPatternAppId(checkMccP, appId);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			AEVO findAEItem = null;
			if (CommonUtil.isEmpty(findAEItem = aEService.findOneAE("appId", appId))) {
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			String aeParentID = findAEItem.getParentID();
			if(CommonUtil.isEmpty(aeParentID) || !aeParentID.equals(remoteCSEID)) {
				throw new IotException("600", CommonUtil.getMessage("msg.device.AE.noauth.text", locale));
			}
			
			ContainerVO findContainerItem = null;
			if (CommonUtil.isEmpty(findContainerItem = containerService.findOneContainerByLabels(appId, containerName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			//responseVo.setContainer(findContainerItem);
			BeanUtils.copyProperties(findContainerItem, responseVo);
			

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}

}