package kr.re.keti.iot.controller.oneM2M;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.CSEBaseVO;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.CSEBaseService;
import kr.re.keti.iot.service.oneM2M.common.MCommonService;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * CSEBase management Action.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Controller
public class CSEBaseAction {

	private static final Log logger = LogFactory.getLog(CSEBaseAction.class);
	
	@Autowired
	private MongoLogService mongoLogService;
	
	@Autowired
	private CSEBaseService cseBaseService;

	@Autowired
	private MCommonService mCommonService;
	
	
	/**
	 * CSEBase retrieve
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> CSEBaseRetrieve(HttpServletRequest request
												, HttpServletResponse response) {
		
		HttpHeaders responseHeaders = new HttpHeaders();

		CSEBaseVO responseVo = new CSEBaseVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg  = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}

		if(isError){
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);
			
			return new ResponseEntity<Object>(responseVo, responseHeaders, HttpStatus.OK);
		}
				
		try {
			
			CSEBaseVO findCSEBaseItem = null;
			if (CommonUtil.isEmpty(findCSEBaseItem = cseBaseService.findOneCSEBase())) {
				throw new IotException("600", CommonUtil.getMessage("msg.device.empty.text", locale));
			}
			
			//responseVo.setCseBase(findCSEBaseItem);
			BeanUtils.copyProperties(findCSEBaseItem, responseVo);
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
		
}
