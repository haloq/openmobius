package kr.re.keti.iot.batch.oneM2M.contentInstance;

import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

/**
 * RedisToMongoDB Bulk Insert batch.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>khlee</li>
 *         </ul>
 */
@Controller
public class ContentInstanceJob {
	
	private static final Log logger = LogFactory.getLog(ContentInstanceJob.class);

	@Autowired
	private MongoLogService mongoLogService;

	@Autowired
	private ContentInstanceDeviceToMongoRegService contentInstanceDeviceToMongoRegService;
	
	/**
	 * RedisToMongoDB Bulk Insert Scheduled.
	 */
	@Scheduled(fixedDelay = 10000)
	private void moveRedis2Mongo() {
		try {
			contentInstanceDeviceToMongoRegService.moveRedis2Mongo();
			logger.debug("moveRedis2Mongo success");
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			e.printStackTrace();
		}

	}
}
