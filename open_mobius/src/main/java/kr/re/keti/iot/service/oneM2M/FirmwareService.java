package kr.re.keti.iot.service.oneM2M;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.FirmwareVO;
import kr.re.keti.iot.domain.oneM2M.NodeVO;
import kr.re.keti.iot.mdao.common.SequenceDao;
import kr.re.keti.iot.mdao.common.SequenceDao.MovType;
import kr.re.keti.iot.mdao.common.SequenceDao.SEQ_PREFIX;
import kr.re.keti.iot.mdao.common.SequenceDao.SeqType;
import kr.re.keti.iot.mdao.oneM2M.FirmwareDao;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.NodeService.MGMT_DEFINITION;
import kr.re.keti.iot.service.oneM2M.NodeService.STATUS;
import kr.re.keti.iot.util.CommonUtil;
import kr.re.keti.iot.util.oneM2M.CommonCode.RESOURCE_TYPE;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

/**
 * firmware management Service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Service
public class FirmwareService {

	private static final Log logger = LogFactory.getLog(FirmwareService.class);

	@Autowired
	private MongoLogService mongoLogService;

	@Autowired
	private MgmtCmdService mgmtCmdService;
	
	@Autowired
	private DeviceInfoService deviceInfoService;
	
	@Autowired
	private FirmwareDao firmwareDao;
	
	@Autowired
	private SequenceDao seqDao;
	
	/**
	 * generate resourceID
	 * @return
	 * @throws IotException
	 */
	private String generateResourceId() throws IotException {
		long longSeq = 0;
		StringBuffer bufSeq = new StringBuffer(SEQ_PREFIX.FIRMWARE.getValue());
		
		try {
			longSeq = seqDao.move(MovType.UP, SeqType.FIRMWARE);
			
			bufSeq.append(String.format("%010d", longSeq));

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.firmware.id.createFail.text"));
		}
		
		return bufSeq.toString();
	}
	
	/**
	 * find firmware by firmwareId
	 * @param firmwareId
	 * @return
	 * @throws IotException
	 */
	public FirmwareVO findOneFirmware(String firmwareId) throws IotException {

		FirmwareVO findFirmwareItem = null;

		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(firmwareId));

		try {

			findFirmwareItem = (FirmwareVO) firmwareDao.findOne(query);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.firmware.findFail.text"));
		}

		return findFirmwareItem;
	}
	
	/**
	 * find firmware by nodeId
	 * @param nodeId
	 * @return
	 * @throws IotException
	 */
	public FirmwareVO findOneFirmwareByNodeId(String nodeId) throws IotException {
		
		FirmwareVO findFirmwareItem = null;
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(nodeId));
		query.addCriteria(Criteria.where("updateStatus").is(STATUS.SUCCESSFUL.getValue()));
		
		try {
			
			findFirmwareItem = (FirmwareVO) firmwareDao.findOne(query);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.firmware.findFail.text"));
		}
		
		return findFirmwareItem;
	}
	
	/**
	 * find firmware count by parentId-updateStatus
	 * @param parentId
	 * @param updateStatus
	 * @return
	 */
	public long getCount(String parentId, String updateStatus){
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(parentId));
		query.addCriteria(Criteria.where("updateStatus").is(updateStatus));
		
		long cnt = 0;
		
		try {
			cnt = firmwareDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "firmware get count");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		return cnt;
	}
	
	/**
	 * find firmware count by parentId-version
	 * @param parentId
	 * @param version
	 * @return
	 */
	public long getCountByVersion(String parentId, String version){
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(parentId));
		query.addCriteria(Criteria.where("version").is(version));
		query.addCriteria(Criteria.where("updateStatus").is(STATUS.SUCCESSFUL.getValue()));
		
		long cnt = 0;
		
		try {
			cnt = firmwareDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "firmware get count");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		return cnt;
	}
	
	/**
	 * find contolling firmware count by parentId
	 * @param parentId
	 * @return
	 * @throws IotException
	 */
	public long getCountControlling(String parentId) throws IotException {
		FirmwareVO findFirmwareItem = null;
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(parentId));
		query.addCriteria(Criteria.where("updateStatus").is(STATUS.IN_PROCESS.getValue()));
		
		long cnt = 0;
		
		try {
			findFirmwareItem = (FirmwareVO)firmwareDao.findOne(query);
			
			if(!CommonUtil.isEmpty(findFirmwareItem)) {
				
				if(CommonUtil.checkElapseTime(findFirmwareItem.getLastModifiedTime(), Long.valueOf(CommonUtil.getConfig("iot.mgmtCmd.status.check.time")), "yyyy-MM-dd'T'HH:mm:ssXXX") > 0){
					this.deleteFirmwareByNodeId(parentId, STATUS.IN_PROCESS.getValue());
				}
			}
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.firmware.findFail.text"));
		}
		
		try {
			cnt = firmwareDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "firmware get count");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		return cnt;
	}
	
	/**
	 * find firmware count by nodeId-labels
	 * @param nodeId
	 * @param labels
	 * @return
	 * @throws IotException
	 */
	public FirmwareVO findOneFirmwareByLabels(String nodeId, String labels) throws IotException {
		FirmwareVO findFirmwareItem = null;
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(nodeId));
		query.addCriteria(Criteria.where("labels").is(labels));
		
		try {
			findFirmwareItem = (FirmwareVO)firmwareDao.findOne(query);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.firmware.findFail.text"));
		}
		
		return findFirmwareItem;
	}
	
	/**
	 * create firmware
	 * @param firmwareItem
	 * @throws IotException
	 */
	public void createFirmware(FirmwareVO firmwareItem) throws IotException {
		
		try {
			firmwareDao.insert(firmwareItem);
			
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.firmware.createFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.INFO, "firmware create success");
		
	}
	
	/**
	 * retrieve create FirmwareVO
	 * @param nodeInfo
	 * @param firmwareProfile
	 * @param isControl
	 * @return
	 * @throws IotException
	 */
	public FirmwareVO getCreateFirmwareVO(NodeVO nodeInfo, FirmwareVO firmwareProfile, boolean isControl) throws IotException {
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		FirmwareVO firmwareItem = new FirmwareVO();
		
		if(!isControl) {
			firmwareItem.setResourceID(this.generateResourceId());
		}
		
		firmwareItem.setResourceType(RESOURCE_TYPE.NODE.getName());
		firmwareItem.setParentID(nodeInfo.getResourceID());
		firmwareItem.setAccessControlPolicyIDs(nodeInfo.getAccessControlPolicyIDs());
		
		firmwareItem.setDescription(firmwareProfile.getDescription());
		firmwareItem.setVersion(firmwareProfile.getVersion());
		firmwareItem.setName(firmwareProfile.getName());
		firmwareItem.setLabels(firmwareProfile.getName());
		firmwareItem.setURL(firmwareProfile.getURL());
		
		firmwareItem.setCreationTime(currentTime);
		firmwareItem.setLastModifiedTime(currentTime);
		
		return firmwareItem;

	}
	
	/**
	 * retrieve create FirmwareVO
	 * @param nodeInfo
	 * @param firmwareProfile
	 * @return
	 * @throws IotException
	 */
	public FirmwareVO createFirmware(NodeVO nodeInfo, FirmwareVO firmwareProfile) throws IotException {
		FirmwareVO firmwareItem = new FirmwareVO();
		
		firmwareItem = this.getCreateFirmwareVO(nodeInfo, firmwareProfile, false);
		firmwareItem.setMgmtDefinition(MGMT_DEFINITION.FIRMWARE.getValue());
		firmwareItem.setUpdateStatus(STATUS.SUCCESSFUL.getValue());
		
		try {
			firmwareDao.insert(firmwareItem);
			
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.firmware.createFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.INFO, "firmware create success");
		
		return this.findOneFirmware(firmwareItem.getResourceID());
	}
	
	/**
	 * update firmware
	 * @param nodeId
	 * @param resourceId
	 * @param newFirmwareProfile
	 * @return
	 * @throws IotException
	 */
	public FirmwareVO updateFirmware(String nodeId, String resourceId, FirmwareVO newFirmwareProfile) throws IotException {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(resourceId));
		
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		String description = newFirmwareProfile.getDescription();
		String version = newFirmwareProfile.getVersion();
		String name = newFirmwareProfile.getName();
		String URL = newFirmwareProfile.getURL();
//		String updateStatus = newFirmwareProfile.getUpdateStatus();
		
		Update updateQuery = new Update();
		if(!CommonUtil.isNull(description))		updateQuery.set("description", description);
		if(!CommonUtil.isNull(version))			updateQuery.set("version", version);
		if(!CommonUtil.isNull(name))			updateQuery.set("name", name);
		if(!CommonUtil.isNull(name))			updateQuery.set("labels", name);
		if(!CommonUtil.isNull(URL))				updateQuery.set("URL", URL);
//		if(!CommonUtil.isNull(updateStatus))	updateQuery.set("updateStatus", updateStatus);
												updateQuery.set("lastModifiedTime", currentTime);
		
		try { 
			firmwareDao.update(query, updateQuery);
			
			deviceInfoService.saveDeviceInfoByFirmware(nodeId, version);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.firmware.upFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "Firmware update success");
		
		return this.findOneFirmware(resourceId);
	}
	
	/**
	 * delete firmware by key-value
	 * @param key
	 * @param value
	 * @throws IotException
	 */
	public void deleteFirmware(String key, String value) throws IotException {
		Query query = new Query();
		query.addCriteria(Criteria.where(key).is(value));
		try {

			firmwareDao.remove(query);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.firmware.delFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "firmware delete success");

	}
	
	/**
	 * delete firmware by nodeId-updateStatus
	 * @param nodeId
	 * @param updateStatus
	 * @throws IotException
	 */
	public void deleteFirmwareByNodeId(String nodeId, String updateStatus) throws IotException {
		
		Query query = new Query(Criteria.where("parentID").is(nodeId));
		query.addCriteria(Criteria.where("updateStatus").is(updateStatus));
		
		try {
			
			firmwareDao.remove(query);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.firmware.delFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "firmware delete success");
		
	}
	
	/**
	 * delete firmware by firmwareID-updateStatus
	 * @param firmwareID
	 * @param updateStatus
	 * @throws IotException
	 */
	public void deleteFirmwareByUpdateStatus(String firmwareID, String updateStatus) throws IotException {
		
		Query query = new Query(Criteria.where("resourceID").is(firmwareID));
		query.addCriteria(Criteria.where("updateStatus").is(updateStatus));
		
		try {
			firmwareDao.remove(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.firmware.delFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "firmware delete success");
	}	
}