package kr.re.keti.iot.mdao.oneM2M;

import org.springframework.stereotype.Repository;

import kr.re.keti.iot.domain.oneM2M.ContainerVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao;

/**
 * container management Dao.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Repository
public class ContainerDao extends GenericMongoDao {
	
	public ContainerDao() {
		collectionName = ONEM2M.CONTAINER.getName();
		cls = ContainerVO.class;
	}
}
