package kr.re.keti.iot.service.oneM2M;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.AccessControlPolicyVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao.MOBIUS;
import kr.re.keti.iot.mdao.common.GenericMongoDao.ONEM2M;
import kr.re.keti.iot.mdao.common.SequenceDao;
import kr.re.keti.iot.mdao.common.SequenceDao.MovType;
import kr.re.keti.iot.mdao.common.SequenceDao.SEQ_PREFIX;
import kr.re.keti.iot.mdao.common.SequenceDao.SeqType;
import kr.re.keti.iot.mdao.oneM2M.AccessControlPolicyDao;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * accessControlPolicy management Service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Service
public class AccessControlPolicyService {

	private static final Log logger = LogFactory.getLog(AccessControlPolicyService.class);

	@Autowired
	private MongoLogService mongoLogService;

	@Autowired
	private AccessControlPolicyDao accessControlPolicyDao;
	
	@Autowired
	private SequenceDao seqDao;	
	
	/**
	 * generate resourceID
	 * @return
	 * @throws IotException
	 */
	private String generateResourceId() throws IotException {
		StringBuffer bufSeq = new StringBuffer(SEQ_PREFIX.ACCESS_CONTROL_POLICY.getValue());
		long longSeq = 0;
		
		try {
			
			longSeq = seqDao.move(MovType.UP, SeqType.ACCESS_CONTROL_POLICY);
			
			bufSeq.append(String.format("%010d", longSeq));
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.accessControlPolicy.id.createFail.text"));
		}
		
		return bufSeq.toString();
	}	
	
	/**
	 * find accessControlPolicy by resourceID
	 * @param resourceID
	 * @return
	 * @throws IotException
	 */
	public AccessControlPolicyVO findOneAccessControlPolicy(String resourceID) throws IotException {

		AccessControlPolicyVO findAccessControlPolicyItem = null;

		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(resourceID));

		try {

			findAccessControlPolicyItem = (AccessControlPolicyVO) accessControlPolicyDao.findOne(query);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.accessControlPolicy.findFail.text"));
		}

		return findAccessControlPolicyItem;
	}
	
	/**
	 * find accessControlPolicy by originators
	 * @param originators
	 * @return
	 * @throws IotException
	 */
	public AccessControlPolicyVO findOneAccessControlPolicyByOriginators(String originators) throws IotException {
		
		AccessControlPolicyVO findAccessControlPolicyItem = null;
		
		Query query = new Query();
		query.addCriteria(Criteria.where("privileges.accessControlOriginators").is(originators));
		
		try {
			
			findAccessControlPolicyItem = (AccessControlPolicyVO) accessControlPolicyDao.findOne(query);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.accessControlPolicy.findFail.text"));
		}
		
		return findAccessControlPolicyItem;
	}
	
	/**
	 * retrieve accessControlPolicy count by resourceID
	 * @param resourceID
	 * @return
	 */
	public long getCount(String resourceID){
		
		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(resourceID));
		
		long cnt = 0;
		
		try {
			cnt = accessControlPolicyDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "accessControlPolicy get count");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		return cnt;
	}
	
	/**
	 * create accessControlPolicy
	 * @param accessControlPolicyVo
	 * @return
	 * @throws IotException
	 */
	public AccessControlPolicyVO createAccessControlPolicy(AccessControlPolicyVO accessControlPolicyVo) throws IotException {
		
		AccessControlPolicyVO rtnAccessControlPolicy = null;
		
		boolean isError = false;
		String errMsg = "";
		
		if(CommonUtil.isEmpty(accessControlPolicyVo)){
			isError = true;
			errMsg = "<AccessControlPolicy> " + CommonUtil.getMessage("msg.input.empty.text");
		}
		else if(CommonUtil.isEmpty(accessControlPolicyVo.getPrivileges())){
			isError = true;
			errMsg = "Privileges " + CommonUtil.getMessage("msg.input.empty.text");
		}
		else if(CommonUtil.isEmpty(accessControlPolicyVo.getPrivileges().getAccessControlOriginators())){
			isError = true;
			errMsg = "AccessControlOriginators " + CommonUtil.getMessage("msg.input.empty.text");
		}

		if(isError){
			mongoLogService.log(logger, LEVEL.ERROR, errMsg);
			throw new IotException("600", errMsg);
		}
		
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		accessControlPolicyVo.setResourceType(ONEM2M.ACCESS_CONTROL_POLICY.getName());
		accessControlPolicyVo.setResourceID(this.generateResourceId());
		accessControlPolicyVo.setParentID(MOBIUS.NAME.getName());
		accessControlPolicyVo.setCreationTime(currentTime);
		accessControlPolicyVo.setLastModifiedTime(currentTime);
		
		try {
			
			accessControlPolicyDao.insert(accessControlPolicyVo);
			
			rtnAccessControlPolicy = this.findOneAccessControlPolicy(accessControlPolicyVo.getResourceID());
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "createAccessControlPolicy Exception : " + e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.accessControlPolicy.regFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "accessControlPolicy create success");

		return rtnAccessControlPolicy;
	}	
}