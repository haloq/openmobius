package kr.re.keti.iot.mdao.oneM2M;

import kr.re.keti.iot.domain.oneM2M.DeviceInfoVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao;

import org.springframework.stereotype.Repository;

/**
 * deviceInfo management Dao.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Repository
public class DeviceInfoDao extends GenericMongoDao {
	
	public DeviceInfoDao() {
		collectionName = ONEM2M.DEVICE_INFO.getName();
		cls = DeviceInfoVO.class;
	}
	
}
