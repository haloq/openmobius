package kr.re.keti.iot.mdao.common;

import java.util.List;

import kr.re.keti.iot.domain.oneM2M.ContainerOnlyIdVO;
import kr.re.keti.iot.domain.oneM2M.MgmtCmdOnlyIdVO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.mongodb.DBObject;

/**
 * GenericMongo management Dao.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Repository 
public class GenericMongoDao {
	
	protected String collectionName = null;
	protected Class cls = null;
	
	public enum ONEM2M{
		ACCESS_CONTROL_POLICY("accessControlPolicy"),
		CSE_BASE("CSEBase"),
		REMOTE_CSE("remoteCSE"),
		AE("AE"), 
		CONTAINER("container"),
		CONTENT_INSTANCE("contentInstance"), 
		LOCATION_POLICY("locationPolicy"),
		GROUP("mgroup"),
		SUBSCRIPTION("subscription"),
		MGMT_OBJ("mgmtObj"),
		MGMT_CMD("mgmtCmd"),  
		EXEC_INSTANCE("execInstance"),
		NODE("node"), 
		FIRMWARE("firmware"), 
		SOFTWARE("software"), 
		DEVICE_INFO("deviceInfo"),
		API_LOG("MMP_API_CALL_LOG_TBL"),
		SUBSCRIPTION_PENDING("MMP_SUBSCRIPTION_PENDING_TBL");
		
		private final String id; 
		ONEM2M(String id){
			this.id =id;
		}
		public String getName(){return id;}
	}
	
	public enum MOBIUS{
		NAME("mobius");
		private final String id; 
		MOBIUS(String id){
			this.id =id;
		}
		public String getName(){return id;}
	}
	
	@Autowired
	protected MongoTemplate mongoTemplate;
	
	/**
	 * upsert
	 * @param obj
	 */
	public void upsert(Object obj) {
		mongoTemplate.save(obj, this.collectionName);
	}

	/**
	 * insert
	 * @param obj
	 */
	public void insert(Object obj) {
		mongoTemplate.insert(obj, this.collectionName);
	}

	/**
	 * find
	 * @param query
	 * @return
	 */
	public List<?> find(Query query) {
		return  mongoTemplate.find(query, this.cls, this.collectionName);
	}

	/**
	 * findOne
	 * @param query
	 * @return
	 */
	public Object findOne(Query query) {
		return  mongoTemplate.findOne(query, this.cls, this.collectionName);
	}

	/**
	 * findAll
	 * @return
	 */
	public List<?> findAll() {
		return  mongoTemplate.findAll(this.cls, this.collectionName);
	}

	/**
	 * count
	 * @param query
	 * @return
	 */
	public long count(Query query) { 
		return mongoTemplate.count(query, this.collectionName);
	}

	/**
	 * update
	 * @param query
	 * @param update
	 */
	public void update(Query query, Update update) {
		mongoTemplate.upsert(query, update, collectionName);
	}

	/**
	 * remove
	 * @param query
	 */
	public void remove(Query query) {
		mongoTemplate.remove(query, this.collectionName);
	}

	/**
	 * remove
	 * @param obj
	 */
	public void remove(Object obj) {
		mongoTemplate.remove(obj, this.collectionName);
	}
	
	/**
	 * findAndModify
	 * @param query
	 * @param update
	 */
	public void findAndModify(Query query, Update update) {
		mongoTemplate.findAndModify(query, update, this.cls, this.collectionName);
	}
	
	/**
	 * findContainerOnlyId
	 * @param query
	 * @return
	 */
	public List<?> findContainerOnlyId(Query query) {
		return  mongoTemplate.find(query, ContainerOnlyIdVO.class, this.collectionName);
	}

	/**
	 * findMgmtCmdOnlyId
	 * @param query
	 * @return
	 */
	public List<?> findMgmtCmdOnlyId(Query query) {
		return  mongoTemplate.find(query, MgmtCmdOnlyIdVO.class, this.collectionName);
	}

	/**
	 * getConverter
	 * @param clazz
	 * @param dbObject
	 * @return
	 */
	public Object getConverter(Class<?> clazz, DBObject dbObject) {
		return mongoTemplate.getConverter().read(clazz, dbObject);
	}
	
}
