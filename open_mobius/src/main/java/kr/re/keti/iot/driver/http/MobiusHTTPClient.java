package kr.re.keti.iot.driver.http;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.springframework.web.bind.annotation.RequestMethod;

import kr.re.keti.iot.domain.common.IotException;

/**
 * HTTP client.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
public class MobiusHTTPClient {
	
	/**
	 * request
	 * @param methodType
	 * @param strUri
	 * @param headers
	 * @param body
	 * @return
	 * @throws IotException
	 */
	public int request(RequestMethod methodType, String strUri, Map<String, String> headers, String body) throws IotException {
		URI uri = null;
		int statusCode = -1;
		
		try {
			uri = new URI(strUri);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		if (RequestMethod.GET.equals(methodType)) {
			statusCode = getMethod(uri, headers, body);
		} else if (RequestMethod.POST.equals(methodType)) {
			statusCode = postMethod(uri, headers, body);
		} else if (RequestMethod.PUT.equals(methodType)) {
			statusCode = putMethod(uri, headers, body);
		} else if (RequestMethod.DELETE.equals(methodType)) {
			statusCode = deleteMethod(uri, headers, body);
		} else {
			
		}
		
		return statusCode;
	}
	
	/**
	 * get send
	 * @param uri
	 * @param headers
	 * @param payloadString
	 * @return
	 */
	private int getMethod(URI uri, Map<String, String> headers, String payloadString) {
		int statusCode = -1;
		int newTimeoutInMilliseconds = 3000;
		
		HttpClient client = new HttpClient();
		client.setTimeout(newTimeoutInMilliseconds);
		GetMethod method = new GetMethod(uri.toString());
		method.setRequestHeader("Accept", "application/xml");
		method.setRequestHeader("Content-Type", "application/xml; charset=UTF-8");
		
		if (headers != null) {
			for( Map.Entry<String, String> header : headers.entrySet() ){
				//System.out.println("header.getKey : " + header.getKey() + ", header.getValue" + header.getValue());
				method.setRequestHeader(header.getKey(), header.getValue());
	        }
		}
		
		try {
			statusCode = client.executeMethod(method);
	        
			String resBody = method.getResponseBodyAsString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return statusCode;
	}
	
	/**
	 * post send
	 * @param uri
	 * @param headers
	 * @param payloadString
	 * @return
	 */
	private int postMethod(URI uri, Map<String, String> headers, String payloadString) {
		int statusCode = -1;
		int newTimeoutInMilliseconds = 3000;
		
		HttpClient client = new HttpClient();
		client.setTimeout(newTimeoutInMilliseconds);
		PostMethod method = new PostMethod(uri.toString());
		method.setRequestHeader("Accept", "application/xml");
		method.setRequestHeader("Content-Type", "application/xml; charset=UTF-8");
		
		if (headers != null) {
			for( Map.Entry<String, String> header : headers.entrySet() ){
				//System.out.println("header.getKey : " + header.getKey() + ", header.getValue" + header.getValue());
				method.setRequestHeader(header.getKey(), header.getValue());
	        }
		}
		method.setRequestBody(payloadString);
		
		try {
			statusCode = client.executeMethod(method);
	        
			String resBody = method.getResponseBodyAsString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return statusCode;
	}
	
	/**
	 * put send
	 * @param uri
	 * @param headers
	 * @param payloadString
	 * @return
	 */
	private int putMethod(URI uri, Map<String, String> headers, String payloadString) {
		int statusCode = -1;
		int newTimeoutInMilliseconds = 3000;
		
		HttpClient client = new HttpClient();
		client.setTimeout(newTimeoutInMilliseconds);
		PutMethod method = new PutMethod(uri.toString());
		method.setRequestHeader("Accept", "application/xml");
		method.setRequestHeader("Content-Type", "application/xml; charset=UTF-8");
		
		if (headers != null) {
			for( Map.Entry<String, String> header : headers.entrySet() ){
				//System.out.println("header.getKey : " + header.getKey() + ", header.getValue" + header.getValue());
				method.setRequestHeader(header.getKey(), header.getValue());
	        }
		}
		
		method.setRequestBody(payloadString);
		
		try {
			statusCode = client.executeMethod(method);
	        
			String resBody = method.getResponseBodyAsString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return statusCode;
	}
	
	/**
	 * delete send
	 * @param uri
	 * @param headers
	 * @param payloadString
	 * @return
	 */
	private int deleteMethod(URI uri, Map<String, String> headers, String payloadString) {
		int statusCode = -1;
		int newTimeoutInMilliseconds = 3000;
		
		HttpClient client = new HttpClient();
		client.setTimeout(newTimeoutInMilliseconds);
		DeleteMethod method = new DeleteMethod(uri.toString());
		method.setRequestHeader("Accept", "application/xml");
		method.setRequestHeader("Content-Type", "application/xml; charset=UTF-8");
		
		if (headers != null) {
			for( Map.Entry<String, String> header : headers.entrySet() ){
				//System.out.println("header.getKey : " + header.getKey() + ", header.getValue" + header.getValue());
				method.setRequestHeader(header.getKey(), header.getValue());
	        }
		}
		
		try {
			statusCode = client.executeMethod(method);
	        
			String resBody = method.getResponseBodyAsString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return statusCode;
	}
}
