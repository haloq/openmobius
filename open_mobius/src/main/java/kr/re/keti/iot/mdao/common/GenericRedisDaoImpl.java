package kr.re.keti.iot.mdao.common;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import kr.re.keti.iot.domain.common.GenericModel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.SortParameters.Order;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SessionCallback;
import org.springframework.data.redis.core.query.SortQueryBuilder;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

/**
 * generic redis management Dao implement.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
public abstract class GenericRedisDaoImpl<E extends GenericModel> implements
		GenericRedisDao<E> {

	@Autowired
	protected RedisTemplate<String, String> template;
	
	private Class<E> entityClass;
	
	public GenericRedisDaoImpl() {
		
		ParameterizedType genericSuperclass = (ParameterizedType) getClass()
				.getGenericSuperclass();
		Type type = genericSuperclass.getActualTypeArguments()[0];
		if (type instanceof ParameterizedType) {
			this.entityClass = (Class) ((ParameterizedType) type).getRawType();
		} else {
			this.entityClass = (Class) type;
		}
	}
	
	/**
	 * put
	 * @param obj
	 * @return
	 */
	public String put(E obj) {
		return this.put(obj,entityClass.getSimpleName());
	}
	
	/**
	 * put
	 * @param obj
	 * @param collectionName
	 * @return
	 */
	public String put(E obj, String collectionName) {
		final String time = getTime();
		final String collection = collectionName;
		
		Gson gson = new Gson();
		Type ObjectType = new TypeToken<E>() {
		}.getType();
		final String json = gson.toJson(obj);
		
		SessionCallback<String> sessionCallback = new SessionCallback<String>() {
			public String execute(RedisOperations operations)
					throws DataAccessException {
				
				String size = getSequence();
				
				operations.multi();

				try {
					String key = time +"."+ size + "_" + collection;
					
					ListOperations<String, String> listOper = operations.opsForList();
					listOper.rightPush("timestamp", key);
					
					BoundValueOperations<String, String> valueOper = operations
							.boundValueOps(key);
					valueOper.set(json);
					
					operations.exec();
					
					return key;

				} catch (Exception e) {
					
					operations.discard();

					e.printStackTrace();

					return "fail";
				}
			}
		};
		return template.execute(sessionCallback);
	}
	
	/**
	 * delete
	 * @param id
	 */
	public void delete(String key) {
		
		template.delete(key);
		
		template.opsForList().remove("timestamp",1,key);
		if(key!=null || !"".equals(key)){
			template.opsForList().remove("timestamp",1,key);
		}
	}
	
	/**
	 * getObjects
	 * @return
	 */
	public List<E> getObjects() {
		return getObjects(0, 10);
	}
	
	/**
	 * getObjects
	 * @param offset
	 * @param count
	 * @return
	 */
	public List<E> getObjects(long offset, long count) {
		Gson gson = new Gson();
		List<E> objs = new ArrayList<E>();
		
		SortQueryBuilder<String> builder = SortQueryBuilder.sort("timestamp");
		builder.alphabetical(true);
		builder.order(Order.DESC);
		builder.limit(offset, count);
		List<String> keys = template.sort(builder.build());
		
		List<String> results = template.opsForValue().multiGet(keys);
		
		for (String item : results) {
			objs.add(gson.fromJson(item, entityClass));
		}
		return objs;
	}
	
	/**
	 * get
	 * @param key
	 * @return
	 */
	public E get(String key) {
		
		Gson gson = new Gson();
		return gson.fromJson((String) template.opsForValue().get(key),
				entityClass);
	}
	
	/**
	 * getSequence
	 * @return
	 */
	public String getSequence() {
		String cnt = "0";
		
		try {
			RedisSequenceScript script = new RedisSequenceScript();
			cnt = template.execute(script);
		} catch (Exception e) {
			System.out.println("timestamp list get count error [" + e.getMessage() + "]");
		}
		
		return cnt;
	}
	
	/**
	 * getTime
	 * @return
	 */
	public String getTime() {
		return String.valueOf(getTimestamp());
	}

	/**
	 * getTimestamp
	 * @return
	 */
	private long getTimestamp() {
		return System.currentTimeMillis();
	}

}
