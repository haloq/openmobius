package kr.re.keti.iot.service.oneM2M;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.ContainerOnlyIdVO;
import kr.re.keti.iot.domain.oneM2M.ContainerVO;
import kr.re.keti.iot.domain.oneM2M.ContentInstanceVO;
import kr.re.keti.iot.mdao.common.SequenceDao;
import kr.re.keti.iot.mdao.common.SequenceDao.MovType;
import kr.re.keti.iot.mdao.common.SequenceDao.SEQ_PREFIX;
import kr.re.keti.iot.mdao.common.SequenceDao.SeqType;
import kr.re.keti.iot.mdao.oneM2M.ContainerDao;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.SubscriptionService.SUBSCRIPTION_RESOURCE_STATUS_TYPE;
import kr.re.keti.iot.util.CommonUtil;
import kr.re.keti.iot.util.oneM2M.CommonCode.RESOURCE_TYPE;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

/**
 * container management Service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>khlee</li>
 *         </ul>
 */
@Service
public class ContainerService {

	private static final Log logger = LogFactory.getLog(ContainerService.class);
	
	@Autowired
	private MongoLogService mongoLogService;
	
	@Autowired
	private ContentInstanceService contentInstanceService;
	
	@Autowired
	private SubscriptionService subscriptionService;
	
	@Autowired
	private SequenceDao seqDao;
	
	@Autowired
	private ContainerDao containerDao;
	
	/**
	 * find container by containerId
	 * @param containerId
	 * @return
	 * @throws IotException
	 */
	public ContainerVO findOneContainer(String containerId) throws IotException {
		return findOneContainer(null, containerId);
	}
	
	/**
	 * find container by parentID-containerId
	 * @param parentID
	 * @param containerId
	 * @return
	 * @throws IotException
	 */
	public ContainerVO findOneContainer(String parentID, String containerId) throws IotException {
		ContainerVO findContainerItem = null;
		
		Query query = new Query();
		if(!CommonUtil.isEmpty(parentID)) {
			query.addCriteria(Criteria.where("parentID").is(parentID));
		}
		
		query.addCriteria(Criteria.where("resourceID").is(containerId));
		try {
			findContainerItem = (ContainerVO)containerDao.findOne(query);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.container.findFail.text"));
		}
		
		return findContainerItem;
	}
	
	/**
	 * find container by parentID-labels
	 * @param parentID
	 * @param labels
	 * @return
	 * @throws IotException
	 */
	public ContainerVO findOneContainerByLabels(String parentID, String labels) throws IotException {
		ContainerVO findContainerItem = null;
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(parentID));
		query.addCriteria(Criteria.where("labels").is(labels));
		
		try {
			findContainerItem = (ContainerVO)containerDao.findOne(query);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.container.findFail.text"));
		}
		
		return findContainerItem;
	}
	
	/**
	 * find container by key-value
	 * @param key
	 * @param value
	 * @return
	 * @throws IotException
	 */
	public List<ContainerVO> findContainer(String key, String value) throws IotException {
		List<ContainerVO> findContainerList = null;
		
		Query query = new Query();
		query.addCriteria(Criteria.where(key).is(value));
		
		try {
			findContainerList = (List<ContainerVO>)containerDao.find(query);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.container.findFail.text"));
		}
		
		return findContainerList;
	}
	
	/**
	 * find containerOnlyId by key-value
	 * @param key
	 * @param value
	 * @return
	 * @throws IotException
	 */
	public List<ContainerOnlyIdVO> findContainerOnlyId(String key, String value) throws IotException {
		List<ContainerOnlyIdVO> findContainerList = null;
		
		Query query = new Query();
		query.addCriteria(Criteria.where(key).is(value));
		
		try {
			findContainerList = (List<ContainerOnlyIdVO>)containerDao.findContainerOnlyId(query);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.container.findFail.text"));
		}
		
		return findContainerList;
	}
	
	/**
	 * create container
	 * @param containerProfile
	 * @return
	 * @throws IotException
	 */
	public ContainerVO createContainer(ContainerVO containerProfile) throws IotException {
		ContainerVO cntrItem = new ContainerVO();
		
		try {
			Long containerId = seqDao.move(MovType.UP,SeqType.CONTAINER);
			cntrItem.setResourceID(SEQ_PREFIX.CONTAINER.getValue() + String.format("%010d", containerId));
			
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.container.id.createFail.text"));
		}
		
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");

		cntrItem.setParentID(containerProfile.getParentID());
		cntrItem.setResourceType(RESOURCE_TYPE.CONTAINER.getName());
		cntrItem.setLabels(containerProfile.getLabels());
		cntrItem.setAccessControlPolicyIDs(containerProfile.getAccessControlPolicyIDs());
		cntrItem.setCreator(containerProfile.getCreator());
		cntrItem.setMaxNrOfInstances("-1");
		cntrItem.setMaxByteSize("-1");
		cntrItem.setMaxInstanceAge("-1");
		cntrItem.setCurrentNrOfInstances("0");
		cntrItem.setCurrentByteSize("0");
		cntrItem.setStateTag("0");
		cntrItem.setCreationTime(currentTime);
		cntrItem.setLastModifiedTime(currentTime);
		
		cntrItem.setUploadCondition(containerProfile.getUploadCondition());
		cntrItem.setUploadConditionValue(containerProfile.getUploadConditionValue());
		cntrItem.setContainerType(containerProfile.getContainerType());
		cntrItem.setHeartbeatPeriod(containerProfile.getHeartbeatPeriod());
		
		try { 
			containerDao.insert(cntrItem);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.container.createFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "container create success");
		
		
		ContainerVO insertContainerItem = this.findOneContainer(cntrItem.getParentID(), cntrItem.getResourceID());
			
		return insertContainerItem;
		
	}
	
	/**
	 * update container
	 * @param containerItem
	 * @return
	 * @throws IotException
	 */
	public ContainerVO updateContainer(ContainerVO containerItem) throws IotException {
			
		ContainerVO findContainerItem = null;
		if(CommonUtil.isEmpty((findContainerItem = this.findOneContainer(containerItem.getParentID(), containerItem.getResourceID())))) {
			throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text"));
		}
		String stateTag 				= String.valueOf(Integer.parseInt(findContainerItem.getStateTag()) + 1);
		String uploadCondition 			= containerItem.getUploadCondition();
		String uploadConditionValue 	= containerItem.getUploadConditionValue();
		String containerType 			= containerItem.getContainerType();
		String heartbeatPeriod 			= containerItem.getHeartbeatPeriod();
		String currentTime 				= CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		Update update = new Update();
		if(!CommonUtil.isNull(stateTag))				update.set("stateTag", stateTag);
		if(!CommonUtil.isNull(uploadCondition))			update.set("uploadCondition", uploadCondition);
		if(!CommonUtil.isNull(uploadConditionValue))	update.set("uploadConditionValue", uploadConditionValue);
		if(!CommonUtil.isNull(containerType))			update.set("containerType", containerType);
		if(!CommonUtil.isNull(heartbeatPeriod))			update.set("heartbeatPeriod", heartbeatPeriod);
														update.set("lastModifiedTime", currentTime);
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(containerItem.getParentID()));
		query.addCriteria(Criteria.where("resourceID").is(containerItem.getResourceID()));
		try { 
			
			containerDao.update(query, update);
			
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.container.upFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "container update success");
		
		subscriptionService.sendSubscription(containerItem.getParentID(), containerItem.getResourceID(), SUBSCRIPTION_RESOURCE_STATUS_TYPE.UPDATED, ContainerVO.class, update);
		
		return this.findOneContainer(containerItem.getParentID(), containerItem.getResourceID());
	}
	
	/**
	 * update containerLocationID
	 * @param containerItem
	 * @throws IotException
	 */
	public void updateContainerLocationPolicy(ContainerVO containerItem) throws IotException {
		
		if(CommonUtil.isEmpty(this.findOneContainer(containerItem.getResourceID()))) {
			throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text"));
		}
		
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
			
		Update update = new Update();
		update.set("locationID", containerItem.getLocationID());
		update.set("lastModifiedTime", currentTime);
		
		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(containerItem.getResourceID()));
		try { 
			
			containerDao.update(query, update);
			
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.container.upFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "container locationID update success");
		
	}
	
	/**
	 * delete container by parentID-containerId
	 * @param parentID
	 * @param containerId
	 * @throws IotException
	 */
	public void deleteContainer(String parentID, String containerId) throws IotException {
		
		ContainerVO findContainerItem = this.findOneContainer(containerId);
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(parentID));
		query.addCriteria(Criteria.where("resourceID").is(containerId));
		
		try {
			
			contentInstanceService.deleteRedisObjectsByContainerId(containerId);
			
			contentInstanceService.deleteContentInstanceByContainerId(containerId);
			
			containerDao.remove(query);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.container.delFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "container delete success");
		
		subscriptionService.sendSubscription(findContainerItem.getParentID(), findContainerItem.getResourceID(), SUBSCRIPTION_RESOURCE_STATUS_TYPE.DELETED, ContainerVO.class, findContainerItem);

	}
	
	/**
	 * delete container by remoteCSEID
	 * @param remoteCSEID
	 * @throws IotException
	 */
	public void deleteContainerByRemoteCSEID(String remoteCSEID) throws IotException {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(remoteCSEID));
		
		try {
			List<ContainerVO> findContainerList = this.findContainer("parentID", remoteCSEID);
			for(ContainerVO containerItem : findContainerList) {
				this.deleteContainer(containerItem.getParentID(), containerItem.getResourceID());
			}
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.container.delFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "container delete success");
		
	}
	
	/**
	 * update ContainerCurrentInstanceValue by contentInstanceList
	 * @param contentInstanceList
	 * @throws Exception
	 */
	public void updateContainerCurrentInstanceValue(List<ContentInstanceVO> contentInstanceList) throws Exception {
		
		List<ContentInstanceVO> newContentInstanceList = new ArrayList<ContentInstanceVO>();
		
		if(!CommonUtil.isEmpty(contentInstanceList) && contentInstanceList.size() > 0) {
			
			Map<String, ContentInstanceVO> listMap = new LinkedHashMap<String, ContentInstanceVO>();
			for(ContentInstanceVO contentInstance : contentInstanceList) {
				if(!listMap.containsKey(contentInstance.getParentID())) {
					listMap.put(contentInstance.getParentID(), contentInstance);
				}
			}
			newContentInstanceList = new ArrayList<ContentInstanceVO>(listMap.values());
			
		}
		
		if(!CommonUtil.isEmpty(newContentInstanceList) && newContentInstanceList.size() > 0) {
			
			for(ContentInstanceVO newContentInstance : newContentInstanceList) {
				try {
					updateContainerCurrentInstanceValue(newContentInstance.getParentID());
				} catch (Exception e) {
					mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
					throw new IotException("600", CommonUtil.getMessage("msg.device.container.upFail.text"));
				}				
			}
		}
		
	}
	
	/**
	 * update ContainerCurrentInstanceValue by containerID
	 * @param containerID
	 * @throws Exception
	 */
	public void updateContainerCurrentInstanceValue(String containerID) throws Exception {
		
		long   instanceCnt = 0;
		long   currentByte = 0;
		
		ContainerVO containerItem = this.findOneContainer(containerID);
		
		containerItem.setCurrentNrOfInstances("");
		containerItem.setCurrentByteSize("");
		containerItem.setLatest("");
		
		List<ContentInstanceVO> findContentInstanceList = contentInstanceService.findContentInstance(containerID);
		
		for(ContentInstanceVO contentInstanceItem : findContentInstanceList) {
			instanceCnt++;
			currentByte = currentByte + CommonUtil.valueOf(contentInstanceItem.getContentSize());	
		}
		if(!CommonUtil.isEmpty(findContentInstanceList) && findContentInstanceList.size() > 0) {
			containerItem.setLatest(findContentInstanceList.get(findContentInstanceList.size()-1).getResourceID());
			containerItem.setCurrentNrOfInstances(String.valueOf(instanceCnt));
			containerItem.setCurrentByteSize(String.valueOf(currentByte));
		}
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(containerItem.getParentID()));
		query.addCriteria(Criteria.where("resourceID").is(containerItem.getResourceID()));
		
		Update update = new Update();
		try {
			String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
			
			update.set("currentNrOfInstances",  containerItem.getCurrentNrOfInstances());
			update.set("currentByteSize",		containerItem.getCurrentByteSize());
			update.set("latest",				containerItem.getLatest());
			update.set("lastModifiedTime", 		currentTime);
			
			containerDao.update(query, update);
			
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.container.upFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "container currentInstanceValue update success");
		
		//subscriptionService.sendSubscription(containerItem.getParentID(), containerItem.getResourceID(), SUBSCRIPTION_RESOURCE_STATUS_TYPE.UPDATED, ContainerVO.class, update);
			
	}
}