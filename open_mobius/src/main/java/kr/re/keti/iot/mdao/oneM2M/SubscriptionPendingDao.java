package kr.re.keti.iot.mdao.oneM2M;

import kr.re.keti.iot.domain.oneM2M.SubscriptionPendingVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao;

import org.springframework.stereotype.Repository;

/**
 * subscription pending management Dao.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Repository
public class SubscriptionPendingDao extends GenericMongoDao {
	
	public SubscriptionPendingDao() {
		collectionName = ONEM2M.SUBSCRIPTION_PENDING.getName();
		cls = SubscriptionPendingVO.class;
	}
}
