package kr.re.keti.iot.domain.oneM2M;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import kr.re.keti.common.base.BaseVO;

/**
 * execInstance domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@XmlRootElement(name="execInstance", namespace="http://www.onem2m.org/xml/protocols")
@XmlAccessorType(value=XmlAccessType.FIELD)
public class ExecInstanceVO extends BaseVO {
	private static final long serialVersionUID = 1L;

	private String resourceType;
	private String resourceID;
	private String parentID;
	private String creationTime;
	private String lastModifiedTime;
	private String expirationTime;
	private String accessControlPolicyIDs;
	private String execStatus;
	private String execResult;
	private String execDisable;
	private String execTarget;
	private String execMode;
	private String execFrequency;
	private String execDelay;
	private String execNumber;
	private String execReqArgs;
	
	@XmlTransient
	private Date expirationDate;

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceID() {
		return resourceID;
	}

	public void setResourceID(String resourceID) {
		this.resourceID = resourceID;
	}

	public String getParentID() {
		return parentID;
	}

	public void setParentID(String parentID) {
		this.parentID = parentID;
	}

	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	public String getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(String lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public String getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getAccessControlPolicyIDs() {
		return accessControlPolicyIDs;
	}

	public void setAccessControlPolicyIDs(String accessControlPolicyIDs) {
		this.accessControlPolicyIDs = accessControlPolicyIDs;
	}

	public String getExecStatus() {
		return execStatus;
	}

	public void setExecStatus(String execStatus) {
		this.execStatus = execStatus;
	}

	public String getExecResult() {
		return execResult;
	}

	public void setExecResult(String execResult) {
		this.execResult = execResult;
	}

	public String getExecDisable() {
		return execDisable;
	}

	public void setExecDisable(String execDisable) {
		this.execDisable = execDisable;
	}

	public String getExecTarget() {
		return execTarget;
	}

	public void setExecTarget(String execTarget) {
		this.execTarget = execTarget;
	}

	public String getExecMode() {
		return execMode;
	}

	public void setExecMode(String execMode) {
		this.execMode = execMode;
	}

	public String getExecFrequency() {
		return execFrequency;
	}

	public void setExecFrequency(String execFrequency) {
		this.execFrequency = execFrequency;
	}

	public String getExecDelay() {
		return execDelay;
	}

	public void setExecDelay(String execDelay) {
		this.execDelay = execDelay;
	}

	public String getExecNumber() {
		return execNumber;
	}

	public void setExecNumber(String execNumber) {
		this.execNumber = execNumber;
	}

	public String getExecReqArgs() {
		return execReqArgs;
	}

	public void setExecReqArgs(String execReqArgs) {
		this.execReqArgs = execReqArgs;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	@Override
	public String toString() {
		return "ExecInstanceVO [resourceType=" + resourceType + ", resourceID="
				+ resourceID + ", parentID=" + parentID + ", creationTime="
				+ creationTime + ", lastModifiedTime=" + lastModifiedTime
				+ ", expirationTime=" + expirationTime
				+ ", accessControlPolicyIDs=" + accessControlPolicyIDs
				+ ", execStatus=" + execStatus + ", execResult=" + execResult
				+ ", execDisable=" + execDisable + ", execTarget=" + execTarget
				+ ", execMode=" + execMode + ", execFrequency=" + execFrequency
				+ ", execDelay=" + execDelay + ", execNumber=" + execNumber
				+ ", execReqArgs=" + execReqArgs + ", expirationDate="
				+ expirationDate + "]";
	}

}
