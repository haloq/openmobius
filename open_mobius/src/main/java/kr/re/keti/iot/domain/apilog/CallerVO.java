package kr.re.keti.iot.domain.apilog;

import kr.re.keti.iot.service.apilog.ApiLogService.CALLER_FG;

/**
 * Caller domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
public class CallerVO {
	private CALLER_FG callerFg;
	private String callerId;
	private String userId;
	private String topic;
	
	public CALLER_FG getCallerFg() {
		return callerFg;
	}
	public void setCallerFg(CALLER_FG callerFg) {
		this.callerFg = callerFg;
	}
	public String getCallerId() {
		return callerId;
	}
	public void setCallerId(String callerId) {
		this.callerId = callerId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	@Override
	public String toString() {
		return "CallerVO [callerFg=" + callerFg + ", callerId=" + callerId
				+ ", userId=" + userId + ", topic=" + topic + "]";
	}
}