package kr.re.keti.iot.domain.common;

import kr.re.keti.common.base.BaseVO;

/**
 * CertKey domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
public class CertKeyVO extends BaseVO  {

	private static final long serialVersionUID = 2465484604492996598L;
	
	private String cert_type;
	private String cert_id;
	private String cert_client_id;
	private String cert_public_key;
	private String cert_private_key;
	private String expire_date;
	
	private String target_uri;
	private String access_token;
	
	private String result_code;
	private String result_msg;
	
	private String OU;
	private String CN;
	private String email;
	private String grant_type;
	private String assertion;
	private String token_signature;
	
	public Long errno = null;
	private String errmsg;
	private String cid;
	private String pri;
	private String pub;
	private String token;
	private String res;
	private String info;
	private String expire;
	
	
	public String getCert_type() {
		return cert_type;
	}



	public void setCert_type(String cert_type) {
		this.cert_type = cert_type;
	}



	public String getCert_id() {
		return cert_id;
	}



	public void setCert_id(String cert_id) {
		this.cert_id = cert_id;
	}



	public String getCert_client_id() {
		return cert_client_id;
	}



	public void setCert_client_id(String cert_client_id) {
		this.cert_client_id = cert_client_id;
	}



	public String getCert_public_key() {
		return cert_public_key;
	}



	public void setCert_public_key(String cert_public_key) {
		this.cert_public_key = cert_public_key;
	}



	public String getCert_private_key() {
		return cert_private_key;
	}



	public void setCert_private_key(String cert_private_key) {
		this.cert_private_key = cert_private_key;
	}



	public String getTarget_uri() {
		return target_uri;
	}



	public void setTarget_uri(String target_uri) {
		this.target_uri = target_uri;
	}




	public String getResult_code() {
		return result_code;
	}



	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}



	public String getResult_msg() {
		return result_msg;
	}



	public void setResult_msg(String result_msg) {
		this.result_msg = result_msg;
	}



	public String getExpire_date() {
		return expire_date;
	}



	public void setExpire_date(String expire_date) {
		this.expire_date = expire_date;
	}



	
	public String getErrmsg() {
		return errmsg;
	}



	public void setErrmsg(String errmsg) {
		this.errmsg = errmsg;
	}



	public String getCid() {
		return cid;
	}



	public void setCid(String cid) {
		this.cid = cid;
	}



	public String getPri() {
		return pri;
	}



	public void setPri(String pri) {
		this.pri = pri;
	}



	public String getPub() {
		return pub;
	}



	public void setPub(String pub) {
		this.pub = pub;
	}



	public Long getErrno() {
		return errno;
	}



	public void setErrno(Long errno) {
		this.errno = errno;
	}



	public String getOU() {
		return OU;
	}



	public void setOU(String oU) {
		OU = oU;
	}



	public String getCN() {
		return CN;
	}



	public void setCN(String cN) {
		CN = cN;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public String getAssertion() {
		return assertion;
	}



	public void setAssertion(String assertion) {
		this.assertion = assertion;
	}



	public String getGrant_type() {
		return grant_type;
	}



	public void setGrant_type(String grant_type) {
		this.grant_type = grant_type;
	}



	public String getToken() {
		return token;
	}



	public void setToken(String token) {
		this.token = token;
	}






	public String getAccess_token() {
		return access_token;
	}



	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}



	public String getRes() {
		return res;
	}



	public void setRes(String res) {
		this.res = res;
	}



	public String getInfo() {
		return info;
	}



	public void setInfo(String info) {
		this.info = info;
	}



	public String getExpire() {
		return expire;
	}



	public void setExpire(String expire) {
		this.expire = expire;
	}



	public String getToken_signature() {
		return token_signature;
	}



	public void setToken_signature(String token_signature) {
		this.token_signature = token_signature;
	}



	
	
}
