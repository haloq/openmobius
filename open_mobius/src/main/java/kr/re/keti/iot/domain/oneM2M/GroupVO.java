package kr.re.keti.iot.domain.oneM2M;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import kr.re.keti.common.base.BaseVO;

/**
 * group domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@XmlRootElement(name="group", namespace="http://www.onem2m.org/xml/protocols")
@XmlAccessorType(value=XmlAccessType.FIELD)
public class GroupVO extends BaseVO {
	private static final long serialVersionUID = 1L;
	
	private String resourceType;
	private String resourceID;
	private String parentID;
	private String creationTime;
	private String lastModifiedTime;
	private String expirationTime;
	private String accessControlPolicyIDs;
	private String labels;
	private String creator;
	private String memberType;
	private String currentNrOfMembers;
	private String maxNrOfMembers;
	private String membersList;
	private String membersAccessControlPolicyIDs;
	private String groupName;

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceID() {
		return resourceID;
	}

	public void setResourceID(String resourceID) {
		this.resourceID = resourceID;
	}

	public String getParentID() {
		return parentID;
	}

	public void setParentID(String parentID) {
		this.parentID = parentID;
	}

	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	public String getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(String lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public String getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getAccessControlPolicyIDs() {
		return accessControlPolicyIDs;
	}

	public void setAccessControlPolicyIDs(String accessControlPolicyIDs) {
		this.accessControlPolicyIDs = accessControlPolicyIDs;
	}

	public String getLabels() {
		return labels;
	}

	public void setLabels(String labels) {
		this.labels = labels;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getMemberType() {
		return memberType;
	}

	public void setMemberType(String memberType) {
		this.memberType = memberType;
	}

	public String getCurrentNrOfMembers() {
		return currentNrOfMembers;
	}

	public void setCurrentNrOfMembers(String currentNrOfMembers) {
		this.currentNrOfMembers = currentNrOfMembers;
	}

	public String getMaxNrOfMembers() {
		return maxNrOfMembers;
	}

	public void setMaxNrOfMembers(String maxNrOfMembers) {
		this.maxNrOfMembers = maxNrOfMembers;
	}

	public String getMembersList() {
		return membersList;
	}

	public void setMembersList(String membersList) {
		this.membersList = membersList;
	}

	public String getMembersAccessControlPolicyIDs() {
		return membersAccessControlPolicyIDs;
	}

	public void setMembersAccessControlPolicyIDs(
			String membersAccessControlPolicyIDs) {
		this.membersAccessControlPolicyIDs = membersAccessControlPolicyIDs;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	@Override
	public String toString() {
		return "GroupVO [resourceType=" + resourceType + ", resourceID="
				+ resourceID + ", parentID=" + parentID + ", creationTime="
				+ creationTime + ", lastModifiedTime=" + lastModifiedTime
				+ ", expirationTime=" + expirationTime
				+ ", accessControlPolicyIDs=" + accessControlPolicyIDs
				+ ", labels=" + labels + ", creator=" + creator
				+ ", memberType=" + memberType + ", currentNrOfMembers="
				+ currentNrOfMembers + ", maxNrOfMembers=" + maxNrOfMembers
				+ ", membersList=" + membersList
				+ ", membersAccessControlPolicyIDs="
				+ membersAccessControlPolicyIDs + ", groupName=" + groupName
				+ "]";
	}

}
