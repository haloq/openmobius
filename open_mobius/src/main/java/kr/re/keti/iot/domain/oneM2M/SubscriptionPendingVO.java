package kr.re.keti.iot.domain.oneM2M;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import kr.re.keti.common.base.BaseVO;

/**
 * subscriptionPending domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@XmlRootElement(name="subscriptionPending", namespace="http://www.onem2m.org/xml/protocols")
@XmlAccessorType(value=XmlAccessType.FIELD)
public class SubscriptionPendingVO extends BaseVO {
	private static final long serialVersionUID = 1L;

	private String padingKey;
	private String notificationUri;
	private String content;
	private String regDt;
	
	@XmlTransient
	private Date expirationDate;
	
	public String getPadingKey() {
		return padingKey;
	}
	public void setPadingKey(String padingKey) {
		this.padingKey = padingKey;
	}
	public String getNotificationUri() {
		return notificationUri;
	}
	public void setNotificationUri(String notificationUri) {
		this.notificationUri = notificationUri;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	
	public Date getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}
	
	@Override
	public String toString() {
		return "SubscriptionPandingVO [padingKey=" + padingKey + ", notificationUri=" + notificationUri + ", content=" + content + ", regDt=" + regDt + "]";
	}
	
}