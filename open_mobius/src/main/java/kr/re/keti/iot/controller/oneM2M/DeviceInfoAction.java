package kr.re.keti.iot.controller.oneM2M;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.DeviceInfoVO;
import kr.re.keti.iot.domain.oneM2M.NodeVO;
import kr.re.keti.iot.domain.oneM2M.RemoteCSEVO;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.DeviceInfoService;
import kr.re.keti.iot.service.oneM2M.NodeService;
import kr.re.keti.iot.service.oneM2M.RemoteCSEService;
import kr.re.keti.iot.service.oneM2M.common.MCommonService;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * deviceInfo management Action.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Controller
public class DeviceInfoAction {

	private static final Log logger = LogFactory.getLog(DeviceInfoAction.class);

	@Autowired
	private MongoLogService mongoLogService;

	@Autowired
	private MCommonService mCommonService;

	@Autowired
	private RemoteCSEService remoteCSEService;

	@Autowired
	private NodeService nodeService;

	@Autowired
	private DeviceInfoService deviceInfoService;
	
	/**
	 * deviceInfo create
	 * @param nodeID
	 * @param labels
	 * @param deviceInfoProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/node-{nodeID:.*}", method = RequestMethod.POST, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" }, params = "ty=deviceInfo")
	@ResponseBody
	public ResponseEntity<Object> deviceInfoCreate(@PathVariable String nodeID
												 , @RequestParam(value = "nm", required = false) String labels
												 , @RequestBody DeviceInfoVO deviceInfoProfile
												 , HttpServletRequest request
												 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		DeviceInfoVO responseVo = new DeviceInfoVO();
		HttpStatus httpStatus = HttpStatus.CREATED;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey 	= CommonUtil.nvl(request.getHeader("dKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(nodeID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "nodeID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(deviceInfoProfile)) {
			isError = true;
			responseCode = "600";
			responseMsg = "<deviceInfo> " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus.OK);
		}

		try {
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSEByNodeLink(nodeID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			NodeVO nodeItem = null;
			if (CommonUtil.isEmpty((nodeItem = nodeService.findOneNode(nodeID)))) {
				throw new IotException("625", CommonUtil.getMessage("msg.device.node.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, null, dKey, nodeID);
			
			DeviceInfoVO deviceInfoItem = null;
			if (!CommonUtil.isEmpty(deviceInfoItem = deviceInfoService.findOneDeviceInfo("parentID", nodeID))) {
				//responseVo.setDeviceInfo(deviceInfoItem);
				BeanUtils.copyProperties(deviceInfoItem, responseVo);
				
				throw new IotException("609", CommonUtil.getMessage("msg.device.deviceInfo.duplicate.text", locale));
			}
			
			deviceInfoProfile.setLabels(labels);
			deviceInfoItem = deviceInfoService.createDeviceInfo(nodeItem, deviceInfoProfile);
			//responseVo.setDeviceInfo(deviceInfoItem);
			BeanUtils.copyProperties(deviceInfoItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);

		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * deviceInfo update
	 * @param nodeID
	 * @param deviceInfoID
	 * @param deviceInfoProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/node-{nodeID}/deviceInfo-{deviceInfoID}", method = RequestMethod.PUT, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> deviceInfoUpdate(@PathVariable String nodeID
												 , @PathVariable String deviceInfoID
												 , @RequestBody DeviceInfoVO deviceInfoProfile
												 , HttpServletRequest request
												 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		DeviceInfoVO responseVo = new DeviceInfoVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey 	= CommonUtil.nvl(request.getHeader("dKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(nodeID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "nodeID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(deviceInfoID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "deviceInfoID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(deviceInfoProfile)) {
			isError = true;
			responseCode = "600";
			responseMsg = "<deviceInfo> " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}

		try {
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSEByNodeLink(nodeID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			if (CommonUtil.isEmpty(nodeService.findOneNode(nodeID))) {
				throw new IotException("625", CommonUtil.getMessage("msg.device.node.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, null, dKey, nodeID);
			
			DeviceInfoVO deviceInfoItem = null;
			if (CommonUtil.isEmpty((deviceInfoItem = deviceInfoService.findOneDeviceInfo("resourceID", deviceInfoID)))) {
				throw new IotException("630", CommonUtil.getMessage("msg.device.deviceInfo.noRegi.text", locale));
			}
			
			deviceInfoProfile.setResourceID(deviceInfoID);
			deviceInfoItem = deviceInfoService.updateDeviceInfo(deviceInfoProfile);
			//responseVo.setDeviceInfo(deviceInfoItem);
			BeanUtils.copyProperties(deviceInfoItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * deviceInfo delete
	 * @param nodeID
	 * @param deviceInfoID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/node-{nodeID}/deviceInfo-{deviceInfoID}", method = RequestMethod.DELETE, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Void> deviceInfoDelete(@PathVariable String nodeID
											   , @PathVariable String deviceInfoID
											   , HttpServletRequest request
											   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		Object responseVo = new Object();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey 	= CommonUtil.nvl(request.getHeader("dKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(nodeID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "nodeID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(deviceInfoID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "deviceInfoID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Void>(httpStatus);
		}

		try {
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSEByNodeLink(nodeID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			if (CommonUtil.isEmpty(nodeService.findOneNode(nodeID))) {
				throw new IotException("625", CommonUtil.getMessage("msg.device.node.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, null, dKey, nodeID);
			
			DeviceInfoVO deviceInfoItem = null;
			if (CommonUtil.isEmpty((deviceInfoItem = deviceInfoService.findOneDeviceInfo("resourceID", deviceInfoID)))) {
				throw new IotException("630", CommonUtil.getMessage("msg.device.deviceInfo.noRegi.text", locale));
			}
			
			deviceInfoService.deleteDeviceInfo("resourceID", deviceInfoID);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Void>(httpStatus);
	}
	
	/**
	 * deviceInfo retrieve
	 * @param nodeID
	 * @param deviceInfoID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/node-{nodeID}/deviceInfo-{deviceInfoID}", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> deviceInfoRetrieve(@PathVariable String nodeID
												   , @PathVariable String deviceInfoID
												   , HttpServletRequest request
												   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();
		
		DeviceInfoVO responseVo = new DeviceInfoVO();
		HttpStatus httpStatus = HttpStatus.OK;
		
		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey		= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic 	= CommonUtil.nvl(request.getHeader("topic"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(nodeID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "nodeID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(deviceInfoID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "deviceInfoID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);
			
			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			
			RemoteCSEVO findRemoteCse = null;
			if (CommonUtil.isEmpty(findRemoteCse = remoteCSEService.findOneRemoteCSEByNodeLink(nodeID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			if (CommonUtil.isEmpty(nodeService.findOneNode(nodeID))) {
				throw new IotException("625", CommonUtil.getMessage("msg.device.node.noRegi.text", locale));
			}
			
			DeviceInfoVO deviceInfoItem = null;
			if (CommonUtil.isEmpty((deviceInfoItem = deviceInfoService.findOneDeviceInfo("resourceID", deviceInfoID)))) {
				throw new IotException("630", CommonUtil.getMessage("msg.device.deviceInfo.noRegi.text", locale));
			}
			
			//responseVo.setDeviceInfo(deviceInfoItem);
			BeanUtils.copyProperties(deviceInfoItem, responseVo);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();
			
			httpStatus = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
}