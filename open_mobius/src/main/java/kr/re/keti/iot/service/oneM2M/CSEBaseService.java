package kr.re.keti.iot.service.oneM2M;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.CSEBaseVO;
import kr.re.keti.iot.mdao.oneM2M.CSEBaseDao;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * CSEBase management Service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Service
public class CSEBaseService {

	private static final Log logger = LogFactory.getLog(CSEBaseService.class);

	@Autowired
	private MongoLogService mongoLogService;

	@Autowired
	private CSEBaseDao cseBaseDao;

	/**
	 * find CSEBase
	 * @return
	 * @throws IotException
	 */
	public CSEBaseVO findOneCSEBase() throws IotException {

		CSEBaseVO findCSEBaseItem = null;

		Query query = new Query();

		try {

			findCSEBaseItem = (CSEBaseVO) cseBaseDao.findOne(query);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.findFail.text"));
		}

		return findCSEBaseItem;
	}
	
	/**
	 * find CSEBase count
	 * @return
	 */
	public long getCount(){
		
		Query query = new Query();
		
		long cnt = 0;
		
		try {
			cnt = cseBaseDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "CSEBase get count");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		return cnt;
	}
	
}