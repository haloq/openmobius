package kr.re.keti.iot.mdao.oneM2M;

import kr.re.keti.iot.domain.oneM2M.ExecInstanceVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao;

import org.springframework.stereotype.Repository;

/**
 * execInstance management Dao.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Repository
public class ExecInstanceDao extends GenericMongoDao {
	
	public ExecInstanceDao() {
		collectionName = ONEM2M.EXEC_INSTANCE.getName();
		cls = ExecInstanceVO.class;
	}
}
