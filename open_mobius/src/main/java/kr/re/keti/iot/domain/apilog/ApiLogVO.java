package kr.re.keti.iot.domain.apilog;

/**
 * ApiLog domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
public class ApiLogVO {
	private String logKey;
	private String callerFg;
	private String callerId;
	private String callType;
	private String successYn;
	private String regDate;
	
	public String getLogKey() {
		return logKey;
	}
	public void setLogKey(String logKey) {
		this.logKey = logKey;
	}
	public String getCallerFg() {
		return callerFg;
	}
	public void setCallerFg(String callerFg) {
		this.callerFg = callerFg;
	}
	public String getCallerId() {
		return callerId;
	}
	public void setCallerId(String callerId) {
		this.callerId = callerId;
	}
	public String getCallType() {
		return callType;
	}
	public void setCallType(String callType) {
		this.callType = callType;
	}
	public String getSuccessYn() {
		return successYn;
	}
	public void setSuccessYn(String successYn) {
		this.successYn = successYn;
	}
	public String getRegDate() {
		return regDate;
	}
	public void setRegDate(String regDate) {
		this.regDate = regDate;
	}
	@Override
	public String toString() {
		return "ApiLogVO [logKey=" + logKey + ", callerFg=" + callerFg
				+ ", callerId=" + callerId + ", callType=" + callType
				+ ", successYn=" + successYn + ", regDate=" + regDate + "]";
	}
}