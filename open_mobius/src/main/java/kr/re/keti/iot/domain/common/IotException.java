package kr.re.keti.iot.domain.common;

/**
 * IotException domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
public class IotException extends Exception{
	private String code = "200";
	private String message = "";
	
	public IotException(){};
	
	public IotException(String code, String msg){
		this.code = code;
		this.message = msg;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}	
}
