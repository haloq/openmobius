package kr.re.keti.iot.mdao.oneM2M;

import kr.re.keti.iot.domain.oneM2M.AEVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao;

import org.springframework.stereotype.Repository;

/**
 * AE management Dao.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Repository
public class AEDao extends GenericMongoDao {
	
	public AEDao() {
		collectionName = ONEM2M.AE.getName();
		cls = AEVO.class;
	}
	
}
