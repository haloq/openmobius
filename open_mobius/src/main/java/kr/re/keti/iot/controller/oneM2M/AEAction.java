package kr.re.keti.iot.controller.oneM2M;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.AEVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao.MOBIUS;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.AEService;
import kr.re.keti.iot.service.oneM2M.MgmtCmdService;
import kr.re.keti.iot.service.oneM2M.RemoteCSEService;
import kr.re.keti.iot.service.oneM2M.common.MCommonService;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import biz.source_code.base64Coder.Base64Coder;

/**
 * AE management Action.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Controller
public class AEAction {

	private static final Log logger = LogFactory.getLog(AEAction.class);
	
	@Autowired
	private MongoLogService mongoLogService;
	
	@Autowired
	private AEService aEService;
	
	@Autowired
	private RemoteCSEService remoteCSEService;
	
	@Autowired
	private MgmtCmdService mgmtCmdService;
	
	@Autowired
	private MCommonService mCommonService;
	
	
	/**
	 * AE create(Root child)
	 * @param labels
	 * @param aeProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius", method = RequestMethod.POST, produces = {"application/onem2m-resource+xml", "application/onem2m-resource+json"}, params = {"ty=AE"})
	@ResponseBody
	public ResponseEntity<Object> AECreate(@RequestParam(value = "nm", required = false) String labels
										 , @RequestBody AEVO aeProfile
										 , HttpServletRequest request
										 , HttpServletResponse response){
		
		HttpHeaders responseHeaders	= new HttpHeaders();
		AEVO responseVo	= new AEVO();
		HttpStatus httpStatus = HttpStatus.CREATED;
		
		String responseCode	= "200";
		String responseMsg	= "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		
		String aKey = "";
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(aeProfile)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<AE> " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(aeProfile.getAppId())){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<AE> app-ID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(aeProfile.getPointOfAccess())){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<AE> pointOfAccess " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}

		if(isError){
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);
			return new ResponseEntity<Object>(responseVo, responseHeaders, HttpStatus.OK);
		}
					
		try {
			
			String appId = aeProfile.getAppId();
			 
			mCommonService.checkPatternAppId(checkMccP, appId);
			
			AEVO findAEItem = null;
			
			if(CommonUtil.isEmpty(findAEItem = aEService.findOneAE(appId))){
				
				mgmtCmdService.getPointOfAccessList(aeProfile.getPointOfAccess());
				
				aKey = mCommonService.createAKey(checkMccP, appId);
				
				aeProfile.setaKey(aKey);
				aeProfile.setParentID(MOBIUS.NAME.getName());
				aeProfile.setLabels(labels);
				aeProfile.setName(labels);			
				
				findAEItem = aEService.createAE(aeProfile);
				
			} else {
				
				aKey = mCommonService.createAKey(checkMccP, appId);
				
				findAEItem.setaKey(aKey);
				findAEItem = aEService.updateAEAkey(findAEItem);
				
				responseCode = "609";
				responseMsg = CommonUtil.getMessage("msg.device.AE.duplicate.text", locale);
			}
			
			if (!CommonUtil.isEmpty(findAEItem.getaKey())) {
				findAEItem.setaKey(Base64Coder.encodeString(findAEItem.getaKey()));
			}
			//responseVo.setAe(findAEItem);
			BeanUtils.copyProperties(findAEItem, responseVo);
		
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();
			httpStatus = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	
	/**
	 * AE create (RemoteCSE child)
	 * @param remoteCSEID
	 * @param labels
	 * @param aeProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}", method = RequestMethod.POST, produces = {"application/onem2m-resource+xml", "application/onem2m-resource+json"}, params = {"ty=AE"})
	@ResponseBody
	public ResponseEntity<Object> remoteCSEAECreate(@PathVariable String remoteCSEID
												  , @RequestParam(value = "nm", required = false) String labels
												  , @RequestBody AEVO aeProfile
												  , HttpServletRequest request
												  , HttpServletResponse response){
		
		HttpHeaders responseHeaders	= new HttpHeaders();
		AEVO responseVo	= new AEVO();
		HttpStatus httpStatus = HttpStatus.CREATED;
		
		String responseCode	= "200";
		String responseMsg	= "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dkey 	= CommonUtil.nvl(request.getHeader("dKey"), "");	
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(remoteCSEID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "device-ID " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		else if(CommonUtil.isEmpty(aeProfile)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<AE> " + CommonUtil.getMessage("msg.input.empty.text", locale);	
			
		}
		else if(CommonUtil.isEmpty(aeProfile.getAppId())){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<AE> app-ID " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		else if(CommonUtil.isEmpty(aeProfile.getAeId())){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<AE> AE-ID " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		else if(CommonUtil.isEmpty(aeProfile.getPointOfAccess())){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<AE> pointOfAccess " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		else if(CommonUtil.isEmpty(aeProfile.getNodeLink())){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<AE> nodeLink " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		
		if(isError){
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);
			return new ResponseEntity<Object>(responseVo, responseHeaders, HttpStatus.OK);
		}
		
		try {
			 
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if(remoteCSEService.getCount(remoteCSEID) < 1){
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dkey);
			 
			mCommonService.checkPatternAppId(checkMccP, aeProfile.getAppId());
			
			AEVO findAEItem = null;
			if (!CommonUtil.isEmpty(findAEItem = aEService.findOneAE(aeProfile.getAppId()))) {
				
				if(!CommonUtil.isEmpty(findAEItem.getaKey())) {
					findAEItem.setaKey(Base64Coder.encodeString(findAEItem.getaKey()));
				}
				
				//responseVo.setAe(findAEItem);
				BeanUtils.copyProperties(findAEItem, responseVo);
				throw new IotException("609", CommonUtil.getMessage("msg.device.AE.duplicate.text", locale));
			}
			
			aeProfile.setParentID(remoteCSEID);
			aeProfile.setLabels(labels);
			aeProfile.setName(labels);
			
			findAEItem = aEService.createAE(aeProfile);
			
			//responseVo.setAe(findAEItem);
			BeanUtils.copyProperties(findAEItem, responseVo);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();
			httpStatus = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	
	
	/**
	 * AE delete (Root child)
	 * @param appID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/AE-{appID:.*}", method = RequestMethod.DELETE, produces = {"application/onem2m-resource+xml", "application/onem2m-resource+json"})

	@ResponseBody
	public ResponseEntity<Void> AEDelete(@PathVariable String appID
										 , HttpServletRequest request
										 , HttpServletResponse response){
		
		HttpHeaders responseHeaders = new HttpHeaders();
		Object responseVo = new Object();
		HttpStatus httpStatus = HttpStatus.OK;
		
		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String akey 	= CommonUtil.nvl(request.getHeader("aKey"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(appID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "app-ID " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		
		if(isError){
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);
			return new ResponseEntity<Void>(httpStatus);
		}
		
		try {
			 
			mCommonService.checkPatternAppId(checkMccP, appID);
			
			if(CommonUtil.isEmpty(aEService.findOneAE(appID))){
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			mCommonService.verifyAKey(checkMccP, appID, akey);
			
			aEService.deleteAE("appId", appID);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Void>(httpStatus);
	}
	
	
	/**
	 * AE retrieve (Root child)
	 * @param appID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/AE-{appID:.*}", method = RequestMethod.GET, produces = {"application/onem2m-resource+xml", "application/onem2m-resource+json"})
	
	@ResponseBody
	public ResponseEntity<Object> AERetrieve(@PathVariable String appID
										   , HttpServletRequest request
										   , HttpServletResponse response){
		
		HttpHeaders responseHeaders = new HttpHeaders();
		AEVO responseVo = new AEVO();
		HttpStatus httpStatus = HttpStatus.OK;
		
		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey		= CommonUtil.nvl(request.getHeader("uKey"), "");;
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(appID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "app-ID " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		
		if(isError){
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);
			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			 
			mCommonService.checkPatternAppId(checkMccP, appID);
			
			AEVO findAEItem = null;
			if(CommonUtil.isEmpty(findAEItem = aEService.findOneAE(appID))){
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			if (!CommonUtil.isEmpty(findAEItem.getaKey())) {
				
				findAEItem.setaKey(Base64Coder.encodeString(findAEItem.getaKey()));
			}
			//responseVo.setAe(findAEItem);
			BeanUtils.copyProperties(findAEItem, responseVo);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	
	
	/**
	 * AE delete (RemoteCSE child)
	 * @param remoteCSEID
	 * @param appID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/AE-{appID:.*}", method = RequestMethod.DELETE, produces = {"application/onem2m-resource+xml", "application/onem2m-resource+json"})
	@ResponseBody
	public ResponseEntity<Void> remoteCSEAEDelete(@PathVariable String remoteCSEID
												  , @PathVariable String appID
												  , HttpServletRequest request
												  , HttpServletResponse response){
		
		HttpHeaders responseHeaders = new HttpHeaders();
		Object responseVo = new Object();
		HttpStatus httpStatus = HttpStatus.OK;
		
		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dkey 	= CommonUtil.nvl(request.getHeader("dKey"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(remoteCSEID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		else if(CommonUtil.isEmpty(appID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "app-ID " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		
		if(isError){
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);
			return new ResponseEntity<Void>(httpStatus);
		}
		
		try {
			 
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if(remoteCSEService.getCount(remoteCSEID) < 1){
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dkey);
			 
			mCommonService.checkPatternAppId(checkMccP, appID);
			
			if(CommonUtil.isEmpty(aEService.findOneAEByRemotCSEID(remoteCSEID, appID))){
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			aEService.deleteAEByRemoteCSEID(remoteCSEID, appID);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Void>(httpStatus);
	}
	
	
	/**
	 * AE retrieve (RemoteCSE child)
	 * @param remoteCSEID
	 * @param appID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/AE-{appID:.*}", method = RequestMethod.GET, produces = {"application/onem2m-resource+xml", "application/onem2m-resource+json"})
	@ResponseBody
	public ResponseEntity<Object> remoteCSEAERetrieve(@PathVariable String remoteCSEID
													, @PathVariable String appID
													, HttpServletRequest request
													, HttpServletResponse response){
		
		HttpHeaders responseHeaders = new HttpHeaders();
		AEVO responseVo = new AEVO();
		HttpStatus httpStatus = HttpStatus.OK;
		
		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey		= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic 	= CommonUtil.nvl(request.getHeader("topic"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(remoteCSEID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		else if(CommonUtil.isEmpty(appID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "app-ID " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		
		if(isError){
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);
			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			 
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if(remoteCSEService.getCount(remoteCSEID) < 1){
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			 
			mCommonService.checkPatternAppId(checkMccP, appID);
			
			AEVO findAEItem = null;
			if(CommonUtil.isEmpty(findAEItem = aEService.findOneAEByRemotCSEID(remoteCSEID, appID))){
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			//responseVo.setAe(findAEItem);
			BeanUtils.copyProperties(findAEItem, responseVo);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	
	/**
	 * AE update(Root child)
	 * @param appID
	 * @param aeProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/AE-{appID:.*}", method = RequestMethod.PUT, produces = {"application/onem2m-resource+xml", "application/onem2m-resource+json"})
	@ResponseBody
	public ResponseEntity<Object> AEUpdate(@PathVariable String appID
										 , @RequestBody AEVO aeProfile
										 , HttpServletRequest request
										 , HttpServletResponse response){
		HttpHeaders responseHeaders = new HttpHeaders();
		AEVO responseVo = new AEVO();
		HttpStatus httpStatus = HttpStatus.OK;
		
		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String akey 	= CommonUtil.nvl(request.getHeader("aKey"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(appID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "app-ID " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		else if(CommonUtil.isEmpty(aeProfile)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<AE> " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		
		if(isError){
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);
			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}		
		
		try {
			 
			mCommonService.checkPatternAppId(checkMccP, appID);
			
			AEVO findAEItem = null;
			if(CommonUtil.isEmpty(findAEItem = aEService.findOneAE(appID))){
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			mCommonService.verifyAKey(checkMccP, appID, akey);
			
			mgmtCmdService.getPointOfAccessList(aeProfile.getPointOfAccess());
			
			findAEItem = aEService.updateAE(aeProfile, findAEItem);
			
			if (!CommonUtil.isEmpty(findAEItem.getaKey())) {
				findAEItem.setaKey(Base64Coder.encodeString(findAEItem.getaKey()));
			}
			//responseVo.setAe(findAEItem);
			BeanUtils.copyProperties(findAEItem, responseVo);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	
	
	/**
	 * AE update (RemoteCSE child)
	 * @param remoteCSEID
	 * @param appID
	 * @param aeProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/AE-{appID:.*}", method = RequestMethod.PUT, produces = {"application/onem2m-resource+xml", "application/onem2m-resource+json"})
	@ResponseBody
	public ResponseEntity<Object> remoteCSEAEUpdate(@PathVariable String remoteCSEID
												  , @PathVariable String appID
												  , @RequestBody AEVO aeProfile
												  , HttpServletRequest request
												  , HttpServletResponse response){
		HttpHeaders responseHeaders = new HttpHeaders();
		AEVO responseVo = new AEVO();
		HttpStatus httpStatus = HttpStatus.OK;
		
		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dkey 	= CommonUtil.nvl(request.getHeader("dKey"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(remoteCSEID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "remoteCSE-ID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(appID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "app-ID " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		else if(CommonUtil.isEmpty(aeProfile)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<AE> " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		
		if(isError){
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);
			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			 
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if(remoteCSEService.getCount(remoteCSEID) < 1){
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dkey);
			 
			mCommonService.checkPatternAppId(checkMccP, appID);
			
			
			AEVO findAEItem = null;
			if(CommonUtil.isEmpty(findAEItem = aEService.findOneAEByRemotCSEID(remoteCSEID, appID))){
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			mgmtCmdService.getPointOfAccessList(aeProfile.getPointOfAccess());
			
			findAEItem = aEService.updateAE(aeProfile, findAEItem);
			
			if(!CommonUtil.isEmpty(findAEItem.getaKey())) {
				findAEItem.setaKey(Base64Coder.encodeString(findAEItem.getaKey()));
			}
			//responseVo.setAe(findAEItem);
			BeanUtils.copyProperties(findAEItem, responseVo);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
}
