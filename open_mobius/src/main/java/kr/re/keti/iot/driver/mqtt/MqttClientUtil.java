package kr.re.keti.iot.driver.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;

/**
 * MQTT client util.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
public class MqttClientUtil implements MqttCallback {
	private String mqttClientId = MqttClient.generateClientId();
	private static String mqttServerUrl = "";
	private static String mqttTopicName = "";
	private static MqttClient mqc;
	
	public MqttClientUtil() throws Exception{
	}
	
	public MqttClientUtil(String mqttServerUrl, String mqttTopicName) throws Exception{
		this.mqc = MqttUtil.getMettClient(mqttServerUrl);
		subscribe(mqttTopicName);
		
		this.mqttServerUrl = mqttServerUrl;
		this.mqttTopicName = mqttTopicName;
	}
	
	/**
	 * subscribe
	 * @param mqttTopic
	 */
	public void subscribe(String mqttTopic) {
		try {
			this.mqttTopicName = mqttTopic;
			mqc.subscribe(this.mqttTopicName);
		} catch (MqttException e) {
			//System.out.println("[KETI MQTT Client] Subscribe Failed - " + mqttTopic);
			e.printStackTrace();
		}
		mqc.setCallback(this);
	}
	
	/**
	 * publish
	 * @param topic
	 * @param mgmtCmdName
	 * @param controlValue
	 * @throws Exception
	 */
	public void publishKetiPayload(String topic, String mgmtCmdName, String controlValue) throws Exception{
		MqttMessage msg = new MqttMessage();
		String payload = mgmtCmdName + "," + controlValue;
		msg.setPayload(payload.getBytes());
		try {
			mqc.publish(topic, msg);
			//System.out.println("[KETI MQTT Client] MQTT Topic \"" + topic + "\" Publish Payload = " + payload);
		} catch (MqttPersistenceException e) {
			//System.out.println("[KETI MQTT Client] Publish Failed - " + topic);
			e.printStackTrace();
		} catch (MqttException e) {
			//System.out.println("[KETI MQTT Client] Publish Failed - " + topic);
			e.printStackTrace();
		}
	}
	
	/**
	 * publishFullPayload
	 * @param topic
	 * @param payload
	 */
	public void publishFullPayload(String topic, String payload) {
		MqttMessage msg = new MqttMessage();
		msg.setPayload(payload.getBytes());
		try {
			mqc.publish(topic, msg);
			//System.out.println("[KETI MQTT Client] MQTT Topic \"" + topic + "\" Publish Payload = " + payload);
		} catch (MqttPersistenceException e) {
			//System.out.println("[KETI MQTT Client] Publish Failed - " + topic);
			e.printStackTrace();
		} catch (MqttException e) {
			//System.out.println("[KETI MQTT Client] Publish Failed - " + topic);
			e.printStackTrace();
		}
	}
	
	/**
	 * connectionLost
	 */
	@Override
	public void connectionLost(Throwable cause) {
		//System.out.println("[KETI MQTT Client] Disconnected from MQTT Server");
		try {
			while(!mqc.isConnected()){
				mqc.connect();
				//System.out.println("[KETI MQTT Client] Connection retry");
			}
			mqc.unsubscribe(this.mqttTopicName);
			mqc.subscribe(this.mqttTopicName);
		} catch (MqttSecurityException e) {
			e.printStackTrace();
		} catch (MqttException e) {
			e.printStackTrace();
		}
		
		//System.out.println("[KETI MQTT Client] Connected to Server - " + this.mqttServerUrl);
	}
	
	/**
	 * messageArrived
	 */
	@Override
	public void messageArrived(String topic, MqttMessage message) {
		//System.out.println("[KETI MQTT Client] MQTT Topic \"" + topic + "\" Subscription Payload = " + byteArrayToString(message.getPayload()));
	}
	
	/**
	 * deliveryComplete
	 */
	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		//System.out.println("[KETI MQTT Client] Message delivered successfully");
	}
	
	/**
	 * byteArrayToString
	 * @param byteArray
	 * @return
	 */
	public String byteArrayToString(byte[] byteArray)
	{
	    String toString = "";

	    for(int i = 0; i < byteArray.length; i++)
	    {
	        toString += (char)byteArray[i];
	    }

	    return toString;    
	}
}