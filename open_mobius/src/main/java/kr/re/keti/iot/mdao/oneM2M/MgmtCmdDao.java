package kr.re.keti.iot.mdao.oneM2M;

import kr.re.keti.iot.domain.oneM2M.MgmtCmdVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao;

import org.springframework.stereotype.Repository;

/**
 * mgmtCmd management Dao.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Repository
public class MgmtCmdDao extends GenericMongoDao {
	
	public MgmtCmdDao() {
		collectionName = ONEM2M.MGMT_CMD.getName();
		cls = MgmtCmdVO.class;
	}
	
}
