package kr.re.keti.iot.domain.oneM2M;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import kr.re.keti.common.base.BaseVO;

/**
 * CSEBase domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@XmlRootElement(name="CSEBase", namespace="http://www.onem2m.org/xml/protocols")
@XmlAccessorType(value=XmlAccessType.FIELD)
public class CSEBaseVO extends BaseVO {
	private static final long serialVersionUID = 1L;

	private String resourceType;
	private String creationTime;
	private String lastModifiedTime;
	private String accessControlPolicyIDs;
	private String labels;
	private String cseType;
	@XmlElement(name = "CSE-ID", required=true)
	private String cseId;
	private String supportedResourceType;
	private String pointOfAccess;
	private String nodeLink;
	
	public String getResourceType() {
		return resourceType;
	}
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}
	public String getLastModifiedTime() {
		return lastModifiedTime;
	}
	public void setLastModifiedTime(String lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}
	public String getAccessControlPolicyIDs() {
		return accessControlPolicyIDs;
	}
	public void setAccessControlPolicyIDs(String accessControlPolicyIDs) {
		this.accessControlPolicyIDs = accessControlPolicyIDs;
	}
	public String getLabels() {
		return labels;
	}
	public void setLabels(String labels) {
		this.labels = labels;
	}
	public String getCseType() {
		return cseType;
	}
	public void setCseType(String cseType) {
		this.cseType = cseType;
	}
	public String getCseId() {
		return cseId;
	}
	public void setCseId(String cseId) {
		this.cseId = cseId;
	}
	public String getSupportedResourceType() {
		return supportedResourceType;
	}
	public void setSupportedResourceType(String supportedResourceType) {
		this.supportedResourceType = supportedResourceType;
	}
	public String getPointOfAccess() {
		return pointOfAccess;
	}
	public void setPointOfAccess(String pointOfAccess) {
		this.pointOfAccess = pointOfAccess;
	}
	public String getNodeLink() {
		return nodeLink;
	}
	public void setNodeLink(String nodeLink) {
		this.nodeLink = nodeLink;
	}
	@Override
	public String toString() {
		return "CSEBaseVO [resourceType=" + resourceType + ", creationTime="
				+ creationTime + ", lastModifiedTime=" + lastModifiedTime
				+ ", accessControlPolicyIDs=" + accessControlPolicyIDs
				+ ", labels=" + labels + ", cseType=" + cseType + ", cseId="
				+ cseId + ", supportedResourceType=" + supportedResourceType
				+ ", pointOfAccess=" + pointOfAccess + ", nodeLink=" + nodeLink
				+ "]";
	}
}