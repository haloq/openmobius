package kr.re.keti.iot.mdao.oneM2M;

import org.springframework.stereotype.Repository;

import kr.re.keti.iot.domain.oneM2M.AccessControlPolicyVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao;

/**
 * accessControlPolicy management Dao.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Repository
public class AccessControlPolicyDao extends GenericMongoDao {
	
	public AccessControlPolicyDao() {
		collectionName = ONEM2M.ACCESS_CONTROL_POLICY.getName();
		cls = AccessControlPolicyVO.class;
	}
}
