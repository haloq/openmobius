package kr.re.keti.iot.mdao.log;

import java.util.List;

import kr.re.keti.iot.domain.mongo.LogVO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

/**
 * log management Dao.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Repository
public class LogDao {
	@Autowired
	private MongoTemplate mongoTemplate;
	
	private final String collectionName = "MMP_LOG_MSG_TBL";

	/**
	 * upsert
	 * @param item
	 * @throws Exception
	 */
	public void upsert(LogVO item) throws Exception {
		mongoTemplate.save(item,collectionName);
	}

	/**
	 * insert
	 * @param item
	 * @throws Exception
	 */
	public void insert(LogVO item) throws Exception {
		mongoTemplate.insert(item, collectionName);
	}

	/**
	 * find
	 * @param query
	 * @return
	 * @throws Exception
	 */
	public List<LogVO> find(Query query) throws Exception {
		return mongoTemplate.find(query, LogVO.class, collectionName);
	}

	/**
	 * findOne
	 * @param query
	 * @return
	 * @throws Exception
	 */
	public LogVO findOne(Query query) throws Exception {
		return mongoTemplate.findOne(query, LogVO.class, collectionName);
	}

	/**
	 * findAll
	 * @return
	 * @throws Exception
	 */
	public List<LogVO> findAll() throws Exception {
		return mongoTemplate.findAll(LogVO.class, collectionName);
	}

	/**
	 * count
	 * @param query
	 * @return
	 * @throws Exception
	 */
	public long count(Query query) throws Exception {
		return mongoTemplate.count(query, collectionName);
	}

	/**
	 * update
	 * @param query
	 * @param update
	 * @throws Exception
	 */
	public void update(Query query, Update update) throws Exception {
		mongoTemplate.updateMulti(query, update, collectionName);
	}

	/**
	 * remove
	 * @param query
	 * @throws Exception
	 */
	public void remove(Query query) throws Exception {
		mongoTemplate.remove(query,collectionName);
	}	
}
