package kr.re.keti.iot.mdao.common;


import kr.re.keti.iot.domain.mongo.Sequence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

/**
 * sequence management Dao.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Repository 
public class SequenceDao {
	
	public enum SeqType{ ACCESS_CONTROL_POLICY, AE, CONTAINER, CONTENT_INSTANCE, LOCATION_POLICY, SUBSCRIPTION, MGMT_OBJ, MGMT_CMD, EXEC_INSTANCE, NODE, MMP_API_CALL_LOG_TBL, MMP_LOG_MSG_TBL, MMP_VERT_CONTENT_TBL, VERT_DEVICE, VERT_CONTAINER, VERT_MGMTOBJ, VERT_TID, SOFTWARE, DEVICEINFO, FIRMWARE, SUBSCRIPTION_PENDING};
	public enum MovType{ UP , DOWN };
	
	public enum SEQ_PREFIX {
		
		CSE_BASE("CB"),
		NODE("ND"),
		FIRMWARE("FW"),
		SOFTWARE("SW"), 
		DEVICE_INFO("DI"), 
		ACCESS_CONTROL_POLICY("AP"),
		REMOTE_CSE("RC"),
		CONTAINER("CT"), 
		CONTENT_INSTANCE("CI"), 
		MGMT_CMD("MC"), 
		EXEC_INSTANCE("EI"), 
		AE("AE"), 
		SUBSCRIPTION("SS"), 
		LOCATION_POLICY("LP"), 
		GROUP(("GP")),
		MMP_API_CALL_LOG_TBL(("CL"))
		; 
		
		private final String id;
		
		SEQ_PREFIX(String id) {
			this.id = id;
		}
		
		public String getValue() {
			return id;
		}
		
	}
	
	@Autowired
	private MongoTemplate mongoTemplate;
	
	private final String COLLECTION_NAME = "MMP_SEQ_TBL";

	/**
	 * insert
	 * @param item
	 */
	public void insert(Sequence item) {
		mongoTemplate.insert(item, COLLECTION_NAME);
	}

	/**
	 * findOne
	 * @param query
	 * @return
	 */
	public Sequence findOne(Query query) {
		return mongoTemplate.findOne(query, Sequence.class, COLLECTION_NAME);
	}

	/**
	 * move
	 * @param m_type
	 * @param s_type
	 * @return
	 */
	public synchronized long move(MovType m_type, SeqType s_type) {
		long move = (m_type == MovType.UP) ? 1:-1;
		String type = s_type.toString();
		
		Query query = new Query();
		query.addCriteria(Criteria.where("type").is(type));
				
		Update update = new Update();
		update.inc("sequence", move);
		
		FindAndModifyOptions option = new FindAndModifyOptions();
		option.upsert(true);
		
		Sequence findObj = mongoTemplate.findAndModify(query, update, option , Sequence.class, COLLECTION_NAME);
		
		return findObj.getSequence();
	}
}
