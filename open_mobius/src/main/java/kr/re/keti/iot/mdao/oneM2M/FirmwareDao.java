package kr.re.keti.iot.mdao.oneM2M;

import kr.re.keti.iot.domain.oneM2M.FirmwareVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao;

import org.springframework.stereotype.Repository;

/**
 * firmware management Dao.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Repository
public class FirmwareDao extends GenericMongoDao {
	
	public FirmwareDao() {
		collectionName = ONEM2M.FIRMWARE.getName();
		cls = FirmwareVO.class;
	}
	
}
