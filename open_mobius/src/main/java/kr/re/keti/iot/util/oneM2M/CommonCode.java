package kr.re.keti.iot.util.oneM2M;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

/**
 * commonCode.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Component
public class CommonCode {

	private static final Log logger = LogFactory.getLog(CommonCode.class);
	

	public enum RESOURCE_TYPE{
		ACCESS_CONTROL_POLICY("1"),
		AE("2"), 
		CONTAINER("3"),
		CONTENT_INSTANCE("4"), 
		CSE_BASE("5"),
		EXEC_INSTANCE("8"),
		GROUP("10"),
		LOCATION_POLICY("11"),
		MGMT_CMD("13"),  
		MGMT_OBJ("14"),
		NODE("15"), 
		REMOTE_CSE("18"),
		SUBSCRIPTION("23");
		
		private final String id; 
		RESOURCE_TYPE(String id){
			this.id =id;
		}
		public String getName(){return id;}
	}
}
