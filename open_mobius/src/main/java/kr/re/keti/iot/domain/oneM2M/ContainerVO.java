package kr.re.keti.iot.domain.oneM2M;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import kr.re.keti.common.base.BaseVO;

/**
 * container domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@XmlRootElement(name="container", namespace="http://www.onem2m.org/xml/protocols")
@XmlAccessorType(value=XmlAccessType.FIELD)
public class ContainerVO extends BaseVO {
	private static final long serialVersionUID = 1L;

	private String resourceType;
	private String resourceID;
	private String parentID;
	private String creationTime;
	private String lastModifiedTime;
	private String expirationTime;
	private String accessControlPolicyIDs;
	private String labels;
	private String creator;
	private String stateTag;
	private String maxNrOfInstances;
	private String maxByteSize;
	private String maxInstanceAge;
	private String currentNrOfInstances;
	private String currentByteSize;
	private String latest;
	private String locationID;
	private String ontologyRef;
	private String uploadCondition;
	private String uploadConditionValue;
	private String containerType;
	private String heartbeatPeriod;

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceID() {
		return resourceID;
	}

	public void setResourceID(String resourceID) {
		this.resourceID = resourceID;
	}

	public String getParentID() {
		return parentID;
	}

	public void setParentID(String parentID) {
		this.parentID = parentID;
	}

	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	public String getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(String lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public String getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getAccessControlPolicyIDs() {
		return accessControlPolicyIDs;
	}

	public void setAccessControlPolicyIDs(String accessControlPolicyIDs) {
		this.accessControlPolicyIDs = accessControlPolicyIDs;
	}

	public String getLabels() {
		return labels;
	}

	public void setLabels(String labels) {
		this.labels = labels;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getStateTag() {
		return stateTag;
	}

	public void setStateTag(String stateTag) {
		this.stateTag = stateTag;
	}

	public String getMaxNrOfInstances() {
		return maxNrOfInstances;
	}

	public void setMaxNrOfInstances(String maxNrOfInstances) {
		this.maxNrOfInstances = maxNrOfInstances;
	}

	public String getMaxByteSize() {
		return maxByteSize;
	}

	public void setMaxByteSize(String maxByteSize) {
		this.maxByteSize = maxByteSize;
	}

	public String getMaxInstanceAge() {
		return maxInstanceAge;
	}

	public void setMaxInstanceAge(String maxInstanceAge) {
		this.maxInstanceAge = maxInstanceAge;
	}

	public String getCurrentNrOfInstances() {
		return currentNrOfInstances;
	}

	public void setCurrentNrOfInstances(String currentNrOfInstances) {
		this.currentNrOfInstances = currentNrOfInstances;
	}

	public String getCurrentByteSize() {
		return currentByteSize;
	}

	public void setCurrentByteSize(String currentByteSize) {
		this.currentByteSize = currentByteSize;
	}

	public String getLatest() {
		return latest;
	}

	public void setLatest(String latest) {
		this.latest = latest;
	}

	public String getLocationID() {
		return locationID;
	}

	public void setLocationID(String locationID) {
		this.locationID = locationID;
	}

	public String getOntologyRef() {
		return ontologyRef;
	}

	public void setOntologyRef(String ontologyRef) {
		this.ontologyRef = ontologyRef;
	}

	public String getUploadCondition() {
		return uploadCondition;
	}

	public void setUploadCondition(String uploadCondition) {
		this.uploadCondition = uploadCondition;
	}

	public String getUploadConditionValue() {
		return uploadConditionValue;
	}

	public void setUploadConditionValue(String uploadConditionValue) {
		this.uploadConditionValue = uploadConditionValue;
	}

	public String getContainerType() {
		return containerType;
	}

	public void setContainerType(String containerType) {
		this.containerType = containerType;
	}

	public String getHeartbeatPeriod() {
		return heartbeatPeriod;
	}

	public void setHeartbeatPeriod(String heartbeatPeriod) {
		this.heartbeatPeriod = heartbeatPeriod;
	}

	@Override
	public String toString() {
		return "ContainerVO [resourceType=" + resourceType + ", resourceID="
				+ resourceID + ", parentID=" + parentID + ", creationTime="
				+ creationTime + ", lastModifiedTime=" + lastModifiedTime
				+ ", expirationTime=" + expirationTime
				+ ", accessControlPolicyIDs=" + accessControlPolicyIDs
				+ ", labels=" + labels + ", creator=" + creator + ", stateTag="
				+ stateTag + ", maxNrOfInstances=" + maxNrOfInstances
				+ ", maxByteSize=" + maxByteSize + ", maxInstanceAge="
				+ maxInstanceAge + ", currentNrOfInstances="
				+ currentNrOfInstances + ", currentByteSize=" + currentByteSize
				+ ", latest=" + latest + ", locationID=" + locationID
				+ ", ontologyRef=" + ontologyRef + ", uploadCondition="
				+ uploadCondition + ", uploadConditionValue="
				+ uploadConditionValue + ", containerType=" + containerType
				+ ", heartbeatPeriod=" + heartbeatPeriod + "]";
	}
}
