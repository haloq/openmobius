package kr.re.keti.iot.mdao.oneM2M;

import java.util.List;

import kr.re.keti.iot.domain.oneM2M.ContentInstanceVO;
import kr.re.keti.iot.mdao.common.GenericRedisDao;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

/**
 * contentInstance management Dao.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Repository
public interface ContentInstanceDao extends GenericRedisDao<ContentInstanceVO> {
	
	/**
	 * retrieve objectsKeys
	 * @param offset
	 * @param count
	 * @return
	 * @throws Exception
	 */
	public List<String> getObjectsKeys(long offset, long count) throws Exception;

	/**
	 * retrieve objects by keys
	 * @param keys
	 * @return
	 * @throws Exception
	 */
	public List<ContentInstanceVO> getObjectsByKeys(List<String> keys) throws Exception;
	
	/**
	 * redis delete objects by keys
	 * @param keys
	 * @throws Exception
	 */
	public void redisDeleteObjectsByKeys(List<String> keys) throws Exception;

	/**
	 * upsert
	 * @param obj
	 * @throws Exception
	 */
	public void upsert(Object obj) throws Exception;

	/**
	 * insert
	 * @param obj
	 * @throws Exception
	 */
	public void insert(Object obj) throws Exception;

	/**
	 * find
	 * @param query
	 * @return
	 * @throws Exception
	 */
	public List<?> find(Query query) throws Exception;

	/**
	 * findOne
	 * @param query
	 * @return
	 * @throws Exception
	 */
	public Object findOne(Query query) throws Exception;

	/**
	 * findAll
	 * @return
	 * @throws Exception
	 */
	public List<?> findAll() throws Exception;

	/**
	 * count
	 * @param query
	 * @return
	 * @throws Exception
	 */
	public long count(Query query) throws Exception;

	/**
	 * update
	 * @param query
	 * @param update
	 * @throws Exception
	 */
	public void update(Query query, Update update) throws Exception;

	/**
	 * remove
	 * @param query
	 * @throws Exception
	 */
	public void remove(Query query) throws Exception;

	/**
	 * remove
	 * @param obj
	 * @throws Exception
	 */
	public void remove(Object obj) throws Exception;

	/**
	 * multi insert
	 * @param contentInstanceList
	 * @throws Exception
	 */
	public void multi_insert(List<ContentInstanceVO> contentInstanceList) throws Exception;
	
	/**
	 * retrieve redis objects by containerId
	 * @param containerId
	 * @return
	 * @throws Exception
	 */
	public List<ContentInstanceVO> getRedisObjectsByContainerId(String containerId) throws Exception;
	
	/**
	 * delete redis objects by containerId
	 * @param containerId
	 * @throws Exception
	 */
	public void deleteRedisObjectsByContainerId(String containerId) throws Exception;
}