package kr.re.keti.iot.mdao.oneM2M;

import kr.re.keti.iot.domain.oneM2M.SubscriptionVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao;

import org.springframework.stereotype.Repository;

/**
 * subscription management Dao.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Repository
public class SubscriptionDao extends GenericMongoDao {
	
	public SubscriptionDao() {
		collectionName = ONEM2M.SUBSCRIPTION.getName();
		cls = SubscriptionVO.class;
	}
	
}
