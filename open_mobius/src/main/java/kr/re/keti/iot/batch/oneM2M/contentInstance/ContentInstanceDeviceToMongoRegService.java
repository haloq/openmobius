package kr.re.keti.iot.batch.oneM2M.contentInstance;

import java.util.ArrayList;
import java.util.List;

import kr.re.keti.iot.domain.apilog.ApiLogVO;
import kr.re.keti.iot.domain.oneM2M.ContainerVO;
import kr.re.keti.iot.domain.oneM2M.ContentInstanceVO;
import kr.re.keti.iot.mdao.oneM2M.ContentInstanceDao;
import kr.re.keti.iot.service.apilog.ApiLogService;
import kr.re.keti.iot.service.apilog.ApiLogService.CALLER_FG;
import kr.re.keti.iot.service.apilog.ApiLogService.CALL_TYPE;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.ContainerService;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * RedisToMongoDB Bulk Insert service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>khlee</li>
 *         </ul>
 */
@Service
public class ContentInstanceDeviceToMongoRegService {

	private static final Log logger = LogFactory.getLog(ContentInstanceDeviceToMongoRegService.class);
	
	@Autowired
	private MongoLogService mongoLogService;
	
	@Autowired
	private ContentInstanceDao contentInstanceDao;
	
	@Autowired
	private ContainerService containerService;

	@Autowired
	private ApiLogService apiLogService;
	
	/**
	 * RedisToMongoDB Bulk Insert.
	 * @throws Exception
	 */
	public void moveRedis2Mongo() throws Exception {
		
		long runTime = System.currentTimeMillis();
		
		mongoLogService.log(logger, LEVEL.DEBUG, "moveRedis2Mongo scheduler start!!!");
		
		try {
			
			List<String> getObjectsKeys = contentInstanceDao.getObjectsKeys(0, -1);
			
			if(!CommonUtil.isEmpty(getObjectsKeys) && getObjectsKeys.size() > 0) {
				
				List<ContentInstanceVO> contentInstanceList = contentInstanceDao.getObjectsByKeys(getObjectsKeys);
				if(logger.isDebugEnabled()) {
					mongoLogService.log(logger, LEVEL.DEBUG, "Redis contentInstance Data List start");
					for(ContentInstanceVO contentInstanceItem : contentInstanceList) {
						mongoLogService.log(logger, LEVEL.DEBUG, ToStringBuilder.reflectionToString(contentInstanceItem, ToStringStyle.DEFAULT_STYLE));
					}
					mongoLogService.log(logger, LEVEL.DEBUG, "Redis contentInstance Data List end");
				}
				
				contentInstanceDao.multi_insert(contentInstanceList);
				
				contentInstanceDao.redisDeleteObjectsByKeys(getObjectsKeys);
				
				containerService.updateContainerCurrentInstanceValue(contentInstanceList);
				
				ContainerVO containerInfo = null;
				List<ApiLogVO> apiLogList  = new ArrayList<ApiLogVO>();
				for(ContentInstanceVO contentInstanceItem : contentInstanceList) {
					containerInfo = containerService.findOneContainer(contentInstanceItem.getParentID());
					if(!CommonUtil.isEmpty(containerInfo) && !CommonUtil.isEmpty(containerInfo.getParentID())) {
						apiLogList.add(apiLogService.setApiLog(CALLER_FG.DEVICE, containerInfo.getParentID(), CALL_TYPE.CONTENT_INSTANCE_CREATE, "Y"));
					}
				}
				apiLogService.log(apiLogList);

			}

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR,  e.getMessage());
			throw new Exception(e);
		} 
		
		mongoLogService.log(logger, LEVEL.DEBUG, "moveRedis2Mongo scheduler end!!! (runTime : " + (System.currentTimeMillis() - runTime) + "ms)");
	}

}
