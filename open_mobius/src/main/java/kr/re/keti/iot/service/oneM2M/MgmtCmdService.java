package kr.re.keti.iot.service.oneM2M;

import java.util.ArrayList;
import java.util.List;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.ExecInstanceVO;
import kr.re.keti.iot.domain.oneM2M.MgmtCmdOnlyIdVO;
import kr.re.keti.iot.domain.oneM2M.MgmtCmdVO;
import kr.re.keti.iot.domain.oneM2M.RemoteCSEVO;
import kr.re.keti.iot.driver.coap.MobiusCoAPClient;
import kr.re.keti.iot.driver.http.MobiusHTTPClient;
import kr.re.keti.iot.driver.mqtt.MqttClientUtil;
import kr.re.keti.iot.mdao.common.SequenceDao;
import kr.re.keti.iot.mdao.common.SequenceDao.MovType;
import kr.re.keti.iot.mdao.common.SequenceDao.SEQ_PREFIX;
import kr.re.keti.iot.mdao.common.SequenceDao.SeqType;
import kr.re.keti.iot.mdao.oneM2M.MgmtCmdDao;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.ExecInstanceService.EXEC_RESULT;
import kr.re.keti.iot.service.oneM2M.ExecInstanceService.EXEC_STATUS;
import kr.re.keti.iot.service.oneM2M.SubscriptionService.SUBSCRIPTION_RESOURCE_STATUS_TYPE;
import kr.re.keti.iot.service.oneM2M.common.MCommonService;
import kr.re.keti.iot.util.CommonUtil;
import kr.re.keti.iot.util.oneM2M.CommonCode.RESOURCE_TYPE;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;

import ch.ethz.inf.vs.californium.coap.CoAP.Code;
import ch.ethz.inf.vs.californium.coap.CoAP.ResponseCode;

/**
 * mgmtCmd management Service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Service
public class MgmtCmdService {

	private static final Log logger = LogFactory.getLog(MgmtCmdService.class);

	@Autowired
	private MongoLogService mongoLogService;
	
	@Autowired
	private ExecInstanceService execInstanceService;
	
	@Autowired
	private RemoteCSEService remoteCSEService;
	
	@Autowired
	private FirmwareService firmwareService;
	
	@Autowired
	private SoftwareService softwareService;
	
	@Autowired
	private SubscriptionService subscriptionService;
	
	@Autowired
	private MCommonService mCommonService;
	
	@Autowired
	private SequenceDao seqDao;

	@Autowired
	private MgmtCmdDao mgmtCmdDao;
	
	
	public enum MGMT_CMD_TYPE{
		
		RESET("1"),
		REBOOT("2"),
		UPLOAD("3"),
		DOWNLOAD("4"),
		SOFTWAREINSTALL("5"),
		SOFTWAREUNINSTALL("6"),
		PROFILE("remoteCSEUpdate"),
		FIRMWARE("firmwareUpgrade"),
		DEVICE_APP("appInstall");
		
		private final String id;
		MGMT_CMD_TYPE(String id){
			this.id = id;
		}
		public String getValue() {return id;}
	}	
	
	public enum MGMT_ACCESS_TYPE {
		
		POLLING("polling"), 
		HTTP("HTTP") , 
		COAP("COAP") , 
		MQTT("MQTT") , 
		SMS("SMS");
		
		private final String id;
		MGMT_ACCESS_TYPE(String id){
			this.id = id;
		}
		public String getValue() {return id;}
	}	
	
	public enum EXEC_MODE {
		IMMEDIATE_ONCE("1"), 
		IMMEDIATE_AND_REPEATEDLY("2") , 
		RANDOM_ONCE("3"),
		RANDOM_AND_REPEATEDLY("4");
		private final String id;
		EXEC_MODE(String id){
			this.id = id;
		}
		public String getValue() {return id;}
	}	
	
	/**
	 * generate resourceID
	 * @return
	 * @throws IotException
	 */
	private String generateResourceId() throws IotException {
		StringBuffer bufSeq = new StringBuffer(SEQ_PREFIX.MGMT_CMD.getValue());
		long longSeq = 0;
		
		try {
			
			longSeq = seqDao.move(MovType.UP, SeqType.MGMT_CMD);
			
			bufSeq.append(String.format("%010d", longSeq));
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.mgmtCmd.id.createFail.text"));
		}
		
		return bufSeq.toString();
	}

	/**
	 * create mgmtCmd
	 * @param remoteCSEItem
	 * @param mgmtCmdItem
	 * @return
	 * @throws IotException
	 */
	public MgmtCmdVO createMgmtCmd(RemoteCSEVO remoteCSEItem, MgmtCmdVO mgmtCmdItem) throws IotException {
		MgmtCmdVO rtnMgmtCmdItem = null;
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");

		mgmtCmdItem.setResourceID(this.generateResourceId());

		mgmtCmdItem.setParentID(remoteCSEItem.getResourceID());
		mgmtCmdItem.setResourceType(RESOURCE_TYPE.MGMT_CMD.getName());
		mgmtCmdItem.setAccessControlPolicyIDs(remoteCSEItem.getAccessControlPolicyIDs());
		mgmtCmdItem.setExecTarget(remoteCSEItem.getResourceID());
		
		mgmtCmdItem.setCreationTime(currentTime);
		mgmtCmdItem.setLastModifiedTime(currentTime);
		
		try {
			mgmtCmdDao.insert(mgmtCmdItem);
			
			rtnMgmtCmdItem = this.findOneMgmtCmdByLabels(mgmtCmdItem.getParentID(), mgmtCmdItem.getLabels());

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.mgmtcmd.createFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "mgmtCmd create success");
		
		return rtnMgmtCmdItem;
	}

	/**
	 * find mgmtCmd by remoteCSEID-labels
	 * @param remoteCSEID
	 * @param labels
	 * @return
	 * @throws IotException
	 */
	public MgmtCmdVO findOneMgmtCmdByLabels(String remoteCSEID, String labels) throws IotException {
		MgmtCmdVO findMgmtCmdItem = null;

		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(remoteCSEID));
		query.addCriteria(Criteria.where("labels").is(labels));

		try {
			findMgmtCmdItem = (MgmtCmdVO) mgmtCmdDao.findOne(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.mgmtcmd.findFail.text"));
		}

		return findMgmtCmdItem;
	}
	
	/**
	 * find mgmtCmd by remoteCSEID-cmdType
	 * @param remoteCSEID
	 * @param cmdType
	 * @return
	 * @throws IotException
	 */
	public MgmtCmdVO findOneMgmtCmdByCmdType(String remoteCSEID, String cmdType) throws IotException {
		MgmtCmdVO findMgmtCmdItem = null;
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(remoteCSEID));
		query.addCriteria(Criteria.where("cmdType").is(cmdType));
		
		try {
			findMgmtCmdItem = (MgmtCmdVO) mgmtCmdDao.findOne(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.mgmtcmd.findFail.text"));
		}
		
		return findMgmtCmdItem;
	}
	
	/**
	 * find mgmtCmd List
	 * @param key
	 * @param value
	 * @return
	 * @throws IotException
	 */
	public List<MgmtCmdVO> findMgmtCmd(String key, String value) throws IotException {
		List<MgmtCmdVO> findMgmtCmdList = null;
		
		Query query = new Query();
		query.addCriteria(Criteria.where(key).is(value));
		
		try {
			findMgmtCmdList = (List<MgmtCmdVO>)mgmtCmdDao.find(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.mgmtcmd.findFail.text"));
		}
		
		return findMgmtCmdList;
	}	
	
	/**
	 * find mgmtCmd
	 * @param key
	 * @param value
	 * @return
	 * @throws IotException
	 */
	public MgmtCmdVO findOneMgmtCmd(String key, String value) throws IotException {
		MgmtCmdVO findMgmtCmdItem = null;
		
		List<MgmtCmdVO> findMgmtCmdList = this.findMgmtCmd(key, value);
		if(CommonUtil.isEmpty(findMgmtCmdList) && findMgmtCmdList.size() > 0) {
			findMgmtCmdItem = findMgmtCmdList.get(0);
		}
		
		return findMgmtCmdItem;
	}
	
	/**
	 * find mgmtCmdOnlyId
	 * @param key
	 * @param value
	 * @return
	 * @throws IotException
	 */
	public List<MgmtCmdOnlyIdVO> findListMgmtCmdOnlyId(String key, String value) throws IotException {
		List<MgmtCmdOnlyIdVO> findMgmtCmdList = null;
		
		Query query = new Query();
		query.addCriteria(Criteria.where(key).is(value));
		
		try {
			findMgmtCmdList = (List<MgmtCmdOnlyIdVO>)mgmtCmdDao.findMgmtCmdOnlyId(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.mgmtcmd.findFail.text"));
		}
		
		return findMgmtCmdList;
	}	

	/**
	 * delete mgmtCmd by remoteCSEID-mgmtCmdId
	 * @param remoteCSEID
	 * @param mgmtCmdId
	 * @throws IotException
	 */
	public void deleteMgmtCmd(String remoteCSEID, String mgmtCmdId) throws IotException {

		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(remoteCSEID));
		query.addCriteria(Criteria.where("resourceID").is(mgmtCmdId));

		try {
			
			execInstanceService.deleteExecInstance(mgmtCmdId);
			
			mgmtCmdDao.remove(query);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.mgmtcmd.delFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "mgmtCmd delete success");

	}
	
	/**
	 * delete mgmtCmdList by remoteCSEID
	 * @param remoteCSEID
	 * @throws IotException
	 */
	public void deleteMgmtCmdByRemoteCSEID(String remoteCSEID) throws IotException {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(remoteCSEID));
		
		try {
			List<MgmtCmdVO> findMgmtCmdList = this.findMgmtCmd("parentID", remoteCSEID);
			for(MgmtCmdVO mgmtCmdItem : findMgmtCmdList) {
				this.deleteMgmtCmd(mgmtCmdItem.getParentID(), mgmtCmdItem.getResourceID());
			}
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.mgmtcmd.delFail.text"));
		}
		mongoLogService.log(logger, LEVEL.DEBUG, "mgmtCmd delete success");
		
	}
	
	/**
	 * retrieve mgmtCmd count by remoteCSEID-labels
	 * @param remoteCSEID
	 * @param labels
	 * @return
	 */
	public long getCountByLabels(String remoteCSEID, String labels){
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(remoteCSEID));
		query.addCriteria(Criteria.where("labels").is(labels));
		
		long cnt = 0;
		
		try {
			cnt = mgmtCmdDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "mgmtCmd get count");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		return cnt;
	}
	
	/**
	 * create execInstance
	 * @param findMgmtCmdItem
	 * @return
	 * @throws IotException
	 */
	public ExecInstanceVO createExecInstance(MgmtCmdVO findMgmtCmdItem) throws IotException{
		
		ExecInstanceVO findExecInstanceItem	= null;

		try {
			
			findExecInstanceItem = execInstanceService.createExecInstance(findMgmtCmdItem);
		} catch (Exception e) {
			throw new IotException("600",CommonUtil.getMessage("msg.device.execInstance.createFail.text"));
		}
		
		if(CommonUtil.isEmpty(findExecInstanceItem) && CommonUtil.isEmpty(findExecInstanceItem.getResourceID())) {
			throw new IotException("600",CommonUtil.getMessage("msg.device.execInstance.createFail.text"));
			
		}
		
		return findExecInstanceItem;
		
	}
	
	/**
	 * retrieve pointOfAccessList by remoteCSEID
	 * @param remoteCSEID
	 * @return
	 * @throws IotException
	 */
	public ArrayList<String[]> getPointOfAccessListByRemoteCSEID(String remoteCSEID) throws IotException{
		RemoteCSEVO findRemoteCSEItem	= null;
		ArrayList<String[]> listAceess	= new ArrayList<String[]>();
		
		try {
			
			findRemoteCSEItem = remoteCSEService.findOneRemoteCSE(remoteCSEID);
			
		} catch (Exception e) {
			throw new IotException("600",CommonUtil.getMessage("msg.device.controlFail.text"));
		}
		
		if(!CommonUtil.isEmpty(findRemoteCSEItem)) {
			String pointOfAccess = findRemoteCSEItem.getPointOfAccess();
			
			if(!CommonUtil.isEmpty(pointOfAccess)) {
				listAceess = this.getPointOfAccessList(pointOfAccess);
			}
			else {
				mongoLogService.log(logger, LEVEL.ERROR, "pointOfAccess " + CommonUtil.getMessage("msg.input.empty.text"));
				
				throw new IotException("600", "pointOfAccess " + CommonUtil.getMessage("msg.input.empty.text"));
			}
		}
		else {
			
			throw new IotException("600", "[MgmtCmd] findRemoteCSEItem is empty");
//			throw new IotException("600", CommonUtil.getMessage("msg.device.mgmtObj.delFail.text"));
		}
		
		//
		mongoLogService.log(logger, LEVEL.DEBUG, "[getPointOfAccessList] success");
		return listAceess;
		
	}
	
	/**
	 * retrieve pointOfAccessList by pointOfAccess
	 * @param pointOfAccess
	 * @return
	 * @throws IotException
	 */
	public ArrayList<String[]> getPointOfAccessList(String pointOfAccess) throws IotException{
		ArrayList<String[]> listAceess	= new ArrayList<String[]>();
		
		if(!CommonUtil.isEmpty(pointOfAccess)) {

			String[] arrAccess = pointOfAccess.split(",");
			
			for (String accessInfo : arrAccess) {
				if(accessInfo.startsWith(MGMT_ACCESS_TYPE.COAP.getValue())) {
					String[]coapServerInfo = accessInfo.split("^");
					if(!CommonUtil.isEmpty(coapServerInfo) && coapServerInfo.length >= 2) {
						accessInfo = coapServerInfo[0];
					}
				}
				listAceess.add(accessInfo.split("\\|"));
				mongoLogService.log(logger, LEVEL.DEBUG, accessInfo);
			}
			mongoLogService.log(logger, LEVEL.DEBUG, "###############################################");
			
			this.checkPointOfAccessType(listAceess);
	
		}
		else {
			mongoLogService.log(logger, LEVEL.ERROR, "The device pointOfAccess is empty.");
			
			throw new IotException("600", "The device pointOfAccess is empty.");
		}
		return listAceess;
		
	}
	
	/**
	 * check pointOfAccess type
	 * @param listAceess
	 * @throws IotException
	 */
	public void checkPointOfAccessType(ArrayList<String[]> listAceess) throws IotException{
		
		if(CommonUtil.isEmpty(listAceess)) {
			throw new IotException("600", "The device pointOfAccess is empty.");
		}
		
		for (String[] arrAccessInfo : listAceess) {
			
			if(arrAccessInfo[0].equalsIgnoreCase(MGMT_ACCESS_TYPE.POLLING.getValue())) {
				return;
				
			} else {
				
				if(arrAccessInfo.length >= 2) {
					String accessType = arrAccessInfo[0];
					String accessInfo = arrAccessInfo[1];
					
					if(CommonUtil.isEmpty(accessType) && CommonUtil.isEmpty(accessInfo)) {
						mongoLogService.log(logger, LEVEL.ERROR, "pointOfAccess is empty.");
						
						throw new IotException("600", "pointOfAccess is empty.");
					} else {
						
						boolean isCorrectAccessType = false;
						for (MGMT_ACCESS_TYPE mgmtAccessType : MGMT_ACCESS_TYPE.values()) {
							if(mgmtAccessType.getValue().equals(accessType)) {
								isCorrectAccessType = true;
								break;
							}
						}
						
						if(!isCorrectAccessType) {
							throw new IotException("600", CommonUtil.getMessage("msg.device.pointOfAccess.type.nosupport", new String[]{accessType}));
						}
					}
					
				} else {
				}					
			}
			

		}
		
	}
	
	/**
	 * check mgmtCmd Validation
	 * @param mgmtCmdProfile
	 * @throws IotException
	 */
	public void checkMgmtCmdValidation(MgmtCmdVO mgmtCmdProfile) throws IotException{
		String execFrequency = mgmtCmdProfile.getExecFrequency();
		String execDelay = mgmtCmdProfile.getExecDelay();
		String execNumber = mgmtCmdProfile.getExecNumber();
		
		if(!CommonUtil.isEmpty(execFrequency) && !CommonUtil.isInteger(execFrequency)) {
			throw new IotException("600", "execFrequency " + CommonUtil.getMessage("msg.device.number.text"));
		}
		
		if(!CommonUtil.isEmpty(execDelay) && !CommonUtil.isInteger(execDelay)) {
			throw new IotException("600", "execDelay " + CommonUtil.getMessage("msg.device.number.text"));
		}
		
		if(!CommonUtil.isEmpty(execNumber) && !CommonUtil.isInteger(execNumber)) {
			throw new IotException("600", "execNumber " + CommonUtil.getMessage("msg.device.number.text"));
		}
		
		if(CommonUtil.isMinus(execFrequency)) {
			throw new IotException("600", CommonUtil.getMessage("msg.mgmtCmd.execNumber.max.text", new String[]{"execFrequency"}));
		}
		if(CommonUtil.isMinus(execDelay)) {
			throw new IotException("600", CommonUtil.getMessage("msg.mgmtCmd.execNumber.max.text", new String[]{"execDelay"}));
		}
		if(CommonUtil.isMinus(execNumber)) {
			throw new IotException("600", CommonUtil.getMessage("msg.mgmtCmd.execNumber.max.text", new String[]{"execNumber"}));
		}
		
		int intExecNumber		= 0;
		int intMaxExecNumber	= 0;
		String maxExecNumber	= CommonUtil.getConfig("iot.mgmtCmd.execNumber.max");
		
		if(!CommonUtil.isEmpty(execNumber) && !CommonUtil.isEmpty(maxExecNumber)) {
			try {
				intExecNumber		= Integer.parseInt(execNumber);
				intMaxExecNumber	= Integer.parseInt(maxExecNumber);
				
				if(intExecNumber > intMaxExecNumber) {
					throw new IotException("600", CommonUtil.getMessage("msg.mgmtCmd.execNumber.max.text", new String[]{maxExecNumber}));
				}
				
			} catch (NumberFormatException ne) {
				ne.printStackTrace();
				mongoLogService.log(logger, LEVEL.ERROR, ne.getMessage());
			}
		}
		
		boolean isExecModeTypeError = true;
		
		String reqExecMode = mgmtCmdProfile.getExecMode();
		for (EXEC_MODE execMode : EXEC_MODE.values()) {
			if(execMode.getValue().equals(reqExecMode)) {
				isExecModeTypeError = false;
				break;
			}
		}
		
		
		if(isExecModeTypeError) {
			throw new IotException("600", CommonUtil.getMessage("msg.mgmtCmd.execMode.type.nosupport"));
		}
		
		if(EXEC_MODE.IMMEDIATE_AND_REPEATEDLY.getValue().equals(reqExecMode)) {
			if(CommonUtil.isEmpty(execNumber)) {
				throw new IotException("600", "execNumber " + CommonUtil.getMessage("msg.input.empty.text"));
			}			
		}
		
		else if(EXEC_MODE.RANDOM_ONCE.getValue().equals(reqExecMode)) {
			if(CommonUtil.isEmpty(execDelay)) {
				throw new IotException("600", "execDelay " + CommonUtil.getMessage("msg.input.empty.text"));
			}
		}
		
		else if(EXEC_MODE.RANDOM_AND_REPEATEDLY.getValue().equals(reqExecMode)) {
			if(CommonUtil.isEmpty(execFrequency)) {
				throw new IotException("600", "execFrequency " + CommonUtil.getMessage("msg.input.empty.text"));
			}
			else if(CommonUtil.isEmpty(execDelay)) {
				throw new IotException("600", "execDelay " + CommonUtil.getMessage("msg.input.empty.text"));
			}
			else if(CommonUtil.isEmpty(execNumber)) {
				throw new IotException("600", "execNumber " + CommonUtil.getMessage("msg.input.empty.text"));
			}
		}
		
	}
	
	/**
	 * request device control
	 * @param mgmtCmdProfile
	 * @param execInstanceItem
	 * @param listPointOfAceess
	 * @return
	 */
	public boolean callDevice(MgmtCmdVO mgmtCmdProfile, ExecInstanceVO execInstanceItem, ArrayList<String[]> listPointOfAceess) {
		
		boolean isRequestSuccess = false;
		
		ArrayList<String[]> listAceess = listPointOfAceess;
		
		
		if(CommonUtil.isEmpty(listAceess)) {
			mongoLogService.log(logger, LEVEL.ERROR, "pointOfAccess is empty.");
			isRequestSuccess = false;
			
			return isRequestSuccess;
		}
		
		String mgmtCmdName	= mgmtCmdProfile.getLabels();		
		
		String[] arrAccessInfo = listAceess.get(0);
		String accessType = "";
		String accessInfo = "";
		String deviceId = mgmtCmdProfile.getExecTarget();
		
		if(!CommonUtil.isEmpty(arrAccessInfo) && arrAccessInfo.length >= 1) {
			accessType = arrAccessInfo[0];
			
			if(accessType.equalsIgnoreCase(MGMT_ACCESS_TYPE.POLLING.getValue())) {
				isRequestSuccess = true;
				return isRequestSuccess;
			}
			
			if(arrAccessInfo.length >= 2) {
				accessInfo = arrAccessInfo[1];
			}
		}
		
		if(MGMT_ACCESS_TYPE.MQTT.getValue().equals(accessType)) {
			isRequestSuccess = this.callMQTT(accessInfo, mgmtCmdName, execInstanceItem);
		}
		else if(MGMT_ACCESS_TYPE.HTTP.getValue().equals(accessType)) {
			isRequestSuccess = this.callHTTP(deviceId, accessInfo, mgmtCmdName, execInstanceItem);
		}
		else if(MGMT_ACCESS_TYPE.COAP.getValue().equals(accessType)){
			isRequestSuccess = this.callCOAP(deviceId, accessInfo, mgmtCmdName, execInstanceItem);
		}
		else {
			
			accessInfo = accessType;
			isRequestSuccess = this.callHTTP(deviceId, accessInfo, mgmtCmdName, execInstanceItem);
		}
		
		if (isRequestSuccess) {
			
			this.requestResultUpdate(execInstanceItem, null, listPointOfAceess);
			
			mongoLogService.log(logger, LEVEL.DEBUG, "[callDevice] success");
		} else {
			
			this.requestResultUpdate(execInstanceItem, EXEC_RESULT.STATUS_REQUEST_DENIED.getValue(), listPointOfAceess);
			
			mongoLogService.log(logger, LEVEL.ERROR, "[callDevice] fail");
		}
		
		return isRequestSuccess;
	}
	
	/**
	 * request mgmtCmd control
	 * @param mgmtCmdProfile
	 * @return
	 * @throws IotException
	 */
	public ExecInstanceVO mgmtCmdControl(MgmtCmdVO mgmtCmdProfile) throws IotException{
		
		String remoteCSEID	= mgmtCmdProfile.getParentID();
		String mgmtCmdName	= mgmtCmdProfile.getLabels();
		
		MgmtCmdVO findMgmtCmdItem			= null;
		ExecInstanceVO findExecInstanceItem	= null;
		
		findMgmtCmdItem = this.findOneMgmtCmdByLabels(remoteCSEID, mgmtCmdName);
		findMgmtCmdItem.setExecReqArgs(mgmtCmdProfile.getExecReqArgs());
		
		ArrayList<String[]> listPointOfAceess = this.getPointOfAccessListByRemoteCSEID(remoteCSEID);
		
		findExecInstanceItem = this.createExecInstance(findMgmtCmdItem);
		
		boolean isRequestSuccess = this.callDevice(findMgmtCmdItem, findExecInstanceItem, listPointOfAceess);
		
		if (isRequestSuccess) {
			
			mongoLogService.log(logger, LEVEL.DEBUG, "[mgmtCmdControl] success");
		} else {
			
			mongoLogService.log(logger, LEVEL.ERROR, "[mgmtCmdControl] fail");
		}
		
		return findExecInstanceItem;
		
	}

	/**
	 * update mgmtCmd
	 * @param remoteCSEID
	 * @param mgmtCmdName
	 * @param newMgmtCmdProfile
	 * @return
	 * @throws IotException
	 */
	public MgmtCmdVO updateMgmtCmd(String remoteCSEID, String mgmtCmdName, MgmtCmdVO newMgmtCmdProfile) throws IotException {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(remoteCSEID));
		query.addCriteria(Criteria.where("labels").is(mgmtCmdName));
		
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
//		String accessControlPolicyIDs = newMgmtCmdProfile.getAccessControlPolicyIDs();
		String description = newMgmtCmdProfile.getDescription();
		String cmdType = newMgmtCmdProfile.getCmdType();
		String execReqArgs = newMgmtCmdProfile.getExecReqArgs();
		String execMode = newMgmtCmdProfile.getExecMode();
		String execFrequency = newMgmtCmdProfile.getExecFrequency();
		String execDelay = newMgmtCmdProfile.getExecDelay();
		String execNumber = newMgmtCmdProfile.getExecNumber();
		
		Update update = new Update();
//		if(!CommonUtil.isNull(accessControlPolicyIDs))	update.set("accessControlPolicyIDs", accessControlPolicyIDs);
		if(!CommonUtil.isNull(description))				update.set("description", description);
		if(!CommonUtil.isNull(cmdType))					update.set("cmdType", cmdType);
		if(!CommonUtil.isNull(execReqArgs))				update.set("execReqArgs", execReqArgs);
		if(!CommonUtil.isNull(execMode))				update.set("execMode", execMode);
		if(!CommonUtil.isNull(execFrequency))			update.set("execFrequency", execFrequency);
		if(!CommonUtil.isNull(execDelay))				update.set("execDelay", execDelay);
		if(!CommonUtil.isNull(execNumber))				update.set("execNumber", execNumber);
														update.set("lastModifiedTime", currentTime);
		
		this.updateMgmtCmd(query, update);
		
		return this.findOneMgmtCmdByLabels(remoteCSEID, mgmtCmdName);
	}
	
	/**
	 * update mgmtCmd
	 * @param query
	 * @param update
	 * @throws IotException
	 */
	public void updateMgmtCmd(Query query, Update update) throws IotException {
		
		try { 
			mgmtCmdDao.update(query, update);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.mgmtcmd.upFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "mgmtCmd update success");
		
		MgmtCmdVO findCmdItem = (MgmtCmdVO) mgmtCmdDao.find(query).get(0);
		subscriptionService.sendSubscription(findCmdItem.getParentID(), findCmdItem.getResourceID(), SUBSCRIPTION_RESOURCE_STATUS_TYPE.UPDATED, MgmtCmdVO.class, update);
		
	}
	
	/**
	 * update request control result
	 * @param execInstanceItem
	 * @param requestResult
	 * @param listPointOfAceess
	 */
	private void requestResultUpdate(ExecInstanceVO execInstanceItem, String requestResult, ArrayList<String[]> listPointOfAceess) {
		
		ArrayList<String[]> listAceess = listPointOfAceess;
		
		String[] arrAccessInfo = listAceess.get(0);
		String accessType = null;
		
		if(!CommonUtil.isEmpty(arrAccessInfo) && arrAccessInfo.length >= 1) {
			accessType = arrAccessInfo[0];
		}
		
		if(accessType.equalsIgnoreCase(MGMT_ACCESS_TYPE.POLLING.getValue())) {
			return;
		}
		
		if(CommonUtil.isEmpty(requestResult)) {
			
			execInstanceItem.setExecStatus(EXEC_STATUS.STARTED.getValue());
			
		} else {
			execInstanceItem.setExecStatus(EXEC_STATUS.FINISHED.getValue());

		}
		try {
			execInstanceItem.setExecResult(requestResult);
			execInstanceService.updateExecInstanceResult(execInstanceItem);
		} catch (IotException e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
	}
	
	/**
	 * request control device by MQTT
	 * @param deviceId
	 * @param mgmtCmdName
	 * @param execInstanceItem
	 * @return
	 */
	private boolean callMQTT(String deviceId, String mgmtCmdName, ExecInstanceVO execInstanceItem) {
		mongoLogService.log(logger, LEVEL.DEBUG, "[callMQTT] MQTT control in");
		boolean isRequestSuccess = false;
		
		try {
			
			String execInstance = CommonUtil.marshalToXmlString(execInstanceItem);
			
			MqttClientUtil mqttClient = new MqttClientUtil();
			mqttClient.publishKetiPayload(deviceId, mgmtCmdName, execInstance);
			
			isRequestSuccess = true;
			mongoLogService.log(logger, LEVEL.INFO, "[callMQTT] Success :: " + deviceId);
			
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, "[callMQTT] Exception : " + e.getMessage());
		}
		
		return isRequestSuccess;
	}
	
	/**
	 * request control device by HTTP
	 * @param deviceId
	 * @param deviceUri
	 * @param mgmtCmdName
	 * @param execInstanceItem
	 * @return
	 */
	private boolean callHTTP(String deviceId, String deviceUri, String mgmtCmdName, ExecInstanceVO execInstanceItem) {
		
		mongoLogService.log(logger, LEVEL.DEBUG, "[callHTTP] HTTP control in");
		boolean isRequestSuccess = false;
		
		try {
			String strUri = null;
			if (deviceUri.contains("http")) {
				
				strUri = deviceUri;
			} else {
				
				strUri = "http://" + deviceUri;
			}
			String execInstance = CommonUtil.marshalToXmlString(execInstanceItem);
			
			MobiusHTTPClient mobiusHTTPClient = new MobiusHTTPClient();
			int httpStatusCode = mobiusHTTPClient.request(RequestMethod.POST, strUri, null, execInstance);
			
			if(httpStatusCode == HttpStatus.OK.value() || httpStatusCode == HttpStatus.CREATED.value()){
				isRequestSuccess = true;
				mongoLogService.log(logger, LEVEL.INFO, "[callHTTP] Success :: " + strUri + ", " + HttpStatus.OK.name());
				
			} else {
				mongoLogService.log(logger, LEVEL.ERROR, "[callHTTP] fail httpStatusCode :: " + httpStatusCode);
				mongoLogService.log(logger, LEVEL.ERROR, "[callHTTP] fail httpStatusCode :: " + HttpStatus.valueOf(httpStatusCode));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, "[callHTTP] HTTP Exception : " + e.getMessage());
		}
		
		return isRequestSuccess;
	}
	
	/**
	 * request control device by CoAP
	 * @param deviceId
	 * @param deviceUri
	 * @param mgmtCmdName
	 * @param execInstanceItem
	 * @return
	 */
	private boolean callCOAP(String deviceId, String deviceUri, String mgmtCmdName, ExecInstanceVO execInstanceItem) {
		
		mongoLogService.log(logger, LEVEL.DEBUG, "[callCOAP] COAP control in");
		boolean isRequestSuccess = false;
		
		try {
			String[] strUriArray = deviceUri.split("\\^");
			String strUriTmp = strUriArray[0];
			
			String strUri = null;
			if (strUriTmp.contains("coap")) {
				
				strUri = strUriTmp;
			} else {
				
				strUri = "coap://" + strUriTmp;
			}
			
			String execInstance = CommonUtil.marshalToXmlString(execInstanceItem);
			
			MobiusCoAPClient mobiusCoAPClient = new MobiusCoAPClient();
			ResponseCode responseCode = mobiusCoAPClient.request(Code.POST, strUri, null, execInstance);
			
			if(!CommonUtil.isEmpty(responseCode) && ResponseCode.isSuccess(responseCode)) {
				isRequestSuccess = true;
				mongoLogService.log(logger, LEVEL.INFO, "[callCOAP] Success :: " + strUri + ", " + responseCode.name());
				
			} else {
				mongoLogService.log(logger, LEVEL.ERROR, "[callCOAP] COAP fail");
				
				if(!CommonUtil.isEmpty(responseCode)) {
					mongoLogService.log(logger, LEVEL.ERROR, "COAP responseCode.name :: " + responseCode.name());
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, "[callCOAP] COAP Exception : " + e.getMessage());
		}
		
		return isRequestSuccess;
	}
	
	/**
	 * update request control result
	 * @param mgmtCmdInfo
	 * @param execInstanceProfile
	 * @param execReqArgs
	 * @return
	 * @throws IotException
	 */
	public ExecInstanceVO mgmtCmdResultUpdate(MgmtCmdVO mgmtCmdInfo, ExecInstanceVO execInstanceProfile, String execReqArgs) throws IotException{

		ExecInstanceVO findExecInstanceItem	= null;
		
		String execResult = execInstanceProfile.getExecResult();
		String execInstanceID = execInstanceProfile.getResourceID();
		String cmdType = mgmtCmdInfo.getCmdType();
		
		try {
			String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
			execInstanceProfile.setExecStatus(EXEC_STATUS.FINISHED.getValue());
			execInstanceProfile.setLastModifiedTime(currentTime);;
			findExecInstanceItem = execInstanceService.updateExecInstanceResult(execInstanceProfile);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "[mgmtCmdResultUpdate] ExecInstance create " + e.getMessage());
			
			throw new IotException("600",CommonUtil.getMessage("msg.device.controlFail.text"));
		}
		
		subscriptionService.sendSubscription(findExecInstanceItem.getParentID(), findExecInstanceItem.getResourceID(), SUBSCRIPTION_RESOURCE_STATUS_TYPE.CHILD_UPDATED, ExecInstanceVO.class, execInstanceProfile);
		
		mongoLogService.log(logger, LEVEL.DEBUG, "[mgmtCmdResultUpdate] success");
		
		return findExecInstanceItem;
		
	}
}