package kr.re.keti.iot.service.oneM2M;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.DeviceInfoVO;
import kr.re.keti.iot.domain.oneM2M.NodeVO;
import kr.re.keti.iot.mdao.common.SequenceDao;
import kr.re.keti.iot.mdao.common.SequenceDao.MovType;
import kr.re.keti.iot.mdao.common.SequenceDao.SEQ_PREFIX;
import kr.re.keti.iot.mdao.common.SequenceDao.SeqType;
import kr.re.keti.iot.mdao.oneM2M.DeviceInfoDao;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.NodeService.MGMT_DEFINITION;
import kr.re.keti.iot.util.CommonUtil;
import kr.re.keti.iot.util.oneM2M.CommonCode.RESOURCE_TYPE;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

/**
 * deviceInfo management Service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Service
public class DeviceInfoService {

	private static final Log logger = LogFactory.getLog(DeviceInfoService.class);

	@Autowired
	private MongoLogService mongoLogService;

	@Autowired
	private DeviceInfoDao deviceInfoDao;
	
	@Autowired
	private NodeService nodeService;	

	@Autowired
	private SequenceDao seqDao;
	
	/**
	 * find deviceInfo count by parentId
	 * @param parentId
	 * @return
	 */
	public long getCount(String parentId){
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(parentId));
		
		long cnt = 0;
		
		try {
			cnt = deviceInfoDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "deviceInfo get count");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		return cnt;
	}

	/**
	 * find deviceInfo by key-value
	 * @param key
	 * @param value
	 * @return
	 * @throws IotException
	 */
	public DeviceInfoVO findOneDeviceInfo(String key, String value) throws IotException {
		DeviceInfoVO findDeviceInfoItem = null;

		Query query = new Query();
		query.addCriteria(Criteria.where(key).is(value));

		try {
			findDeviceInfoItem = (DeviceInfoVO) deviceInfoDao.findOne(query);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.deviceInfo.findFail.text"));
		}

		return findDeviceInfoItem;
	}
	
	/**
	 * find deviceInfo by nodeId-labels
	 * @param nodeId
	 * @param labels
	 * @return
	 * @throws IotException
	 */
	public DeviceInfoVO findOneDeviceInfoByLabels(String nodeId, String labels) throws IotException {
		DeviceInfoVO findDeviceInfoItem = null;
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(nodeId));
		query.addCriteria(Criteria.where("labels").is(labels));
		
		try {
			findDeviceInfoItem = (DeviceInfoVO)deviceInfoDao.findOne(query);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.deviceInfo.findFail.text"));
		}
		
		return findDeviceInfoItem;
	}
	
	/**
	 * update deviceInfo fwVersion by nodeID
	 * @param nodeID
	 * @param fwVersion
	 * @throws IotException
	 */
	public void saveDeviceInfoByFirmware(String nodeID, String fwVersion) throws IotException {
		
		if(this.getCount(nodeID) < 1) {
			
			NodeVO nodeInfo = nodeService.findOneNode(nodeID);
			
			DeviceInfoVO deviceInfoItem = new DeviceInfoVO();
			deviceInfoItem.setFwVersion(fwVersion);
			
			this.createDeviceInfo(nodeInfo, deviceInfoItem);
			
		} else {
			
			this.updateDeviceInfoByFirmware(nodeID, fwVersion);
		}
		
	}
	
	/**
	 * create deviceInfo
	 * @param nodeInfo
	 * @param deviceInfoProfile
	 * @return
	 * @throws IotException
	 */
	public DeviceInfoVO createDeviceInfo(NodeVO nodeInfo, DeviceInfoVO deviceInfoProfile) throws IotException {
		DeviceInfoVO deviceInfoItem = new DeviceInfoVO();
		
		try {
			Long deviceInfoId = seqDao.move(MovType.UP, SeqType.DEVICEINFO);
			deviceInfoItem.setResourceID(SEQ_PREFIX.DEVICE_INFO.getValue() + String.format("%010d", deviceInfoId));

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.deviceInfo.id.createFail.text"));
		}

		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		deviceInfoItem.setResourceType(RESOURCE_TYPE.NODE.getName());
		deviceInfoItem.setParentID(nodeInfo.getResourceID());
		deviceInfoItem.setAccessControlPolicyIDs(nodeInfo.getAccessControlPolicyIDs());
		deviceInfoItem.setLabels(deviceInfoProfile.getLabels());
		deviceInfoItem.setMgmtDefinition(MGMT_DEFINITION.DEVICE_INFO.getValue());
//		deviceInfoItem.setObjectIDs(deviceInfoProfile.getObjectIDs());
//		deviceInfoItem.setObjectPaths(deviceInfoProfile.getObjectPaths());
		deviceInfoItem.setDescription(deviceInfoProfile.getDescription());
//		deviceInfoItem.setDeviceLabel(deviceInfoProfile.getDeviceLabel());
		deviceInfoItem.setManufacturer(deviceInfoProfile.getManufacturer());
		deviceInfoItem.setModel(deviceInfoProfile.getModel());
		deviceInfoItem.setDeviceType(deviceInfoProfile.getDeviceType());
		deviceInfoItem.setFwVersion(deviceInfoProfile.getFwVersion());
//		deviceInfoItem.setSwVersion(deviceInfoProfile.getSwVersion());
		deviceInfoItem.setHwVersion(deviceInfoProfile.getHwVersion());
		deviceInfoItem.setCreationTime(currentTime);
		deviceInfoItem.setLastModifiedTime(currentTime);
		
		try {
			deviceInfoDao.insert(deviceInfoItem);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.deviceInfo.createFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "deviceInfo create success");
		
		return this.findOneDeviceInfo("resourceID", deviceInfoItem.getResourceID());
	}

	/**
	 * delete deviceInfo
	 * @param key
	 * @param value
	 * @throws IotException
	 */
	public void deleteDeviceInfo(String key, String value) throws IotException {

		Query query = new Query();
		query.addCriteria(Criteria.where(key).is(value));
		try {

			deviceInfoDao.remove(query);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.deviceInfo.delFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "deviceInfo delete success");

	}
	
	/**
	 * update deviceInfo fwVersion by nodeID
	 * @param nodeID
	 * @param firmwareVersion
	 * @return
	 * @throws IotException
	 */
	public DeviceInfoVO updateDeviceInfoByFirmware(String nodeID, String firmwareVersion) throws IotException {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(nodeID));
		
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		Update updateQuery = new Update();
		updateQuery.set("lastModifiedTime", currentTime);
		if(!CommonUtil.isNull(firmwareVersion))	updateQuery.set("fwVersion", firmwareVersion);
		
		try { 
			deviceInfoDao.update(query, updateQuery);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.deviceInfo.upFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "Firmware update success");
		
		return this.findOneDeviceInfo("parentID", nodeID);
	}
	
	/**
	 * update deviceInfo
	 * @param deviceInfoProfile
	 * @return
	 * @throws IotException
	 */
	public DeviceInfoVO updateDeviceInfo(DeviceInfoVO deviceInfoProfile) throws IotException {
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		String labels 					= deviceInfoProfile.getLabels();
//		String mgmtDefinition 			= deviceInfoProfile.getMgmtDefinition();
//		String objectIDs 				= deviceInfoProfile.getObjectIDs();
//		String objectPaths 				= deviceInfoProfile.getObjectPaths();
		String description 				= deviceInfoProfile.getDescription();
//		String deviceLabel				= deviceInfoProfile.getDeviceLabel();
		String manufacturer 			= deviceInfoProfile.getManufacturer();
		String model 					= deviceInfoProfile.getModel();
		String deviceType 				= deviceInfoProfile.getDeviceType();
		String fwVersion 				= deviceInfoProfile.getFwVersion();
//		String swVersion				= deviceInfoProfile.getSwVersion();
		String hwVersion 				= deviceInfoProfile.getHwVersion();
		
		Update update = new Update();
		update.set("lastModifiedTime", currentTime);
		
		if(!CommonUtil.isNull(labels))			update.set("labels", labels);
//		if(!CommonUtil.isNull(mgmtDefinition))	update.set("mgmtDefinition", mgmtDefinition);
//		if(!CommonUtil.isNull(objectIDs))		update.set("objectIDs", objectIDs);
//		if(!CommonUtil.isNull(objectPaths))		update.set("objectPaths", objectPaths);
		if(!CommonUtil.isNull(description))		update.set("description", description);
//		if(!CommonUtil.isNull(deviceLabel))		update.set("deviceLabel", deviceLabel);
		if(!CommonUtil.isNull(manufacturer))	update.set("manufacturer", manufacturer);
		if(!CommonUtil.isNull(model))			update.set("model", model);
		if(!CommonUtil.isNull(deviceType))		update.set("deviceType", deviceType);
		if(!CommonUtil.isNull(fwVersion))		update.set("fwVersion", fwVersion);
//		if(!CommonUtil.isNull(swVersion))		update.set("swVersion", swVersion);
		if(!CommonUtil.isNull(hwVersion))		update.set("hwVersion",hwVersion);
		
		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(deviceInfoProfile.getResourceID()));
		try { 
			deviceInfoDao.update(query, update);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.deviceInfo.upFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "deviceInfo update success");
		
		return this.findOneDeviceInfo("resourceID", deviceInfoProfile.getResourceID());
		
	}

}