@XmlSchema(xmlns=
{
    @XmlNs(prefix = "m2m", namespaceURI = "http://www.onem2m.org/xml/protocols"),
    @XmlNs(prefix = "xsi", namespaceURI = "http://www.w3.org/2001/XMLSchema-instance")
})
package kr.re.keti.iot.domain.oneM2M;
import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlSchema;