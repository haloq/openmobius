package kr.re.keti.iot.domain.oneM2M;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import kr.re.keti.common.base.BaseVO;

/**
 * firmware domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@XmlRootElement(name="firmware", namespace="http://www.onem2m.org/xml/protocols")
@XmlAccessorType(value=XmlAccessType.FIELD)
public class FirmwareVO extends BaseVO {
	private static final long serialVersionUID = 1L;

	private String resourceType;
	private String resourceID;
	private String parentID;
	private String creationTime;
	private String lastModifiedTime;
	private String expirationTime;
	private String accessControlPolicyIDs;
	private String labels;
	private String mgmtDefinition;
	private String objectIDs;
	private String objectPaths;
	private String description;
	private String version;
	private String name;
	private String URL;
	private String update;
	private String updateStatus;

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceID() {
		return resourceID;
	}

	public void setResourceID(String resourceID) {
		this.resourceID = resourceID;
	}

	public String getParentID() {
		return parentID;
	}

	public void setParentID(String parentID) {
		this.parentID = parentID;
	}

	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	public String getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(String lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public String getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getAccessControlPolicyIDs() {
		return accessControlPolicyIDs;
	}

	public void setAccessControlPolicyIDs(String accessControlPolicyIDs) {
		this.accessControlPolicyIDs = accessControlPolicyIDs;
	}

	public String getLabels() {
		return labels;
	}

	public void setLabels(String labels) {
		this.labels = labels;
	}

	public String getMgmtDefinition() {
		return mgmtDefinition;
	}

	public void setMgmtDefinition(String mgmtDefinition) {
		this.mgmtDefinition = mgmtDefinition;
	}

	public String getObjectIDs() {
		return objectIDs;
	}

	public void setObjectIDs(String objectIDs) {
		this.objectIDs = objectIDs;
	}

	public String getObjectPaths() {
		return objectPaths;
	}

	public void setObjectPaths(String objectPaths) {
		this.objectPaths = objectPaths;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public String getUpdate() {
		return update;
	}

	public void setUpdate(String update) {
		this.update = update;
	}

	public String getUpdateStatus() {
		return updateStatus;
	}

	public void setUpdateStatus(String updateStatus) {
		this.updateStatus = updateStatus;
	}

	@Override
	public String toString() {
		return "FirmwareVO [resourceType=" + resourceType + ", resourceID="
				+ resourceID + ", parentID=" + parentID + ", creationTime="
				+ creationTime + ", lastModifiedTime=" + lastModifiedTime
				+ ", expirationTime=" + expirationTime
				+ ", accessControlPolicyIDs=" + accessControlPolicyIDs
				+ ", labels=" + labels + ", mgmtDefinition=" + mgmtDefinition
				+ ", objectIDs=" + objectIDs + ", objectPaths=" + objectPaths
				+ ", description=" + description + ", version=" + version
				+ ", name=" + name + ", URL=" + URL + ", update=" + update
				+ ", updateStatus=" + updateStatus + "]";
	}

}
