package kr.re.keti.iot.domain.oneM2M;

/**
 * cseDown domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
public class CseDownVO {

	private long downKey;
	private String downCriteriaId;
	private String cseId;
	private String startDt;
	private String endDt;
	private String lastDownYn;
	private String regiDate;
	private String regiUser;

	public long getDownKey() {
		return downKey;
	}

	public void setDownKey(long downKey) {
		this.downKey = downKey;
	}

	public String getDownCriteriaId() {
		return downCriteriaId;
	}

	public void setDownCriteriaId(String downCriteriaId) {
		this.downCriteriaId = downCriteriaId;
	}

	public String getCseId() {
		return cseId;
	}

	public void setCseId(String cseId) {
		this.cseId = cseId;
	}

	public String getStartDt() {
		return startDt;
	}

	public void setStartDt(String startDt) {
		this.startDt = startDt;
	}

	public String getEndDt() {
		return endDt;
	}

	public void setEndDt(String endDt) {
		this.endDt = endDt;
	}

	public String getLastDownYn() {
		return lastDownYn;
	}

	public void setLastDownYn(String lastDownYn) {
		this.lastDownYn = lastDownYn;
	}

	public String getRegiDate() {
		return regiDate;
	}

	public void setRegiDate(String regiDate) {
		this.regiDate = regiDate;
	}

	public String getRegiUser() {
		return regiUser;
	}

	public void setRegiUser(String regiUser) {
		this.regiUser = regiUser;
	}

	@Override
	public String toString() {
		return "FaultCseDownVO [downKey=" + downKey + ", downCriteriaId=" + downCriteriaId + ", cseId=" + cseId + ", startDt=" + startDt + ", endDt=" + endDt + ", lastDownYn=" + lastDownYn + ", regiDate=" + regiDate + ", regiUser=" + regiUser + "]";
	}

}
