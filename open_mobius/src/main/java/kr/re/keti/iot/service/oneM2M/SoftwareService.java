package kr.re.keti.iot.service.oneM2M;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.NodeVO;
import kr.re.keti.iot.domain.oneM2M.SoftwareVO;
import kr.re.keti.iot.mdao.common.SequenceDao;
import kr.re.keti.iot.mdao.common.SequenceDao.MovType;
import kr.re.keti.iot.mdao.common.SequenceDao.SEQ_PREFIX;
import kr.re.keti.iot.mdao.common.SequenceDao.SeqType;
import kr.re.keti.iot.mdao.oneM2M.SoftwareDao;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.MgmtCmdService.MGMT_CMD_TYPE;
import kr.re.keti.iot.service.oneM2M.NodeService.MGMT_DEFINITION;
import kr.re.keti.iot.service.oneM2M.NodeService.STATUS;
import kr.re.keti.iot.util.CommonUtil;
import kr.re.keti.iot.util.oneM2M.CommonCode.RESOURCE_TYPE;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

/**
 * software management Service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Service
public class SoftwareService {

	private static final Log logger = LogFactory.getLog(SoftwareService.class);

	@Autowired
	private MongoLogService mongoLogService;
	
	@Autowired
	private MgmtCmdService mgmtCmdService;
	
//	@Autowired
//	private AEService aEService;

	@Autowired
	private SoftwareDao softwareDao;

	@Autowired
	private SequenceDao seqDao;
	
	public enum SOFTWARE_STATUS{
		TRUE("true") , 
		FALSE("false");
		
		private final String id;
		SOFTWARE_STATUS(String id){
			this.id = id;
		}
		public String getValue() {return id;}
			
	}
	
	/**
	 * generate resourceID
	 * @return
	 * @throws IotException
	 */
	private String generateResourceId() throws IotException {
		long longSeq = 0;
		StringBuffer bufSeq = new StringBuffer(SEQ_PREFIX.SOFTWARE.getValue());
		
		try {
			longSeq = seqDao.move(MovType.UP, SeqType.SOFTWARE);
			
			bufSeq.append(String.format("%010d", longSeq));

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.software.id.createFail.text"));
		}
		
		return bufSeq.toString();
	}
	
	/**
	 * find software count by resourceId
	 * @param resourceId
	 * @return
	 */
	public long getCount(String resourceId){
		
		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(resourceId));
		
		long cnt = 0;
		
		try {
			cnt = softwareDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "software get count");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		return cnt;
	}	
	
	/**
	 * find software count by nodeId-name
	 * @param nodeId
	 * @param name
	 * @return
	 */
	public long getCountByName(String nodeId, String name){
		return this.getCountByNameVersion(nodeId, name, null);
	}
	
	/**
	 * find software count by nodeId-name-version
	 * @param nodeId
	 * @param name
	 * @param version
	 * @return
	 */
	public long getCountByNameVersion(String nodeId, String name, String version){
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(nodeId));
		query.addCriteria(Criteria.where("name").is(name));
		if(!CommonUtil.isEmpty(version)) {
			query.addCriteria(Criteria.where("version").is(version));
			
		}
		query.addCriteria(Criteria.where("installStatus").is(STATUS.SUCCESSFUL.getValue()));
		
		
		long cnt = 0;
		
		try {
			cnt = softwareDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "software get count");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		return cnt;
	}
	
	/**
	 * find software count in request control by parentId-name
	 * @param parentId
	 * @param name
	 * @return
	 * @throws IotException
	 */
	public long getCountControlling(String parentId, String name) throws IotException {
		SoftwareVO findSoftwareItem = null;
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(parentId));
		query.addCriteria(Criteria.where("name").is(name));
		query.addCriteria(Criteria.where("installStatus").is(STATUS.IN_PROCESS.getValue()));
		
		long cnt = 0;
		
		try {
			findSoftwareItem = (SoftwareVO)softwareDao.findOne(query);
			
			if(!CommonUtil.isEmpty(findSoftwareItem)) {
				
				if(CommonUtil.checkElapseTime(findSoftwareItem.getLastModifiedTime(), Long.valueOf(CommonUtil.getConfig("iot.mgmtCmd.status.check.time")), "yyyy-MM-dd'T'HH:mm:ssXXX") > 0){
					this.deleteSoftwareByInstallStatus(parentId, name, STATUS.IN_PROCESS.getValue());
				}
			}
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.software.findFail.text"));
		}
		
		try {
			cnt = softwareDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "software get count");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		return cnt;
	}	

	/**
	 * retrieve software cmdType
	 * @param findSoftwareItem
	 * @return
	 * @throws IotException
	 */
	public String getSoftwareCmdType(SoftwareVO findSoftwareItem) throws IotException {
		
		String cmdType = MGMT_CMD_TYPE.SOFTWAREINSTALL.getValue();
		
		if(!CommonUtil.isEmpty(findSoftwareItem)) {
			String install = findSoftwareItem.getInstall();
			String uninstall = findSoftwareItem.getUninstall();
			
			if(!CommonUtil.isEmpty(install) && !CommonUtil.isEmpty(uninstall)) {
				if(install.equals(SOFTWARE_STATUS.FALSE.getValue()) && uninstall.equals(SOFTWARE_STATUS.TRUE.getValue())) {
					cmdType = MGMT_CMD_TYPE.SOFTWAREUNINSTALL.getValue();
				}
			}
		} else {
			throw new IotException("600", CommonUtil.getMessage("msg.device.software.findFail.text"));
		}

		return cmdType;
	}
	
	/**
	 * find software by key-value
	 * @param key
	 * @param value
	 * @return
	 * @throws IotException
	 */
	public SoftwareVO findOneSoftware(String key, String value) throws IotException {
		SoftwareVO findSoftwareItem = null;

		Query query = new Query();
		query.addCriteria(Criteria.where(key).is(value));

		try {
			findSoftwareItem = (SoftwareVO) softwareDao.findOne(query);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.software.findFail.text"));
		}

		return findSoftwareItem;
	}
	
	/**
	 * find software by nodeId-name
	 * @param nodeId
	 * @param name
	 * @return
	 * @throws IotException
	 */
	public SoftwareVO findOneSoftwareByName(String nodeId, String name) throws IotException {
		SoftwareVO findSoftwareItem = null;
		
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(nodeId));
		query.addCriteria(Criteria.where("name").is(name));
		query.addCriteria(Criteria.where("installStatus").is(STATUS.SUCCESSFUL.getValue()));
		
		try {
			findSoftwareItem = (SoftwareVO)softwareDao.findOne(query);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.software.findFail.text"));
		}
		
		return findSoftwareItem;
	}
	
	/**
	 * retrieve createSoftware info
	 * @param nodeInfo
	 * @param softwareProfile
	 * @param isControl
	 * @return
	 * @throws IotException
	 */
	public SoftwareVO getCreateSoftwareVO(NodeVO nodeInfo, SoftwareVO softwareProfile, boolean isControl) throws IotException {
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		SoftwareVO softwareItem = new SoftwareVO();
		
		if(!isControl) {
			softwareItem.setResourceID(this.generateResourceId());
		}

		softwareItem.setResourceType(RESOURCE_TYPE.NODE.getName());
		softwareItem.setParentID(nodeInfo.getResourceID());
		softwareItem.setAccessControlPolicyIDs(nodeInfo.getAccessControlPolicyIDs());
		softwareItem.setLabels(softwareProfile.getName());
		softwareItem.setName(softwareProfile.getName());
		softwareItem.setDescription(softwareProfile.getDescription());
		softwareItem.setVersion(softwareProfile.getVersion());
		softwareItem.setURL(softwareProfile.getURL());
		softwareItem.setInstallStatus(softwareProfile.getInstallStatus());
		softwareItem.setInstall(softwareProfile.getInstall());
		softwareItem.setUninstall(softwareProfile.getUninstall());
		softwareItem.setActivate(softwareProfile.getActivate());
		softwareItem.setDeactivate(softwareProfile.getDeactivate());
		softwareItem.setActiveStatus(softwareProfile.getActiveStatus());
		softwareItem.setCreationTime(currentTime);
		softwareItem.setLastModifiedTime(currentTime);

		return softwareItem;
	}
	
	/**
	 * create software
	 * @param nodeInfo
	 * @param softwareProfile
	 * @return
	 * @throws IotException
	 */
	public SoftwareVO createSoftware(NodeVO nodeInfo, SoftwareVO softwareProfile) throws IotException {
		SoftwareVO softwareItem = new SoftwareVO();
		
		softwareItem = this.getCreateSoftwareVO(nodeInfo, softwareProfile, false);
		softwareItem.setMgmtDefinition(MGMT_DEFINITION.SOFTWARE.getValue());
		softwareItem.setInstall(SOFTWARE_STATUS.TRUE.getValue());
		softwareItem.setUninstall(SOFTWARE_STATUS.FALSE.getValue());
		softwareItem.setInstallStatus(STATUS.SUCCESSFUL.getValue());
		
		try {
			softwareDao.insert(softwareItem);
			
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.software.createFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.INFO, "software create success");
		
		return this.findOneSoftware("resourceID", softwareItem.getResourceID());
	}

	/**
	 * delete software by key-value
	 * @param key
	 * @param value
	 * @throws IotException
	 */
	public void deleteSoftware(String key, String value) throws IotException {

		Query query = new Query();
		query.addCriteria(Criteria.where(key).is(value));
		try {
			
			softwareDao.remove(query);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.software.delFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "software delete success");

	}
	
	/**
	 * delete software by InstallStatus
	 * @param nodeId
	 * @param name
	 * @param installStatus
	 * @throws IotException
	 */
	public void deleteSoftwareByInstallStatus(String nodeId, String name, String installStatus) throws IotException {
		
		Query query = new Query(Criteria.where("parentID").is(nodeId));
		query.addCriteria(Criteria.where("name").is(name));
		if(!CommonUtil.isEmpty(installStatus)) {
			query.addCriteria(Criteria.where("installStatus").is(installStatus));
		}
		
		try {
			softwareDao.remove(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.software.delFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "software delete success");
	}
	
	/**
	 * update software
	 * @param softwareProfile
	 * @param isControl
	 * @return
	 * @throws IotException
	 */
	public SoftwareVO updateSoftware(SoftwareVO softwareProfile, boolean isControl) throws IotException {

		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
//		String labels 					= softwareProfile.getLabels();
		String description 				= softwareProfile.getDescription();
		String version 					= softwareProfile.getVersion();
		String name 					= softwareProfile.getName();
		String URL 						= softwareProfile.getURL();
		String install 					= softwareProfile.getInstall();
		String uninstall 				= softwareProfile.getUninstall();
//		String installStatus 			= softwareProfile.getInstallStatus();
		String activate					= softwareProfile.getActivate();
		String deactivate 				= softwareProfile.getDeactivate();
//		String activeStatus 			= softwareProfile.getActiveStatus();
		
		if(!isControl) {
			install = null;
			uninstall = null;
		}
		
		Update update = new Update();
		if(!CommonUtil.isNull(name))			update.set("labels", name);
		if(!CommonUtil.isNull(name))			update.set("name",name );
		if(!CommonUtil.isNull(version))			update.set("version",version );
		if(!CommonUtil.isNull(description))		update.set("description", description);
		if(!CommonUtil.isNull(URL))				update.set("URL", URL);
		if(!CommonUtil.isNull(install))			update.set("install",install);
		if(!CommonUtil.isNull(uninstall))		update.set("uninstall",uninstall );
//		if(!CommonUtil.isNull(installStatus))	update.set("installStatus",installStatus );
		if(!CommonUtil.isNull(activate))		update.set("activate", activate);
		if(!CommonUtil.isNull(deactivate))		update.set("deactivate", deactivate);
//		if(!CommonUtil.isNull(activeStatus))	update.set("activeStatus", activeStatus);
												update.set("lastModifiedTime", currentTime);
		
		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(softwareProfile.getResourceID()));
		try { 
			softwareDao.update(query, update);
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.software.upFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "software update success");
		
		return this.findOneSoftware("resourceID", softwareProfile.getResourceID());

	}
	
}