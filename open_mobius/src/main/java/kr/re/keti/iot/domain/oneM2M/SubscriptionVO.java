package kr.re.keti.iot.domain.oneM2M;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import kr.re.keti.common.base.BaseVO;

/**
 * subscription domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@XmlRootElement(name="subscription", namespace="http://www.onem2m.org/xml/protocols")
@XmlAccessorType(value=XmlAccessType.FIELD)
public class SubscriptionVO extends BaseVO {
	private static final long serialVersionUID = 1L;

	private String resourceType;
	private String resourceID;
	private String parentID;
	private String creationTime;
	private String lastModifiedTime;
	private String expirationTime;
	private String accessControlPolicyIDs;
	private String labels;
	private EventNotificationCriteriaVO eventNotificationCriteria;
	private String expirationCounter;
	private String notificationURI;
	private String pendingNotification;
	private String notificationContentType;
	private String creator;
	private String subscriberURI;
	
	public SubscriptionVO() {
		eventNotificationCriteria = new EventNotificationCriteriaVO();
	}
	
	@XmlRootElement(name="eventNotificationCriteria")
	public static class EventNotificationCriteriaVO {
		private String modifiedSince;
		private String unmodifiedSince;
		private String resourceStatus;
		private String stateTagSmaller;
		private String stateTagBigger;
		private String sizeAbove;
		private String sizeBelow;
		
		public String getModifiedSince() {
			return modifiedSince;
		}
		public void setModifiedSince(String modifiedSince) {
			this.modifiedSince = modifiedSince;
		}
		public String getUnmodifiedSince() {
			return unmodifiedSince;
		}
		public void setUnmodifiedSince(String unmodifiedSince) {
			this.unmodifiedSince = unmodifiedSince;
		}
		public String getResourceStatus() {
			return resourceStatus;
		}
		public void setResourceStatus(String resourceStatus) {
			this.resourceStatus = resourceStatus;
		}
		public String getStateTagSmaller() {
			return stateTagSmaller;
		}
		public void setStateTagSmaller(String stateTagSmaller) {
			this.stateTagSmaller = stateTagSmaller;
		}
		public String getStateTagBigger() {
			return stateTagBigger;
		}
		public void setStateTagBigger(String stateTagBigger) {
			this.stateTagBigger = stateTagBigger;
		}
		public String getSizeAbove() {
			return sizeAbove;
		}
		public void setSizeAbove(String sizeAbove) {
			this.sizeAbove = sizeAbove;
		}
		public String getSizeBelow() {
			return sizeBelow;
		}
		public void setSizeBelow(String sizeBelow) {
			this.sizeBelow = sizeBelow;
		}
		@Override
		public String toString() {
			return "eventNotificationCriteriaVO [modifiedSince="
					+ modifiedSince + ", unmodifiedSince=" + unmodifiedSince
					+ ", resourceStatus=" + resourceStatus
					+ ", stateTagSmaller=" + stateTagSmaller
					+ ", stateTagBigger=" + stateTagBigger + ", sizeAbove="
					+ sizeAbove + ", sizeBelow=" + sizeBelow + "]";
		}
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceID() {
		return resourceID;
	}

	public void setResourceID(String resourceID) {
		this.resourceID = resourceID;
	}

	public String getParentID() {
		return parentID;
	}

	public void setParentID(String parentID) {
		this.parentID = parentID;
	}

	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	public String getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(String lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public String getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getAccessControlPolicyIDs() {
		return accessControlPolicyIDs;
	}

	public void setAccessControlPolicyIDs(String accessControlPolicyIDs) {
		this.accessControlPolicyIDs = accessControlPolicyIDs;
	}

	public String getLabels() {
		return labels;
	}

	public void setLabels(String labels) {
		this.labels = labels;
	}

	public EventNotificationCriteriaVO getEventNotificationCriteria() {
		return eventNotificationCriteria;
	}

	public void setEventNotificationCriteria(
			EventNotificationCriteriaVO eventNotificationCriteria) {
		this.eventNotificationCriteria = eventNotificationCriteria;
	}

	public String getExpirationCounter() {
		return expirationCounter;
	}

	public void setExpirationCounter(String expirationCounter) {
		this.expirationCounter = expirationCounter;
	}

	public String getNotificationURI() {
		return notificationURI;
	}

	public void setNotificationURI(String notificationURI) {
		this.notificationURI = notificationURI;
	}

	public String getPendingNotification() {
		return pendingNotification;
	}

	public void setPendingNotification(String pendingNotification) {
		this.pendingNotification = pendingNotification;
	}

	public String getNotificationContentType() {
		return notificationContentType;
	}

	public void setNotificationContentType(String notificationContentType) {
		this.notificationContentType = notificationContentType;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getSubscriberURI() {
		return subscriberURI;
	}

	public void setSubscriberURI(String subscriberURI) {
		this.subscriberURI = subscriberURI;
	}

	@Override
	public String toString() {
		return "SubscriptionVO [resourceType=" + resourceType + ", resourceID="
				+ resourceID + ", parentID=" + parentID + ", creationTime="
				+ creationTime + ", lastModifiedTime=" + lastModifiedTime
				+ ", expirationTime=" + expirationTime
				+ ", accessControlPolicyIDs=" + accessControlPolicyIDs
				+ ", labels=" + labels + ", eventNotificationCriteria="
				+ eventNotificationCriteria + ", expirationCounter="
				+ expirationCounter + ", notificationURI=" + notificationURI
				+ ", pendingNotification=" + pendingNotification
				+ ", notificationContentType=" + notificationContentType
				+ ", creator=" + creator + ", subscriberURI=" + subscriberURI
				+ "]";
	}
	
}
