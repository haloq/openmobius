package kr.re.keti.iot.domain.oneM2M;

/**
 * cseDownCriteria domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
public class CseDownCriteriaVO {

	private String downCriteriaId;
	private String downCriteriaName;
	private String useYn;
	private int value;
	private String insideDownYn;
	private String description;
	private String regiDate;
	private String regiUser;
	public String getDownCriteriaId() {
		return downCriteriaId;
	}
	public void setDownCriteriaId(String downCriteriaId) {
		this.downCriteriaId = downCriteriaId;
	}
	public String getDownCriteriaName() {
		return downCriteriaName;
	}
	public void setDownCriteriaName(String downCriteriaName) {
		this.downCriteriaName = downCriteriaName;
	}
	public String getUseYn() {
		return useYn;
	}
	public void setUseYn(String useYn) {
		this.useYn = useYn;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public String getInsideDownYn() {
		return insideDownYn;
	}
	public void setInsideDownYn(String insideDownYn) {
		this.insideDownYn = insideDownYn;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRegiDate() {
		return regiDate;
	}
	public void setRegiDate(String regiDate) {
		this.regiDate = regiDate;
	}
	public String getRegiUser() {
		return regiUser;
	}
	public void setRegiUser(String regiUser) {
		this.regiUser = regiUser;
	}
	@Override
	public String toString() {
		return "FaultCseDownCriteriaVO [downCriteriaId=" + downCriteriaId + ", downCriteriaName=" + downCriteriaName + ", useYn=" + useYn + ", value=" + value + ", insideDownYn=" + insideDownYn + ", description=" + description + ", regiDate=" + regiDate + ", regiUser=" + regiUser + "]";
	}

}
