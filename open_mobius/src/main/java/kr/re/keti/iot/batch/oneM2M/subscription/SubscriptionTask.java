package kr.re.keti.iot.batch.oneM2M.subscription;

import java.util.concurrent.atomic.AtomicInteger;

import kr.re.keti.iot.domain.oneM2M.SubscriptionVO;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.MgmtCmdService;
import kr.re.keti.iot.service.oneM2M.SubscriptionService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * subscription Task.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>khlee</li>
 *         </ul>
 */
@Component
public class SubscriptionTask implements Runnable {

	private static final Log logger = LogFactory.getLog(MgmtCmdService.class);

	@Autowired
	private MongoLogService mongoLogService;

	@Autowired
	private SubscriptionRunnableService subscriptionRunnableService;

	@Autowired
	private SubscriptionService subscriptionService;

	private SubscriptionVO subscriptionProfile;
	
	private Object objectEntity; 

	private AtomicInteger runCount;

	/**
	 * subscription run function
	 */
	@Override
	public void run() {
		try {
			if (runCount.get() > 0) {
				mongoLogService.log(logger, LEVEL.ERROR, "SubscriptionTask " + subscriptionProfile.getResourceID() + " Start!! runCount = " + runCount);
//				subscriptionService.sendSubscription(subscriptionProfile.getNotificationURI(), stringEntity);
				subscriptionService.sendSubscription(subscriptionProfile, objectEntity);
				
			} else {
				mongoLogService.log(logger, LEVEL.ERROR, "SubscriptionTask " + subscriptionProfile.getResourceID() + " stop!!  runCount = " + runCount);
				subscriptionRunnableService.stopSchedule(subscriptionProfile.getResourceID());
			}

			runCount.decrementAndGet();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public SubscriptionVO getSubscriptionProfile() {
		return subscriptionProfile;
	}

	public void setSubscriptionProfile(SubscriptionVO subscriptionProfile) {
		this.subscriptionProfile = subscriptionProfile;
	}

	public void setMgmtCmdRunnableService(SubscriptionRunnableService mgmtCmdRunnableService) {
		this.subscriptionRunnableService = mgmtCmdRunnableService;
	}

	public void setSubscriptionService(SubscriptionService subscriptionService) {
		this.subscriptionService = subscriptionService;
	}
	
	public void setObjectEntity(Object objectEntity) {
		this.objectEntity = objectEntity;
	}

	public void setMongoLogService(MongoLogService mongoLogService) {
		this.mongoLogService = mongoLogService;
	}

	public int getRunCount() {
		return runCount.get();
	}

	public void setRunCount(int runCount) {
		this.runCount = new AtomicInteger(runCount);
	}

}
