package kr.re.keti.iot.batch.oneM2M.subscription;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.SubscriptionVO;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.SubscriptionService;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * subscription runnable service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>khlee</li>
 *         </ul>
 */
@Service
public class SubscriptionRunnableService {

	private static final Log logger = LogFactory.getLog(SubscriptionRunnableService.class);

	@Autowired
	private MongoLogService mongoLogService;

	@Autowired
	private SubscriptionService subscriptionService;
	

	private final Map<String, Future<?>> futuresMapping = new HashMap<String, Future<?>>();
	private static ScheduledExecutorService exec = Executors.newScheduledThreadPool(100);
	
	/**
	 * subscription start schedule
	 * @param subscriptionProfile
	 * @param objectEntity
	 * @throws IotException
	 */
	public void startSchedule(SubscriptionVO subscriptionProfile, Object objectEntity) throws IotException {
	
		long initialDelay =  0;
		int  runCount     =  1;
	
		SubscriptionTask task = new SubscriptionTask();
		task.setMgmtCmdRunnableService(this);
		task.setMongoLogService(mongoLogService);
		task.setSubscriptionService(subscriptionService);
		task.setSubscriptionProfile(subscriptionProfile);
		task.setObjectEntity(objectEntity);
		task.setRunCount(runCount);
		
		try {
			if (!CommonUtil.isEmpty(futuresMapping.get(subscriptionProfile.getResourceID()))) {
				this.stopSchedule(subscriptionProfile.getResourceID());
			}

			exec.schedule(task, initialDelay, TimeUnit.SECONDS);

		} catch (Exception e) {
			e.printStackTrace();
			throw new IotException("600", "SubscriptionTask schedule Error");
		}
	}

	/**
	 * subscription stop schedule
	 * @param subscriptionProfile
	 * @param objectEntity
	 * @throws IotException
	 */
	public void stopSchedule(String resourceId) throws IotException {
		
		mongoLogService.log(logger, LEVEL.ERROR, "SubscriptionTask " + resourceId + " Schedule stop");
		Future<?> future = futuresMapping.get(resourceId);
		try {
			if (!CommonUtil.isEmpty(future) && !future.isDone()) {
				future.cancel(true);
				futuresMapping.remove(resourceId);
			}
		} catch (Exception e) {
			throw new IotException("600", "SubscriptionTask schedule cancle Error");
		}
	}

}
