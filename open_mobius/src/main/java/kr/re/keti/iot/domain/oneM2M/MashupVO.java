package kr.re.keti.iot.domain.oneM2M;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

/**
 * mashup domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@XmlRootElement(name="mashup")
public class MashupVO {
	private String orders;
	private String data;

	@XmlAttribute(name = "order")
	public String getOrders() {
		return orders;
	}

	public void setOrders(String orders) {
		this.orders = orders;
	}

	@XmlValue
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
