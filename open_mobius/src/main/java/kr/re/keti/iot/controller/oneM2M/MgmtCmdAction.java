package kr.re.keti.iot.controller.oneM2M;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.re.keti.iot.domain.apilog.CallerVO;
import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.ExecInstanceVO;
import kr.re.keti.iot.domain.oneM2M.ListOfResourceVO;
import kr.re.keti.iot.domain.oneM2M.MgmtCmdOnlyIdVO;
import kr.re.keti.iot.domain.oneM2M.MgmtCmdVO;
import kr.re.keti.iot.domain.oneM2M.RemoteCSEVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao.ONEM2M;
import kr.re.keti.iot.service.apilog.ApiLogService;
import kr.re.keti.iot.service.apilog.ApiLogService.CALLER_FG;
import kr.re.keti.iot.service.apilog.ApiLogService.CALL_TYPE;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.ExecInstanceService;
import kr.re.keti.iot.service.oneM2M.ExecInstanceService.EXEC_STATUS;
import kr.re.keti.iot.service.oneM2M.MgmtCmdService;
import kr.re.keti.iot.service.oneM2M.RemoteCSEService;
import kr.re.keti.iot.service.oneM2M.common.MCommonService;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * mgmtCmd management Action.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Controller
public class MgmtCmdAction {

	private static final Log logger = LogFactory.getLog(MgmtCmdAction.class);
	
	@Autowired
	private MongoLogService mongoLogService;
	
	@Autowired
	private MCommonService mCommonService;

	@Autowired
	private RemoteCSEService remoteCSEService;

	@Autowired
	private MgmtCmdService mgmtCmdService;
	
	@Autowired
	private ExecInstanceService execInstanceService;
	
	@Autowired
	private ApiLogService apiLogService;
	
	/**
	 * create mgmtCmd
	 * @param remoteCSEID
	 * @param labels
	 * @param mgmtCmdProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}", method = RequestMethod.POST, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" }, params = "ty=mgmtCmd")
	@ResponseBody
	public ResponseEntity<Object> mgmtCmdCreate(@PathVariable String remoteCSEID
											  , @RequestParam(value="nm", required=true) String labels
											  , @RequestBody MgmtCmdVO mgmtCmdProfile
											  , HttpServletRequest request
											  , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		MgmtCmdVO responseVo = new MgmtCmdVO();
		HttpStatus httpStatus = HttpStatus.CREATED;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dkey		= CommonUtil.nvl(request.getHeader("dKey"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(remoteCSEID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(labels)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "nm " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(mgmtCmdProfile)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<mgmtCmd> " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		
		if(isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus.OK);
		}
		
		try {
			
			RemoteCSEVO remoteCSEItem = null;
			if (CommonUtil.isEmpty((remoteCSEItem = remoteCSEService.findOneRemoteCSE(remoteCSEID)))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dkey);
			
			MgmtCmdVO findMgmtCmdItem = null;
			if (!CommonUtil.isEmpty(findMgmtCmdItem = mgmtCmdService.findOneMgmtCmdByLabels(remoteCSEID, labels))) {
				//responseVo.setMgmtCmd(findMgmtCmdItem);
				BeanUtils.copyProperties(findMgmtCmdItem, responseVo);
				throw new IotException("609", CommonUtil.getMessage("msg.mgmtCmd.duplicate.text", locale));
			}
			 
			mgmtCmdService.checkMgmtCmdValidation(mgmtCmdProfile);
			
			mgmtCmdProfile.setParentID(remoteCSEID);
			mgmtCmdProfile.setLabels(labels);
			findMgmtCmdItem = mgmtCmdService.createMgmtCmd(remoteCSEItem, mgmtCmdProfile);
			
			//responseVo.setMgmtCmd(findMgmtCmdItem);
			BeanUtils.copyProperties(findMgmtCmdItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);

		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * delete mgmtCmd
	 * @param remoteCSEID
	 * @param mgmtCmdName
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID}/mgmtCmd-{mgmtCmdName}", method = RequestMethod.DELETE, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Void> mgmtCmdDelete(@PathVariable String remoteCSEID
											, @PathVariable String mgmtCmdName
											, HttpServletRequest request
											, HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		Object responseVo = new Object();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg  = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dkey		= CommonUtil.nvl(request.getHeader("dKey"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(remoteCSEID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(mgmtCmdName)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "mgmtCmdName " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		
		if(isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Void>(httpStatus);
		}
		
		try {
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dkey);
			
			MgmtCmdVO mgmtCmdItem = null;
			if (CommonUtil.isEmpty(mgmtCmdItem = mgmtCmdService.findOneMgmtCmdByLabels(remoteCSEID, mgmtCmdName))) {
				throw new IotException("627", CommonUtil.getMessage("msg.mgmtCmd.noRegi.text", locale));
			}
			
			mgmtCmdService.deleteMgmtCmd(remoteCSEID, mgmtCmdItem.getResourceID());

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Void>(httpStatus);
	}
	
	/**
	 * request control mgmtCmd
	 * @param remoteCSEID
	 * @param mgmtCmdName
	 * @param mgmtCmdProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:[^9.9.999.9].*}/mgmtCmd-{mgmtCmdName}", method = RequestMethod.POST, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" }, params = "ty=execInstance")
	@ResponseBody
	public ResponseEntity<Object> mgmtCmdControl(@PathVariable String remoteCSEID
											   , @PathVariable String mgmtCmdName
											   , @RequestBody MgmtCmdVO mgmtCmdProfile
											   , HttpServletRequest request
											   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();
		
		ExecInstanceVO responseVo = new ExecInstanceVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey		= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic 	= CommonUtil.nvl(request.getHeader("topic"), "");
		CallerVO callerVO = null;
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(remoteCSEID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(mgmtCmdName)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "mgmtCmdName " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		else if(CommonUtil.isEmpty(mgmtCmdProfile)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<mgmtCmd> " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		
		if(isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			
			if(remoteCSEService.getCount(remoteCSEID) < 1) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			if(mgmtCmdService.getCountByLabels(remoteCSEID, mgmtCmdName) < 1) {
				throw new IotException("627", CommonUtil.getMessage("msg.mgmtCmd.noRegi.text", locale));
			}
			
			mgmtCmdProfile.setParentID(remoteCSEID);
			mgmtCmdProfile.setLabels(mgmtCmdName);
			ExecInstanceVO findExecInstanceItem = mgmtCmdService.mgmtCmdControl(mgmtCmdProfile);
			
			//responseVo.setExecInstance(findExecInstanceItem);
			BeanUtils.copyProperties(findExecInstanceItem, responseVo);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);

		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * retrieve mgmtCmd List
	 * @param remoteCSEID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/mgmtCmd", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> mgmtCmdListRetrieve(@PathVariable String remoteCSEID
													, HttpServletRequest request
													, HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ListOfResourceVO responseVo = new ListOfResourceVO();
		responseVo.setName(ONEM2M.MGMT_CMD.getName());
		
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg  = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey		= CommonUtil.nvl(request.getHeader("uKey"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(remoteCSEID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if(isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			
			if(remoteCSEService.getCount(remoteCSEID) < 1) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			List<MgmtCmdOnlyIdVO> findMgmtCmdList = null;
			if(CommonUtil.isEmpty(findMgmtCmdList = mgmtCmdService.findListMgmtCmdOnlyId("parentID", remoteCSEID))) {
				throw new IotException("604", CommonUtil.getMessage("msg.mgmtCmd.notFound.text", locale));
			}
			
			responseVo.setMgmtCmdList(findMgmtCmdList);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}

	/**
	 * retrieve mgmtCmd
	 * @param remoteCSEID
	 * @param mgmtCmdName
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID}/mgmtCmd-{mgmtCmdName}", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> mgmtCmdRetrieve(@PathVariable String remoteCSEID
												, @PathVariable String mgmtCmdName
												, HttpServletRequest request
												, HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();
		
		MgmtCmdVO responseVo = new MgmtCmdVO();
		HttpStatus httpStatus = HttpStatus.OK;
		
		String responseCode = "200";
		String responseMsg  = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey		= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic 	= CommonUtil.nvl(request.getHeader("topic"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(remoteCSEID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(mgmtCmdName)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "mgmtCmdName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if(isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);
			
			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			 
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if(remoteCSEService.getCount(remoteCSEID) < 1) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			MgmtCmdVO findMgmtCmdItem = null;
			if(CommonUtil.isEmpty(findMgmtCmdItem = mgmtCmdService.findOneMgmtCmdByLabels(remoteCSEID, mgmtCmdName))) {
				throw new IotException("604", CommonUtil.getMessage("msg.mgmtCmd.notFound.text", locale));
			}
			
			//responseVo.setMgmtCmd(findMgmtCmdItem);
			BeanUtils.copyProperties(findMgmtCmdItem, responseVo);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();
			
			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * update mgmtCmd control result
	 * @param remoteCSEID
	 * @param mgmtCmdName
	 * @param execInstanceID
	 * @param execInstanceProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID}/mgmtCmd-{mgmtCmdName}/execInstance-{execInstanceID}", method = RequestMethod.PUT, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> mgmtCmdResultUpdate(@PathVariable String remoteCSEID
													, @PathVariable String mgmtCmdName
													, @PathVariable String execInstanceID
													, @RequestBody ExecInstanceVO execInstanceProfile
													, HttpServletRequest request
													, HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();
		
		ExecInstanceVO responseVo = new ExecInstanceVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey		= CommonUtil.nvl(request.getHeader("dKey"), "");
		String sKey		= CommonUtil.nvl(request.getHeader("sKey"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(remoteCSEID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(mgmtCmdName)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "mgmtCmdName " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		else if(CommonUtil.isEmpty(execInstanceID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "execInstanceID " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		else if(CommonUtil.isEmpty(execInstanceProfile)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<execInstance> " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		
		if(isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			
			RemoteCSEVO findFemoteCSEItem = null;
			if(CommonUtil.isEmpty(findFemoteCSEItem = remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dKey);
			
			MgmtCmdVO findMgmtCmdItem = null;
			if(CommonUtil.isEmpty(findMgmtCmdItem = mgmtCmdService.findOneMgmtCmdByLabels(remoteCSEID, mgmtCmdName))) {
				throw new IotException("627", CommonUtil.getMessage("msg.mgmtCmd.noRegi.text", locale));
			}
			
			if(execInstanceService.getCount(execInstanceID) < 1) {
				throw new IotException("628", CommonUtil.getMessage("msg.device.execInstance.noRegi.text", locale));
			}
			
			ExecInstanceVO execInstanceItem = null;
			if(CommonUtil.isEmpty(execInstanceItem = execInstanceService.findOneExecInstanceByMgmtCmdId(findMgmtCmdItem.getResourceID(), execInstanceID))) {
				throw new IotException("691", CommonUtil.getMessage("msg.mgmtCmd.execInstancesList.noauth", new String[]{execInstanceID}));
			}
			
			if(execInstanceItem.getExecStatus().equals(EXEC_STATUS.FINISHED.getValue())) {
				throw new IotException("600", CommonUtil.getMessage("msg.device.execInstance.finish.text", locale));
			}			
			
			execInstanceProfile.setResourceID(execInstanceID);
			ExecInstanceVO findExecInstanceItem = mgmtCmdService.mgmtCmdResultUpdate(findMgmtCmdItem, execInstanceProfile, execInstanceItem.getExecReqArgs());
			
			//responseVo.setExecInstance(findExecInstanceItem);
			BeanUtils.copyProperties(findExecInstanceItem, responseVo);

			apiLogService.log(CALLER_FG.DEVICE, remoteCSEID, CALL_TYPE.EXEC_INSTANCE_UPDATE, "Y");
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			apiLogService.log(CALLER_FG.DEVICE, remoteCSEID, CALL_TYPE.EXEC_INSTANCE_UPDATE, "N");
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);

		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * update mgmtCmd
	 * @param remoteCSEID
	 * @param mgmtCmdName
	 * @param mgmtCmdProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID}/mgmtCmd-{mgmtCmdName}", method = RequestMethod.PUT, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> mgmtCmdUpdate(@PathVariable String remoteCSEID
											  , @PathVariable String mgmtCmdName
											  , @RequestBody MgmtCmdVO mgmtCmdProfile
											  , HttpServletRequest request
											  , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();
		
		MgmtCmdVO responseVo = new MgmtCmdVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dkey		= CommonUtil.nvl(request.getHeader("dKey"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(remoteCSEID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(mgmtCmdName)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "mgmtCmdName " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		else if(CommonUtil.isEmpty(mgmtCmdProfile)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<mgmtCmd> " + CommonUtil.getMessage("msg.input.empty.text", locale);
			
		}
		
		if(isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			
			if(remoteCSEService.getCount(remoteCSEID) < 1) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dkey);
			
			if(mgmtCmdService.getCountByLabels(remoteCSEID, mgmtCmdName) < 1) {
				throw new IotException("627", CommonUtil.getMessage("msg.mgmtCmd.noRegi.text", locale));
			}
			 
			mgmtCmdService.checkMgmtCmdValidation(mgmtCmdProfile);
			
			MgmtCmdVO mgmtCmdItem = mgmtCmdService.updateMgmtCmd(remoteCSEID, mgmtCmdName, mgmtCmdProfile);
			
			//responseVo.setMgmtCmd(mgmtCmdItem);
			BeanUtils.copyProperties(mgmtCmdItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);

		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
}