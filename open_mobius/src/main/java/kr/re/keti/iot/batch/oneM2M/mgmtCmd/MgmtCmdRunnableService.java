package kr.re.keti.iot.batch.oneM2M.mgmtCmd;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.MgmtCmdVO;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.MgmtCmdService;
import kr.re.keti.iot.service.oneM2M.MgmtCmdService.EXEC_MODE;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * mgmtCmd runnable service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Service
public class MgmtCmdRunnableService {

	private static final Log logger = LogFactory.getLog(MgmtCmdRunnableService.class);

	@Autowired
	private MongoLogService mongoLogService;

	@Autowired
	private MgmtCmdService mgmtCmdService;
	

	private final Map<String, Future<?>> futuresMapping = new HashMap<String, Future<?>>();
	private static ScheduledExecutorService exec = Executors.newScheduledThreadPool(100);
	
	/**
	 * mgmtCmd start schedule
	 * @param mgmtCmdProfile
	 * @throws IotException
	 */
	public void startSchedule(MgmtCmdVO mgmtCmdProfile) throws IotException {
			
		MgmtCmdVO findMgmtCmdItem = null;
		if (CommonUtil.isEmpty((findMgmtCmdItem = mgmtCmdService.findOneMgmtCmdByLabels(mgmtCmdProfile.getParentID(), mgmtCmdProfile.getLabels())))) {
			throw new IotException("627", CommonUtil.getMessage("msg.mgmtCmd.noRegi.text"));
		}
		
		long initialDelay =  0;
		long period       =  0;
		int  runCount     =  1;
		
		String execMode 		= findMgmtCmdItem.getExecMode();
		String execFrequency    = CommonUtil.nvl(findMgmtCmdItem.getExecFrequency(), "0");
		String execDelay        = CommonUtil.nvl(findMgmtCmdItem.getExecDelay(), "0");
		String execNumber		= findMgmtCmdItem.getExecNumber();
		if(execMode.equalsIgnoreCase(EXEC_MODE.IMMEDIATE_ONCE.getValue())) {
			
			
		} else if(execMode.equalsIgnoreCase(EXEC_MODE.IMMEDIATE_AND_REPEATEDLY.getValue())) {			
			runCount = Integer.parseInt(execNumber);
			
		} else if(execMode.equalsIgnoreCase(EXEC_MODE.RANDOM_ONCE.getValue())) {
			initialDelay = Integer.parseInt(execDelay);
			
		} else if(execMode.equalsIgnoreCase(EXEC_MODE.RANDOM_AND_REPEATEDLY.getValue())) {
			initialDelay = Integer.parseInt(execDelay);
			period   = (long)(Integer.parseInt(execFrequency));
			runCount = Integer.parseInt(execNumber);
		}
		
		mgmtCmdProfile.setResourceID(findMgmtCmdItem.getResourceID());
		MgmtCmdTask task = new MgmtCmdTask();
		task.setMgmtCmdRunnableService(this);
		task.setMongoLogService(mongoLogService);
		task.setMgmtCmdService(mgmtCmdService);
		task.setMgmtCmdProfile(mgmtCmdProfile);
		task.setRunCount(runCount);
		try {
			if (!CommonUtil.isEmpty(futuresMapping.get(mgmtCmdProfile.getResourceID()))) {
				this.stopSchedule(mgmtCmdProfile.getResourceID());
			}

			if(runCount == 1) {
				exec.schedule(task, initialDelay, TimeUnit.SECONDS);
				
			} else {
				Future<?> future = exec.scheduleWithFixedDelay(task, initialDelay, period, TimeUnit.SECONDS);
				futuresMapping.put(mgmtCmdProfile.getResourceID(), future);
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new IotException("600", "MgmtCmdTask schedule Error");
		}
	}

	/**
	 * mgmtCmd stop schedule
	 * @param resourceId
	 * @throws IotException
	 */
	public void stopSchedule(String resourceId) throws IotException {
		
		mongoLogService.log(logger, LEVEL.ERROR, "MgmtCmdTask " + resourceId + " Schedule stop");
		Future<?> future = futuresMapping.get(resourceId);
		try {
			if (!CommonUtil.isEmpty(future) && !future.isDone()) {
				future.cancel(true);
				futuresMapping.remove(resourceId);
			}
		} catch (Exception e) {
			throw new IotException("600", "MgmtCmdTask schedule cancle Error");
		}
	}

}
