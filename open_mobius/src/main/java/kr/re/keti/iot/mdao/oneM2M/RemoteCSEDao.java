package kr.re.keti.iot.mdao.oneM2M;

import kr.re.keti.iot.domain.oneM2M.RemoteCSEVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao;

import org.springframework.stereotype.Repository;

/**
 * remoteCSE management Dao.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Repository
public class RemoteCSEDao extends GenericMongoDao {
	
	public RemoteCSEDao() {
		collectionName = ONEM2M.REMOTE_CSE.getName();
		cls = RemoteCSEVO.class;
	}
}
