package kr.re.keti.iot.domain.oneM2M;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import kr.re.keti.common.base.BaseVO;

/**
 * AE domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@XmlRootElement(name="AE", namespace="http://www.onem2m.org/xml/protocols")
@XmlAccessorType(value=XmlAccessType.FIELD)
public class AEVO extends BaseVO {
	private static final long serialVersionUID = 1L;

	private String resourceType;
	private String parentID;
	private String creationTime;
	private String lastModifiedTime;
	private String expirationTime;
	private String accessControlPolicyIDs;
	private String labels;
	private String name;
	@XmlElement(name = "App-ID", required=true)
	private String appId;
	@XmlElement(name = "AE-ID", required=true)
	private String aeId;
	private String pointOfAccess;
	private String ontologyRef;
	private String nodeLink;
	private String aKey;
	
	public String getResourceType() {
		return resourceType;
	}
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}
	public String getParentID() {
		return parentID;
	}
	public void setParentID(String parentID) {
		this.parentID = parentID;
	}
	public String getCreationTime() {
		return creationTime;
	}
	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}
	public String getLastModifiedTime() {
		return lastModifiedTime;
	}
	public void setLastModifiedTime(String lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}
	public String getExpirationTime() {
		return expirationTime;
	}
	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}
	public String getAccessControlPolicyIDs() {
		return accessControlPolicyIDs;
	}
	public void setAccessControlPolicyIDs(String accessControlPolicyIDs) {
		this.accessControlPolicyIDs = accessControlPolicyIDs;
	}
	public String getLabels() {
		return labels;
	}
	public void setLabels(String labels) {
		this.labels = labels;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getAeId() {
		return aeId;
	}
	public void setAeId(String aeId) {
		this.aeId = aeId;
	}
	public String getPointOfAccess() {
		return pointOfAccess;
	}
	public void setPointOfAccess(String pointOfAccess) {
		this.pointOfAccess = pointOfAccess;
	}
	public String getOntologyRef() {
		return ontologyRef;
	}
	public void setOntologyRef(String ontologyRef) {
		this.ontologyRef = ontologyRef;
	}
	public String getNodeLink() {
		return nodeLink;
	}
	public void setNodeLink(String nodeLink) {
		this.nodeLink = nodeLink;
	}
	public String getaKey() {
		return aKey;
	}
	public void setaKey(String aKey) {
		this.aKey = aKey;
	}
	
	@Override
	public String toString() {
		return "AEVO [resourceType=" + resourceType + ", parentID=" + parentID
				+ ", creationTime=" + creationTime + ", lastModifiedTime="
				+ lastModifiedTime + ", expirationTime=" + expirationTime
				+ ", accessControlPolicyIDs=" + accessControlPolicyIDs
				+ ", labels=" + labels + ", name=" + name + ", appId=" + appId
				+ ", aeId=" + aeId + ", pointOfAccess=" + pointOfAccess
				+ ", ontologyRef=" + ontologyRef + ", nodeLink=" + nodeLink
				+ ", aKey=" + aKey + "]";
	}
	
}