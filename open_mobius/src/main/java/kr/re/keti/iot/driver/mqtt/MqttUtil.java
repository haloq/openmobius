package kr.re.keti.iot.driver.mqtt;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 * MQTT util.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
public class MqttUtil {
	private static String mqttClientId = MqttClient.generateClientId();
	private static String mqttServerUrl = "";
	private static MqttClient mqc = null;
	private static final Log logger = LogFactory.getLog(MqttUtil.class);
	
	/**
	 * get static MQTTClient
	 * @param serverUrl
	 * @return
	 * @throws Exception
	 */
	public static MqttClient getMettClient(String serverUrl) throws Exception{
		
		try {
			if(mqc == null){
				mqttServerUrl = serverUrl;
				mqc = new MqttClient(mqttServerUrl, mqttClientId);
				logger.info("[MQTT Client] Client Initialize");
			}
			if(!mqc.isConnected()){
				while(!mqc.isConnected()){
					mqc.connect();
					logger.info("[MQTT Client] Connection try");
				}
				
				logger.info("[ MQTT Client] Connected to Server - " + mqttServerUrl);
			}
			return mqc;
		} catch (MqttException e) {
			e.printStackTrace();
		}
		return null;
	}
	
}