package kr.re.keti.iot.service.oneM2M;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import kr.re.keti.iot.batch.oneM2M.subscription.SubscriptionRunnableService;
import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.ContainerVO;
import kr.re.keti.iot.domain.oneM2M.ContentInstanceVO;
import kr.re.keti.iot.domain.oneM2M.ExecInstanceVO;
import kr.re.keti.iot.domain.oneM2M.MgmtCmdVO;
import kr.re.keti.iot.domain.oneM2M.SubscriptionPendingVO;
import kr.re.keti.iot.domain.oneM2M.SubscriptionVO;
import kr.re.keti.iot.domain.oneM2M.SubscriptionVO.EventNotificationCriteriaVO;
import kr.re.keti.iot.mdao.common.SequenceDao;
import kr.re.keti.iot.mdao.common.SequenceDao.MovType;
import kr.re.keti.iot.mdao.common.SequenceDao.SEQ_PREFIX;
import kr.re.keti.iot.mdao.common.SequenceDao.SeqType;
import kr.re.keti.iot.mdao.oneM2M.SubscriptionDao;
import kr.re.keti.iot.mdao.oneM2M.SubscriptionPendingDao;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.util.CommonUtil;
import kr.re.keti.iot.util.oneM2M.CommonCode.RESOURCE_TYPE;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.mongodb.BasicDBObject;

/**
 * subscription management Service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>khlee</li>
 *         </ul>
 */
@Service
public class SubscriptionService {

	private static final Log logger = LogFactory.getLog(SubscriptionService.class);

	@Autowired
	private MongoLogService mongoLogService;
	
	@Autowired
	private ContainerService containerService;
	
	@Autowired
	private MgmtCmdService mgmtCmdService;
	
	@Autowired
	private ContentInstanceService contentInstanceService;
	
	@Autowired
	private ExecInstanceService execInstanceService;
	
	@Autowired
	private SubscriptionRunnableService subscriptionRunnableService;
	
	@Autowired
	private SequenceDao seqDao;

	@Autowired
	private SubscriptionDao subscriptionDao;
	
	@Autowired
	private SubscriptionPendingDao subscriptionPendingDao;
	
	public enum SUBSCRIPTION_RESOURCE_STATUS_TYPE {
		CHILD_CREATED("1"),
		CHILD_DELETED("2"),
		UPDATED("3"),
		DELETED("4"),
		CHILD_UPDATED("9");

		private final String id;

		SUBSCRIPTION_RESOURCE_STATUS_TYPE(String id) {
			this.id = id;
		}

		public String getValue() {
			return id;
		}
		
		public static String fromString(String value) {
			String result = "";
			for(SUBSCRIPTION_RESOURCE_STATUS_TYPE type : SUBSCRIPTION_RESOURCE_STATUS_TYPE.values()) {
				if(value.equals(type.getValue())) {
					result = type.name();
				}
			}
			return result;
		}
	}	
	
	public enum SUBSCRIPTION_PENDING_NOTIFICATION_TYPE {
		SEND_LATEST("1"),
		SEND_ALL_PENDING("2");

		private final String id;

		SUBSCRIPTION_PENDING_NOTIFICATION_TYPE(String id) {
			this.id = id;
		}

		public String getValue() {
			return id;
		}
		
		public static String fromString(String value) {
			String result = "";
			for(SUBSCRIPTION_PENDING_NOTIFICATION_TYPE type : SUBSCRIPTION_PENDING_NOTIFICATION_TYPE.values()) {
				if(value.equals(type.getValue())) {
					result = type.name();
				}
			}
			return result;
		}
	}
	
	public enum SUBSCRIPTION_NOTIFICATION_CONTENT_TYPE {
		MODIFIED_ATTRIBUTES_ONLY("1"),
		WHOLE_RESOURCE("2"),
		OPTIONALLY_THE_REFERENCE_TO_THIS_SUBSCRIPTION_RESOURCE("3");

		private final String id;

		SUBSCRIPTION_NOTIFICATION_CONTENT_TYPE(String id) {
			this.id = id;
		}
		
		public String getValue() {
			return id;
		}
		
		public static String fromString(String value) {
			String result = "";
			for(SUBSCRIPTION_NOTIFICATION_CONTENT_TYPE type : SUBSCRIPTION_NOTIFICATION_CONTENT_TYPE.values()) {
				if(value.equals(type.getValue())) {
					result = type.name();
				}
			}
			return result;
		}
	}
	
	/**
	 * find subscription by subscriptionId
	 * @param subscriptionId
	 * @return
	 * @throws IotException
	 */
	public SubscriptionVO findOneSubscription(String subscriptionId) throws IotException {
		return findOneSubscription(null, subscriptionId);
	}

	/**
	 * find subscription by parentID-subscriptionId
	 * @param parentID
	 * @param subscriptionId
	 * @return
	 * @throws IotException
	 */
	public SubscriptionVO findOneSubscription(String parentID, String subscriptionId) throws IotException {
		SubscriptionVO findSubscriptionItem = null;

		Query query = new Query();
		if (!CommonUtil.isEmpty(parentID)) {
			query.addCriteria(Criteria.where("parentID").is(parentID));
		}

		query.addCriteria(Criteria.where("resourceID").is(subscriptionId));
		try {
			findSubscriptionItem = (SubscriptionVO) subscriptionDao.findOne(query);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.subscription.findFail.text"));
		}

		return findSubscriptionItem;
	}

	/**
	 * find subscription by parentID-labels
	 * @param parentID
	 * @param labels
	 * @return
	 * @throws IotException
	 */
	public SubscriptionVO findOneSubscriptionByLabels(String parentID, String labels) throws IotException {
		SubscriptionVO findSubscriptionItem = null;

		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(parentID));
		query.addCriteria(Criteria.where("labels").is(labels));

		try {
			findSubscriptionItem = (SubscriptionVO) subscriptionDao.findOne(query);
		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.subscription.findFail.text"));
		}

		return findSubscriptionItem;
	}

	/**
	 * create subscription
	 * @param subscriptionProfile
	 * @return
	 * @throws IotException
	 */
	public SubscriptionVO createSubscription(SubscriptionVO subscriptionProfile) throws IotException {
		SubscriptionVO subscriptionItem = new SubscriptionVO();
		
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");

		try {
			Long subscriptionId = seqDao.move(MovType.UP, SeqType.SUBSCRIPTION);
			subscriptionItem.setResourceID(SEQ_PREFIX.SUBSCRIPTION.getValue() + String.format("%010d", subscriptionId));

		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.subscription.createFail.text"));
		}

		subscriptionItem.setParentID(subscriptionProfile.getParentID());
		subscriptionItem.setResourceType(RESOURCE_TYPE.SUBSCRIPTION.getName());
		subscriptionItem.setLabels(subscriptionProfile.getLabels());
		subscriptionItem.setEventNotificationCriteria(subscriptionProfile.getEventNotificationCriteria());
		subscriptionItem.setExpirationCounter(subscriptionProfile.getExpirationCounter());
		subscriptionItem.setNotificationURI(subscriptionProfile.getNotificationURI());
		subscriptionItem.setPendingNotification(subscriptionProfile.getPendingNotification());
		subscriptionItem.setNotificationContentType(subscriptionProfile.getNotificationContentType());
		subscriptionItem.setCreator(subscriptionProfile.getCreator());
		subscriptionItem.setSubscriberURI(subscriptionProfile.getSubscriberURI());
		subscriptionItem.setCreationTime(currentTime);
		subscriptionItem.setLastModifiedTime(currentTime);

		try {
			subscriptionDao.insert(subscriptionItem);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.subscription.createFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "subscription create success");
		
		return subscriptionItem;

	}

	/**
	 * update subscription
	 * @param subscriptionItem
	 * @return
	 * @throws IotException
	 */
	public SubscriptionVO updateSubscription(SubscriptionVO subscriptionItem) throws IotException {

		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		if (CommonUtil.isEmpty(this.findOneSubscription(subscriptionItem.getParentID(), subscriptionItem.getResourceID()))) {
			throw new IotException("604", CommonUtil.getMessage("msg.device.subscription.noRegi.text"));
		}

		SubscriptionVO.EventNotificationCriteriaVO eventNotificationCriteria 	= subscriptionItem.getEventNotificationCriteria();
		String expirationCounter 												= subscriptionItem.getExpirationCounter();
		String notificationURI 													= subscriptionItem.getNotificationURI();
		String pendingNotification 												= subscriptionItem.getPendingNotification();
		String notificationContentType 											= subscriptionItem.getNotificationContentType();
		String subscriberURI													= subscriptionItem.getSubscriberURI();
		
		Update update = new Update();
		if(!CommonUtil.isEmpty(eventNotificationCriteria))						update.set("eventNotificationCriteria", eventNotificationCriteria);
		if(!CommonUtil.isNull(expirationCounter))								update.set("expirationCounter", 		expirationCounter);
		if(!CommonUtil.isNull(notificationURI))									update.set("notificationURI", 			notificationURI);
		if(!CommonUtil.isNull(pendingNotification))								update.set("pendingNotification", 		pendingNotification);
		if(!CommonUtil.isNull(notificationContentType))							update.set("notificationContentType", 	notificationContentType);
		if(!CommonUtil.isNull(subscriberURI))									update.set("subscriberURI", 			subscriberURI);
																				update.set("lastModifiedTime", 			currentTime);
		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(subscriptionItem.getParentID()));
		query.addCriteria(Criteria.where("resourceID").is(subscriptionItem.getResourceID()));
		try {

			subscriptionDao.update(query, update);

		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.subscription.upFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "subscription update success");

		return this.findOneSubscription(subscriptionItem.getParentID(), subscriptionItem.getResourceID());
	}

	/**
	 * delete subscription
	 * @param parentID
	 * @param subscriptionId
	 * @throws IotException
	 */
	public void deleteSubscription(String parentID, String subscriptionId) throws IotException {

		Query query = new Query();
		query.addCriteria(Criteria.where("parentID").is(parentID));
		query.addCriteria(Criteria.where("resourceID").is(subscriptionId));

		try {
			subscriptionDao.remove(query);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.subscription.delFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "subscription delete success");

	}
	
	/**
	 * check subscription validation
	 * @param subscriptionItem
	 * @throws IotException
	 */
	public void checkSubscriptionValidation(SubscriptionVO subscriptionItem) throws IotException{
		
		if(CommonUtil.isEmpty(subscriptionItem.getNotificationURI()) || !CommonUtil.checkUrl(subscriptionItem.getNotificationURI())) {
			throw new IotException("600", "notificationURI " + CommonUtil.getMessage("device.error.invalid.pocurl"));
		}
			
		if(CommonUtil.isEmpty(subscriptionItem.getNotificationContentType()) || CommonUtil.isEmpty(SUBSCRIPTION_NOTIFICATION_CONTENT_TYPE.fromString(subscriptionItem.getNotificationContentType()))) {
			throw new IotException("600", "notificationContentType " + CommonUtil.getMessage("msg.device.subscription.type.nosupport"));
		}
		
		if(!CommonUtil.isEmpty(subscriptionItem.getSubscriberURI()) && !CommonUtil.checkUrl(subscriptionItem.getSubscriberURI())) {
			throw new IotException("600", "subscriberURI " + CommonUtil.getMessage("device.error.invalid.pocurl"));
		}
		
		if(!CommonUtil.isEmpty(subscriptionItem.getExpirationCounter()) && !CommonUtil.isInteger(subscriptionItem.getExpirationCounter())) {
			throw new IotException("600", "expirationCounter " + CommonUtil.getMessage("msg.device.number.text"));
		}
		
		if(!CommonUtil.isEmpty(subscriptionItem.getPendingNotification()) && CommonUtil.isEmpty(SUBSCRIPTION_PENDING_NOTIFICATION_TYPE.fromString(subscriptionItem.getPendingNotification()))) {
			throw new IotException("600", "pendingNotification " + CommonUtil.getMessage("msg.device.subscription.type.nosupport"));
		}
		
		SubscriptionVO.EventNotificationCriteriaVO criteriaVO = subscriptionItem.getEventNotificationCriteria();
		if(CommonUtil.isEmpty(subscriptionItem.getEventNotificationCriteria())) {
			throw new IotException("600", "eventNotificationCriteria " + CommonUtil.getMessage("msg.input.empty.text"));
		}
			
		if(!CommonUtil.isEmpty(criteriaVO.getResourceStatus()) && CommonUtil.isEmpty(SUBSCRIPTION_RESOURCE_STATUS_TYPE.fromString(criteriaVO.getResourceStatus()))) {
			throw new IotException("600", "resourceStatus " + CommonUtil.getMessage("msg.device.subscription.type.nosupport"));
		}
		
		if(!CommonUtil.isEmpty(criteriaVO.getModifiedSince()) && CommonUtil.isEmpty(CommonUtil.parseDate(criteriaVO.getModifiedSince(), "yyyy-MM-dd'T'HH:mm:ssXXX"))) {
			throw new IotException("600", "modifiedSince " + CommonUtil.getMessage("msg.device.date.text"));
		}
		
		if(!CommonUtil.isEmpty(criteriaVO.getUnmodifiedSince()) && CommonUtil.isEmpty(CommonUtil.parseDate(criteriaVO.getUnmodifiedSince(), "yyyy-MM-dd'T'HH:mm:ssXXX"))) {
			throw new IotException("600", "unmodifiedSince " + CommonUtil.getMessage("msg.device.date.text"));
		}
		
		if(!CommonUtil.isEmpty(criteriaVO.getStateTagSmaller()) && !CommonUtil.isInteger(criteriaVO.getStateTagSmaller())) {
			throw new IotException("600", "stateTagSmaller " + CommonUtil.getMessage("msg.device.number.text"));
		}
		
		if(!CommonUtil.isEmpty(criteriaVO.getStateTagBigger()) && !CommonUtil.isInteger(criteriaVO.getStateTagBigger())) {
			throw new IotException("600", "stateTagBigger " + CommonUtil.getMessage("msg.device.number.text"));
		}
		
		if(!CommonUtil.isEmpty(criteriaVO.getSizeAbove()) && !CommonUtil.isInteger(criteriaVO.getSizeAbove())) {
			throw new IotException("600", "sizeAbove " + CommonUtil.getMessage("msg.device.number.text"));
		}
		
		if(!CommonUtil.isEmpty(criteriaVO.getSizeBelow()) && !CommonUtil.isInteger(criteriaVO.getSizeBelow())) {
			throw new IotException("600", "sizeBelow " + CommonUtil.getMessage("msg.device.number.text"));
		}
	}
	
	/**
	 * send with subscription
	 * @param parentID
	 * @param resourceID
	 * @param resourceStatus
	 * @param clazz
	 * @param objectEntity
	 * @throws IotException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void sendSubscription(String parentID, String resourceID, SUBSCRIPTION_RESOURCE_STATUS_TYPE resourceStatus, Class clazz, Object objectEntity) throws IotException {
		mongoLogService.log(logger, LEVEL.INFO, "subscriptionService sendSubscrition call");
		
		try {
			
			if(objectEntity.getClass().getSimpleName().indexOf("Update") > -1) {
				BasicDBObject dbObject = new BasicDBObject();
				HashMap<String, Object> setMap = (HashMap<String, Object>) ((Update) objectEntity).getUpdateObject().get("$set");
				for(String key : setMap.keySet()) {
					dbObject.put(key, setMap.get(key));
				}
				dbObject.put("resourceID", resourceID);
				if(resourceStatus.getValue().indexOf("child") <= -1) {
					parentID = resourceID;
				}
				objectEntity = subscriptionDao.getConverter(clazz, dbObject);
			}
			
			String resourceType     = "";
			String lastModifiedTime = "";
			String objClassName = objectEntity.getClass().getSimpleName();
			if(objClassName.indexOf("ContainerVO") > -1) {
				resourceType = "container";	
				lastModifiedTime = ((ContainerVO) objectEntity).getLastModifiedTime();
			} else if(objClassName.indexOf("MgmtCmdVO") > -1) {
				resourceType = "mgmtCmd";
				lastModifiedTime = ((MgmtCmdVO) objectEntity).getLastModifiedTime();
			} else if(objClassName.indexOf("ContentInstanceVO") > -1) {
				resourceType = "contentInstance";
				lastModifiedTime = ((ContentInstanceVO) objectEntity).getLastModifiedTime();
			} else if(objClassName.indexOf("ExecInstanceVO") > -1) {
				resourceType = "execInstance";
				lastModifiedTime = ((ExecInstanceVO) objectEntity).getLastModifiedTime();
			}
			
			Query query = new Query();
			query.addCriteria(Criteria.where("parentID").is(parentID));
			
			List<SubscriptionVO> findSubscriptionList = (List<SubscriptionVO>)subscriptionDao.find(query);
			
			for(SubscriptionVO subscriptionItem : findSubscriptionList) {
				
				if (SUBSCRIPTION_NOTIFICATION_CONTENT_TYPE.WHOLE_RESOURCE.getValue().equals(subscriptionItem.getNotificationContentType())) {
					
					if ((resourceStatus.equals(SUBSCRIPTION_RESOURCE_STATUS_TYPE.UPDATED) || resourceStatus.equals(SUBSCRIPTION_RESOURCE_STATUS_TYPE.CHILD_UPDATED))) {
						if(resourceType.equals("container")) {
							objectEntity = containerService.findOneContainer(resourceID);	
						} else if(resourceType.equals("mgmtCmd")) {
							objectEntity = mgmtCmdService.findOneMgmtCmd("resourceID", resourceID);
						} else if(resourceType.equals("contentInstance")) {
							objectEntity = contentInstanceService.findOneContentInstance(subscriptionItem.getParentID(), resourceID);
						} else if(resourceType.equals("execInstance")) {
							objectEntity = execInstanceService.findOneExecInstance(resourceID);
						}
					}					
				}
				
				EventNotificationCriteriaVO eventNotificationCriteriaItem = subscriptionItem.getEventNotificationCriteria();
				if(!CommonUtil.isEmpty(eventNotificationCriteriaItem)) {
					
					String strResourceStatus = eventNotificationCriteriaItem.getResourceStatus();
					if(!CommonUtil.isEmpty(strResourceStatus) && strResourceStatus.equals(resourceStatus.getValue())) {
						
						subscriptionRunnableService.startSchedule(subscriptionItem, objectEntity);
						continue;
					}
					
					boolean isModifiedTimedSend = false;
					boolean isModifiedTimedChkSuccess = false;
					Date modifiedTime = CommonUtil.parseDate(lastModifiedTime, "yyyy-MM-dd'T'HH:mm:ssXXX");
					
					if(!CommonUtil.isEmpty(eventNotificationCriteriaItem.getModifiedSince())) {
						isModifiedTimedSend = true;
						Date modifiedSince = CommonUtil.parseDate(eventNotificationCriteriaItem.getModifiedSince(), "yyyy-MM-dd'T'HH:mm:ssXXX");
 						if(modifiedSince.after(modifiedTime)) {
 							isModifiedTimedChkSuccess = false;
						} else {
							isModifiedTimedChkSuccess = true;
						}
					}
					
					if(!(isModifiedTimedSend && !isModifiedTimedChkSuccess) && !CommonUtil.isEmpty(eventNotificationCriteriaItem.getUnmodifiedSince())) {
						isModifiedTimedSend = true;
						Date unmodifiedSince = CommonUtil.parseDate(eventNotificationCriteriaItem.getUnmodifiedSince(), "yyyy-MM-dd'T'HH:mm:ssXXX");
						if(unmodifiedSince.before(modifiedTime)) {
							isModifiedTimedChkSuccess = false;
						} else {
							isModifiedTimedChkSuccess = true;
						}
					}
					
					if(isModifiedTimedSend && isModifiedTimedChkSuccess) {
						
						subscriptionRunnableService.startSchedule(subscriptionItem, objectEntity);
						continue;
					}
					
					if(resourceType.equals("container") && resourceStatus.equals(SUBSCRIPTION_RESOURCE_STATUS_TYPE.UPDATED)) {
						boolean isStateTagSend = false;
						boolean isStateTagChkSuccess = false;
						
						ContainerVO containerItem = (ContainerVO) objectEntity;
						String strStateTag			= containerItem.getStateTag();
						
						String strStateTagSmaller	= eventNotificationCriteriaItem.getStateTagSmaller();
						String strStateTagBigger	= eventNotificationCriteriaItem.getStateTagBigger();
						
						if(!CommonUtil.isEmpty(strStateTagBigger) && CommonUtil.isInteger(strStateTagBigger)) {
							isStateTagSend = true;
							if(CommonUtil.isEmpty(strStateTag) || !CommonUtil.isInteger(strStateTag) || (Integer.parseInt(strStateTagBigger) >= Integer.parseInt(strStateTag))) {
								isStateTagChkSuccess = false;
							} else {
								isStateTagChkSuccess = true;
							}
						}
						
						if(!(isStateTagSend && !isStateTagChkSuccess) && !CommonUtil.isEmpty(strStateTagSmaller) && CommonUtil.isInteger(strStateTagSmaller)) {
							isStateTagSend = true;
							if(CommonUtil.isEmpty(strStateTag) || !CommonUtil.isInteger(strStateTag) || (Integer.parseInt(strStateTagSmaller) <= Integer.parseInt(strStateTag))) {
								isStateTagChkSuccess = false;
							} else {
								isStateTagChkSuccess = true;
							}
						}
						
						if(isStateTagSend && isStateTagChkSuccess) {
							
							subscriptionRunnableService.startSchedule(subscriptionItem, objectEntity);
							continue;
						}
					}
					
					if(resourceType.equals("contentInstance") && (resourceStatus.equals(SUBSCRIPTION_RESOURCE_STATUS_TYPE.CHILD_CREATED) || resourceStatus.equals(SUBSCRIPTION_RESOURCE_STATUS_TYPE.CHILD_UPDATED))) {
						boolean isContentSizeSend = false;
						boolean isContentSizeChkSuccess = false;
						
						ContentInstanceVO contentInstanceItem = (ContentInstanceVO) objectEntity;
						String strContentSize = contentInstanceItem.getContentSize();
						
						String strSizeAbove = eventNotificationCriteriaItem.getSizeAbove();
						String strSizeBelow = eventNotificationCriteriaItem.getSizeBelow();
						
						if(!CommonUtil.isEmpty(strSizeAbove) && CommonUtil.isInteger(strSizeAbove)) {
							isContentSizeSend = true;
							if(CommonUtil.isEmpty(strContentSize) || !CommonUtil.isInteger(strContentSize) || (Integer.parseInt(strSizeAbove) > Integer.parseInt(strContentSize))) {
								isContentSizeChkSuccess = false;
							} else {
								isContentSizeChkSuccess = true;
							}
						}
						
						if(!(isContentSizeSend && !isContentSizeChkSuccess) && !CommonUtil.isEmpty(strSizeBelow) && CommonUtil.isInteger(strSizeBelow)) {
							isContentSizeSend = true;
							if(CommonUtil.isEmpty(strContentSize) || !CommonUtil.isInteger(strContentSize) || (Integer.parseInt(strSizeBelow) <= Integer.parseInt(strContentSize))) {
								isContentSizeChkSuccess = false;
							} else {
								isContentSizeChkSuccess = true;
							}
						}
						
						if(isContentSizeSend && isContentSizeChkSuccess) {
							subscriptionRunnableService.startSchedule(subscriptionItem, objectEntity);
							continue;
						}
					}
				}
			} 
		} catch(IotException e) {
			e.printStackTrace();
			
		} catch(Exception e) {
			e.printStackTrace();
			throw new IotException("600", e.getMessage());
			
		} 
		
		mongoLogService.log(logger, LEVEL.INFO, "subscriptionService sendSubscrition end");
		
	}
	
	/**
	 * send with subscription
	 * @param subscriptionItem
	 * @param objectEntity
	 * @throws IotException
	 */
	public void sendSubscription(SubscriptionVO subscriptionItem, Object objectEntity)  throws IotException {
		try {
			if (!CommonUtil.isEmpty(subscriptionItem.getNotificationURI())) {
			
				mongoLogService.log(logger, LEVEL.INFO, "subscriptionService objectEntity = " + ToStringBuilder.reflectionToString(objectEntity));
				
				String entity = CommonUtil.marshalToXmlString(objectEntity);
				try {
					this.sendSubscription(subscriptionItem.getNotificationURI(), entity);
				} catch(Exception e) {
					e.printStackTrace();
					if(SUBSCRIPTION_PENDING_NOTIFICATION_TYPE.SEND_ALL_PENDING.getValue().equals(subscriptionItem.getPendingNotification())) {
						SubscriptionPendingVO subscriptionPendingProfile = new SubscriptionPendingVO();
						subscriptionPendingProfile.setNotificationUri(subscriptionItem.getNotificationURI());
						subscriptionPendingProfile.setContent(entity);
						
						this.createSubscriptionPending(subscriptionPendingProfile);
					}
				}
			}
			
			this.updateSubscriptionExpirationCounter(subscriptionItem);
		
		} catch(IotException e) {
			e.printStackTrace();
			
		} catch(Exception e) {
			e.printStackTrace();
			throw new IotException("600", e.getMessage());
			
		} 
		
		mongoLogService.log(logger, LEVEL.INFO, "subscriptionService sendSubscrition end");		
		
	}
	
	/**
	 * update subscription expirationCounter
	 * @param subscriptionItem
	 * @throws IotException
	 */
	public void updateSubscriptionExpirationCounter(SubscriptionVO subscriptionItem) throws IotException {
		
		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		if(CommonUtil.isInteger(subscriptionItem.getExpirationCounter())) {
			int expirationCounter = Integer.parseInt(subscriptionItem.getExpirationCounter()) -1;
			if(expirationCounter > 0) {
				Query query = new Query();
				query.addCriteria(Criteria.where("parentID").is(subscriptionItem.getParentID()));
				query.addCriteria(Criteria.where("resourceID").is(subscriptionItem.getResourceID()));
				
				Update update = new Update();
				update.set("expirationCounter", 		expirationCounter);
				update.set("lastModifiedTime", 			currentTime);
				try {
					subscriptionDao.update(query, update);
				} catch (Exception e) {
					e.printStackTrace();
					mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
					throw new IotException("600", CommonUtil.getMessage("msg.device.subscription.upFail.text"));
				}
				
				mongoLogService.log(logger, LEVEL.INFO, "subscription update success");
			} else {
				this.deleteSubscription(subscriptionItem.getParentID(), subscriptionItem.getResourceID());
				
				if(!CommonUtil.isEmpty(subscriptionItem.getSubscriberURI())) {
					try {
						String entity = CommonUtil.marshalToXmlString(subscriptionItem);
						this.sendSubscription(subscriptionItem.getSubscriberURI(), entity);
					} catch(Exception e) {
						e.printStackTrace();
						mongoLogService.log(logger, LEVEL.ERROR, "subscription delete sendSubscription Exception" + e.getMessage());
					}
				}
				mongoLogService.log(logger, LEVEL.INFO, "subscription delete success");
			}
		}
		
	}
	
	/**
	 * send with subscription
	 * @param url
	 * @param stringEntity
	 * @return
	 * @throws Exception
	 */
	public String sendSubscription(String url, String stringEntity) throws Exception {
		String responseBody = "";

		HttpClient httpClient = new HttpClient();
		PostMethod method     = new PostMethod(url);
		method.setRequestHeader("Accept", 		"application/xml;charset=UTF-8");
		method.setRequestHeader("Content-Type", MediaType.APPLICATION_XML_VALUE);
		
		StringRequestEntity requestEntity = new StringRequestEntity(stringEntity, "application/xml", "UTF-8");
		method.setRequestEntity(requestEntity);		
		if(logger.isDebugEnabled()) {
			mongoLogService.log(logger, LEVEL.DEBUG, "subscription sendSubscription call");
			mongoLogService.log(logger, LEVEL.DEBUG, "subscription url  = " + url);
			mongoLogService.log(logger, LEVEL.DEBUG, "subscription data = " + stringEntity);
		}
		int responseStatus = httpClient.executeMethod(method);
		if(logger.isDebugEnabled()) {
			mongoLogService.log(logger, LEVEL.DEBUG, "subscription responseStatus Code = " + responseStatus);
		}
		
		if(responseStatus == HttpStatus.SC_OK || responseStatus == HttpStatus.SC_CREATED) {
			String line = "";
			BufferedReader rd = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(method.getResponseBody())));
			while ((line = rd.readLine()) != null) {
				responseBody.concat(line);
			}
			mongoLogService.log(logger, LEVEL.DEBUG, "subscription sendSubscription Success responseStatus Code = " + responseStatus);
			
		} else {
			if(logger.isDebugEnabled()) {
				mongoLogService.log(logger, LEVEL.DEBUG, "subscription sendSubscription Exception responseStatus Code = " + responseStatus);
			}
			throw new Exception();
		}

		if(logger.isDebugEnabled()) {
			mongoLogService.log(logger, LEVEL.DEBUG, "subscription sendSubscription responseBody = " + responseBody);
			mongoLogService.log(logger, LEVEL.DEBUG, "subscription sendSubscription end");
		}
		
		return responseBody;

	}
	
	/**
	 * find subscription pending list
	 * @return
	 * @throws IotException
	 */
	public List<SubscriptionPendingVO> findSubscriptionPending() throws IotException {
		return findSubscriptionPending(null, null);
	}
	
	/**
	 * find subscription pending list
	 * @param key
	 * @param value
	 * @return
	 * @throws IotException
	 */
	@SuppressWarnings("unchecked")
	public List<SubscriptionPendingVO> findSubscriptionPending(String key, String value) throws IotException {
		List<SubscriptionPendingVO> findSubscriptionPendingList = null;

		Query query = new Query();
		if (!CommonUtil.isEmpty(key) && !CommonUtil.isEmpty(value)) {
			query.addCriteria(Criteria.where(key).is(value));
		}

		try {

			findSubscriptionPendingList = (List<SubscriptionPendingVO>) subscriptionPendingDao.find(query);

		} catch (Exception e) {
			e.printStackTrace();
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.subscriptionPending.findFail.text"));
		}

		return findSubscriptionPendingList;
	}

	/**
	 * create subscription pending info
	 * @param subscriptionPendingProfile
	 * @return
	 * @throws IotException
	 */
	public SubscriptionPendingVO createSubscriptionPending(SubscriptionPendingVO subscriptionPendingProfile) throws IotException {

		String currentTime = CommonUtil.getNow("yyyy-MM-dd'T'HH:mm:ssXXX");
		
		try {
			subscriptionPendingProfile.setPadingKey(String.format("%010d", seqDao.move(MovType.UP, SeqType.SUBSCRIPTION_PENDING)));
			subscriptionPendingProfile.setRegDt(currentTime);
			subscriptionPendingDao.insert(subscriptionPendingProfile);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.subscriptionPending.createFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "subscriptionPending create success");

		return subscriptionPendingProfile;

	}

	/**
	 * delete subscription panding by padingKey
	 * @param padingKey
	 * @throws IotException
	 */
	public void deleteSubscriptionPending(String padingKey) throws IotException {

		Query query = new Query();
		query.addCriteria(Criteria.where("padingKey").is(padingKey));

		try {
			subscriptionPendingDao.remove(query);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.subscriptionPending.delFail.text"));
		}
		mongoLogService.log(logger, LEVEL.INFO, "subscriptionPending delete success");

	}

}