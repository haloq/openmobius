package kr.re.keti.iot.domain.common;

import java.io.Serializable;

/**
 * GenericModel domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
public abstract class GenericModel implements Serializable {
}
