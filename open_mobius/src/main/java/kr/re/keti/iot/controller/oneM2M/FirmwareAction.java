package kr.re.keti.iot.controller.oneM2M;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.FirmwareVO;
import kr.re.keti.iot.domain.oneM2M.NodeVO;
import kr.re.keti.iot.domain.oneM2M.RemoteCSEVO;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.DeviceInfoService;
import kr.re.keti.iot.service.oneM2M.FirmwareService;
import kr.re.keti.iot.service.oneM2M.NodeService;
import kr.re.keti.iot.service.oneM2M.RemoteCSEService;
import kr.re.keti.iot.service.oneM2M.common.MCommonService;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * firmware management Action.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Controller
public class FirmwareAction {

	private static final Log logger = LogFactory.getLog(FirmwareAction.class);
	
	@Autowired
	private MongoLogService mongoLogService;
	
	@Autowired
	private MCommonService mCommonService;

	@Autowired
	private RemoteCSEService remoteCSEService;
	
	@Autowired
	private NodeService nodeService;

	@Autowired
	private FirmwareService firmwareService;
	
	@Autowired
	private DeviceInfoService deviceInfoService;
	
	
	/**
	 * firmware create
	 * @param nodeID
	 * @param labels
	 * @param firmwareProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/node-{nodeID:.*}", method = RequestMethod.POST, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" }, params = "ty=firmware")
	@ResponseBody
	public ResponseEntity<Object> firmwareCreate(@PathVariable String nodeID
											   , @RequestParam(value = "nm", required = false) String labels
											   , @RequestBody FirmwareVO firmwareProfile
											   , HttpServletRequest request
											   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		FirmwareVO responseVo = new FirmwareVO();
		HttpStatus httpStatus = HttpStatus.CREATED;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey 	= CommonUtil.nvl(request.getHeader("dKey"), "");
		String version 		= firmwareProfile.getVersion();
		String updateStatus = firmwareProfile.getUpdateStatus();

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(nodeID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "nodeID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(firmwareProfile)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<firmware> " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(version)) {
			isError = true;
			responseCode = "600";
			responseMsg = "<firmware> version " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(updateStatus)) {
			isError = true;
			responseCode = "600";
			responseMsg = "<firmware> updateStatus " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus.OK);
		}

		try {
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSEByNodeLink(nodeID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			NodeVO nodeItem = null;
			if (CommonUtil.isEmpty((nodeItem = nodeService.findOneNode(nodeID)))) {
				throw new IotException("625", CommonUtil.getMessage("msg.device.node.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, null, dKey, nodeID);
			
			FirmwareVO firmwareItem = null;
			if (!CommonUtil.isEmpty(firmwareItem = firmwareService.findOneFirmwareByNodeId(nodeID))) {
				//responseVo.setFirmware(firmwareItem);
				BeanUtils.copyProperties(firmwareItem, responseVo);
				throw new IotException("609", CommonUtil.getMessage("msg.device.firmware.duplicate.text", locale));
			}
			
			firmwareProfile.setLabels(labels);
			firmwareProfile.setName(labels);
			firmwareItem = firmwareService.createFirmware(nodeItem, firmwareProfile);
			//responseVo.setFirmware(firmwareItem);
			BeanUtils.copyProperties(firmwareItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);

		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * firmware update
	 * @param nodeID
	 * @param firmwareID
	 * @param firmwareProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/node-{nodeID}/firmware-{firmwareID}", method = RequestMethod.PUT, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> firmwareUpdate(@PathVariable String nodeID
											   , @PathVariable String firmwareID
											   , @RequestBody FirmwareVO firmwareProfile
											   , HttpServletRequest request
											   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		FirmwareVO responseVo = new FirmwareVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey 	= CommonUtil.nvl(request.getHeader("dKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(nodeID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "nodeID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(firmwareID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "firmwareID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(firmwareProfile)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<firmware> " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}

		try {
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSEByNodeLink(nodeID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			if (CommonUtil.isEmpty(nodeService.findOneNode(nodeID))) {
				throw new IotException("625", CommonUtil.getMessage("msg.device.node.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, null, dKey, nodeID);
			
			FirmwareVO findFirmwareItem = null;
			if (CommonUtil.isEmpty(findFirmwareItem = firmwareService.findOneFirmware(firmwareID))) {
				throw new IotException("631", CommonUtil.getMessage("msg.device.firmware.noRegi.text", locale));
			}
/*			
			String parentID = findFirmwareItem.getParentID();
			if(CommonUtil.isEmpty(parentID) || !parentID.equals(nodeID)) {
				throw new IotException("631", CommonUtil.getMessage("msg.device.firmware.noRegi.text", locale));
			}
			
			if(deviceInfoService.getCount(nodeID) < 1) {
				throw new IotException("630", CommonUtil.getMessage("msg.device.deviceInfo.noRegi.text", locale));
			}
*/			
			
			firmwareProfile.setResourceID(firmwareID);
			FirmwareVO firmwareItem = firmwareService.updateFirmware(nodeID, firmwareID, firmwareProfile);
			//responseVo.setFirmware(firmwareItem);
			BeanUtils.copyProperties(firmwareItem, responseVo);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * firmware delete
	 * @param nodeID
	 * @param firmwareID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/node-{nodeID}/firmware-{firmwareID}", method = RequestMethod.DELETE, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Void> firmwareDelete(@PathVariable String nodeID
											 , @PathVariable String firmwareID
											 , HttpServletRequest request
											 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		Object responseVo = new Object();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey 	= CommonUtil.nvl(request.getHeader("dKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(nodeID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "nodeID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(firmwareID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "firmwareID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Void>(httpStatus);
		}

		try {
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSEByNodeLink(nodeID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			if (CommonUtil.isEmpty(nodeService.findOneNode(nodeID))) {
				throw new IotException("625", CommonUtil.getMessage("msg.device.node.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, null, dKey, nodeID);
			
			FirmwareVO firmwareItem = null;
			if (CommonUtil.isEmpty((firmwareItem = firmwareService.findOneFirmware(firmwareID)))) {
				throw new IotException("631", CommonUtil.getMessage("msg.device.firmware.noRegi.text", locale));
			}
			
			firmwareService.deleteFirmware("resourceID", firmwareID);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();

			httpStatus = HttpStatus.OK;

			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Void>(httpStatus);
	}
	
	/**
	 * firmware retrieve
	 * @param nodeID
	 * @param firmwareID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/node-{nodeID}/firmware-{firmwareID}", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> firmwareRetrieve(@PathVariable String nodeID
												 , @PathVariable String firmwareID
												 , HttpServletRequest request
												 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();
		
		FirmwareVO responseVo = new FirmwareVO();
		HttpStatus httpStatus = HttpStatus.OK;
		
		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey		= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic 	= CommonUtil.nvl(request.getHeader("topic"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(nodeID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "nodeID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(firmwareID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "firmwareID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);
			
			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			
			RemoteCSEVO findRemoteCse = null;
			if (CommonUtil.isEmpty(findRemoteCse = remoteCSEService.findOneRemoteCSEByNodeLink(nodeID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			if (CommonUtil.isEmpty(nodeService.findOneNode(nodeID))) {
				throw new IotException("625", CommonUtil.getMessage("msg.device.node.noRegi.text", locale));
			}
			
			FirmwareVO firmwareItem = null;
			if (CommonUtil.isEmpty((firmwareItem = firmwareService.findOneFirmware(firmwareID)))) {
				throw new IotException("631", CommonUtil.getMessage("msg.device.firmware.noRegi.text", locale));
			}
			
			//responseVo.setFirmware(firmwareItem);
			BeanUtils.copyProperties(firmwareItem, responseVo);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();
			
			httpStatus = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
}