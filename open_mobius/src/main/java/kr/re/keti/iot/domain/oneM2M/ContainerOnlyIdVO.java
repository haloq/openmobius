package kr.re.keti.iot.domain.oneM2M;

import kr.re.keti.common.base.BaseVO;

/**
 * containerOnlyId domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
public class ContainerOnlyIdVO extends BaseVO {
	private static final long serialVersionUID = 1L;

	private String resourceID;
	private String containerType;
	private String labels;

	public String getResourceID() {
		return resourceID;
	}

	public void setResourceID(String resourceID) {
		this.resourceID = resourceID;
	}

	public String getContainerType() {
		return containerType;
	}

	public void setContainerType(String containerType) {
		this.containerType = containerType;
	}

	public String getLabels() {
		return labels;
	}

	public void setLabels(String labels) {
		this.labels = labels;
	}

	@Override
	public String toString() {
		return "ContainerOnlyIdVO [resourceID=" + resourceID + ", containerType=" + containerType + ", labels=" + labels + "]";
	}

}
