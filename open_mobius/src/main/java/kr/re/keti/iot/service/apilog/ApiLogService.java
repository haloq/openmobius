package kr.re.keti.iot.service.apilog;

import java.util.List;

import kr.re.keti.iot.domain.apilog.ApiLogVO;
import kr.re.keti.iot.mdao.apilog.ApiLogDao;
import kr.re.keti.iot.mdao.common.SequenceDao;
import kr.re.keti.iot.mdao.common.SequenceDao.MovType;
import kr.re.keti.iot.mdao.common.SequenceDao.SEQ_PREFIX;
import kr.re.keti.iot.mdao.common.SequenceDao.SeqType;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * apiLog management Service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Service
public class ApiLogService {
	
	private static final Log logger = LogFactory.getLog(ApiLogService.class);
	
	@Autowired
	private MongoLogService mongoLogService;
	
	@Autowired
	private ApiLogDao apiLogDao;
	
	@Autowired
	private SequenceDao seqDao;

	public enum CALLER_FG {
		DEVICE("O07001"),
		USER("O07002"),
		TOPIC("O07003");

		private final String id;
		CALLER_FG(String id) { this.id = id; }
		public String getName() { return id; }
	}
	
	public enum CALL_TYPE {
		CONTENT_INSTANCE_CREATE("O06001"),
		CONTENT_INSTANCE_RETRIEVE("O06002"),
		EXEC_INSTANCE_CREATE("O06011"),
		EXEC_INSTANCE_RETRIEVE("O06012"),
		EXEC_INSTANCE_UPDATE("O06013");

		private final String id;
		CALL_TYPE(String id) { this.id = id; }
		public String getName() { return id; }
	}
	
	/**
	 * set apilog
	 * @param callerFg
	 * @param callerId
	 * @param callType
	 * @param successYn
	 * @return
	 */
	public ApiLogVO setApiLog(CALLER_FG callerFg, String callerId, CALL_TYPE callType, String successYn) {
		String regDate = CommonUtil.getNow("yyyyMMddHHmmssSSS");
		ApiLogVO logProfile = new ApiLogVO();
		
		logProfile.setLogKey(this.generateResourceId());
		logProfile.setCallerFg(callerFg.getName());
		logProfile.setCallerId(callerId);
		logProfile.setCallType(callType.getName());
		if(!CommonUtil.isEmpty(successYn)) {
			logProfile.setSuccessYn(successYn);	
		}
		logProfile.setRegDate(regDate);
		
		return logProfile;

	}
	
	/**
	 * call log
	 * @param callerFg
	 * @param callerId
	 * @param callType
	 * @param successYn
	 */
	public void log(CALLER_FG callerFg, String callerId, CALL_TYPE callType, String successYn) {
		String regDate = CommonUtil.getNow("yyyyMMddHHmmssSSS");
		ApiLogVO logProfile = new ApiLogVO();
		
		logProfile.setLogKey(this.generateResourceId());
		logProfile.setCallerFg(callerFg.getName());
		logProfile.setCallerId(callerId);
		logProfile.setCallType(callType.getName());
		if(!CommonUtil.isEmpty(successYn)) {
			logProfile.setSuccessYn(successYn);	
		}
		logProfile.setRegDate(regDate);
		
		this.log(logProfile);
		
	}
	
	/**
	 * call log
	 * @param apiLogList
	 */
	public void log(List<ApiLogVO> apiLogList) {
		
		if(!CommonUtil.isEmpty(apiLogList) && apiLogList.size() > 0) {
			try { 
				apiLogDao.multi_insert(apiLogList);
			} catch (Exception e) {
				mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			}	
		}
		
	}

	/**
	 * call log
	 * @param logProfile
	 */
	public void log(ApiLogVO logProfile) {
		try { 
			apiLogDao.insert(logProfile);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
	}
	
	/**
	 * generate resourceId
	 * @return
	 */
	private String generateResourceId() {
		StringBuffer bufSeq = new StringBuffer(SEQ_PREFIX.MMP_API_CALL_LOG_TBL.getValue());
		try { 
			Long longSeq = seqDao.move(MovType.UP, SeqType.MMP_API_CALL_LOG_TBL);
			bufSeq.append(String.format("%017d", longSeq));
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}	
		
		return bufSeq.toString();
	}

}
