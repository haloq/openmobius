package kr.re.keti.iot.domain.oneM2M;

import kr.re.keti.common.base.BaseVO;

/**
 * mgmtCmdOnlyId domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
public class MgmtCmdOnlyIdVO extends BaseVO {
	private static final long serialVersionUID = 1L;

	private String resourceID;
	private String labels;
	private String cmdType;

	public String getResourceID() {
		return resourceID;
	}

	public void setResourceID(String resourceID) {
		this.resourceID = resourceID;
	}

	public String getLabels() {
		return labels;
	}

	public void setLabels(String labels) {
		this.labels = labels;
	}

	public String getCmdType() {
		return cmdType;
	}

	public void setCmdType(String cmdType) {
		this.cmdType = cmdType;
	}

	@Override
	public String toString() {
		return "MgmtCmdOnlyIdVO [resourceID=" + resourceID + ", labels="
				+ labels + ", cmdType=" + cmdType + "]";
	}

}
