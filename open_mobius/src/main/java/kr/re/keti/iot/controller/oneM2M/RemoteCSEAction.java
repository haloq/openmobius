package kr.re.keti.iot.controller.oneM2M;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.RemoteCSEVO;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.MgmtCmdService;
import kr.re.keti.iot.service.oneM2M.NodeService;
import kr.re.keti.iot.service.oneM2M.RemoteCSEService;
import kr.re.keti.iot.service.oneM2M.common.MCommonService;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import biz.source_code.base64Coder.Base64Coder;

/**
 * remoteCSE management Action.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Controller
public class RemoteCSEAction {

	private static final Log logger = LogFactory.getLog(RemoteCSEAction.class);
	
	@Autowired
	private MongoLogService mongoLogService;
	
	@Autowired
	private RemoteCSEService remoteCSEService;
	
	@Autowired
	private MgmtCmdService mgmtCmdService;	
	
	@Autowired
	private NodeService nodeService;
	
	@Autowired
	private MCommonService mCommonService;
	
	
	/**
	 * create remoteCSE
	 * @param labels
	 * @param remoteCSEVOProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius", method = RequestMethod.POST, produces = {"application/onem2m-resource+xml", "application/onem2m-resource+json"}, params = "ty=remoteCSE")
	@ResponseBody
	public ResponseEntity<Object> remoteCSECreate(@RequestParam(value = "nm", required = false) String labels
												, @RequestBody RemoteCSEVO remoteCSEVOProfile
												, HttpServletRequest request
												, HttpServletResponse response){
		
		HttpHeaders responseHeaders	= new HttpHeaders();
		RemoteCSEVO responseVo		= new RemoteCSEVO();
		HttpStatus httpStatus		= HttpStatus.CREATED;
		
		String responseCode	= "200";
		String responseMsg	= "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String passCode	= CommonUtil.nvl(request.getHeader("passCode"), "");
		
		String dKey = "";
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(remoteCSEVOProfile)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<remoteCSE> " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(remoteCSEVOProfile.getCseId())) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<remoteCSE> CSE-ID " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}

		if(isError){
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);
			
			return new ResponseEntity<Object>(responseVo, responseHeaders, HttpStatus.OK);
		}
		
		try {
			
			String deviceID = remoteCSEVOProfile.getCseId();
			 
			mCommonService.checkPatternDeviceId(checkMccP, deviceID);
			
			RemoteCSEVO findRemoteCSEItem = null;
			
			if(CommonUtil.isEmpty(findRemoteCSEItem = remoteCSEService.findOneRemoteCSE(deviceID))){
				
				mgmtCmdService.getPointOfAccessList(remoteCSEVOProfile.getPointOfAccess());
				
				if(nodeService.getCount(deviceID) > 0){
					throw new IotException("609", CommonUtil.getMessage("msg.device.node.duplicate.text", locale));
				}
				
				dKey = mCommonService.createDeviceKey(checkMccP, deviceID);
				
				RemoteCSEVO remoteCSEItem = new RemoteCSEVO();
				remoteCSEItem.setResourceID(deviceID);
				remoteCSEItem.setCseId(deviceID);
				remoteCSEItem.setLabels(labels);
				remoteCSEItem.setPassCode(passCode);
				remoteCSEItem.setdKey(dKey);
				
				findRemoteCSEItem = remoteCSEService.createRemoteCSE(remoteCSEItem, remoteCSEVOProfile);
				
			} else {
				
				dKey = mCommonService.createDeviceKey(checkMccP, deviceID);
				
				findRemoteCSEItem.setdKey(dKey);
				findRemoteCSEItem = remoteCSEService.updateRemoteCSEDkey(findRemoteCSEItem);
				
				responseCode = "609";
				responseMsg = CommonUtil.getMessage("msg.device.duplicate.text", locale);
				
			}
			
			if (!CommonUtil.isEmpty(findRemoteCSEItem.getdKey())) {
				findRemoteCSEItem.setdKey(Base64Coder.encodeString(findRemoteCSEItem.getdKey()));
			}
			//responseVo.setRemoteCSE(findRemoteCSEItem);
			BeanUtils.copyProperties(findRemoteCSEItem, responseVo);
		
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();
			httpStatus = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * delete remoteCSE
	 * @param remoteCSEID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}", method = RequestMethod.DELETE, produces = {"application/onem2m-resource+xml", "application/onem2m-resource+json"})
	@ResponseBody
	public ResponseEntity<Void> remoteCSEDelete(@PathVariable String remoteCSEID
											  , HttpServletRequest request
											  , HttpServletResponse response){
		
		HttpHeaders responseHeaders = new HttpHeaders();
		Object responseVo = new Object();
		HttpStatus httpStatus = HttpStatus.OK;
		
		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dkey 	= CommonUtil.nvl(request.getHeader("dKey"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(remoteCSEID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if(isError){
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);
			
			return new ResponseEntity<Void>(httpStatus);
		}
		
		try {
			 
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if(remoteCSEService.getCount(remoteCSEID) < 1){
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dkey);
			
			remoteCSEService.deleteRemoteCSEChild(remoteCSEID);
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Void>(httpStatus);
	}
	
	/**
	 * update remoteCSE
	 * @param remoteCSEID
	 * @param remoteCSEVOProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}", method = RequestMethod.PUT, produces = {"application/onem2m-resource+xml", "application/onem2m-resource+json"})
	@ResponseBody
	public ResponseEntity<Object> remoteCSEUpdate(@PathVariable String remoteCSEID
												, @RequestBody RemoteCSEVO remoteCSEVOProfile
												, HttpServletRequest request
												, HttpServletResponse response){
		HttpHeaders responseHeaders = new HttpHeaders();
		RemoteCSEVO responseVo = new RemoteCSEVO();
		HttpStatus httpStatus = HttpStatus.OK;
		
		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dkey 	= CommonUtil.nvl(request.getHeader("dKey"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(remoteCSEID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if(CommonUtil.isEmpty(remoteCSEVOProfile)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "<remoteCSE> " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if(isError){
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);
			
			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			 
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if(remoteCSEService.getCount(remoteCSEID) < 1){
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dkey);
			
			String pointOfAccess = remoteCSEVOProfile.getPointOfAccess();
			if (pointOfAccess != null && !"".equals(pointOfAccess)) {
				
				mgmtCmdService.getPointOfAccessList(remoteCSEVOProfile.getPointOfAccess());
			}
			
			RemoteCSEVO findRemoteCSEItem = remoteCSEService.updateRemoteCSE(remoteCSEID, remoteCSEVOProfile);
			
			if (!CommonUtil.isEmpty(findRemoteCSEItem.getdKey())) {
				findRemoteCSEItem.setdKey(Base64Coder.encodeString(findRemoteCSEItem.getdKey()));
			}
			
			BeanUtils.copyProperties(findRemoteCSEItem, responseVo);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	/**
	 * retrieve remoteCSE
	 * @param remoteCSEID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}", method = RequestMethod.GET, produces = {"application/onem2m-resource+xml", "application/onem2m-resource+json"})
	@ResponseBody
	public ResponseEntity<Object> remoteCSERetrieve(@PathVariable String remoteCSEID
												  , HttpServletRequest request
												  , HttpServletResponse response){
		
		HttpHeaders responseHeaders = new HttpHeaders();
		RemoteCSEVO responseVo = new RemoteCSEVO();
		HttpStatus httpStatus = HttpStatus.OK;
		
		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey		= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic 	= CommonUtil.nvl(request.getHeader("topic"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(remoteCSEID)){
			isError			= true;
			responseCode	= "600";
			responseMsg		= "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if(isError){
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);
			
			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
		
		try {
			 
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			RemoteCSEVO findRemoteCSEItem = null;
			if(CommonUtil.isEmpty(findRemoteCSEItem = remoteCSEService.findOneRemoteCSE(remoteCSEID))){
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			if (!CommonUtil.isEmpty(findRemoteCSEItem.getdKey())) {
				findRemoteCSEItem.setdKey(Base64Coder.encodeString(findRemoteCSEItem.getdKey()));
			}
			//responseVo.setRemoteCSE(findRemoteCSEItem);
			BeanUtils.copyProperties(findRemoteCSEItem, responseVo);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg = e.getMessage();
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
}
