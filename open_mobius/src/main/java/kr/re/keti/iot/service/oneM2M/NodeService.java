package kr.re.keti.iot.service.oneM2M;

import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.NodeVO;
import kr.re.keti.iot.domain.oneM2M.RemoteCSEVO;
import kr.re.keti.iot.mdao.common.SequenceDao;
import kr.re.keti.iot.mdao.common.GenericMongoDao.MOBIUS;
import kr.re.keti.iot.mdao.common.GenericMongoDao.ONEM2M;
import kr.re.keti.iot.mdao.common.SequenceDao.MovType;
import kr.re.keti.iot.mdao.common.SequenceDao.SEQ_PREFIX;
import kr.re.keti.iot.mdao.common.SequenceDao.SeqType;
import kr.re.keti.iot.mdao.oneM2M.NodeDao;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

/**
 * node management Service.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Service
public class NodeService {

	private static final Log logger = LogFactory.getLog(NodeService.class);

	@Autowired
	private MongoLogService mongoLogService;
	
	@Autowired
	private FirmwareService firmwareService;	
	
	@Autowired
	private SoftwareService softwareService;	
	
	@Autowired
	private DeviceInfoService deviceInfoService;	

	@Autowired
	private NodeDao nodeDao;
	
	@Autowired
	private SequenceDao seqDao;
	
	public enum STATUS{
		
		SUCCESSFUL("1"), 
		FAILURE("2"), 
		IN_PROCESS("3");
		
		private final String id;
		STATUS(String id){
			this.id = id;
		}
		public String getValue() {return id;}
			
	}
	
	public enum MGMT_DEFINITION{
		
		FIRMWARE("1001"), 
		SOFTWARE("1002"), 
		DEVICE_INFO("1007");
		
		private final String id;
		MGMT_DEFINITION(String id){
			this.id = id;
		}
		public String getValue() {return id;}
			
	}
	
	/**
	 * generate resourceID
	 * @return
	 * @throws IotException
	 */
	private String generateResourceId() throws IotException {
		StringBuffer bufSeq = new StringBuffer(SEQ_PREFIX.NODE.getValue());
		long longSeq = 0;
		
		try {
			
			longSeq = seqDao.move(MovType.UP, SeqType.NODE);
			
			bufSeq.append(String.format("%010d", longSeq));
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.node.id.createFail.text"));
		}
		
		return bufSeq.toString();
	}	
	
	/**
	 * find node by deviceId
	 * @param deviceId
	 * @return
	 * @throws IotException
	 */
	public NodeVO findOneNode(String deviceId) throws IotException {

		NodeVO findNodeItem = null;

		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(deviceId));

		try {

			findNodeItem = (NodeVO) nodeDao.findOne(query);

		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.node.findFail.text"));
		}

		return findNodeItem;
	}
	
	/**
	 * find node count by deviceId
	 * @param deviceId
	 * @return
	 */
	public long getCount(String deviceId){
		
		Query query = new Query();
		query.addCriteria(Criteria.where("resourceID").is(deviceId));
		
		long cnt = 0;
		
		try {
			cnt = nodeDao.count(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "node get count");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}
		
		return cnt;
	}
	
	/**
	 * create node
	 * @param remoteCSEVOProfile
	 * @param currentTime
	 * @return
	 * @throws IotException
	 */
	public NodeVO createNode(RemoteCSEVO remoteCSEVOProfile, String currentTime) throws IotException {
		
		NodeVO rtnNode = null;
		NodeVO nodeItem = new NodeVO();
		
		boolean isError = false;
		String errMsg = "";
		
		if(CommonUtil.isEmpty(remoteCSEVOProfile)) {
			isError = true;
			errMsg = "<remoteCSE> " + CommonUtil.getMessage("msg.input.empty.text");
		}
		
		if(isError){
			mongoLogService.log(logger, LEVEL.ERROR, errMsg);
			throw new IotException("600", errMsg);
		}
		
		String nodeId = remoteCSEVOProfile.getResourceID();
//		String nodeResourceId = this.generateResourceId();
		
		nodeItem.setResourceID(nodeId);
		nodeItem.setNodeID(nodeId);
		nodeItem.setResourceType(ONEM2M.NODE.getName());
//		nodeItem.setResourceID(remoteCSEVOProfile.getResourceID());
		nodeItem.setParentID(MOBIUS.NAME.getName());
		nodeItem.setAccessControlPolicyIDs(remoteCSEVOProfile.getAccessControlPolicyIDs());
//		nodeItem.setHostedCSEID(remoteCSEVOProfile.getCseId());
		
		nodeItem.setCreationTime(currentTime);
		nodeItem.setLastModifiedTime(currentTime);
		
		try {
			
			nodeDao.insert(nodeItem);
			
			rtnNode = this.findOneNode(nodeItem.getResourceID());
			
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600",CommonUtil.getMessage("msg.device.node.createFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "node create success");

		return rtnNode;
	}
	
	/**
	 * delete node
	 * @param deviceId
	 * @throws IotException
	 */
	public void deleteNode(String deviceId) throws IotException {
		
		Query query = new Query(Criteria.where("resourceID").is(deviceId));
		
		try {
			nodeDao.remove(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "node remove");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.node.delFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "node delete success");
	}
	
	/**
	 * delete node with child
	 * @param deviceId
	 * @throws IotException
	 */
	public void deleteNodeChild(String deviceId) throws IotException {
		
		try {
			firmwareService.deleteFirmware("parentID", deviceId);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "node remove");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			
			throw new IotException("600", CommonUtil.getMessage("msg.device.firmware.delFail.text"));
		}
		
		try {
			softwareService.deleteSoftware("parentID", deviceId);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "node remove");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			
			throw new IotException("600", CommonUtil.getMessage("msg.device.software.delFail.text"));
		}
		
		try {
			deviceInfoService.deleteDeviceInfo("parentID", deviceId);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "node remove");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			
			throw new IotException("600", CommonUtil.getMessage("msg.device.deviceInfo.delFail.text"));
		}
		
		
		Query query = new Query(Criteria.where("resourceID").is(deviceId));
		
		try {
			nodeDao.remove(query);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "node remove");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.node.delFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "node delete success");
	}
	
	/**
	 * update node
	 * @param remoteCSEVOProfile
	 * @param currentTime
	 * @throws IotException
	 */
	public void updateNode(RemoteCSEVO remoteCSEVOProfile, String currentTime) throws IotException {
		
		String accessControlPolicyIDs = remoteCSEVOProfile.getAccessControlPolicyIDs();
//		String hostedCSEID = remoteCSEVOProfile.getCseId();
		
		Query query = new Query(Criteria.where("resourceID").is(remoteCSEVOProfile.getResourceID()));
		
		Update update = new Update();
		if(!CommonUtil.isNull(currentTime))			update.set("lastModifiedTime", currentTime);
		if(!CommonUtil.isNull(accessControlPolicyIDs))	update.set("accessControlPolicyIDs", accessControlPolicyIDs);
//		if(!CommonUtil.isNull(hostedCSEID))			update.set("hostedCSEID", hostedCSEID);
		
		try {
			nodeDao.update(query, update);
		} catch (Exception e) {
			mongoLogService.log(logger, LEVEL.ERROR, "node update");
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
			throw new IotException("600", CommonUtil.getMessage("msg.device.node.upFail.text"));
		}
		
		mongoLogService.log(logger, LEVEL.DEBUG, "node update success");
	}
	
	
	
}