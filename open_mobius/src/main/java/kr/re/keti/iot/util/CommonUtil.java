package kr.re.keti.iot.util;

import java.io.StringReader;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * commonUtil.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Component
public class CommonUtil {

	private static final Log logger = LogFactory.getLog(CommonUtil.class);
	
	
	private static MessageSource messageSource;

	public static final int COUNT_PER_PAGE_COMMON = 20;
	public static final int COUNT_PER_PAGE_SEARCH = 20;
	public static final int COUNT_PER_PAGE_SEARCHAPP = 9;
	public static final int COUNT_PER_PAGE_POPUP = 10;
	public static final int PAGE_BLOCK_COUNT = 10;
	public static final String GLOBAL_PUBLIC_TOPIC_CODE = "T010000001";
	public static final String FTP_SERVER_ID = "F020000002";

	public static final String REDIRECT_TO_URL = "/IoTPortal/main/redirectToPage";
	public static final String REDIRECT_TO_FUNCTION_PAGE = "pp/common/redirectToFunction";
	public static final String REDIRECT_TO_PAGE = "pp/common/redirectToPage";
	public static final String LINE_SEPERATOR = "\n";
	
	//public static boolean RUN_D_API = true;
	public static boolean D_STATUS_ALL_TRUE = false;
	public static boolean RUN_VLRIFY_FLAG = false;
	public static boolean STOP_API_ACTION = false;

	public static final HashMap<String,String> cert_types
	= new HashMap<String,String>(){
	/**
		 *
		 */
		private static final long serialVersionUID = 6261806771750977413L;

	{
		put("device","K01001");
		put("user","K01002");
		put("server","K01003");
	}};

	/**
	 * System User Group
	 */
	public static final String ALL_ADMIN_GROUP = "G020000001";
	public static final String B2C_ADMIN_GROUP = "G020000002";
	public static final String B2B_ADMIN_GROUP = "G020000003";
	
	public static final String ALL_USER_GROUP 	= "G020000100";
	public static final String B2C_USER_GROUP 	= "G020000101";
	public static final String B2C_DEV_GROUP 	= "G020000102";
	public static final String B2B_USER_GROUP 	= "G020000103";
	
	public static MessageSource getMessageSource() {
		return messageSource;
	}
	public static void setMessageSource(MessageSource messageSource) {
		CommonUtil.messageSource = messageSource;
	}
	
	/**
	 * getRequest
	 * @return
	 */
	public static HttpServletRequest getRequest() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		return request;
	}
	
	/**
	 * setLocale
	 * @param sLocale
	 * @return
	 */
	public static boolean setLocale(String sLocale) {
	  HttpServletRequest request = getRequest();
	  Locale locale = null;
	  try {
		  
		  if ("ko".equals(sLocale)) {
			  locale = Locale.KOREAN;
		  } else if ("en".equals(sLocale)) {
			  locale = Locale.ENGLISH;
//		  } else if ("zh".equals(sLocale)) {
//			  locale = Locale.CHINESE;
		  } else {
			  locale = request.getLocale();
		  }
		  HttpSession session = request.getSession(true);
		  session.setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, locale);


	  } catch (Exception ex){
		  return false;
	  }
	  return true;
	 }

	/**
	 * getLocale
	 * @return
	 */
	public static Locale getLocale(){
		HttpServletRequest request = getRequest();
		HttpSession session = request.getSession(false);
		Locale locale = null;
		if(session != null){
			locale = (Locale)session.getAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME);
			if(locale != null){
				logger.debug("######################################");
				logger.debug("Locale From Session");
				logger.debug("######################################");
			}
		}
		
		return locale;
	}
	
	/**
	 * getMessage
	 * @param sMsgCode
	 * @param sLocale
	 * @return
	 */
	public static String getMessage (String sMsgCode, String sLocale) {
		Locale locale = Locale.KOREAN;
		if(! isEmpty(sLocale)){
			sLocale = sLocale.toLowerCase();
			if(! "ko".equals(sLocale)){
				if(sLocale.equals("en")){
					locale = Locale.ENGLISH;
//				} else if (sLocale.equals("zh")){
//					locale = Locale.CHINESE;
				}
			}
		}
		String sMessage = messageSource.getMessage(sMsgCode, null, locale);
		return sMessage;
	}
	
	/**
	 * getMessage
	 * @param sMsgCode
	 * @return
	 */
	public static String getMessage (String sMsgCode) {
		Locale locale = getLocale();

		String sMessage = messageSource.getMessage(sMsgCode, null, locale);
		return sMessage;
	}

	/**
	 * getMessage
	 * @param sMsgCode
	 * @param arr
	 * @return
	 */
	public static String getMessage (String sMsgCode, Object[] arr) {
		Locale locale = getLocale();

		String sMessage = messageSource.getMessage(sMsgCode, arr, locale);
		return sMessage;
	}

	/**
	 * isNull
	 * @param str
	 * @return
	 */
	public static boolean isNull(String str){
		if(str == null)
			return true;

		return false;
	}
	
	/**
	 * isEmpty
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(String str){
		if(str == null || str.isEmpty())
			return true;
		
		return false;
	}

	/**
	 * isEmpty
	 * @param obj
	 * @return
	 */
	public static boolean isEmpty(Object obj){
		if(obj == null)
			return true;

		return false;
	}

	/**
	 * isEmpty
	 * @param obj
	 * @return
	 */
	public static boolean isEmpty(Object[] obj){
		if(obj == null || obj.length == 0)
			return true;

		return false;
	}

	/**
	 * nvl
	 * @param sTarget
	 * @param sReplace
	 * @return
	 */
	public static String nvl(String sTarget, String sReplace){
		if(sTarget == null || sTarget.isEmpty()){
			return sReplace;
		}
		return sTarget;
	}
	
	/**
	 * getConfig
	 * @param sCfgCode
	 * @return
	 */
	public static String getConfig(String sCfgCode){
		return getMessage(sCfgCode, "root");
	}
	
	/**
	 * valueOf
	 * @param l
	 * @return
	 */
	public static long valueOf(String l){
		long ret = 0;
		try {
			ret = Long.valueOf(l);
		} catch (NumberFormatException e) {
		}
		
		return ret;
	}
	
	/**
	 * unMarshal from xml
	 * @param cls
	 * @param in
	 * @return
	 */
	public static Object unMarshalFromXmlString(Class cls, String in){
		Object obj = null;
		StringReader sr = new StringReader(in);
		
		try {
			JAXBContext context = JAXBContext.newInstance(cls);
			Unmarshaller msh =  context.createUnmarshaller();
			obj = msh.unmarshal(sr);
		} catch (Exception e) {
			return null;
		}
		
		return obj;
	}
	
	/**
	 * getNow
	 * @param format
	 * @return
	 */
	public static String getNow(String format){
		String now = "";
		
		SimpleDateFormat simpleFomat = new SimpleDateFormat(format);
		Date currentTime = new Date();
		now = simpleFomat.format(currentTime);
		
		return now;
	}
	
	/**
	 * parseDate
	 * @param dateValue
	 * @param format
	 * @return
	 */
	public static Date parseDate(String dateValue, String format){
		boolean parseError = false;
		Date ret = null;
		
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		try {
			ret = sdf.parse(dateValue);
		} catch (ParseException e) {
			parseError = true;
		}
		
		if(parseError){
			sdf = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
			
			try {
				ret = sdf.parse(dateValue);
			} catch (ParseException e) {
				return null;
			}
		}
		
		return ret;
	}
	
	/**
	 * checkElapseTime
	 * @param dateString
	 * @param diff
	 * @param format
	 * @return
	 */
	public static int checkElapseTime(String dateString, long diff, String format){
		int result = 0;
		
		Date now =  new Date();
		Date date = parseDate(dateString, format);
		
		long getDiff = now.getTime() - date.getTime();
		
		if(diff == getDiff) 		result = 0;
		else if(diff < getDiff)	result = 1;
		else if(diff > getDiff)	result = -1;
		
		return result;
	}
	
	/**
	 * marshal to xml
	 * @param obj
	 * @return
	 */
	public static String marshalToXmlString(Object obj){
		
		return marshalToXmlString(obj, false);
	}
	
	/**
	 * marshal to xml
	 * @param obj
	 * @param isJaxbFragment
	 * @return
	 */
	public static String marshalToXmlString(Object obj, boolean isJaxbFragment){
		StringWriter sw = new StringWriter();
		
		try {
			
			JAXBContext context = JAXBContext.newInstance(obj.getClass());
			Marshaller msh = context.createMarshaller();
			msh.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
			msh.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);
			msh.setProperty(Marshaller.JAXB_FRAGMENT, isJaxbFragment);
			msh.marshal(obj, sw);
		} catch (Exception e) {
			return null;
		}
		
		return sw.toString();
	}
	
	/**
	 * isInteger
	 * @param str
	 * @return
	 */
	public static boolean isInteger(String str){
		try {
			Integer.parseInt(str);
		} catch (NumberFormatException e) {
			return false;
		}
		return true;
		
	}
	
	/**
	 * isMinus
	 * @param str
	 * @return
	 */
	public static boolean isMinus(String str){
		try {
			if(Integer.parseInt(str) < 0) return true;
		} catch (NumberFormatException e) {}
		
		return false;
	}
	
    /**
     * checkUrl
     * @param url
     * @return
     */
    public static boolean checkUrl(String url) {
    	if (url.startsWith("http://")) {
    		return true;
    	} else if (url.startsWith("https://")) {
    		return true;
    	}
    	
    	return false;
    }
    
   /**
    * getURL
    * @param req
    * @return
    */
    public static String getURL(HttpServletRequest req) {

        String scheme = req.getScheme();
        String serverName = req.getServerName();
        int serverPort = req.getServerPort();
        String contextPath = req.getContextPath();
        //String servletPath = req.getServletPath();
        String pathInfo = req.getPathInfo();
        //String queryString = req.getQueryString();

        StringBuffer url =  new StringBuffer();
        url.append(scheme).append("://").append(serverName);

        if ((serverPort != 80) && (serverPort != 443)) {
            url.append(":").append(serverPort);
        }

//        url.append(contextPath).append(servletPath);
        url.append(contextPath);

        if (pathInfo != null) {
            url.append(pathInfo);
        }
//        if (queryString != null) {
//            url.append("?").append(queryString);
//        }
        return url.toString();
    }
    
    
}
