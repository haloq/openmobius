package kr.re.keti.iot.domain.oneM2M;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import kr.re.keti.iot.domain.common.GenericModel;

/**
 * contentInstance domain.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@XmlRootElement(name="contentInstance", namespace="http://www.onem2m.org/xml/protocols")
@XmlAccessorType(value=XmlAccessType.FIELD)
public class ContentInstanceVO extends GenericModel { 
	private static final long serialVersionUID = 1L;

	private String resourceType;
	private String resourceID;
	private String parentID;
	private String creationTime;
	private String lastModifiedTime;
	@XmlTransient
	private String expirationTime;
	private String labels;
	private String stateTag;
	private String typeOfContent;
	private String contentSize;
	private String ontologyRef;
	private String content;
	private String linkType;
	
	@XmlTransient
	private Date expirationDate;

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceID() {
		return resourceID;
	}

	public void setResourceID(String resourceID) {
		this.resourceID = resourceID;
	}

	public String getParentID() {
		return parentID;
	}

	public void setParentID(String parentID) {
		this.parentID = parentID;
	}

	public String getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	public String getLastModifiedTime() {
		return lastModifiedTime;
	}

	public void setLastModifiedTime(String lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}

	public String getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(String expirationTime) {
		this.expirationTime = expirationTime;
	}

	public String getLabels() {
		return labels;
	}

	public void setLabels(String labels) {
		this.labels = labels;
	}

	public String getStateTag() {
		return stateTag;
	}

	public void setStateTag(String stateTag) {
		this.stateTag = stateTag;
	}

	public String getTypeOfContent() {
		return typeOfContent;
	}

	public void setTypeOfContent(String typeOfContent) {
		this.typeOfContent = typeOfContent;
	}

	public String getContentSize() {
		return contentSize;
	}

	public void setContentSize(String contentSize) {
		this.contentSize = contentSize;
	}

	public String getOntologyRef() {
		return ontologyRef;
	}

	public void setOntologyRef(String ontologyRef) {
		this.ontologyRef = ontologyRef;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getLinkType() {
		return linkType;
	}

	public void setLinkType(String linkType) {
		this.linkType = linkType;
	}

	@Override
	public String toString() {
		return "ContentInstanceVO [resourceType=" + resourceType
				+ ", resourceID=" + resourceID + ", parentID=" + parentID
				+ ", creationTime=" + creationTime + ", lastModifiedTime="
				+ lastModifiedTime + ", expirationTime=" + expirationTime
				+ ", labels=" + labels + ", stateTag=" + stateTag
				+ ", typeOfContent=" + typeOfContent + ", contentSize="
				+ contentSize + ", ontologyRef=" + ontologyRef + ", content="
				+ content + ", linkType=" + linkType + "]";
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	};
	
	
}
