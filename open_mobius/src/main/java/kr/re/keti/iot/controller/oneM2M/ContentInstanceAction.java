package kr.re.keti.iot.controller.oneM2M;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.re.keti.iot.domain.apilog.CallerVO;
import kr.re.keti.iot.domain.common.IotException;
import kr.re.keti.iot.domain.oneM2M.ContainerVO;
import kr.re.keti.iot.domain.oneM2M.ContentInstanceVO;
import kr.re.keti.iot.domain.oneM2M.ListOfResourceVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao.ONEM2M;
import kr.re.keti.iot.service.apilog.ApiLogService;
import kr.re.keti.iot.service.apilog.ApiLogService.CALLER_FG;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.ApiLogService.CALL_TYPE;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.AEService;
import kr.re.keti.iot.service.oneM2M.ContainerService;
import kr.re.keti.iot.service.oneM2M.ContentInstanceService;
import kr.re.keti.iot.service.oneM2M.RemoteCSEService;
import kr.re.keti.iot.service.oneM2M.common.MCommonService;
import kr.re.keti.iot.util.CommonUtil;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * contentInstance management Action.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>khlee</li>
 *         </ul>
 */
@Controller
public class ContentInstanceAction {

	private static final Log logger = LogFactory.getLog(ContentInstanceAction.class);
	
	@Autowired
	private MongoLogService mongoLogService;
	
	@Autowired
	private MCommonService mCommonService;

	@Autowired
	private RemoteCSEService remoteCSEService;
	
	@Autowired
	private AEService aEService;

	@Autowired
	private ContainerService containerService;
	
	@Autowired
	private ContentInstanceService contentInstanceService;
	
	@Autowired
	private ApiLogService apiLogService;

	
	/**
	 * contentInstance create
	 * @param remoteCSEID
	 * @param containerName
	 * @param labels
	 * @param contentInstanceProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID}/container-{containerName}", method = RequestMethod.POST, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" }, params = {"ty=contentInstance"})
	@ResponseBody
	public ResponseEntity<Object> remoteCSEContentInstanceCreate(@PathVariable String remoteCSEID
															   , @PathVariable String containerName
															   , @RequestParam(value = "nm", required = false) String labels
															   , @RequestBody ContentInstanceVO contentInstanceProfile
															   , HttpServletRequest request
															   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ContentInstanceVO responseVo = new ContentInstanceVO();
		HttpStatus httpStatus = HttpStatus.CREATED;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey 	= CommonUtil.nvl(request.getHeader("dKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(contentInstanceProfile)) {
			isError = true;
			responseCode = "600";
			responseMsg = "<contentInstance> " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if(isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
					
		try {
			 
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dKey);
			
			ContainerVO containerItem = null;
			if (CommonUtil.isEmpty((containerItem = containerService.findOneContainerByLabels(remoteCSEID, containerName)))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			contentInstanceProfile.setParentID(containerItem.getResourceID());
			contentInstanceProfile.setLabels(labels);
			ContentInstanceVO contentInstanceItem = contentInstanceService.createContentInstance(contentInstanceProfile);
			
			//responseVo.setContentInstance(contentInstanceItem);
			BeanUtils.copyProperties(contentInstanceItem, responseVo);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();
			
			httpStatus   = HttpStatus.OK;
			
			apiLogService.log(CALLER_FG.DEVICE, remoteCSEID, CALL_TYPE.CONTENT_INSTANCE_CREATE, "N");
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	
	/**
	 * contentInstance delete
	 * @param remoteCSEID
	 * @param containerName
	 * @param contentInstanceID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID}/container-{containerName}/contentInstance-{contentInstanceID}", method = RequestMethod.DELETE, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Void> remoteCSEContentInstanceDelete(@PathVariable String remoteCSEID
															 , @PathVariable String containerName
															 , @PathVariable String contentInstanceID
															 , HttpServletRequest request
															 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		Object responseVo = new Object();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg  = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey 	= CommonUtil.nvl(request.getHeader("dKey"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(contentInstanceID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "contentInstanceID" + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Void>(httpStatus);
		}
		
		try {
			 
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dKey);
			
			ContainerVO containerItem = null;
			if (CommonUtil.isEmpty(containerItem = containerService.findOneContainerByLabels(remoteCSEID, containerName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			ContentInstanceVO contentInstanceItem = null;
			if (CommonUtil.isEmpty(contentInstanceItem = contentInstanceService.findOneContentInstance(containerItem.getResourceID(), contentInstanceID))) {
				throw new IotException("629", CommonUtil.getMessage("msg.contentinstance.noRegi.text", locale));
			}
			
			contentInstanceService.deleteContentInstance(containerItem.getResourceID(), contentInstanceItem);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Void>(httpStatus);
	}
	
	/**
	 * contentInstance retrieve
	 * @param remoteCSEID
	 * @param containerName
	 * @param countPerPage
	 * @param startIndex
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:[^9.9.999.9].*}/container-{containerName}/latest", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> remoteCSEContentInstanceRetrieve(@PathVariable String remoteCSEID
																 , @PathVariable String containerName
																 , @RequestParam(required=false) Integer countPerPage
																 , @RequestParam(required=false) Integer startIndex
																 , HttpServletRequest request
																 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ListOfResourceVO responseVo = new ListOfResourceVO();
		responseVo.setName(ONEM2M.CONTENT_INSTANCE.getName());
		
		ContentInstanceVO responseContentInstanceVo = new ContentInstanceVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg  = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey   	= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic  	= CommonUtil.nvl(request.getHeader("topic"), "");
		CallerVO callerVO = null;

		boolean checkLatest = countPerPage == null || startIndex == null ? true : false;
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);
			
			if (!checkLatest) {
				return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
			} else {
				return new ResponseEntity<Object>(responseContentInstanceVo, responseHeaders, httpStatus);
			}
		}
		
		try {
			
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			ContainerVO containerItem = null;
			if (CommonUtil.isEmpty(containerItem = containerService.findOneContainerByLabels(remoteCSEID, containerName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			if(!CommonUtil.isEmpty(countPerPage) && !CommonUtil.isEmpty(startIndex)) {
				
				long totalCount = contentInstanceService.findCountContentInstance("parentID", containerItem.getResourceID());
				if(totalCount <= 0) {
					throw new IotException("604", CommonUtil.getMessage("msg.device.contentinstance.notFound.text", locale));
				}
				
				Query query = new Query();
				query.addCriteria(Criteria.where("parentID").is(containerItem.getResourceID()));
				//query.with(new Sort(Sort.Direction.DESC, "creationTime"));
				query.with(new Sort(Sort.Direction.DESC, "resourceID"));
				query.skip(startIndex == 1 ? 0 : ((startIndex * countPerPage) - countPerPage));
				query.limit(countPerPage);
				
				List<ContentInstanceVO> findContentInstanceList = contentInstanceService.findContentInstance(query);
				if (CommonUtil.isEmpty(findContentInstanceList) && findContentInstanceList.size() <= 0) {
					throw new IotException("604", CommonUtil.getMessage("msg.device.contentinstance.notFound.text", locale));
				}				
				
				//responseVo.setTotalListCount(String.valueOf(totalCount));
				//responseVo.setCountPerPage(String.valueOf(countPerPage));
				//responseVo.setStartIndex(String.valueOf(startIndex));
				responseVo.setContentInstanceList(findContentInstanceList);
				
				if (findContentInstanceList.size() > 0) {
					BeanUtils.copyProperties(findContentInstanceList.get(0), responseContentInstanceVo);
				}
			
			} else {
				ContentInstanceVO findContentInstanceItem = null;
				if (CommonUtil.isEmpty(findContentInstanceItem = contentInstanceService.findOneContentInstance(containerItem.getResourceID(), containerItem.getLatest()))) {
					throw new IotException("604", CommonUtil.getMessage("msg.device.contentinstance.notFound.text", locale));
				}
				responseVo.setContentInstance(findContentInstanceItem);
				
				BeanUtils.copyProperties(findContentInstanceItem, responseContentInstanceVo);
			}

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		
		if (!checkLatest) {
			mCommonService.setResponseHeader(request, response, responseVo);
			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		} else {
			request.getParameterMap().put("resourceID", responseContentInstanceVo.getResourceID());
			mCommonService.setResponseHeader(request, response, responseContentInstanceVo);
			return new ResponseEntity<Object>(responseContentInstanceVo, responseHeaders, httpStatus);
		}
	}
	
	/**
	 * AE contentInstance create
	 * @param appId
	 * @param containerName
	 * @param labels
	 * @param contentInstanceProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/AE-{appId:.*}/container-{containerName}", method = RequestMethod.POST, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" }, params = {"ty=contentInstance"})
	@ResponseBody
	public ResponseEntity<Object> AEContentInstanceCreate(@PathVariable String appId
														, @PathVariable String containerName
														, @RequestParam(value = "nm", required = false) String labels
														, @RequestBody ContentInstanceVO contentInstanceProfile
														, HttpServletRequest request
														, HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ContentInstanceVO responseVo = new ContentInstanceVO();
		HttpStatus httpStatus = HttpStatus.CREATED;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String aKey 	= CommonUtil.nvl(request.getHeader("aKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(appId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "app-ID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(contentInstanceProfile)) {
			isError = true;
			responseCode = "600";
			responseMsg = "<contentInstance> " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if(isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
					
		try {
			 
			mCommonService.checkPatternDeviceId(checkMccP, appId);
			
			if (CommonUtil.isEmpty(aEService.findOneAE("appId", appId))) {
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			mCommonService.verifyAKey(checkMccP, appId, aKey);
			
			ContainerVO containerItem = null;
			if (CommonUtil.isEmpty((containerItem = containerService.findOneContainerByLabels(appId, containerName)))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			contentInstanceProfile.setParentID(containerItem.getResourceID());
			contentInstanceProfile.setLabels(labels);
			ContentInstanceVO contentInstanceItem = contentInstanceService.createContentInstance(contentInstanceProfile);
			
			//responseVo.setContentInstance(contentInstanceItem);
			BeanUtils.copyProperties(contentInstanceItem, responseVo);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();
			
			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	
	/**
	 * AE contentInstance delete
	 * @param appId
	 * @param containerName
	 * @param contentInstanceID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/AE-{appId:.*}/container-{containerName}/contentInstance-{contentInstanceID}", method = RequestMethod.DELETE, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Void> AEContentInstanceDelete(@PathVariable String appId
													  , @PathVariable String containerName
													  , @PathVariable String contentInstanceID
													  , HttpServletRequest request
													  , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		Object responseVo = new Object();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg  = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String aKey 	= CommonUtil.nvl(request.getHeader("aKey"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(appId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "appId " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(contentInstanceID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "contentInstanceID" + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Void>(httpStatus);
		}
		
		try {
			 
			mCommonService.checkPatternDeviceId(checkMccP, appId);
			
			if (CommonUtil.isEmpty(aEService.findOneAE("appId", appId))) {
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			mCommonService.verifyAKey(checkMccP, appId, aKey);
			
			ContainerVO containerItem = null;
			if (CommonUtil.isEmpty(containerItem = containerService.findOneContainerByLabels(appId, containerName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			ContentInstanceVO contentInstanceItem = null;
			if (CommonUtil.isEmpty(contentInstanceItem = contentInstanceService.findOneContentInstance(containerItem.getResourceID(), contentInstanceID))) {
				throw new IotException("629", CommonUtil.getMessage("msg.contentinstance.noRegi.text", locale));
			}
			
			contentInstanceService.deleteContentInstance(containerItem.getResourceID(), contentInstanceItem);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Void>(httpStatus);
	}
	
	/**
	 * AE contentInstance retrieve
	 * @param appId
	 * @param containerName
	 * @param countPerPage
	 * @param startIndex
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/AE-{appId:.*}/container-{containerName}/latest", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> AEContentInstanceRetrieve(@PathVariable String appId
														  , @PathVariable String containerName
														  , @RequestParam(required=false) Integer countPerPage
														  , @RequestParam(required=false) Integer startIndex
														  , HttpServletRequest request
														  , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ListOfResourceVO responseVo = new ListOfResourceVO();
		responseVo.setName(ONEM2M.CONTENT_INSTANCE.getName());
		
		ContentInstanceVO responseContentInstanceVo = new ContentInstanceVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg  = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey   	= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic  	= CommonUtil.nvl(request.getHeader("topic"), "");
		CallerVO callerVO = null;
		
		boolean checkLatest = countPerPage == null || startIndex == null ? true : false;
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		mongoLogService.log(logger, LEVEL.DEBUG, "from ==> " + from);
		mongoLogService.log(logger, LEVEL.DEBUG, "requestId ==> " + requestId);
		mongoLogService.log(logger, LEVEL.DEBUG, "appId ==> " + appId);
		mongoLogService.log(logger, LEVEL.DEBUG, "containerName ==> " + containerName);
		mongoLogService.log(logger, LEVEL.DEBUG, "checkMccP ==> " + checkMccP);
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(appId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "appId " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			if (!checkLatest) {
				return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
			} else {
				return new ResponseEntity<Object>(responseContentInstanceVo, responseHeaders, httpStatus);
			}
		}
		
		try {
			 
			mCommonService.checkPatternDeviceId(checkMccP, appId);
			
			if (CommonUtil.isEmpty(aEService.findOneAE("appId", appId))) {
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			ContainerVO containerItem = null;
			if (CommonUtil.isEmpty(containerItem = containerService.findOneContainerByLabels(appId, containerName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			if(!CommonUtil.isEmpty(countPerPage) && !CommonUtil.isEmpty(startIndex)) {
				
				long totalCount = contentInstanceService.findCountContentInstance("parentID", containerItem.getResourceID());
				if(totalCount <= 0) {
					throw new IotException("604", CommonUtil.getMessage("msg.device.contentinstance.notFound.text", locale));
				}
				
				Query query = new Query();
				query.addCriteria(Criteria.where("parentID").is(containerItem.getResourceID()));
				//query.with(new Sort(Sort.Direction.DESC, "creationTime"));
				query.with(new Sort(Sort.Direction.DESC, "resourceID"));
				query.skip(startIndex == 1 ? 0 : ((startIndex * countPerPage) - countPerPage));
				query.limit(countPerPage);
				
				List<ContentInstanceVO> findContentInstanceList = contentInstanceService.findContentInstance(query);
				if (CommonUtil.isEmpty(findContentInstanceList) && findContentInstanceList.size() <= 0) {
					throw new IotException("604", CommonUtil.getMessage("msg.device.contentinstance.notFound.text", locale));
				}				
				
				//responseVo.setTotalListCount(String.valueOf(totalCount));
				//responseVo.setCountPerPage(String.valueOf(countPerPage));
				//responseVo.setStartIndex(String.valueOf(startIndex));
				responseVo.setContentInstanceList(findContentInstanceList);
				
				if (findContentInstanceList.size() > 0) {
					BeanUtils.copyProperties(findContentInstanceList.get(0), responseContentInstanceVo);
				}
				
			} else {
				ContentInstanceVO findContentInstanceItem = null;
				if (CommonUtil.isEmpty(findContentInstanceItem = contentInstanceService.findOneContentInstance(containerItem.getResourceID(), containerItem.getLatest()))) {
					throw new IotException("604", CommonUtil.getMessage("msg.device.contentinstance.notFound.text", locale));
				}
				responseVo.setContentInstance(findContentInstanceItem);
				
				BeanUtils.copyProperties(findContentInstanceItem, responseContentInstanceVo);
			}
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		if (!checkLatest) {
			mCommonService.setResponseHeader(request, response, responseVo);
			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		} else {
			request.getParameterMap().put("resourceID", responseContentInstanceVo.getResourceID());
			mCommonService.setResponseHeader(request, response, responseContentInstanceVo);
			return new ResponseEntity<Object>(responseContentInstanceVo, responseHeaders, httpStatus);
		}
		
	}
	
	/**
	 * remoteCSE AE contentInstance create
	 * @param remoteCSEID
	 * @param appId
	 * @param containerName
	 * @param labels
	 * @param contentInstanceProfile
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/AE-{appId:.*}/container-{containerName}", method = RequestMethod.POST, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" }, params = {"ty=contentInstance"})
	@ResponseBody
	public ResponseEntity<Object> remoteCSEAEContentInstanceCreate(@PathVariable String remoteCSEID
																 , @PathVariable String appId
																 , @PathVariable String containerName
																 , @RequestParam(value = "nm", required = false) String labels
																 , @RequestBody ContentInstanceVO contentInstanceProfile
																 , HttpServletRequest request
																 , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ContentInstanceVO responseVo = new ContentInstanceVO();
		HttpStatus httpStatus = HttpStatus.CREATED;

		String responseCode = "200";
		String responseMsg = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey 	= CommonUtil.nvl(request.getHeader("dKey"), "");

		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(appId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "appId " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(contentInstanceProfile)) {
			isError = true;
			responseCode = "600";
			responseMsg = "<contentInstance> " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if(isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		}
					
		try {
			
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			mCommonService.checkPatternAppId(checkMccP, appId);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dKey);
			
			if (CommonUtil.isEmpty(aEService.findOneAE("appId", appId))) {
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			ContainerVO containerItem = null;
			if (CommonUtil.isEmpty((containerItem = containerService.findOneContainerByLabels(appId, containerName)))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			contentInstanceProfile.setParentID(containerItem.getResourceID());
			contentInstanceProfile.setLabels(labels);
			ContentInstanceVO contentInstanceItem = contentInstanceService.createContentInstance(contentInstanceProfile);
			
			//responseVo.setContentInstance(contentInstanceItem);
			BeanUtils.copyProperties(contentInstanceItem, responseVo);
			
		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();
			
			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
	}
	
	
	/**
	 * remoteCSE AE contentInstance delete
	 * @param remoteCSEID
	 * @param appId
	 * @param containerName
	 * @param contentInstanceID
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:.*}/AE-{appId:.*}/container-{containerName}/contentInstance-{contentInstanceID}", method = RequestMethod.DELETE, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Void> remoteCSEAEContentInstanceDelete(@PathVariable String remoteCSEID
															   , @PathVariable String appId
															   , @PathVariable String containerName
															   , @PathVariable String contentInstanceID
															   , HttpServletRequest request
															   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		Object responseVo = new Object();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg  = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String dKey 	= CommonUtil.nvl(request.getHeader("dKey"), "");
		
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(appId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "appId " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(contentInstanceID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "contentInstanceID" + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			return new ResponseEntity<Void>(httpStatus);
		}
		
		try {
			
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			mCommonService.checkPatternAppId(checkMccP, appId);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			mCommonService.verifyDKey(checkMccP, remoteCSEID, dKey);
			
			if (CommonUtil.isEmpty(aEService.findOneAE("appId", appId))) {
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}			
			
			ContainerVO containerItem = null;
			if (CommonUtil.isEmpty(containerItem = containerService.findOneContainerByLabels(appId, containerName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			ContentInstanceVO contentInstanceItem = null;
			if (CommonUtil.isEmpty(contentInstanceItem = contentInstanceService.findOneContentInstance(containerItem.getResourceID(), contentInstanceID))) {
				throw new IotException("629", CommonUtil.getMessage("msg.contentinstance.noRegi.text", locale));
			}
			
			contentInstanceService.deleteContentInstance(containerItem.getResourceID(), contentInstanceItem);

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		mCommonService.setResponseHeader(request, response, responseVo);
		return new ResponseEntity<Void>(httpStatus);
	}
	
	
	/**
	 * remoteCSE AE contentInstance retrieve
	 * @param remoteCSEID
	 * @param appId
	 * @param containerName
	 * @param countPerPage
	 * @param startIndex
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value = "/Mobius/remoteCSE-{remoteCSEID:[^9.9.999.9].*}/AE-{appId:.*}/container-{containerName}/latest", method = RequestMethod.GET, produces = { "application/onem2m-resource+xml", "application/onem2m-resource+json" })
	@ResponseBody
	public ResponseEntity<Object> remoteCSEAEContentInstanceRetrieve(@PathVariable String remoteCSEID
																   , @PathVariable String appId
																   , @PathVariable String containerName
																   , @RequestParam(required=false) Integer countPerPage
																   , @RequestParam(required=false) Integer startIndex
																   , HttpServletRequest request
																   , HttpServletResponse response) {
		HttpHeaders responseHeaders = new HttpHeaders();

		ListOfResourceVO responseVo = new ListOfResourceVO();
		responseVo.setName(ONEM2M.CONTENT_INSTANCE.getName());
		
		ContentInstanceVO responseContentInstanceVo = new ContentInstanceVO();
		HttpStatus httpStatus = HttpStatus.OK;

		String responseCode = "200";
		String responseMsg  = "";
		
		String from 	= CommonUtil.nvl(request.getHeader("From"), "");
		String requestId= CommonUtil.nvl(request.getHeader("X-M2M-RI"), "");
		String locale 	= CommonUtil.nvl(request.getHeader("locale"), "ko");
		String uKey   	= CommonUtil.nvl(request.getHeader("uKey"), "");
		String topic  	= CommonUtil.nvl(request.getHeader("topic"), "");
		CallerVO callerVO = null;

		boolean checkLatest = countPerPage == null || startIndex == null ? true : false;
		boolean checkMccP = mCommonService.getCheckMccP(from);
		boolean isError = false;
		
		if(CommonUtil.isEmpty(from)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "From " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if(CommonUtil.isEmpty(requestId)) {
			isError			= true;
			responseCode	= "600";
			responseMsg		= "X-M2M-RI " + CommonUtil.getMessage("msg.input.empty.text", locale);	
		}
		else if (CommonUtil.isEmpty(remoteCSEID)) {
			isError = true;
			responseCode = "600";
			responseMsg = "remoteCSEID " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(appId)) {
			isError = true;
			responseCode = "600";
			responseMsg = "appId " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		else if (CommonUtil.isEmpty(containerName)) {
			isError = true;
			responseCode = "600";
			responseMsg = "containerName " + CommonUtil.getMessage("msg.input.empty.text", locale);
		}
		
		if (isError) {
			//responseVo.setResultCode(responseCode);
			//responseVo.setResultMsg(responseMsg);

			if (!checkLatest) {
				return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
			} else {
				return new ResponseEntity<Object>(responseContentInstanceVo, responseHeaders, httpStatus);
			}
		}
		
		try {
			
			mCommonService.checkPatternDeviceId(checkMccP, remoteCSEID);
			mCommonService.checkPatternAppId(checkMccP, appId);
			
			if (CommonUtil.isEmpty(remoteCSEService.findOneRemoteCSE(remoteCSEID))) {
				throw new IotException("621", CommonUtil.getMessage("msg.device.noRegi.text", locale));
			}
			
			if (CommonUtil.isEmpty(aEService.findOneAE("appId", appId))) {
				throw new IotException("626", CommonUtil.getMessage("msg.device.AE.noRegi.text", locale));
			}
			
			ContainerVO containerItem = null;
			if (CommonUtil.isEmpty(containerItem = containerService.findOneContainerByLabels(appId, containerName))) {
				throw new IotException("623", CommonUtil.getMessage("msg.container.noRegi.text", locale));
			}
			
			if(!CommonUtil.isEmpty(countPerPage) && !CommonUtil.isEmpty(startIndex)) {
				
				long totalCount = contentInstanceService.findCountContentInstance("parentID", containerItem.getResourceID());
				if(totalCount <= 0) {
					throw new IotException("604", CommonUtil.getMessage("msg.device.contentinstance.notFound.text", locale));
				}
				
				Query query = new Query();
				query.addCriteria(Criteria.where("parentID").is(containerItem.getResourceID()));
				//query.with(new Sort(Sort.Direction.DESC, "creationTime"));
				query.with(new Sort(Sort.Direction.DESC, "resourceID"));
				query.skip(startIndex == 1 ? 0 : ((startIndex * countPerPage) - countPerPage));
				query.limit(countPerPage);
				
				List<ContentInstanceVO> findContentInstanceList = contentInstanceService.findContentInstance(query);
				if (CommonUtil.isEmpty(findContentInstanceList) && findContentInstanceList.size() <= 0) {
					throw new IotException("604", CommonUtil.getMessage("msg.device.contentinstance.notFound.text", locale));
				}				
				
				//responseVo.setTotalListCount(String.valueOf(totalCount));
				//responseVo.setCountPerPage(String.valueOf(countPerPage));
				//responseVo.setStartIndex(String.valueOf(startIndex));
				responseVo.setContentInstanceList(findContentInstanceList);
				
				if (findContentInstanceList.size() > 0) {
					BeanUtils.copyProperties(findContentInstanceList.get(0), responseContentInstanceVo);
				}
				
			} else {
				ContentInstanceVO findContentInstanceItem = null;
				if (CommonUtil.isEmpty(findContentInstanceItem = contentInstanceService.findOneContentInstance(containerItem.getResourceID(), containerItem.getLatest()))) {
					throw new IotException("604", CommonUtil.getMessage("msg.device.contentinstance.notFound.text", locale));
				}
				responseVo.setContentInstance(findContentInstanceItem);
				
				BeanUtils.copyProperties(findContentInstanceItem, responseContentInstanceVo);
			}

		} catch (IotException e) {
			responseCode = e.getCode();
			responseMsg  = e.getMessage();

			httpStatus   = HttpStatus.OK;
			
			mongoLogService.log(logger, LEVEL.ERROR, e.getMessage());
		}

		//responseVo.setResultCode(responseCode);
		//responseVo.setResultMsg(responseMsg);
		
		if (!checkLatest) {
			mCommonService.setResponseHeader(request, response, responseVo);
			return new ResponseEntity<Object>(responseVo, responseHeaders, httpStatus);
		} else {
			request.getParameterMap().put("resourceID", responseContentInstanceVo.getResourceID());
			mCommonService.setResponseHeader(request, response, responseContentInstanceVo);
			return new ResponseEntity<Object>(responseContentInstanceVo, responseHeaders, httpStatus);
		}
	}
	
	
}