package kr.re.keti.iot.mdao.oneM2M;

import kr.re.keti.iot.domain.oneM2M.CSEBaseVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao;

import org.springframework.stereotype.Repository;

/**
 * CSEBase management Dao.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Repository
public class CSEBaseDao extends GenericMongoDao {
	
	public CSEBaseDao() {
		collectionName = ONEM2M.CSE_BASE.getName();
		cls = CSEBaseVO.class;
	}
}
