package kr.re.keti.iot.mdao.oneM2M;

import kr.re.keti.iot.domain.oneM2M.SoftwareVO;
import kr.re.keti.iot.mdao.common.GenericMongoDao;

import org.springframework.stereotype.Repository;

/**
 * software management Dao.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         </ul>
 */
@Repository
public class SoftwareDao extends GenericMongoDao {
	
	public SoftwareDao() {
		collectionName = ONEM2M.SOFTWARE.getName();
		cls = SoftwareVO.class;
	}
	
}
