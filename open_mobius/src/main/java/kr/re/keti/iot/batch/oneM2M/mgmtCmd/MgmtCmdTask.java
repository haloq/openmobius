package kr.re.keti.iot.batch.oneM2M.mgmtCmd;

import java.util.concurrent.atomic.AtomicInteger;

import kr.re.keti.iot.domain.oneM2M.MgmtCmdVO;
import kr.re.keti.iot.service.apilog.MongoLogService;
import kr.re.keti.iot.service.apilog.MongoLogService.LEVEL;
import kr.re.keti.iot.service.oneM2M.MgmtCmdService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * mgmtCmd Task.
 * @author <ul>
 *         <li>Sang June Lee < blue4444eye@hanmail.net > < blue4444eye@gmail.com ></li>
 *         <li>hkchoi</li>
 *         </ul>
 */
@Component
public class MgmtCmdTask implements Runnable {

	private static final Log logger = LogFactory.getLog(MgmtCmdService.class);

	@Autowired
	private MongoLogService mongoLogService;

	@Autowired
	private MgmtCmdRunnableService mgmtCmdRunnableService;

	@Autowired
	private MgmtCmdService mgmtCmdService;

	private MgmtCmdVO mgmtCmdProfile;

	private AtomicInteger runCount;

	/**
	 * mgmtCmd run function
	 */
	@Override
	public void run() {
		try {
			if (runCount.get() > 0) {
				mongoLogService.log(logger, LEVEL.ERROR, "MgmtCmdTask " + mgmtCmdProfile.getResourceID() + " Start!! runCount = " + runCount);
				mgmtCmdService.mgmtCmdControl(mgmtCmdProfile);
			} else {
				mongoLogService.log(logger, LEVEL.ERROR, "MgmtCmdTask " + mgmtCmdProfile.getResourceID() + " stop!!  runCount = " + runCount);
				mgmtCmdRunnableService.stopSchedule(mgmtCmdProfile.getResourceID());
			}

			runCount.decrementAndGet();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public MgmtCmdVO getMgmtCmdProfile() {
		return mgmtCmdProfile;
	}

	public void setMgmtCmdProfile(MgmtCmdVO mgmtCmdProfile) {
		this.mgmtCmdProfile = mgmtCmdProfile;
	}

	public void setMgmtCmdRunnableService(MgmtCmdRunnableService mgmtCmdRunnableService) {
		this.mgmtCmdRunnableService = mgmtCmdRunnableService;
	}

	public void setMgmtCmdService(MgmtCmdService mgmtCmdService) {
		this.mgmtCmdService = mgmtCmdService;
	}

	public void setMongoLogService(MongoLogService mongoLogService) {
		this.mongoLogService = mongoLogService;
	}

	public int getRunCount() {
		return runCount.get();
	}

	public void setRunCount(int runCount) {
		this.runCount = new AtomicInteger(runCount);
	}

}
